
package ALinguagem;

import javax.swing.JOptionPane;// importação


public class Argumentos{// nome classe igual ao do arquivo e o qualificador publico 
	
	
	public static void main(String[] args) {// se for um texto com mais de uma palavra, usar aspas
		// public:visibilidade static:metodo que pode ser chamado da propria classe
		//main: executavel main void:sem retorno String[]: tipo de atributo que pode receber argumentos 
		///no começo da execução do método
		Integer tamanho = args.length;
		String st = args[0];// aqui recebemos os parametros que foram passado no início
		String st2 = args[1];// aqui recebemos os parametros que foram passado no início
		
		JOptionPane.showMessageDialog(null, st +  "  "  + st2 + " Seja bem Vindo"); // + é um concatenador	
		System.out.println(tamanho);
		System.exit(0);// o parametro 0 quer dizer saida normal
		//System  é usada para obter referência a entrada e saida padrão do sistema
		//System esta localizada no pacote java.lang e derivadiretamente da classe Object
		//Os atributos nela contido são usado para a realização de diversas operações de sistema
		
		
	}
	

}
