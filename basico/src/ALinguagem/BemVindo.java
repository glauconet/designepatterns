
package ALinguagem;// nome do pacote

import javax.swing.JOptionPane;// importação pacote.pacote.classe


public class BemVindo{// nome classe igual ao do arquivo e o qualificador publico 
	
	
	public static void main(String[] args) {
		// public:visibilidade 
		//static:metodo que pode ser chamado da propria classe
		//main: executavel main 
		//void:sem retorno (tipo de retorno obrigatorio 
		//String[]: tipo de atributo para receber argumentos 
		//no começo da execução do método
		// args é o nome do parametro, poderia ser qualquer nome
		
		String st = JOptionPane.showInputDialog(null,"Digite seu Nome");
		// interação com interface gráfica - entrada
		
		JOptionPane.showMessageDialog(null, st + " Seja bem Vindo"); // + é um concatenador	
		// interação com interface gráfica - saída
		System.out.println("Alo Mundão");// sáida no prompt 
		// sempre que um aplicativo gerar um elementro gráfico, é preciso solicitar encerremento
		System.exit(0);
	}
	

}
