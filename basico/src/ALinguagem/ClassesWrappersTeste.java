package ALinguagem;
public class ClassesWrappersTeste {
	
	public static void main(String[] args) {
		
		// exemplo de passagem devalor via conjunto args[]
		// exemplo de conves�o de valores via classes wrappers
		// exemplo de tratamento de erros
		
		try{
		
		System.out.println(Integer.parseInt(args[0])+ Integer.parseInt(args[1]));
		
		}catch(ArrayIndexOutOfBoundsException e1 ){

			// pega o erro e prende nesta variavel e
			// falta n�mero	
			System.out.println("Falta argumento");	
		
		}catch(NumberFormatException e2 ){

			// pega o erro e prende nesta variavel e
			// foi passado texto	
			System.out.println("Formato errado");	
		}
	}
}
