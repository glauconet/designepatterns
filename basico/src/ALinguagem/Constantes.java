package ALinguagem;

	// EXEMPLO DE CONSTANTES
/* As constantes são unidades básicas de armazenamento de dados que 
 * não devem sofrer alterações ao longo da execução do aplicativo.
 * A declaração de uma constante contem um elemento a mais que a declaração de uma variável: a palavra reservada final
 * Vc pode declarar a constante e torna-la acessível às classes através do qualificador static
 *  Usar letras maiusculas no seu identificador.
 *  O procedimento mais corretos é cria uma classe para armazená-las
 */

public class Constantes {// o valor da constante é acessada através do NomeDaClasse.NOMECONSTANTE
		
	static final double COFINS = 0.03;
	static final double PIS = 0.0065;

}
