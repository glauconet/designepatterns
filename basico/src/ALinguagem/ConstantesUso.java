﻿package ALinguagem;


			// EXEMPLO DE CONSTANTES


public class ConstantesUso {
	/** caucula a receita*/

	public static void main(String[] args) {
		
		// usa a classe constantes
		
		double bruta = 15000.0;
		double cofins = bruta*Constantes.COFINS; // o valor da constante é acessada através do NomeDaClasse.NOMECONSTANTE
		double pis = bruta*Constantes.PIS;
		double liquida =  bruta - cofins - pis;
		
		
		System.out.print("\n");
		
		String st = 
		"     Receita Bruta: R$ " + bruta  + "\n" +
		"   _______________________________\n"+
		"      COFINS:  	\t- " + cofins 	+ 	"\n" +
		"      PIS:     	\t- " + pis		+ 	"\n" +
		"   _______________________________\n"+
		"\n"+
		"     Receita Líquidaa:= R$ " + liquida;
		
		System.out.print(st);
		

	}

}
