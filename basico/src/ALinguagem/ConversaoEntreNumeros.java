package ALinguagem;
import javax.swing.JOptionPane;

public class ConversaoEntreNumeros {
		// conversao entre numeros
	public static void main(String[] args) {
		
		int Inteiro1 = 15635;
		long InteiroLongo1 =  Inteiro1; // não precisa de conversão (inteiro->longo)
		float Flutuante1 = Inteiro1;//não precisa conversão (inteiro->flutuante)
		short Curto1 = (short)Inteiro1;// precisa da conversão
		
		double FlutuanteLongo1 = 24.75; 
		int Inteiro2 = (int)FlutuanteLongo1;
		//(double->inteiro) perdeu oque tem depois da virgula
		
		int Inteiro3 = (int)Math.round(FlutuanteLongo1 );
		//(double->inteiro) arredoundou
		
			
		
		
		String mensagem = "CONVERSÃO DE NÚMEROS" +
		"\n"   					    			+
		"\n Inteiro1:	    \t " + Inteiro1 	+
		"\n InteiroLongo1:  \t " + InteiroLongo1+
		"\n Flutuante1:     \t " + Flutuante1 	+
		"\n Curto1: 	    \t " + Curto1 		+
		"\n" + 
		"\n FlutuanteLongo1 \t " + FlutuanteLongo1+
		"\n Inteiro2	    \t " + Inteiro2       +
		"\n Inteiro3        \t " + Inteiro3;  
		
		JOptionPane.showMessageDialog(null,mensagem);
		System.exit(0);	
	
	}

}
