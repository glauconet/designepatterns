package ALinguagem;
import javax.swing.JOptionPane;

public class ConversaoNumerosTexto {// String.valueOf()
		
		
	public static void main(String[] args) {
		
				
		double db = 1536.67;
		int it = 650;
		
		String st1 = String.valueOf(db);// String.valueOf(): método de transformação da classe String
		String st2 = String.valueOf(it);
			
						
		String mensagem = "CONVERSÃO DE NÚMERO PARA TEXTO" +
		"\n"   				+
		"\n st1	 " + st1+
		"\n st2  " + st2;  
		
		JOptionPane.showMessageDialog(null,mensagem);
		System.exit(0);	
	
	}

}
