package ALinguagem;
import javax.swing.JOptionPane;

public class ConversaoTextoNumeros {
		
		
	public static void main(String[] args) {
		
		String peganum;
		peganum = JOptionPane.showInputDialog(null,"Digite um Número");
		
		double db 	= 	Double.parseDouble(peganum);
		float 	fl 	= 	Float.parseFloat(peganum);
		long 	lg 	= 	Integer.parseInt(peganum);
		short 	sh 	=	(short)Integer.parseInt(peganum);
		byte 	bt 	=	(byte)Integer.parseInt(peganum);
		char 	ch 	= 	(char)Integer.parseInt(peganum);
					
		String mensagem = "CONVERSÃO DE NÚMEROS" +
		"\n"   					    			+
		"\n db	 " + db	+
		"\n fl   " + fl +
		"\n lg   " + lg	+
		"\n sh 	 " + sh +
		"\n bt   " + bt +
		"\n ch	 " + ch ;  
		
		JOptionPane.showMessageDialog(null,mensagem);
		System.exit(0);	
	
	}

}
