package ALinguagem;

import javax.swing.JOptionPane;		
		
		// Exemplo de Break
		// Uso do  javax.swing.JOptionPane

public class DecisBreak {
	
	public static void main(String[] args) {
				
		String st;
		
		while(true){
			
			st = "Informe seu nome";
			
			st = JOptionPane.showInputDialog(null,st);
		
			if (st == null) System.exit(0);
					
			if (!st.equals ("")){ 
				break; // se essa condição for verdadeira para o loop
			}
		}
		
		st = "Nome captado : "  + st;
		
		JOptionPane.showMessageDialog(null,st);
		
	System.exit(0);
	}

}
