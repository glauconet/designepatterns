package ALinguagem;

public class Inteiros{

	/** mostra valores máximos e mínimos
	 * das variaveis do tipo inteiro
	 *
	 */
	public static void main(String[] args) {
		
		byte  inteiroByte; //
		short inteiroShort; // 
		int   inteiroInteiro; //
		long  inteiroLong; // 
		
		inteiroByte = 127;
		inteiroShort = 32767;
		inteiroInteiro = 2147483647;
		inteiroLong = 9223372036854L;
		
		
		System.out.println("");
		System.out.println("NÚMEROS INTEIROS EM JAVA\n");
		System.out.println("Limite superior:");
		System.out.println("Byte:\t\t" + inteiroByte);
		System.out.println("Short:\t\t" + inteiroShort);
		System.out.println("Inteiro:\t" + inteiroInteiro);
		System.out.println("Long:\t\t" + inteiroLong);

		System.out.println("\n");
		
		inteiroByte = -128;
		inteiroShort = -32768;
		inteiroInteiro = -2147483648;
		inteiroLong = -9223372036854775808L;

		System.out.println("Limite inferior:");
		System.out.println("Byte:\t\t" + inteiroByte);
		System.out.println("Short:\t\t" + inteiroShort);
		System.out.println("Inteiro:\t" + inteiroInteiro);
		System.out.println("Long:\t\t" + inteiroLong);
	
	
	
	}

}
