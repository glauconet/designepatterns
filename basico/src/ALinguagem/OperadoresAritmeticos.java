package ALinguagem;

public class OperadoresAritmeticos{

	
	public static void main(String[] args) {
		
		
		double db1, db2;
		db1 = 5;
		db2 = 3;
		
		
		System.out.println("\n");
		
		System.out.println("Valores iniciais das Variaveis");
		
		System.out.println("db1:" + db1);
		System.out.println("db2:" + db2);
		
		System.out.println("Operações aritméticas básicas");
		
		System.out.println("Soma (db1+db2):"); 
		System.out.println(db1+db2); 
		
		System.out.println("Subtração (db1-db2):"); 
		System.out.println(db1-db2); 
		
		System.out.println("Multipicação (db1*db2):"); 
		System.out.println(db1*db2);
		
		System.out.println("Divisão (db1/db2):"); 
		System.out.println(db1/db2);
		
		System.out.println("Módulo (db1/db2):"); 
		System.out.println(db1%db2);
		
		System.out.println("Incremento e decremento");
		
		db1++;
		db2--;
		
		System.out.println("db1:" + db1);
		System.out.println("db2:" + db2);
		
		System.out.println("Oprerações de atribuição");
		System.out.println("Atribuição aditiva: (bd1 += 2");
		System.out.println(db1+=2);
		System.out.println("Atribuição aditiva: (bd1 -= 3");
		System.out.println(db1-=3);
		System.out.println("Atribuição aditiva: (bd1 /*= 2");
		System.out.println(db1*=2);
		System.out.println("Atribuição aditiva: (bd1 /= 2");
		System.out.println(db1/=2);
		System.out.println("Atribuição aditiva: (bd1 %= 2");
		System.out.println(db1%=2);
		
		
	}

}
