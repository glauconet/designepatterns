package ALinguagem;

public class OperadoresLogicos {

	/**
	 * Operadores Lógicos
	 */
	public static void main(String[] args) {
					
		boolean bl1, bl2;
		bl1 = false;
		bl2 = true;
		
		System.out.println("");
		System.out.println("bl1:" + bl1);
		System.out.println("bl2:" + bl2);
		
		System.out.println("");
		System.out.print("bl1 || bl2:\t");
		System.out.print(bl1 || bl2);
		
		System.out.println("");
		System.out.print("bl1 && bl2:\t");
		System.out.print(bl1 && bl2);
		
		System.out.println("");
		System.out.print("bl1 ^ bl2:\t");
		System.out.print(bl1 ^ bl2);
		
		System.out.println("");
		System.out.print("!bl1:\t");
		System.out.print(!bl1);
		
		System.out.println("");
		System.out.print("!bl2:\t");
		System.out.print(!bl2);
		
		System.out.println("");
		System.out.println("Atribuições");
		
		bl1 |= bl2;
		System.out.print("bl1 |= bl2:\t");
		System.out.print("bl1 recebeu: " + bl1);
		System.out.println("");
		bl1 &= !bl2;
		System.out.print("bl1 &= !bl2:\t");
		System.out.print("bl1 recebeu: " + bl1);
		System.out.println("");
		
	}
}
