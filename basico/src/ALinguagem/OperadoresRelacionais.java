package ALinguagem;

public class OperadoresRelacionais {

	public static void main(String[]args){
	
		int inteiro1, inteiro2;
		inteiro1 = 5;
		inteiro2 = 3;
		
		System.out.println("\n");
		
		System.out.println("Valores das Variaveis");
		System.out.println("\n");
		
		System.out.println("Inteiro1: " + inteiro1);
		System.out.println("Inteiro2: " + inteiro2);
		System.out.println("");
		
		System.out.println("Comparações entre variáveis");
		System.out.println("\n");
		System.out.print("inteiro1 == inteiro2 ");
		System.out.println(inteiro1 == inteiro2);
		System.out.print("inteiro1 != inteiro2 ");
		System.out.println(inteiro1 != inteiro2);
		System.out.print("inteiro1 > inteiro2 ");
		System.out.println(inteiro1 > inteiro2);
		System.out.print("inteiro1 < inteiro2 ");
		System.out.println(inteiro1 < inteiro2);
	}
	
}
