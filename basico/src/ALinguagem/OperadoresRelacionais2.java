package ALinguagem;

public class OperadoresRelacionais2 {

	/**
	 * @param  mostra operadores relacionais
	 */
	public static void main(String[] args) {
		
		String st1 = "abc";
		String st2 = "abc";
				
		byte byte1 = 10;
		byte byte2 = 12;
		byte byte3 = 10;
		
		boolean bl = st1.equals(st2);
		boolean bl1 = byte1 > byte2;
		boolean bl2 = byte1 < byte2;
		boolean bl3 = byte1 >= byte2;
		boolean bl4 = byte1 <= byte3;
	
	
		System.out.print(bl1);
		System.out.println("");
		System.out.print(bl2);
		System.out.println("");
		System.out.print(bl3);
		System.out.println("");
		System.out.print(bl4);
		System.out.println("");
		System.out.print(bl);
		
	}

}
