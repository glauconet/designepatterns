package ALinguagem;

public class Recursiva {
	
	public Recursiva recursiva(int n){
		if(n==0){
			return null;
		}//else{
			System.out.println(n);
			return recursiva(n-1);
		//}
	}
	public void recursivaEnquanto(int n){
		while(n>0){
			n = n-1;
			System.out.println(n);
		}
	}
	
	
	void myMethod( int counter){
		
		if(counter == 0){
	     
			return;
		
		}else{
	       System.out.println(""+counter);
	       myMethod(--counter);
	       return;
	    }
	} 
	
	
	public static void main(String[] args) {
		Recursiva rs = new Recursiva();
		rs.recursiva(10);  
		System.out.println("");
		rs.recursivaEnquanto(10); 
		System.out.println("");
		rs.myMethod(10);		 
		System.out.println("");
	}

}
