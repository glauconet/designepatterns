package ALinguagem;

		
		
public class TryCatch2 {

	public static void main(String[] args) {
				
		
		try{
			
			int int1 = Integer.parseInt("3");
			int int2 = Integer.parseInt("5");
		
			int int3 = int1/int2;
			
			System.out.println("resultado " + int3);
			
		}catch(ArithmeticException ae) {
			
			System.out.println("Erro aritmético");// se colocar 0 no int2 do teste
			System.out.println(ae);
			
		}catch(Exception e) {
			
			System.out.println("Erro: outros erros");
			System.out.println(e);
		}
	}
}

