package ALinguagem;

public class VariavelEstatica{
	
	private static double numDouble = 10.0; //static { taxaServico = 10.0;}
		
	public static double getNum(){	
		return VariavelEstatica.numDouble;// uso o nome da classe
		
	}

	public static void setNum(double numDouble) {
		VariavelEstatica.numDouble = numDouble;
	}
}