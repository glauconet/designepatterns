package ALinguagem;

import java.util.Scanner ;

public class VetorScanner{
	
	
	public static void main(String[] args) {
		
		int tam;
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Digite o tamanho do vetor \n");
		
		tam = sc.nextInt();
		
		
		int valores[]= new int[tam]; 	// um array é um objeto , que precisa ser instanciado
					// os [] são usados para indicar um array e sua dimensão
					// o indice do array começa com 0
					// para referencia o array indicamos o indice [indice]
					
		for (int i=0;i<tam;i++){
			
			System.out.println("Digite u numero \n");
			valores[i] = sc.nextInt();
		}
		
		for (int i=0;i<tam;i++){
			System.out.print(valores[i]);
			System.out.println("\n");
		}	
		
		
		
	}

}
