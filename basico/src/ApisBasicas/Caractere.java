package ApisBasicas;

import java.io.PrintStream;

public class Caractere {
  public static void main(String[] args) {
    PrintStream saida = System.out;
    
    saida.println("\nCódigos de caracteres:");
    saida.println("A =\t" + Character.getNumericValue('A'));
    saida.println("B =\t" + Character.getNumericValue('B'));
    saida.println("C =\t" + Character.getNumericValue('C'));
    
    saida.println("\nTestes diversos:\n");
    
    saida.print("\"5\" � um digito?\t\t");
    saida.println(Character.isDigit('5'));
    saida.print("\"5\" � uma letra?\t\t");
    saida.println(Character.isLetter('5'));
    saida.print("\"5\" � um digito ou letra?\t");
    saida.println(Character.isLetterOrDigit('5'));
    saida.print("\"-\" � um espa�o em branco?\t");
    saida.println(Character.isWhitespace('-'));
    
    saida.print("\"A\" � uma letra min�scula?\t");
    saida.println(Character.isLowerCase('A'));
    saida.print("\"A\" � uma letra mai�scula?\t");
    saida.println(Character.isUpperCase('A'));
    
    saida.print("\nConvers�o de \"A\" para min�sculo:\t");
    saida.println(Character.toLowerCase('A'));
    saida.print("Convers�o de \"b\" para mai�sculo:\t");
    saida.println(Character.toUpperCase('b'));
  }
}

/********************************************************************
 *  *
 *      *
 *                                                                  *
 *                    *
 *                *
 *        *
 *                                             *
 ********************************************************************/