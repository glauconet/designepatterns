package ApisBasicas;
import java.math.BigDecimal;
import java.text.DecimalFormat;

import javax.swing.JOptionPane;


public class Reais {

	/**
	 * Exemplo de Decimais (Reais) (float)
	 */
	public static void main(String[] args) {
		
		float fl1, fl2;	// 4 bytes  de -3,4028E+38  até  3,4028E+38  precisão: 6-7
		double  db1 = 5.123456789, db2 = 10.0;		
		// 8 bytes 	de -1,7976E+308 até  1,7976E+308 precisão: 15 dígitos
		// float é considerado limitado, o mais comum para ponto flutuante é o double
		fl1 = 1.02f;
		fl2 = 2.0f;// para utilzarmo o float precisamos escrever f
		
		String st = "Valores armazenados:"+
		"\nfl1 = " + fl1 + "\nfl2 = " + fl2 +
		"\ndb1= " + db1 + "\ndb2 = " + db2;
		
			
		//BigDecimal a =  new BigDecimal("999999999999999999994.00");
				
		
		//a.toString();
		Double d =  new Double("999399999999999.93777");
		
		String formato = "R$ #,##0.00";  
        
		DecimalFormat df = new DecimalFormat(formato);  
        
               // System.out.println(df.format(a)); 
        
        System.out.println(df.format(d));  
			

	}

	

}
