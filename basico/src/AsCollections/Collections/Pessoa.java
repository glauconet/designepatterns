package AsCollections.Collections;
  public class Pessoa{
	
	private String nome;
	private String nascimento;
	private int idade;
	
	public String getNascimento() {
		return nascimento;
	}

	public String getNome() {
		return nome;
	}

	public void setNascimento (String nascimento) {
		this.nascimento = nascimento;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setIdade(int i) {
		this.idade = i;
		
	}

	public int getIdade() {
		
		return idade;
	}

	public boolean equals(Object obj) {
		
		boolean flag = false;

		if (obj instanceof Pessoa) {
			Pessoa estaPessoa = (Pessoa) obj;
		
			flag = estaPessoa.nome.equals(this.nome) && estaPessoa.nascimento.equals(this.nascimento);
		}
		return flag;
		
	}

}