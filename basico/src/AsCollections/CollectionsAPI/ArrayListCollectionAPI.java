package AsCollections.CollectionsAPI;

import java.util.ArrayList;

public class ArrayListCollectionAPI {
	
	
	public static void display( ArrayList< String > teste ){
		
		for (String num:teste){
			
			System.out.print(num);
			
		}
		
	}
			
public static void main (String[] args ){
	
	ArrayList<String> teste = new ArrayList<String>();
		
	teste.add("red");
	
	teste.add(0 ,"yellon");
	
	teste.add(0 ,"blue");
	
	for ( int i = 0 ; i < teste.size(); i++ ){
			System.out.printf(" %s", teste.get(i));
	}
	
	System.out.println("");
	
	teste.remove("blue");	
	teste.remove(1);
	
	System.out.println("-----------------------");
	for ( int i = 0 ; i < teste.size(); i++ ){
		System.out.printf(" %s", teste.get(i));
	}
	System.out.println("-->");
	String nome = teste.get(0);
	System.out.println(nome);

	display(teste);
}

}
