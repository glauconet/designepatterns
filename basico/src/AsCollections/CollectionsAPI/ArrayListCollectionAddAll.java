package AsCollections.CollectionsAPI;
import java.util.Collections;

import java.util.ArrayList;

public class ArrayListCollectionAddAll{
  public static void main(String[] args){
    // cria uma ArrayList que conterá strings
    ArrayList<String> nomes = new ArrayList<String>();

    // cria uma segunda ArrayList que conterá mais strings
    ArrayList<String> nomes2 = new ArrayList<String>();
    
    // adiciona itens na primeira lista
    nomes.add("Carlos");
    nomes.add("Maria");
    nomes.add("Fernanda");
    
    // adiciona itens na segunda lista
    nomes2.add("Osmar");
    nomes2.add("Zacarias");    
	
    // vamos adicionar os elementos da segunda lista
    // no final da primeira lista
    nomes.addAll(nomes2);
    	
    // Collections.shuffle(nomes);  use esse comando se quiser embaralhar

    
    
    // vamos exibir o resultado
    for(int i = 0; i < nomes.size(); i++){
      System.out.println(nomes.get(i));
    } 

    System.exit(0);
  }
}
