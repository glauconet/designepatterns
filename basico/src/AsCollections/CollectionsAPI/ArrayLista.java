package AsCollections.CollectionsAPI;

import java.util.ArrayList;

public class ArrayLista{
	
  public static void main(String[] args){
    // cria uma ArrayList que conterá inteiros
    ArrayList<Integer> valores  =  new ArrayList<Integer>();
    
    ArrayList<String>    nomes  =  new ArrayList<String>();
        
    valores.add(34);// adiciona itens na lista
    valores.add(12);
    valores.add(8);
    valores.add(23);
    
    nomes.add("Carlos");
    nomes.add("Carol");
    nomes.add("Fernanda");
    nomes.add("Oliveira");
    nomes.add("Torchettis");
        	
    // exibe os valores da lista
    for(int i = 0; i < valores.size(); i++)
      System.out.println(valores.get(i));
    
    for(int i = 0; i < nomes.size(); i++)
        System.out.println(nomes.get(i));
    
    System.exit(0);
  }
}
