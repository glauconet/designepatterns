
package AsCollections.CollectionsAPI;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar; 


public class DateCalendar {

	
	public static void main(String[] args) {
		
		
		GregorianCalendar dataAtual = new GregorianCalendar();
		GregorianCalendar minhaData = new GregorianCalendar(2011, 3, 29 );
		
		System.out.println(dataAtual.getTime());
				
		DateFormat formata = DateFormat.getDateInstance(); // exemplo com DateFormat
		
		SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy"); // exemplo com SimpleDateFormat
		
		String resultado = formata.format(dataAtual.getTime());
		
		String resultado2 = formatador.format(dataAtual.getTime());
		
		String resultado3 = formatador.format(minhaData.getTime());
		
		System.out.println("");
		System.out.println(resultado);
		System.out.println(resultado2);
		System.out.println(resultado3);
		
	}

}