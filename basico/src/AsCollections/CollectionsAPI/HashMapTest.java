package AsCollections.CollectionsAPI;

import java.util.*;

public class HashMapTest{
	
    public static void main(String[] args)    {
        // Create a HashMap with three key/value pairs.
        HashMap hm = new HashMap();
        
        hm.put("Um", new Integer(1));
        hm.put("Dois", new Integer(2));
        hm.put("Tres", new Integer(3));

        // Get the value associated with the key "Three".
        Object v = hm.get("Tres");
        
        if(v != null){
            
        	System.out.println("The value associated with \"Três\" is " +
                v.toString());
        }else{
        	
            System.out.println("There is no key named \"Three\" " +
                "in the HashMap.");
        }
    }
}



