package AsCollections.CollectionsAPI;

// classe MapaPessoas.java
//// Manipuliando objetos do tipo Pessoa dentro de um HashMap

import java.util.*;

public class MapaPessoas {
	
	private HashMap<String,Pessoa> pessoas;

	public MapaPessoas(){
		
		pessoas = new HashMap<String,Pessoa>();

	}

	public void adiciona(Pessoa pessoa){
		
		if (pessoa !=null) {
		// cria um objeto Interger para a chave da pessoa
			String chave = pessoa.getNome();
	
			// Se o mapa das pessoas nao contiver a chave
			if (!pessoas.containsKey(chave)){
				
				pessoas.put(chave,pessoa);
			}

		}


	}
	
	public Pessoa recupera(String nome){
		Pessoa pessoa = null;
		if (nome !=null) {
			if (pessoas.containsKey(nome)) {
				pessoa = (Pessoa) pessoas.get(nome);
			}
		}

	return pessoa;

	} 
}