package AsCollections.CollectionsAPI;

import java.util.*;

// classe que manipula objetos do tipo pessoa dentro de um ArrayList

public class PessoasLista{
	private List<Pessoa> pessoas;

	public PessoasLista() {
		pessoas = new ArrayList<Pessoa>();
	}

	public void adiciona(Pessoa pessoa) {

	// se pessoa nao for nulo e pessoas nao contiver pessoa
	// sera usado o Pessoa.equals(obj) para testar se pessoa
	// nao esta em pessoas

	if (pessoa != null && ! pessoas.contains(pessoa)) {
		pessoas.add(pessoa);
		System.out.println(" Objeto adicionado na lista por" + pessoa);
	} else {
		System.out.println(" Objeto nao adicionado por ja existir na lista ou por ser nulo !");
		}
}

	public Pessoa recupera(int index) {
		Pessoa pessoa = null;
		int tamanho = pessoas.size();
		if (index < tamanho) {
			pessoa = (Pessoa) pessoas.get(index);
		} else {
			System.out.println("Indice nao encontrado !");
			}
		return pessoa;
	}

	//Como o ArrayList nao possui metodos de busca, temos que
	// fazer uma varredura completa nos elementos da lista

	public Pessoa recupera(String nome) {
		Pessoa pessoa = null;
		//Varrendo a lista pessoas do inicio ao fim

		Iterator it = pessoas.iterator();
		while(it.hasNext()){
			Pessoa p = (Pessoa) it.next();
			//se nome for igual a algum nome da lista
			if(p.getNome().equals(nome)) {
				pessoa = p;
				break;
			}
		}

		return pessoa;
	}
}