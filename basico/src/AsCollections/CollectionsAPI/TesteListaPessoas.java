package AsCollections.CollectionsAPI;

 public class TesteListaPessoas {


	public static void main(String args[]) {
		PessoasLista lp = new PessoasLista();
		Pessoa p1 = new Pessoa();
		p1.setNome("Marco");
		
		lp.adiciona(p1);
		
		Pessoa p2 = new Pessoa();
		p2.setNome("Maria");
		
		lp.adiciona(p2);
		
		Pessoa p3 = lp.recupera("Marco");
		
		System.out.println(p3);
		
		Pessoa p4 = lp.recupera(1);
		
		System.out.println(p4);
	}

}