package AsCollections.CollectionsAPI;

import java.util.Date;

// classe Testa o Mapa de Pessoas

public class TesteMapaPessoas {
	
	public static void main(String[] args) {
		
	PessoasMapa mapaPessoas = new PessoasMapa();
	
	Pessoa p1 = new Pessoa();
	p1.setNome("Manuel");
	p1.setNascimento(new Date(70,8,22));
	mapaPessoas.adiciona(p1);
	
	Pessoa p2 = new Pessoa();
	p2.setNome("Joaquim");
	p2.setNascimento(new Date(85,4,24));
	mapaPessoas.adiciona(p2);
	
	Pessoa p3 = mapaPessoas.recupera("Manuel");
	System.out.println(p3);
	
	Pessoa p4 = mapaPessoas.recupera("Jose");
	System.out.println(p4);
	}

}