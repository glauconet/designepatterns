package AsCollections.DadosColecoes;

import static javax.swing.JOptionPane.*;
import java.util.LinkedList;

public class ExemploPilha {
  private static LinkedList<String> pilha;
  
  public static void main(String[] args) {
    pilha = new LinkedList<String>();
    
    while (true) {
      String str = showInputDialog("Informe um texto");
      if (str == null) break;
      pilha.push( str );
    }
    
    String str = "Itens da pilha:";
    while(!pilha.isEmpty())
      str += "\n" + pilha.pop();
    showMessageDialog(null, str);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/