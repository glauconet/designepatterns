package AsCollections.DadosColecoes;

import static javax.swing.JOptionPane.*;
import java.util.TreeSet;
import java.util.Set;

public class ExemploTreeSet {
  private static Set<String> conjunto;
  
  public static void main(String[] args) {
    conjunto = new TreeSet<String>();
    
    while (true) {
      String str = showInputDialog("Item de compra");
      if (str == null) break;
      conjunto.add( str );
    }
    
    String str = "Lista de compras:";
    for (String s : conjunto)
      str += "\n" + s;
    showMessageDialog(null, str);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/