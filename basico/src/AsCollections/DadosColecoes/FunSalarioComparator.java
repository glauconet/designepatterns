package AsCollections.DadosColecoes;

import java.util.Comparator;

public class FunSalarioComparator implements Comparator<Funcionario>{
  public int compare(Funcionario func1, Funcionario func2) {
    if (func1.getSalario() > func2.getSalario()) return 1;
    else if (func1.getSalario() < func2.getSalario()) return -1;
    else return 0;
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/