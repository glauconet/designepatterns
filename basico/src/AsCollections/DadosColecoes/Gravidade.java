package AsCollections.DadosColecoes;

public enum Gravidade {
  MINIMA (1,"M�nima"),
  PEQUENA (2,"Pequena"),
  MEDIA (3,"M�dia"),
  ALTA (4,"Alta"),
  ALTISSIMA (5,"Alt�ssima");
  
  private int indicador;
  private String descricao;
  
  Gravidade(int indicador, String descricao) {
    this.indicador = indicador;
    this.descricao = descricao;
  }

  public int getIndicador() {
    return indicador;
  }

  public String getDescricao() {
    return descricao;
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/