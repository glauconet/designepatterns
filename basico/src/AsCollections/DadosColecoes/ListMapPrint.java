package AsCollections.DadosColecoes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListMapPrint {
	
	public static void main(String[] args) {
				
	final List<String> list = new ArrayList<String>();
		 
		    list.add("one");
		    list.add("two");
		    list.add("three");
		    list.add("four");
		    System.out.println(list);
		    
		for (String s : list) {
		    System.out.println(s);
		 }

		    
		    
	final String[] array = {"four", "three", "two", "one"};
		    System.out.println(Arrays.asList(array));

	final Map<String, String> map = new HashMap<String, String>();
		    
		    map.put("1", "value");
		    map.put("2", "value");
		    map.put("3", "value");
		    
		    System.out.println(map.entrySet());
		  

	}

}
