package AsCollections.Date;
import java.sql.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class ExemploCalendarGregorianCalendar {
	public static void main(String[] args) {
			Calendar calendar =  Calendar.getInstance();
			calendar.setTime(leData("Digite uma data (AAAA/MM/DD): "));
			System.out.println("Ano corrente			: " + calendar.get(Calendar.YEAR));
			System.out.println("Mes corrente			: " + (calendar.get(Calendar.MONTH) + 1) + " - Mes em Java comeca em 0. Somar 1");
			System.out.println("Semana do ano			: " + calendar.get(Calendar.WEEK_OF_YEAR));
			System.out.println("Semana do mes			: " + calendar.get(Calendar.WEEK_OF_MONTH));
			System.out.println("Dia	Corrente		: " + calendar.get(Calendar.DATE));
			System.out.println("Dia do ano			: " + calendar.get(Calendar.DAY_OF_YEAR));
	}
	private static  Date leData(String msg) {
		/* Estes passos podem ser utilizados para a leitura de uma data no 
		 * formato string e ser convertida para um objeto Date, 
		 * a ser usada em uma pesquisa em  um banco de dados, 
		 * para efetivar uma pesquisa, alteracapo, insercao ou remocaoo de uma
		 *  determinada linha da tabela. 
			 */
			Scanner var = new Scanner(System.in).useDelimiter("\r\n");
			String data;
			do {
				do {
					// Apresenta a mensagem na tela
					System.out.print(msg);
					// Realiza a leitura da data
					data = var.nextLine();
					// valida se a data lida esta correta.
				} while (data.length() != 10);
			} while (data.charAt(4)!= '/' || data.charAt(7) != '/');
			//Quebra a data lida em um vetor, usando o / como separador.
			String[] datVet = data.split("/");
			//Converte a string lida em uma data do tipo Calendar
			Calendar cal = new GregorianCalendar(Integer.parseInt(datVet[0]), 	Integer.parseInt(datVet[1]) - 1, Integer.parseInt(datVet[2]));
			// Converte a data de Calendar para Date.
			Date dt = new Date(cal.getTimeInMillis());
			return dt;
	}
}
