package AsCollections.Date;
import java.util.Date;
public class ExemploDate {
	public static void main(String[] args) {
			Date hoje = new Date ();
			// Executando explicitamente o m�todo toString (). Este m�todo pertence a superclasse Object
			System.out.println("Hoje: " + hoje.toString());
			hoje.setTime(hoje.getTime() + (1 * 60 * 60 * 1000));
			System.out.println("Hoje uma hora depois: " + hoje);
			hoje.setTime(hoje.getTime() + (1 * 60 * 60 * 1000 * 24));
			System.out.println("Amanh�: " + hoje);
	}
}
