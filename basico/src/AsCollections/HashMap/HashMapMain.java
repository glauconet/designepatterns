package AsCollections.HashMap;
import java.util.*;
//Cria um HashMap com três chave/valor pair.
public class HashMapMain{
	
    public static void main(String[] args)    {
        
        HashMap<String, Integer> hm = new HashMap<String, Integer>();
        
        hm.put("ChaveUm", new Integer(1));
        hm.put("ChaveDois", new Integer(2));
        hm.put("ChaveTres", new Integer(3));
        hm.put("ChaveQuatro", new Integer(4));
        // pega o valor associado a chave  "tres".

        Object v = hm.get("ChaveTres");
        
        if(v != null){
            
        	System.out.println("O Valor associado a chave  \"Tres\" é " +
                v.toString());
        }else{
        	
            System.out.println("There is no key named \"Three\" " +
                "in the HashMap.");
        }
    }
}



