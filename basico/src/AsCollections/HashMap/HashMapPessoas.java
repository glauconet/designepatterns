package AsCollections.HashMap;
// classe que manipula objetos do tipo Pessoa dentro de um HashMap
import java.util.*;



public class HashMapPessoas {
	
	private HashMap<String,Pessoa> pessoas;
	
	public HashMapPessoas() {
		 pessoas = new HashMap<String, Pessoa>();
	}

	public void adiciona(Pessoa pessoa) {
		if (pessoa !=null){
			String chave = pessoa.getNome();
			if (!pessoas.containsKey(chave)) {//containsKey
				this.pessoas.put(chave, pessoa);
			}
		}
	}
	
	public Pessoa recupera(String nome){
		Pessoa pessoa = null;
		if (nome !=null) {
			if (pessoas.containsKey(nome)) {//containsKey
				pessoa =  pessoas.get(nome);
			}
		}

	return pessoa;

	} 
}