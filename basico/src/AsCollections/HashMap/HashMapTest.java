package AsCollections.HashMap;
import java.util.*;

public class HashMapTest{
	
    public static void main(String[] args)    {
        // Create a HashMap with three key/value pairs.
        HashMap<String, Integer> hm = new HashMap<String, Integer>();
                
        hm.put("One", new Integer(1));
        
        hm.put("Two", new Integer(2));
        
        hm.put("Three", new Integer(3));

        // Get the value associated with the key "Three".
        Object v = hm.get("Three");
        
        if (v != null) {
            System.out.println("The value associated with \"Three\" is " +
                v.toString());
        } else{
            System.out.println("There is no key named \"Three\" " +
                "in the HashMap.");
        }
    }
}

/*
Output:
The value associated with "Three" is 3
*/


