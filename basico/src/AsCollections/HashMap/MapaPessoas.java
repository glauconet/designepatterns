package AsCollections.HashMap;
import java.util.HashMap;


// classe Testa o Mapa de Pessoas

public class MapaPessoas {
	
	public static void main(String[] args) {
	
	
	HashMap<String,Pessoa> pessoas = new HashMap<String, Pessoa>();
	
	Pessoa p1 = new Pessoa();
	Pessoa p2 = new Pessoa();
	Pessoa p3 = new Pessoa();
	Pessoa p4 = new Pessoa();
	
	p1.setNome("Manuel");
	p1.setIdade(20);
	p1.setNascimento("21/05/1972");
	
	p2.setNome("Joaquim");
	p2.setIdade(50);
	p2.setNascimento("01/01/1980");
	
	pessoas.put(p1.getNome(), p1);
	pessoas.put(p2.getNome(), p2);
		
	p3 = pessoas.get("Manuel");
	p4 = pessoas.get("Joaquim");
			
	System.out.println(p3.getNome());
	System.out.println(p3.getIdade());
	System.out.println(p3.getNascimento());
	System.out.println(p4.getNome());
	System.out.println(p4.getIdade());
	System.out.println(p4.getNascimento());
	}

}