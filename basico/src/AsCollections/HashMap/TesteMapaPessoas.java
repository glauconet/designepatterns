
package AsCollections.HashMap;// classe Testa um Mapa de Pessoas

public class TesteMapaPessoas {
	
	public static void main(String[] args) {
		
	HashMapPessoas mapaPessoas = new HashMapPessoas();
		
	Pessoa p1 = new Pessoa();
	Pessoa p2 = new Pessoa();
	Pessoa p3 = new Pessoa();
	Pessoa p4 = new Pessoa();
	
	p1.setNome("Manuel");
	p1.setIdade(20);
	p1.setNascimento("21/05/1972");
	
	p2.setNome("Joaquim");
	p2.setIdade(50);
	p2.setNascimento("01/01/1960");
		
	mapaPessoas.adiciona(p1);
	mapaPessoas.adiciona(p2);
	
	p3 = mapaPessoas.recupera("Manuel");
	p4 = mapaPessoas.recupera("Joaquim");
		
	System.out.println(p3.getNome());
	System.out.println(p3.getIdade());
	System.out.println(p3.getNascimento());
	
	System.out.println(p4.getNome());
	System.out.println(p4.getIdade());
	System.out.println(p4.getNascimento());
		
	}

}