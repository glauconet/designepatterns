package AsCollections.HashSet;
// Manipuliando objetos do tipo Pessoa dentro de um HashSet
// Classe Conjunto de Pessoas
import java.util.*;



public class HashSetPessoas {

	private HashSet<Pessoa> pessoas;

	public HashSetPessoas() {
		pessoas = new HashSet<Pessoa>();
	}

	public void adiciona(Pessoa pessoa) {// Se o conjunto das pessoas nao contiver a pessoa 
		if (pessoa !=null) { //testando pelo HashCode
			
			if (! pessoas.contains(pessoa)) {
				pessoas.add(pessoa);
			}
		}
	}
	// como o set nao possui metodos de busca nem pesquisa por chave, 
	// necessitamos fazer uma varredura completa nos elementos do conjunto
	public Pessoa recupera(String nome) {
		
		Pessoa pessoa  = new Pessoa();
		
		Iterator<Pessoa> i = pessoas.iterator();
		
		while(i.hasNext()){
			
			pessoa = i.next();
			if(pessoa.getNome().equals(nome)){
											
				break;
			}
		}
		return pessoa;
		
		
	}
}

