package AsCollections.HashSet;
// Manipuliando objetos do tipo Pessoa dentro de um HashSet
// Classe Conjunto de Pessoas
import java.util.*;


public class PessoasConjunto {
	private HashSet<Pessoa> pessoas;

	public PessoasConjunto() {
		pessoas = new HashSet<Pessoa>();
	}

	public void adiciona(Pessoa pessoa) {
		if (pessoa !=null) {
		// Se o conjunto das pessoas nao contiver a pessoa
		//testando pelo HashCode
		if (! pessoas.contains(pessoa)) {
			pessoas.add(pessoa);
		}

		}
	}

	// como o set nao possui metodos de busca nem pesquisa
	// por chave, necessitamos fazer uma varredura completa
	// nos elementos do conjunto
	public Pessoa recupera(String nome) {
		Pessoa pessoa =  new Pessoa();
		Iterator<Pessoa> it = pessoas.iterator();
		while(it.hasNext()) {
			Pessoa p = it.next();
			if (p.getNome().equals(nome)) {
			pessoa = p;
			break;
			}
		}
		return pessoa;
	}
}

