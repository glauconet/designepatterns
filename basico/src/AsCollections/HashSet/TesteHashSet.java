package AsCollections.HashSet;
import java.util.HashSet;
import java.util.Iterator;


//import Collections.Comparador.TesteTreeSet.Funcionario;

public class TesteHashSet {

public static void main(String[] args) {
	
	HashSet<Pessoa> meuHashSet = new HashSet<Pessoa>();
		
	Pessoa pessoa1 = new Pessoa();
	pessoa1.setNome("João");
	pessoa1.setIdade(20);
	meuHashSet.add(pessoa1);
	
	Pessoa pessoa2 = new Pessoa();
	pessoa2.setNome("José");
	pessoa2.setIdade(30);
	meuHashSet.add(pessoa2);
	
	for(Pessoa elemento: meuHashSet){
		System.out.println(elemento.getNome() + ", idade: "+ elemento.getIdade());
	}
	
	Iterator<Pessoa> p = meuHashSet.iterator();
	
	while (p.hasNext()) {
		Pessoa  x = p.next();
		System.out.println(x.getNome());
		
		if(x.getNome().equals(pessoa1.getNome())){
			
			System.out.println(pessoa1.getIdade());
		}
	}
	
		
	System.out.println("meuHashSet está vazio? " + meuHashSet.isEmpty());
	
	System.out.println("Tamanho do HashSet: " + meuHashSet.size());
	
	if(meuHashSet.remove(pessoa1)){
		System.out.println("Removeu pessoa1");
	}
			
	System.out.println("Contêm 'pessoa2'? " + meuHashSet.contains(pessoa2));
	
	meuHashSet.clear();
	
	}
}
