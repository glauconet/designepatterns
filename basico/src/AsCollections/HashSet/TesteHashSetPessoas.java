package AsCollections.HashSet;


public class TesteHashSetPessoas {

	
	public static void main(String[] args) {
		
		HashSetPessoas hsp = new HashSetPessoas();
			
		Pessoa pessoa1 = new Pessoa();
		pessoa1.setNome("João");
		pessoa1.setIdade(20);
				
		Pessoa pessoa2 = new Pessoa();
		pessoa2.setNome("José");
		pessoa2.setIdade(30);
		
		Pessoa pessoa3 = new Pessoa();
		pessoa3.setNome("Katia");
		pessoa3.setIdade(33);
		
		
		Pessoa pessoa4 = new Pessoa();
		pessoa4.setNome("Glauco");
		pessoa4.setIdade(39);
				
		hsp.adiciona(pessoa1);
		hsp.adiciona(pessoa2);
		hsp.adiciona(pessoa3);
		hsp.adiciona(pessoa4);
		
		
		Pessoa pessoaRecuperada1 = new Pessoa();
		Pessoa pessoaRecuperada2 = new Pessoa();
		
		pessoaRecuperada1 = hsp.recupera(pessoa4.getNome());
		pessoaRecuperada2 = hsp.recupera("Katia");
				
		System.out.println(pessoaRecuperada1.getNome());
		System.out.println(pessoaRecuperada1.getIdade());
		
		System.out.println(pessoaRecuperada2.getNome());
		System.out.println(pessoaRecuperada2.getIdade());
		
	}

}
