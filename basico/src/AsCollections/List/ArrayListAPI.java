package AsCollections.List;

import java.util.ArrayList;

public class ArrayListAPI {
		
	public static void display(ArrayList<String> oqueFoiPassado ){
		
		for (String qualquerNome: oqueFoiPassado){// for especial
			System.out.println(qualquerNome);
		}
	}
			
public static void main (String[] args ){
	
	ArrayList<String> teste = new ArrayList<String>();
		
	teste.add("red");// demostra colocalação num arrayList numa posição
	teste.add(0,"yellon");
	teste.add(0,"blue");// metodo add
	teste.add(1,"green");
		
	for ( int i = 0 ; i < teste.size(); i++ ){// for normal
			System.out.println(teste.get(i));
	}
	
	System.out.println("-----------------------");
	
	teste.remove("blue");	// metodo remove
	teste.remove(1); // metodo remove por elemento
		
	String nome = teste.get(0);// pegando a string da posição com método get
	System.out.println(nome);
	
	System.out.println("-----------------------");
	display(teste);
}

}
