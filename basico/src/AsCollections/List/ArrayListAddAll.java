package AsCollections.List;
import java.util.ArrayList;

public class ArrayListAddAll{
  public static void main(String[] args){
	  
   
	  
   ArrayList<String> nomes = new ArrayList<String>(); // cria uma ArrayList que conterá strings
   
   ArrayList<String> nomes2 = new ArrayList<String>(); // cria uma segunda ArrayList que conterá mais strings
    
    // adiciona itens na primeira lista
    nomes.add("Carlos");
    nomes.add("Maria");
    nomes.add("Fernanda");
    
    // adiciona itens na segunda lista
    nomes2.add("Osmar");
    nomes2.add("Zacarias");    
    nomes2.add("Katia");    
    // vamos adicionar os elementos da segunda lista
    // no final da primeira lista
    nomes.addAll(nomes2);
    	
    // Collections.shuffle(nomes);  use esse comando se quiser embaralhar

    // vamos exibir o resultado
    for(int i = 0; i < nomes.size(); i++){
      System.out.println(nomes.get(i));
    } 

    System.exit(0);
  }
}
