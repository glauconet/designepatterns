package AsCollections.List;

import java.util.ArrayList;

public class ArrayListRetornaArrayList{ // um método que retorna um ArrayList
	
	 public static ArrayList<String> obterLista(){
		 // criar um ArrayList, adicionar elementos e retornar
   	 
	    ArrayList<String> nomes = new ArrayList<String>();
		    
	    nomes.add("Osmar J. Silva");    
	    nomes.add("Fernanda de Castro");
	    nomes.add("Marcos de Oliveira");

	    return nomes;
	 }
	
		
	  public static void main(String[] args){
	      
	    ArrayList<String> lista = obterLista();//obter o ArrayList  
	    
	    for(int i = 0; i < lista.size(); i++){// exibir os valores do array
	      System.out.println(lista.get(i));
	    }
	    
	    for (String qualquerNome: lista){// com for especial
			System.out.println(qualquerNome);
		}
	    
	    
	    System.exit(0);
	  }
	  
	}
