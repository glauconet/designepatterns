package AsCollections.List;

import java.util.ArrayList;

public class ArrayLista{
	
  public static void main(String[] args){
    
    ArrayList<Integer> valores  =  new ArrayList<Integer>();// cria uma ArrayList que conterá inteiros
    
    ArrayList<String> nomes  =  new ArrayList<String>();// cria um arraylist  que contém strings
 
    // adiciona itens na lista  
    
    valores.add(34); valores.add(12); valores.add(8); valores.add(23);
    
    nomes.add("Carlos"); nomes.add("Carol"); nomes.add("Fernanda"); 
    nomes.add("Torchetti"); nomes.add("Hikazudani"); nomes.add("Oliveira"); 
        	
    // exibe os valores da lista
    
    for(int i = 0; i < valores.size(); i++)
      System.out.println(valores.get(i));
    
    for(int i = 0; i < nomes.size(); i++)
        System.out.println(nomes.get(i));
    
    System.exit(0);
  }
}
