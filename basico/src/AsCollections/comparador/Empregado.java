package AsCollections.comparador;

public class Empregado  implements Comparable<Empregado>  {

	private String nome;
	private String funcao;
	private int salario;

	public Empregado(String nome, String funcao, int salario) {
		this.nome = nome;
		this.funcao = funcao;
		this.salario = salario;
	}

	@Override
	public int compareTo(Empregado emp) {
		return this.getNome().compareTo(emp.getNome());
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setFuncao(String funcao) {
		this.funcao = funcao;
	}

	public String getFuncao() {
		return funcao;
	}
	
	public void setSalario(int salario) {
		this.salario = salario;
	}

	public int getSalario() {
		return salario;
	}

	public String toString() {
		return nome + ", " + funcao + ", " + salario; 
	}

}
