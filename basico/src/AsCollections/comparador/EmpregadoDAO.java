package AsCollections.comparador;
import java.util.ArrayList;
import java.util.List;

public class EmpregadoDAO {
	
	public List<Empregado> getEmpregados() {
		List<Empregado> lista = new ArrayList<Empregado>();
		
		lista.add(new Empregado("josé",     "analista", 3300));
		lista.add(new Empregado("manuel",   "auxiliar", 2000));
		lista.add(new Empregado("carlota",  "analista", 5400));
		lista.add(new Empregado("joão",     "técnico",  2000));
		lista.add(new Empregado("antonio",  "analista", 3500));
		lista.add(new Empregado("luis",     "analista", 2700));
		lista.add(new Empregado("florinda", "auxiliar", 1200));
		lista.add(new Empregado("alice",    "auxiliar", 5000));
		lista.add(new Empregado("emilia",   "analista", 8300));
		lista.add(new Empregado("carlos",   "técnico",  1200));
		
		return lista;
	}

}
