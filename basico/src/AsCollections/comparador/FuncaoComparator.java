package AsCollections.comparador;
import java.util.Comparator;

public class FuncaoComparator implements Comparator<Empregado> {

	@Override
	public int compare(Empregado emp1, Empregado emp2) {
		return emp1.getFuncao().compareTo(emp2.getFuncao());
	}

}
