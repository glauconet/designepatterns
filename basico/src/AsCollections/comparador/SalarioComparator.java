package AsCollections.comparador;
import java.util.Comparator;

public class SalarioComparator implements Comparator<Empregado> {

	@Override
	public int compare(Empregado emp1, Empregado emp2) {
		if (emp1.getSalario() > emp2.getSalario()) {
			return 1;
		} else if (emp1.getSalario() < emp2.getSalario()) {
			return -1;
		} else {
			return 0;
		}
	}

}
