package AsCollections.comparador;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Teste1 {

	public static void main(String[] args) {
		String[] nomes = {"joaquim", "antonio", "manuel"};

		Arrays.sort(nomes);

		for (String nome: nomes) {
			System.out.println(nome);
		}
		
		System.out.println();
		
		List<String> apelidos = Arrays.asList(new String[] {"joca", "tonico", "mané"});
		
		Collections.sort(apelidos);

		for (String apelido: apelidos) {
			System.out.println(apelido);
		}
	
	}

}
