package AsCollections.comparador;
import java.util.Collections;
import java.util.List;

public class Teste2 {

	public static void main(String[] args) {
		
		EmpregadoDAO dao = new EmpregadoDAO();
		List<Empregado> lista = dao.getEmpregados();

		System.out.println("--> lista original");
		for (Empregado emp: lista) {
			System.out.println(emp);
		}
		
		Collections.sort(lista);
//		Collections.sort(lista, new SalarioComparator());
//		Collections.sort(lista, new FuncaoComparator());

		System.out.println("--> lista classificada");
		for (Empregado emp: lista) {
			System.out.println(emp);
		}
		
		System.out.println("--> utilitários");
		int ind = Collections.binarySearch(lista, new Empregado("josé", null, 0));
		System.out.println("josé é o " + (ind+1) + "º objeto da lista");

		Empregado maraja = Collections.max(lista, new SalarioComparator());
		System.out.println("o marajá é: " + maraja);
		
		Empregado marmitao = Collections.min(lista, new SalarioComparator());
		System.out.println("o marmitão é: " + marmitao);
	}
	
}
