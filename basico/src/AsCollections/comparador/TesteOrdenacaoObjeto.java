package AsCollections.comparador;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class TesteOrdenacaoObjeto {

	public static void main(String[] args) {

		List<Empregado> lista = new ArrayList<Empregado>();
		
		lista.add(new Empregado("zezinho", "gerente", 3000));
		lista.add(new Empregado("huguinho", "acessor", 2500));
		lista.add(new Empregado("luizinho", "consultor", 2000));
		
		Collections.sort(lista, new SalarioComparator());
		
		for (Empregado e : lista) {
			System.out.println(e);
		}

	}

}
