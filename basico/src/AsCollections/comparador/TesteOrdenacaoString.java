package AsCollections.comparador;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TesteOrdenacaoString {

	public static void main(String[] args) {

		List<String> lista = new ArrayList<String>();
		
		lista.add("zezinho");
		lista.add("huguinho");
		lista.add("luizinho");
		
		Collections.sort(lista);
		
		for (String e : lista) {
			System.out.println(e);
		}
		
	}

}
