package AsCollections.comparador;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;


public class TesteTreeSet {

	public static void main(String[] args) {
		Set<Funcionario> conjunto = new TreeSet<Funcionario>(new LotacaoComparator());
		
		conjunto.add(new Funcionario("marcos","sunac"));
		conjunto.add(new Funcionario("bruno","sunmf"));
		
		Iterator<Funcionario> i = conjunto.iterator();
		
		while (i.hasNext()) {
			Funcionario f = i.next();
			System.out.println(f.getNome());
		}
	}

	public static class Funcionario implements Comparable<Funcionario> {
		private String nome;
		private String lotacao;

		public Funcionario(String nome, String lotacao) {
			this.nome = nome;
			this.lotacao = lotacao;
		}
		
		public String getNome() {
			return nome;
		}
		public String getLotacao() {
			return lotacao;
		}

		@Override
		public int compareTo(Funcionario f) {
			return this.getNome().compareTo(f.getNome());
		}
	}
	
	public static class LotacaoComparator implements Comparator<Funcionario> {

		@Override
		public int compare(Funcionario f1, Funcionario f2) {
			return f1.getLotacao().compareTo(f2.getLotacao());
		}
		
	}
}
