package AsCollections.equals;
public class PagamentoA {
	private int id;
	private int valor;
	private int data;

	public PagamentoA (int id, int valor, int data) {
		this.id = id;
		this.valor = valor;
		this.data = data;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getValor() {
		return valor;
	}
	public void setValor(int valor) {
		this.valor = valor;
	}
	public int getData() {
		return data;
	}
	public void setData(int data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "pagamentoA";
	}


}
