package AsCollections.equals;
public class PagamentoB {
	private int id;
	private int valor;
	private int data;

	public PagamentoB (int id, int valor, int data) {
		this.id = id;
		this.valor = valor;
		this.data = data;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getValor() {
		return valor;
	}
	public void setValor(int valor) {
		this.valor = valor;
	}
	public int getData() {
		return data;
	}
	public void setData(int data) {
		this.data = data;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + data;
		result = prime * result + id;
		result = prime * result + valor;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PagamentoB other = (PagamentoB) obj;
		if (data != other.data)
			return false;
		if (id != other.id)
			return false;
		if (valor != other.valor)
			return false;
		return true;
	}

}