package AsCollections.equals;
import java.util.HashSet;
import java.util.Set;


public class Teste1 {

	public static void main(String[] args) {
		
		String s1 = "banana";
		String s2 = "banana";

		System.out.println(s1.equals(s2));

		PagamentoA p1 = new PagamentoA(1, 1000, 20001201);
		PagamentoA p2 = new PagamentoA(1, 1000, 20001201);

		System.out.println(p1.equals(p2));

	
		Set<Object> lista = new HashSet<Object>();
		lista.add(s1);
		lista.add(s2);
		lista.add(p1);
		lista.add(p2);

		System.out.println(lista.size());
	
	
	}

}
