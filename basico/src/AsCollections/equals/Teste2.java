package AsCollections.equals;

public class Teste2 {

	public static void main(String[] args) {
		
		String s1 = "banana";
		String s2 = "banana";

		System.out.println(s1.equals(s2));
		System.out.println(s1.hashCode());
		System.out.println(s2.hashCode());

		PagamentoA p1 = new PagamentoA(1, 1000, 20001201);
		PagamentoA p2 = new PagamentoA(1, 1000, 20001201);

		System.out.println(p1.equals(p2));
		System.out.println(p1.hashCode());
		System.out.println(p2.hashCode());
	}

}
