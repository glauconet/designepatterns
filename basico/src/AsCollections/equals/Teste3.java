package AsCollections.equals;

public class Teste3 {

	public static void main(String[] args) {
		
		String s1 = "banana";
		String s2 = "banana";

		System.out.println(s1.equals(s2));
		System.out.println(s1.hashCode());
		System.out.println(s2.hashCode());

		PagamentoB p1 = new PagamentoB(1, 1000, 20001201);
		PagamentoB p2 = new PagamentoB(1, 1000, 20001201);

		System.out.println(p1.equals(p2));
		System.out.println(p1.hashCode());
		System.out.println(p2.hashCode());
		
	}

}