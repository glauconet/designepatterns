package AsCollections.sqlada;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class Config {

	private static Properties p = new Properties();

	static {
		try {
			p.load(new FileReader("sqlada.properties"));
		} catch (FileNotFoundException e1) {
			System.out.println("arquivo de propriedades não encontrado");
		} catch (IOException e1) {
			System.out.println("erro de acesso ao arquivo de propriedades");
		}
	}

	public static String getProperty(String propriedade) {
		return p.getProperty(propriedade);
	}

}
