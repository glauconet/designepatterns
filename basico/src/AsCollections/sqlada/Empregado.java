package AsCollections.sqlada;

import java.math.BigDecimal;
import java.util.Date;

public class Empregado {

	private String id;
	private String nome;
	private char estadoCivil;
	private char sexo;
	private Date dataNascimento;
	private BigDecimal salario;

	@Override
	public String toString() {
		return "[ id=" + id + " nome=" + nome + " estadoCivil=" + estadoCivil + " sexo=" + sexo + " dataNascimento=" + dataNascimento + " salario=" + salario;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public char getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(char estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public char getSexo() {
		return sexo;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public BigDecimal getSalario() {
		return salario;
	}

	public void setSalario(BigDecimal salario) {
		this.salario = salario;
	}

}
