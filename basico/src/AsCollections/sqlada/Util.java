package AsCollections.sqlada;

import java.util.Date;
import java.util.GregorianCalendar;

public class Util {

	public static Date getDDMMAA(String ddmmaa) {
		int dd = Integer.valueOf(ddmmaa.substring(0, 2));
		int mm = Integer.valueOf(ddmmaa.substring(2, 4));
		int aaaa = Integer.valueOf("19" + ddmmaa.substring(4, 6));

		return new GregorianCalendar(aaaa, mm, dd).getTime();
	}
}
