package AsConstanteseVariaveis;
import java.util.Scanner;

public class ConversaoCaractereNumero {
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    
    System.out.print("\nInforme um caractere:\t");
    String texto = scan.nextLine();
    char caractere = texto.charAt(0);
    int codigo = caractere;
    
    System.out.println("C�digo do caractere:\t" + codigo);
    System.out.println();
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/