package AsConstanteseVariaveis;
import javax.swing.JOptionPane;

public class NumerosDecimais {
  public static void main(String[] args) {
    float float_1,float_2;
    double double_1 = 5.123456789,double_2 = 10.0;
    
    float_1 = 1.02F;
    float_2 = 2.0F;
    
    String mensagem = "Conte�do das vari�veis:" +
      "\nfloat_1 = " + float_1 + "\nfloat_2 = " + float_2 +
      "\ndouble_1 = " + double_1 + "\ndouble_2 = " + double_2;
    
    JOptionPane.showMessageDialog(null,mensagem);
    System.exit(0);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/