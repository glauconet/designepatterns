package AsDecisoes;

import javax.swing.JOptionPane;

public class DecisContinue {
	
	public static void main(String[] args) {
				
	
		for(int i = 0; true; i++){
			
			String st = "Informe seu nome";
			
			st = JOptionPane.showInputDialog(null,st);
			
			if (st == null) System.exit(0);
			
			if (st.equals ("")){
				
				continue;// neste passo retorna a execução do for até que a condição seja satisfeita
			}// ou seja desvia para o começo da execução
			
			
			if (i > 0){
				st= "Voce confirmou " + i + " vezes sem digitar nada." +
				"\nMas, ao final , informou seu nome: " + st;
			} else{
				st = "Obrigado por informar seu nome, " + st;
			}
			
			JOptionPane.showMessageDialog(null,st);
			System.exit(0);
		}
		
	}

}
