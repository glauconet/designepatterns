package AsExcecoes;
import javax.swing.JOptionPane;

public class Excecao {
  public static void main(String[] args) {
    String str = "Informe um número inteiro";
    str = JOptionPane.showInputDialog(null,str,"Informe",3);
       
    int numero = Integer.parseInt(str);
    int resultado = (int)Math.pow(numero,3);
    
    str = "O cubo de " + numero + " é " + resultado;
    JOptionPane.showMessageDialog(null,str,"Mensagem",1);
    System.exit(0);
  }
}

// Este teste consiste em digitar um caracter alfanumérico em vez de um número
// Ele causará o seguinte tipo de erro: java.lang.NumberFormatException

//Exception in thread "main" java.lang.NumberFormatException: For input string: "cccc"
	//at java.lang.NumberFormatException.forInputString(NumberFormatException.java:48)
	//at java.lang.Integer.parseInt(Integer.java:449)
	//at java.lang.Integer.parseInt(Integer.java:499)
	//at Excecao.Excecao.main(Excecao.java:9) 