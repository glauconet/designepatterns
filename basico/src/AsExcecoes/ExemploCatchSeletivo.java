package AsExcecoes;

// Capturando exce��es mais espec�ficas antes da mais gen�rica
public class ExemploCatchSeletivo {
	public static void main(String args[]) {
		try {
			int num = Integer.parseInt(args[0]);
			int num2 = Integer.parseInt(args[1]);
			System.out.println(num);
			System.out.println(num / num2);
			if (num2 > num) {
				throw new Exception("Segundo par�metro maior que o primeiro");
			}
		} catch (ArrayIndexOutOfBoundsException e1) {
			System.out.println("N�o foi fornecido um argumento.");
		} catch (NumberFormatException e2) {
			System.out.println(" Argumento n�o � um inteiro.");
		} catch (ArithmeticException e1) {
			System.out.println(" Divis�o de n�mero por zero.");
		}
		/*
		 * caso o bloco contendo a classe Exception seja colocado antes dos
		 * outros tipos de exceptions dar� erro de compila��o.
		 */
		catch (Exception e1) {
			System.out.println("Erro geral.");
		}
	}
}