package AsExcecoes;

// Usando o comando finally
import java.io.*;
import java.util.*;

public class ExemploFinally {
	Vector<Integer> num = null;

	public static void main(String[] args) {
		ExemploFinally obj = new ExemploFinally();
		try {
			obj.recuperar();
			obj.imprimirArquivo();
		} catch (NumberFormatException e) {
			System.err.println("Exce��o capturada: NumberFormatException: "
					+ e.getMessage());
		} catch (IOException e) {
			System.err.println("Exce��o capturada: IOException: "
					+ e.getMessage());
		} catch (ArrayIndexOutOfBoundsException e) {
			System.err
					.println("Exce��o capturada: ArrayIndexOutOfBoundsException: "
							+ e.getMessage());
		} finally {
			// mesmo ocorrendo algum erro esta mensagem ser� apresentada na tela
			System.out.println("Encerrando o programa");
		}
	}

	private void recuperar() throws NumberFormatException, IOException {
		FileReader tArq1 = null;
		BufferedReader tArq2 = null;
		this.num = new Vector<Integer>();
		try {
			Scanner s = new Scanner(System.in); // prepara console
			// Para funcionar, digitar mod7.txt
			System.out.print("Entre com o nome do arquivo:");
			String nomeArq = s.nextLine();
			/*
			 * o construtor da classe FileReader lan�a a exce��o
			 * FileNotFoundException que por sua vez estende IOException. Por
			 * isto aparece somente IOException na assinatura do m�todo
			 * ecuperar().
			 */
			tArq1 = new FileReader(nomeArq);
			tArq2 = new BufferedReader(tArq1);
			String valor;
			// lan�a a exce��o IOException
			while ((valor = tArq2.readLine()) != null) {
				this.num.add(Integer.parseInt(valor));
			}

			/*
			 * como podemos observar o bloco try � seguido por um bloco finally.
			 * N�o � necess�rio ter um bloco catch quando temos um bloco
			 * finally. Entretanto se for necess�rio um bloco catch pode ser
			 * utilizado em conjunto.
			 */
		} finally {
			if (tArq2 != null) {
				System.out.println("Fechando o arquivo");
				try {
					tArq2.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				System.out.println("Arquivo n�o foi aberto");
			}
		}
	}

	private void imprimirArquivo() {
		/*
		 * o arquivo tem 9 n�meros, entretanto o loop est� sendo executado 10
		 * vezes. Devido a este problema este programa ir� lan�ar uma exce��o
		 * unchecked do tipo da classe ArrayIndexOutOfBoundsException.
		 */
		for (int i = 0; i < 10; i++)
			System.out.println(i + " : " + this.num.get(i));
	}
}