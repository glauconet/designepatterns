package AsExcecoes;

// Utilizando o comando throw para lan�ar uma exce��o
public class ExemploThrow {
	public static void main(String args[]) {
		ExemploThrow obj = new ExemploThrow();
		try {
			obj.calcular();
		} catch (MyClassException e) {
			System.out.println("Classe: " + e.getClasse());
			System.out.println("Mensagem: " + e.getMessage());
			e.printStackTrace();
		}
	}

	public void calcular() throws MyClassException {
		try {
			int num = 0;
			num = num / 0;
		} catch (ArithmeticException e) {
			MyClassException myObj = new MyClassException(
					"Erro causado pela divis�o por zero");
			myObj.setClasse(this.getClass().getName().toString());
			myObj.setMensagem(e.getMessage());
			// se a classe n�o estiver em um pacote dar� erro
			myObj.setPacote(this.getClass().getPackage().toString());
			throw myObj;
		}
	}
}