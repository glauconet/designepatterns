package AsExcecoes;

import java.util.Scanner;

// Uma implementa��o alternativa ao uso do comando if para o tratamento de erros em um programa
public class ExemploUsandoExceptions {
	public static void main(String args[]) {
		ExemploUsandoExceptions obj = new ExemploUsandoExceptions();
		try {
			obj.lerNota();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	private void lerNota() throws Exception {
		double nota = 0.0;
		System.out.println("Entre com uma nota: ");
		Scanner s = new Scanner(System.in);
		nota = s.nextInt();
		if ((nota < 0) || (nota > 100)) {
			throw new Exception("Erro na leitura dos dados");
		}
		calcularNota(nota);
	}

	private void calcularNota(double nota) throws Exception {
		if (nota >= 70) {
			throw new Exception("Aprovado");
		} else if ((nota >= 40) && (nota < 70)) {
			throw new Exception("Exame");
		} else {
			throw new Exception("Reprovado");
		}
	}
}