package AsExcecoes;
public class TratamentoArray {
  public static void main(String[] args) {
    String[] amigos = {"Cl�udia","Vanessa","Raul","Sidnei"};
    
    for (byte num = 0; num < amigos.length; num++)
      System.out.println("Amigo " + (num + 1) + ": " + amigos[num]);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/