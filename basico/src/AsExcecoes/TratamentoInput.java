package AsExcecoes;
import java.util.Scanner;
import java.util.InputMismatchException;

public class TratamentoInput {
  public static void main(String[] args) {
    byte idade = -1;
    
    while (idade < 0) {
      System.out.print("\nInforme sua idade:\t");
      Scanner scan = new Scanner(System.in);
      
      try {
        idade = scan.nextByte();
      }
      catch (InputMismatchException ime) {
        System.out.println("Idade inv�lida!");
      }
    }
    
    System.out.println("Sua idade:\t\t" + idade);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/