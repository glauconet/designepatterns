package AsExcecoes;
import javax.swing.JOptionPane;

public class Try {
  public static void main(String[] args) {
	  
    String str;
    
    int quociente = 0;
    
    try {//try, inicio do bloco que será protegido/avaliado/executado
    	
      str = JOptionPane.showInputDialog(null,"Informe o dividendo");
      
      int dividendo = Integer.parseInt(str);
      
      str = JOptionPane.showInputDialog(null,"Informe o divisor");
      
      int divisor = Integer.parseInt(str);
      
      quociente = dividendo / divisor;
    }// fim do try
    catch (Exception classeExcecao){//O tipo da classe e a váriavel que recebera a classe instanciada
    	
      str = "Ocorreu uma exceção! \nTipo: " + classeExcecao.getClass() + "\nMensagem: " + classeExcecao.getMessage();
      // tipo da classe e sua mensagem      
      JOptionPane.showMessageDialog(null,str,"Erro",0);
      
     // classeExcecao.printStackTrace();// imprimindo a linha com a mensagem de erro
        
            
      System.exit(0);
    }
    
    JOptionPane.showMessageDialog(null,"Resultado: " + quociente);
  }
}

/******************************************************************************
 *   			                                                              *
 *   			                                                              *
 ******************************************************************************/