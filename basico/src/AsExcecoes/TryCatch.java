package AsExcecoes;

		
		
public class TryCatch {

	public static void main(String[] args) {
		
		
		try {
			int x = Integer.parseInt(args[0]);
			
			System.out.println("Tabuada Java");
			
			for( int i = 1; i <= 10; i++ ){
			
			System.out.println( i + " x " + x + " = " + x*i );
			
			}
			
		} catch (Exception e) {
			System.out.println("Erro: Digite um Número");
			System.out.println(e);
		}
		
		
		
	}

}
