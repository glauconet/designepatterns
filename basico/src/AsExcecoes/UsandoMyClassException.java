package AsExcecoes;
// Usando a classe MyClassException
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.Vector;

public class UsandoMyClassException {
	Vector<Integer> num = null;

	public static void main(String[] args) {
		UsandoMyClassException obj = new UsandoMyClassException();

		try {
			obj.recuperar();
			obj.imprimirArquivo();
		} catch (MyClassException e) {
			System.out.println("Classe: " + e.getClasse());
			// mensagem explicitamente informada quando um erro foi gerado
			System.out.println("Minha mensagem: " + e.getMessage());
			// mensagem obtida do objeto que gerou o erro
			System.out.println("Mensagem do gerador do erro: "
					+ e.getMensagem());
			System.out.println("M�todo: " + e.getMetodo());
			System.out.println("Pacote: " + e.getPacote());
		} finally {
			// mesmo ocorrendo algum erro esta mensagem ser� apresentada
			System.out.println("Encerrando o programa");
		}
	}

	private void recuperar() throws MyClassException {
		FileReader tArq1 = null;
		BufferedReader tArq2 = null;
		this.num = new Vector<Integer>();
		try {
			Scanner s = new Scanner(System.in);
			// para funcionar, digitar mod7.txt
			System.out.print("Entre com o nome do arquivo:");
			String nomeArq = s.nextLine();
			/*
			 * lan�a a exce��o FileNotFoundException que por sua vez estende
			 * IOException. Por isto aparece somente IOException na assinatura
			 * do m�todo.
			 */
			tArq1 = new FileReader(nomeArq);
			tArq2 = new BufferedReader(tArq1);
			String valor;
			// lan�a a exce��o IOException
			while ((valor = tArq2.readLine()) != null) {
				this.num.add(Integer.parseInt(valor));
			}
			/*
			 * como podemos observar o bloco try � seguido por um bloco finally.
			 * N�o � necess�rio ter um bloco catch quando temos um bloco
			 * finally. Entretanto se for necess�rio um bloco catch pode ser
			 * utilizado em conjunto.
			 */
		} catch (NumberFormatException e) {
			/*
			 * uso do construtor que recebe uma string. Para obter este conte�do
			 * usar o m�todo getMessage herdado de Throwable.
			 */
			MyClassException obj = new MyClassException(
					"Erro na formata��o do n�mero - Exce��o capturada: NumberFormatException");
			obj.setClasse(getClass().getName());
			obj.setMensagem(e.getMessage()); // mensagem do objeto de erro
			obj.setMetodo("recuperar");
			obj.setPacote(getClass().getPackage().toString());
			throw obj;

		} catch (IOException e) {
			/*
			 * uso do construtor que recebe uma string. Para obter este conte�do
			 * usar o m�todo getMessage() herdado de Throwable.
			 */
			MyClassException obj = new MyClassException(
					"Erro na formata��o do n�mero - Exce��o capturada: IOException");
			obj.setClasse(getClass().getName());
			obj.setMensagem(e.getMessage()); // mensagem do objeto de erro
			obj.setMetodo("recuperar");
			obj.setPacote(getClass().getPackage().toString());
			throw obj;
		} finally {
			if (tArq2 != null) {
				System.out.println("Fechando o arquivo");
				try {
					tArq2.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				System.out.println("Arquivo n�o foi aberto");
			}
		}
	}

	private void imprimirArquivo() throws MyClassException {
		/*
		 * o arquivo tem 9 n�meros, entretanto o loop aqui est� sendo executado
		 * 10 vezes. Devido a este problema o programa ir� lan�ar uma exce��o
		 * unchecked do tipo da classe ArrayIndexOutOfBoundsException.
		 */
		try {
			for (int i = 0; i < 10; i++)
				System.out.println(i + " : " + this.num.get(i));
		} catch (ArrayIndexOutOfBoundsException e) {
			/*
			 * uso do construtor que recebe uma string e um objeto do tipo
			 * Throwable. para obter este conte�do usar o m�todo getMessage()
			 * herdado de Throwable.
			 */
			MyClassException obj = new MyClassException(
					"Erro na formata��o do n�mero - Exce��o capturada: ArrayIndexOutOfBoundsException",
					e);
			obj.setClasse(getClass().getName());
			obj.setMensagem(e.getMessage()); // Mensagem de erro do objeto
												// capturado
			obj.setMetodo("recuperar");
			obj.setPacote(getClass().getPackage().toString());
			throw obj;
		}
	}
}
