package AsMatrizeseVetores;
import javax.swing.JOptionPane;

public class MatrizFor {
	
  public static void main(String[] args) {
	  
    int numero = 0;
    
    while(numero <= 0) {
      String str = "Numero de palavras";
      str = JOptionPane.showInputDialog(null,str);
      if (str == null) System.exit(0);
      numero = Integer.parseInt(str);
    }
    
    String[] palavras = new String[numero];
    
    for (int i = 1; i <= palavras.length; i++) {
    	
      String str = "Palavra  " + i;
      
      str = JOptionPane.showInputDialog(null,str);
      
      if (str == null) break;
      
      str = str.trim();
      
      palavras[i - 1] = str;
      
    }
    
    String mensagem = "Lista de palavras:";
    
    for (int posicao = 0; posicao < palavras.length; posicao++) {
      if (palavras[posicao] == null) break;
      mensagem += "\n- " + palavras[posicao];
    }
    
    JOptionPane.showMessageDialog(null,mensagem);
    System.exit(0);
  }
}

