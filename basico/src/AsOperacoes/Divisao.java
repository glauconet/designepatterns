package AsOperacoes;
import java.io.PrintStream;
import java.util.Scanner;

public class Divisao {
  public static void main(String[] args) {
    PrintStream saida = System.out;
    Scanner scan = new Scanner(System.in);
    
    saida.print("\nInforme o dividendo:\t");
    double n1 = scan.nextDouble();
    
    saida.print("Informe o divisor:\t");
    double n2 = scan.nextDouble();
    
    boolean erro = n2 == 0 ? true : false;
    String msg = erro == true ? "Erro" : String.valueOf(n1 / n2);
    
    saida.println("Resultado:\t\t" + msg + "\n");
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/