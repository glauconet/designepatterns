package AsOperacoes;
import java.io.PrintStream;
import java.util.Scanner;

public class OperadoresLogicos {
  public static void main(String[] args) {
    PrintStream saida = System.out;
    Scanner scan = new Scanner(System.in);
    
    saida.print("\nInforme um valor booleano:\t");
    boolean b1 = scan.nextBoolean();
    
    saida.print("Informe outro valor booleano:\t");
    boolean b2 = scan.nextBoolean();
    
    saida.println("\nOpera��es l�gicas:");
    saida.println("b1 || b2:\t" + (b1 || b2));
    saida.println("b1 && b2:\t" + (b1 && b2));
    saida.println("b1 ^ b2:\t" + (b1 ^ b2));
    saida.println("!b1:\t\t" + !b1);
    saida.println("!b2:\t\t" + !b2);
    
    saida.println("\nAtribui��es:");
    b1 |= b2;
    saida.println("b1 |= b2:\tb1 recebeu " + b1);
    b1 &= !b2;
    saida.println("b1 &= !b2:\tb1 recebeu " + b1);
    b1 ^= b2;
    saida.println("b1 ^= b2:\tb1 recebeu " + b1);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/