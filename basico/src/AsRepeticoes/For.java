package AsRepeticoes;
public class For {
  public static void main(String[] args) {
    System.out.print("Primeiro laço:\t");
    for (byte num = 1; num <= 5; num++)
      System.out.print(num + " ");
    
    System.out.print("\nSegundo laço:\t");
    for(byte num = 5;num >= 1; num--)
      System.out.print(num + " ");
    
    System.out.println();
  }
}

/********************************************************************
 *                                                                  *
 ********************************************************************/