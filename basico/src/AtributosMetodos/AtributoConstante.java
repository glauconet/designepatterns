package AtributosMetodos;


public class AtributoConstante extends Pessoa{
	
	
	public final double Creditobase = 300;// use a palavra final
// sua inicializacao e feita sempre na declaracao e nao podem ser alterado
	
	public static void main(String[] args){
		
		AtributoConstante p1 = new AtributoConstante();
		AtributoConstante p2 = new AtributoConstante();
		
		p1.setNome("X");
		p2.setNome("Y");
	
		System.out.print("Nome: " + p1.getNome());
		System.out.println(" credito: " + p1.Creditobase);
		System.out.print("Nome: " + p2.getNome());
		System.out.print(" credito: " + p2.Creditobase );
		
		
	}

}
