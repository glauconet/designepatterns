package AtributosMetodos;

public abstract class MetodosAbstract {
	// vira classe abstrata por causa do seu metodo abstrato
	public static String st = "Atributo da Classe";
		
	public void metodo1(){
		System.out.println("M�todo com void nao retorna nada");
		System.out.println("");
	}
	
	public final void metodoConstante(){
		System.out.println("Metodo constante");
		System.out.println("Use palavra final na assinatura do m�todo e");
		System.out.println("ele n�o podera ser modificado numa extens�o");
		System.out.println("");
	}
		
	public static void metodoEstatico(String st){
		System.out.println(" Metodo Estatico");
		System.out.println(" Use a palava static na assinatura do metodo e ");
		System.out.println("ele s� devera se usado chamando direto ");
		System.out.println("pelo nome  classe e pelo nome do metodo,");
		System.out.println("trabalha portanto com atributos estaticos");
		System.out.println("Exemplo:");
		System.out.println(st);
	}
	
	public abstract void metodoAbstrato();
	//metodo abstrato n�o possui corpo
	//sera descrito na classe que estender essa

	
}
