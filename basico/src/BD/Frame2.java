package BD;
import java.awt.*;
import javax.swing.*;

public class Frame2 extends JFrame {

		public Frame2(String titulo, Dimension tamanho) {
		
		setTitle(titulo);
		setSize(tamanho);
		centralize();
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(null);
		getContentPane().setBackground(new Color(200,230,200));
				
		}
		
		public void centralize(){
		Dimension T = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension J = getSize();
		if(J.height > T.height) setSize(J.width,T.height);
		if (J.width > T.width) setSize (T.width, J.height);
		setLocation((T.width - J.width)/2 ,(T.height - J.height)/2); 
		}

		
}  