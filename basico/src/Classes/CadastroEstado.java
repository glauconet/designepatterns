package Classes;
import javax.swing.JOptionPane;

public class CadastroEstado {
  public static void main(String[] args) {
    Estado estado = new Estado();
    
    String str = JOptionPane.showInputDialog("Nome do estado");
    if (str == null) System.exit(0);
    estado.nome = str;
    
    str = JOptionPane.showInputDialog("Sigla do estado");
    if (str == null) System.exit(0);
    estado.sigla = str;
    
    estado.exibirDados();
    System.exit(0);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/