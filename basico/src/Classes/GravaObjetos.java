package Classes;
import java.io.*;
import javax.swing.*;
import OOConstrutores.Pessoa;

public class GravaObjetos {
	public static void main(String[] args) {
		try {
				
			//FileInputStream in = new FileInputStream("dadosnaRaiz.txt");
			//int ch;
			//while ((ch = in.read()) != -1)
				//System.out.print((char)ch);
			//in.close();
						
		    FileOutputStream fos = new FileOutputStream("dadosnaRaiz.txt");
		    
			ObjectOutputStream out = new ObjectOutputStream(fos);

			for (int cont = 1; cont <= 3; cont++) {
				
				String nome = JOptionPane.showInputDialog("Digite o nome");
				Pessoa pessoa = new Pessoa();
				pessoa.setNome(nome);
				out.writeObject(pessoa.getNome());
			}
			out.close();
			JOptionPane.showMessageDialog(null, "Dados Gravados");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}

