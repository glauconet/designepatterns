package Classes;
import java.io.*;
import javax.swing.JOptionPane;
import OOConstrutores.Pessoa;

public class LeObjetos {
	public static void main(String[] args) {
		try {
FileInputStream fis = new FileInputStream("dadosnaRaiz.txt");
			ObjectInputStream in = new ObjectInputStream(fis);

			Pessoa pessoa = new Pessoa();
			while ( (pessoa = (Pessoa) in.readObject()) != null)
				System.out.println(pessoa.getNome());
			in.close();
			fis.close();
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null, "Arquivo n�o encontrado");
		} catch (EOFException e) {
			JOptionPane.showMessageDialog(null, "Leitura completada");
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}
