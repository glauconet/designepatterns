package Classes;
import java.awt.*;
import java.io.*;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import OOConstrutores.Pessoa;



public class LeObjetos2 {
	public static void main(String[] args) {
		try {
			JFileChooser jfc = new JFileChooser();
		    int resposta = jfc.showOpenDialog(null);
		    if(resposta != JFileChooser.APPROVE_OPTION)
		    	System.exit(0);
		    String nomeArquivo = jfc.getSelectedFile().getName();
			
			FileInputStream fis = new FileInputStream(nomeArquivo);
			ObjectInputStream in = new ObjectInputStream(fis);

			Pessoa pessoa = new Pessoa();
			while ( (pessoa = (Pessoa) in.readObject()) != null)
				//System.out.println(pessoa.getNome() + " - " + pessoa.getRg());
			in.close();
			fis.close();
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null, "Arquivo n�o encontrado");
		} catch (EOFException e) {
			JOptionPane.showMessageDialog(null, "Leitura completada");
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}
