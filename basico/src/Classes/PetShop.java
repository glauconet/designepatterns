package Classes;


public class PetShop {

	public static void main(String[] args) {
		
		Animal animais[] = new Animal[3];
		
		Cao c1 = new Cao("Rex");
		Gato g1 = new Gato("Felix");
		Rato r1 = new Rato("Mickey");
		
		animais[0] = (Animal) c1;
		animais[1] = (Animal) g1;
		animais[2] = (Animal) r1;
		
		for(int i = 0 ; i < animais.length; i++  ){
			animais[i].emitirSom();
			System.out.println(animais[i].getNome());
		}
		
	}
}
