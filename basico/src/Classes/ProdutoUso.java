package Classes;

public class ProdutoUso {
	
	public static void main(String[] args) {
				
		Produto p1 = new Produto();
		Produto p2 = new Produto();
		
		p1.nome = "Produto1";
		p1.preco = 2;
		p1.codigo = 1;
		
		p2.nome = "Produto2";
		p2.preco = 50;
		p2.codigo = 2;
		
		System.out.println("EXEMPLO DE INSTANCIACAO");
		System.out.println(p1.nome);
		System.out.println(p1.preco);
		System.out.println(p1.codigo);

		System.out.println(p2.nome);
		System.out.println(p2.preco);
		System.out.println(p2.codigo);
		
		// para compilar digite no prompt: javac ProdutoCriar.Java
		// para rodar digite no prompt: java ProdutoCriar

	}

}
