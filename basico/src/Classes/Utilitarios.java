package Classes;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import org.hibernate.Hibernate;


public class Utilitarios {
	public Utilitarios() {}


	/**
	 * Calcular DV do Número de Processo.
	 */
	public static String dvNumeroProcesso(String numeroSemDv) {
		String dvRetorno = "";
		Integer tamanho = numeroSemDv.length();
		if (tamanho > 0) {
			Integer dig1 = 0;
			Integer dig2 = 0;
			for (Integer contador = tamanho; contador >= 1; contador--) {
				Integer indice = tamanho - contador;
				Integer fator = Integer.parseInt(numeroSemDv.substring(indice,indice + 1));
				dig2 +=  ( (contador+2) * fator );
				if (contador > 0) dig1 += ( (1 + contador) * fator );
			}
			dig1 = 11 - (dig1 % 11);
			if (dig1 >= 10) dig1 -= 10;
			dig2 += (2 * dig1);
			dig2 = 11 - (dig2 % 11);
			if (dig2 >= 10) dig2 -= 10;
			dvRetorno = String.valueOf( 100 + (10 * dig1) + dig2).substring(1);
		}
		return dvRetorno;
	}
		
	/**
	 * retorna o nome do mes por extenso.
	 */
	public static String retornaMesExtenso(String mes){
		if (mes.equals("01")){
			return "Janeiro";
		}else if (mes.equals("02")){
			return "Fevereiro";
		}else if (mes.equals("03")){
			return "Março";
		}else if (mes.equals("04")){
			return "Abril";
		}else if (mes.equals("05")){
			return "Maio";
		}else if (mes.equals("06")){
			return "Junho";
		}else if (mes.equals("07")){
			return "Julho";
		}else if (mes.equals("08")){
			return "Agosto";
		}else if (mes.equals("09")){
			return "Setembro";
		}else if (mes.equals("10")){
			return "Outubro";
		}else if (mes.equals("11")){
			return "Novembro";
		}else if (mes.equals("12")){
			return "Dezembro";
		}else{
			return "????";
		}
	}
        
        /**
	 *  Formatar data com retorno em String
	 */
	public static String formataData(Date data, String formato){		
	    SimpleDateFormat formatter = null;		
	    try{
			formatter = new SimpleDateFormat(formato);
		}
	    catch (RuntimeException e){			
			e.printStackTrace();
		}        
		return formatter.format(data);
	}
        
	/**
	 * Retorna o ano que esta atribuido no atributo Date
	 */
	public static int retornaAno(Date data){
		if (data == null){
			return 0;
		}else{
			String dt = Utilitarios.formataData(data, "yyyy");
			return Integer.parseInt(dt);
		}
	}

	/**
	 * Calcula DV do CNPJ Novo
	 */
	public static String dvCnpjNovo(String cnpj){
		Integer[] val = new Integer[14];
		int i;
		for (i = 0; i < cnpj.length()-1; i++)
			val[i] = Integer.parseInt(cnpj.substring(i, i+1));
		val[i] = Integer.parseInt(cnpj.substring(cnpj.length()-1));

		
		Integer[] digitos = new Integer[13];
		System.arraycopy(val, 0, digitos, 0, 12);
	    Integer[] pesos1 = new Integer[]{5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2};
	    Integer[] pesos2 = new Integer[]{6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2};
	
	    int dv1 = calculaParteDV(digitos, pesos1);
	    digitos[12] = dv1;
	    int dv2 = calculaParteDV(digitos, pesos2);
	    
	    return String.format("%02d", dv1 * 10 + dv2);
	}

	private static int calculaParteDV(Integer[] digitos, Integer[] pesos) {
	    int sum = 0;
	    int i = 0;
        for (Integer peso : pesos)
            sum += peso * digitos[i++];
	    int parcial = sum % 11;
	    return parcial < 2 ? 0 : 11 - parcial;
	}

    /**
     * Calcula DV de CPF
     */
	public static String dvCpf(String cpf){
		cpf = String.format("%09d", Long.valueOf(cpf));

		int soma = 0, mult = 11;
		int[] var = new int[11];

		// Recebe os numeros e realiza a multiplicacao e soma.
		for (int i = 0; i < 9; i++) {
			var[i] = Integer.parseInt("" + cpf.charAt(i));
			if (i < 9)
				soma += (var[i] * --mult);
		}

		// Cria o primeiro dï¿½gito verificador.
		int resto = soma % 11;
		if (resto < 2) {
			var[9] = 0;
		} else {
			var[9] = 11 - resto;
		}

		// Reinicia os valores.
		soma = 0;
		mult = 11;

		// Realiza a multiplicaï¿½ï¿½o e soma do segundo dï¿½gito.
		for (int i = 0; i < 10; i++)
			soma += var[i] * mult--;

		// Cria o segundo dï¿½gito verificador.
		resto = soma % 11;
		if (resto < 2) {
			var[10] = 0;
		} else {
			var[10] = 11 - resto;
		}

		String digitoVerifCalculado = Integer.valueOf(var[9]).toString()
				+ Integer.valueOf(var[10]).toString();
		return digitoVerifCalculado;
	}
	/**
	 * Calcula a diferenca em dias entre duas Datas.
	 */
	public static Long diferencaEmDias(Date data1, Date data2){
		
		  long dt = (data2.getTime() - data1.getTime()) + 3600000;         
		  long dias = (dt / 86400000L);   
		  
		  return Math.abs(dias);   
	}
	/**
	 * Recupera a Data Corrente.
	 */
	public static Date getDataCorrente(){
		return new Date();
	}
	/**
	 * Verifica preenchimento numerico.
	 */
    public static Boolean isNumeric(String s){
    	// retirar caracteres de edição padrao(caso tenha) antes de chamar este metodo
    	if(s == null || s.length() == 0 ){
    		return false;
    	}
    	for(int i=0; i < s.length(); i++){
    		char c = s.charAt(i);
    		if (!Character.isDigit(c)){
    			return false;
    		}
    	}
    	return true;
    }
    /**
     * Converter String para BigDecimal
     */
    public static BigDecimal converteStringtoBigdecimal(String str){
    	if (str ==  null){
    		return null;
    	}
    	BigDecimal valorBig;
    	String valor1 = str.replace(".", "");
    	String valor2 = valor1.replace(",", ".");
     	return valorBig = new BigDecimal(valor2); 
    }
    /**
     * Converte BigDecimal para String com mascara. 
     * @param big
     * @return
     */
    public static String converteBigdecimaltoString(BigDecimal big){
    	if (big == null){
    		return null;
    	}
        String pattern = "##,###,###,###,###,##0.00";

        Locale locale=new Locale("pt","BR");
   
        DecimalFormatSymbols symbols=new DecimalFormatSymbols(locale);
        symbols.setDecimalSeparator(',');
        symbols.setGroupingSeparator('.');
        DecimalFormat f=new DecimalFormat(pattern,symbols);
        return f.format(big);
    }
    
    /**
     * Formata(mascara) valor de BigDecimal retorno em String , similar ao converteBigdecimaltoString, so que com mascara menor. 
     * @param valor
     * @return
     */
    public static String formataValor(BigDecimal valor){
    	String valorEditado = valor.toString();
		BigDecimal bd = new BigDecimal(valorEditado);
		String pattern = "###,###,###,##0.00";
		Locale locale=new Locale("pt","BR");
		DecimalFormatSymbols symbols=new DecimalFormatSymbols(locale);
		symbols.setDecimalSeparator(',');
		symbols.setGroupingSeparator('.');
		DecimalFormat f=new DecimalFormat(pattern,symbols);
		return f.format(bd);
    }
    
    /**
     * Formata numero de Processo (mascara).
     * @param processo
     * @return
     */
    public static String formataNroProcesso(String processo){
	    /*Formata número do processo:
	    	Se 15 posiçoes: NNNNN-NNN.NNN/NN-NN
	    	Se 17 posiçoes: NNNNN.NNNNNN/NNNN-NN*/
    	
    	String procComplFormatado = "";
    	if (processo.length() == 15){
            procComplFormatado = processo.substring(0, 5) + '-' +
    		processo.substring(5, 8) + '.' +		
    		processo.substring(8, 11) + '/' +										
    		processo.substring(11, 13) + '-' +
    		processo.substring(13, 15);
    	}else{
            procComplFormatado = processo.substring(0, 5) + '.' +
            processo.substring(5, 11) + '/' +										
            processo.substring(11, 15) + '-' +
            processo.substring(15, 17);
    	} 
        return procComplFormatado;	 
    }
    
    
    /**
     * Converte File para Blob.
     */
    
    public static Blob converteFileParaBlob(File file)throws IOException{
		File arquivo = file;
		if ((arquivo == null) ||(arquivo.length() == 0)){
			return null;
		}
		FileInputStream fis = null;  
		fis = new FileInputStream(arquivo); 
    	try{
    		byte[] arrayBytes = new byte[(int) arquivo.length()];  
    		fis.read(arrayBytes);
    		//Blob blob = Hibernate.createBlob(arrayBytes); 
    		//return blob;
    		return null;
    	}finally {  
    		if (fis != null) {  
    			fis.close();
    		}	
		}  
    	
    } 
    /**
     * Converte Array de Bytes para Blob.
     */
    public static Blob converteArrayByteParaBlob(byte[] arrayBytes){
    	if ((arrayBytes == null) || (arrayBytes.length == 0)){
    		return null;
    	}
    	//Blob blob = Hibernate.createBlob(arrayBytes); 
    	//return blob;
    	return null;
    }
    /**
     * Converte Blob para File.
     */
    public static File converteBlobParaFile(Blob blob, String caminho)
        throws SQLException, IOException {
    	if ((blob == null) || (blob.length() ==0)){
    		return null;
    	}
    	if ((caminho == null) || (caminho.length() == 0)){
    		return null;
    	}
    	BufferedInputStream input = null;  
    	BufferedOutputStream output = null;  
        try {  
        	byte[] arrayBytes = new byte[(int) blob.length()];  
        	input  = new BufferedInputStream(blob.getBinaryStream());  
        	output = new BufferedOutputStream(new FileOutputStream(caminho));  
        	input.read(arrayBytes);
        	output.write(arrayBytes);
        	File arquivo = new File(caminho);  
        	return arquivo;  
        } finally {  
        	try {  
        		if (output != null) {  
        			output.close();  
        		}  
        	} finally {  
        		if (input != null) {  
        			input.close();  
        		}  
        	}  
        }  
    }  
    /**
     * Converte File para List de String contendo linhas/registros, a quebra e por /n.
     */
    public static List<String> converteFileParaListString(File arquivo)
        throws SQLException, IOException {
    	if ((arquivo == null) || (arquivo.length() == 0)){
    		return  null;
    	}
    	FileReader reader = new FileReader(arquivo);
    	BufferedReader leitor = new BufferedReader(reader);
    	try {
    	    String linhaInt = null;
    	    List<String> linhaArquivo = new ArrayList<String>();
    	    while ( (linhaInt = leitor.readLine()) != null ){
    	    	if (linhaInt.length() != 0){
        		linhaArquivo.add(linhaInt);
    	    	}
    	    }
    	    return linhaArquivo;
    	}finally {     
    		if (leitor != null){
    			leitor.close();
    		}
    		if (reader != null){
    			reader.close();
    		}
    	}
    }
    /**
     * Converte List de String contendo linhas/registros, em File.
     */
    public static File  converteListStringParaFile(List<String> lista, String caminho)
        throws SQLException, IOException {
    	if ((lista == null) || (lista.size() == 0)){
    		return  null;
    	}
       	if ((caminho == null) || (caminho.length() == 0)){
    		return  null;
    	}
       	File file = new File(caminho);
       	FileOutputStream saida = new FileOutputStream(file);
        for (String linha : lista){
        	linha = linha + "/n"; 
        	saida.write(linha.getBytes());
        }
        saida.close();
        return file;
    }    
    	
	/**
	 * Reseta a hora de um data
	 * @param date - Date que terá a hora resetada
	 * @return Date com a hora resetada
	 */
	public static Date resetaHora(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.HOUR, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		c.set(Calendar.AM_PM, Calendar.AM);

		return c.getTime();
	}
        
         /**
	 * Retorna somente dados númericos de um String.
	 */
	public static String onlyNumber(String num) {
		String onlyNumber = "";
		Character c;
		for ( int i = 0; i < num.length(); i++) {
			c = num.charAt(i);
			if ( Character.isDigit(c)) onlyNumber += c;
		}
		return onlyNumber;
	}
        
	/**
	 * Colocar mascara em String de campo numerico.
	 */
	public static String mask(String numero, String mascara){
		if (numero == null) numero = "0";
		String num  = onlyNumber(numero);
		char n;
		String mask = num;
		int in = num.length()-1;
		if (mascara != null ) {
			mask = "";
			for ( int im = (mascara.length()-1); im > -1 ; im-- ) {
				char m = mascara.charAt(im);
				if (in >= 0 ) {
					n = num.charAt(in);
					if ( m == '9' || m =='#') {
							mask = n + mask;
							in--;
					} else {
							mask = m + mask;
					}
				} else {
					switch( m ) {
						case '9':
							mask = '0' + mask;
							break;
							// falta dar mais tratamento nesta opção.
						case '#':
							mask = ' ' + mask;
							break;
						default:
							mask = m + mask;
					}
				}
			}
		}
		return mask;
	}
        
	
	/**
	 * Método responsável por verificar se o número do processo tem seus 11 primeiros dígitos dentro da faixa de valores
	 * Faixa de Valores: de 10.000.000.000 a 19.999.999.999 (obs.: valores sem os pontos)
	 * Para essa verificação esse método retida toda a formatação do número do processo. 
	 * 
	 * @param numeroProcesso
	 * @return boolean   - Verdadeiro se estiver dentro da Faixa de Valores
	 */
	public static boolean isNumeroProcessoNaFaixaValores(String numeroProcesso) {
		numeroProcesso = numeroProcesso.replaceAll("[^0-9]", "");
		Pattern p = Pattern.compile("1[0-9]{10}[0-9]*");
		Matcher m = p.matcher(numeroProcesso);
		return m.matches();
	}
	
	/**
	 * Método para validar o número do Processo
	 * 
	 *  @param numero do processo
	 */
	public static boolean isNumeroProcessoValido(String numeroProcesso){
		
		if (numeroProcesso == null) { return false; }
		
		numeroProcesso = numeroProcesso.replaceAll("[^0-9]", "");
		
		if (numeroProcesso.length()!=17 ){
			return false; 
		}else{	
			return numeroProcesso.substring(15).equals(dvNumeroProcesso(numeroProcesso.substring(0, 15)));
		}
		
	}

	/**
	 * Validar Data Incial e Final Preenchidas e Data FInal maior ou igual a
	 * data de Inicio, sem fomatar mensagem.
	 */
	public static boolean validaDataInicioFim(Date dataInicio, Date dataFim) {
		boolean ok = true;
		if ( ( dataInicio == null) && ( dataFim == null) ) {
			ok = false;
		}
		if ( ( dataInicio != null) && ( dataFim != null) ) {
			if (dataFim.before(dataInicio)){
				ok = false;
			} 
		}
		return ok;
	}
	
/**
 * Calcula DV de NIRF (Imovel Rural ), recebe NIRF com 7 ou 8 digitos(com DV), 
 * calcula o DV do NIRF sobrenirf  e retorna o NIRF com o DV tamanho de 8 digitos 
*/
	public static String dvNIRF(String nirf){
		String nirfDV = String.format("%07d", Long.valueOf(nirf));
		if (nirfDV.length() == 8){
			nirfDV = nirfDV.substring(0, 7);
		}

		int soma = 0, mult = 9;
		int[] var = new int[8];

		// Recebe os numeros e realiza a multiplicacao e soma.
		for (int i = 0; i < 7; i++) {
			var[i] = Integer.parseInt("" + nirfDV.charAt(i));
			if (i < 7)
				soma += (var[i] * --mult);
		}

		// Cria o primeiro dï¿½gito verificador.
		int resto = soma % 11;
		if (resto < 2) {
			var[7] = 0;
		} else {
			var[7] = 11 - resto;
		}
		String nirfCalculado = nirfDV + Integer.valueOf(var[7]).toString();
		return nirfCalculado;
	}
/**
 * Calcula DV do numnero do Pagamento, recebe numero do pagamento com 10 ou 11 digitos(com DV), 
 * calcula o DV do numero do pagamento sobre 10 digitos e retorna o numero do pagamento com o DV tamanho de 11 digitos 
*/
		public static String dvPagamento(String nuPagamento){
			String pagamentoDV = String.format("%10d", Long.valueOf(nuPagamento));
			if (pagamentoDV.length() == 11){
				pagamentoDV = pagamentoDV.substring(0, 10);
			}
			StringBuffer stPagamento = new StringBuffer(mask(pagamentoDV,"9999999999")) ;
			// calcular primeiro dv

	    	int peso = 3;
			int resultado = 0; 
			for (int i = 0; i < stPagamento.length() ; i++){
				resultado += peso * Integer.parseInt("" + stPagamento.charAt(i));
				if ( i < 10 ){
					if ( peso != 2){
						peso--; 
					}else{
						peso = 9;
					}	
				}
			}

			int resto = resultado % 11 ;
			Integer dv1   =  Integer.valueOf(0);
			if (resto > 1){
				dv1 = 11 - resto;
			} 
			return stPagamento.toString() + dv1.toString();
	}
    
    /**
	 * Converte um IP que esteja no formato X.X.X.X, onde X é um número de 0-255 para uma String Hexadecimal.
	 * Ex: 10.15.10.7 -> 0A0F0A07
	 */
	public static String IptoHexa(String ip){
		int posicaoInicialPonto = 0;
		int posicaoFinalPonto = 0;
		String hex = null;
		StringBuilder resultado = new StringBuilder("");
		
		for (int i = 0; i<=3 ; i++){
			if (i==0){
				posicaoInicialPonto = 0;
				posicaoFinalPonto = ip.indexOf(".");
			}
			if (i==1 || i==2){
				posicaoInicialPonto = posicaoFinalPonto + 1;
				posicaoFinalPonto = ip.indexOf(".",posicaoInicialPonto);
			}
			if (i==3){
				posicaoInicialPonto = posicaoFinalPonto + 1;
				posicaoFinalPonto = ip.length();
			}
			hex = Integer.toHexString(Integer.parseInt(ip.substring(posicaoInicialPonto, posicaoFinalPonto))).toUpperCase();
			hex = hex.length()==1? 0+hex:hex;
			resultado.append(hex);
		}
			
		return resultado.toString();
	}
	
	/**
	* Converte uma String Hexadecimal em um IP que esteja no formato X.X.X.X, onde X é um número de 0-255.
	* Ex: 0A0F0A07 -> 10.15.10.7 
	*/
	public static String HexatoIp(String ipHexa){
		int posicaoInicial = 0;
		int posicaoFinal = 0;
		StringBuilder resultado = new StringBuilder("");
		
		for (int i = 0; i<=3 ; i++){
			posicaoInicial = posicaoFinal;
			posicaoFinal = posicaoInicial + 2;
			if (i<3){
				resultado.append(Integer.parseInt(ipHexa.substring(posicaoInicial, posicaoFinal), 16) + ".");
			}else{
				resultado.append(Integer.parseInt(ipHexa.substring(posicaoInicial, posicaoFinal), 16) + "");
			}
		}
		
		return resultado.toString();
	}
	    
}
