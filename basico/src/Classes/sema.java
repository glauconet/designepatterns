package Classes ;

import java.awt.BorderLayout;
import javax.swing.JPanel;
import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class sema extends JFrame {

	private JPanel jContentPane = null;
	private JMenuBar jJMenuBar = null;
	private JMenu jMenu1 = null;
	private JMenu jMenu2 = null;
	private JMenu jMenu3 = null;
	private JMenu jMenu4 = null;
	private JMenu jMenu5 = null;
	private JMenuItem jMenuItem = null;
	private JMenuItem jMenuItem1 = null;
	private JMenu jMenu6 = null;
	private JMenuItem jMenuItem2 = null;
	private JMenuItem jMenuItem3 = null;
	private JMenuItem jMenuItem4 = null;
	private JMenuItem jMenuItem5 = null;
	private JMenuItem jMenuItem6 = null;
	private JMenuItem jMenuItem7 = null;
	private JMenuItem jMenuItem8 = null;
	private JMenuItem jMenuItem9 = null;
	private JMenu jMenu = null;
	private JMenuItem jMenuItem10 = null;
	private JMenu jMenu7 = null;
	private JMenuItem jMenuItem11 = null;
	private JMenuItem jMenuItem12 = null;
	private JMenuItem jMenuItem13 = null;
	private JMenuItem jMenuItem14 = null;
	private JMenuItem jMenuItem15 = null;
	private JMenuItem jMenuItem16 = null;
	private JMenuItem jMenuItem17 = null;
	private JMenu jMenu8 = null;
	private JMenuItem jMenuItem18 = null;
	private JMenuItem jMenuItem19 = null;
	private JMenuItem jMenuItem20 = null;
	private JMenu jMenu9 = null;
	private JMenu jMenu10 = null;
	private JMenu jMenu11 = null;
	private JMenu jMenu12 = null;
	private JMenu jMenu13 = null;
	private JMenu jMenu14 = null;
	private JMenuItem jMenuItem21 = null;
	private JMenuItem jMenuItem22 = null;
	private JMenuItem jMenuItem23 = null;
	private JMenuItem jMenuItem24 = null;
	private JMenuItem jMenuItem25 = null;
	private JMenuItem jMenuItem26 = null;
	private JMenuItem jMenuItem27 = null;
	private JMenuItem jMenuItem28 = null;
	private JMenuItem jMenuItem29 = null;
	private JMenu jMenu15 = null;
	private JMenuItem jMenuItem30 = null;
	private JMenuItem jMenuItem31 = null;
	private JMenuItem jMenuItem32 = null;
	private JMenuItem jMenuItem33 = null;
	private JMenuItem jMenuItem34 = null;
	private JMenuItem jMenuItem35 = null;
	private JMenu jMenu16 = null;
	private JMenuItem jMenuItem36 = null;
	private JMenuItem jMenuItem37 = null;
	private JMenu jMenu17 = null;
	private JMenuItem jMenuItem38 = null;
	private JMenuItem jMenuItem39 = null;
	private JMenu jMenu18 = null;
	private JMenuItem jMenuItem40 = null;
	private JMenuItem jMenuItem41 = null;
	private JMenuItem jMenuItem42 = null;
	private JMenuItem jMenuItem43 = null;
	private JMenu jMenu19 = null;
	private JMenuItem jMenuItem44 = null;
	private JMenuItem jMenuItem45 = null;
	private JMenuItem jMenuItem46 = null;
	private JMenuItem jMenuItem47 = null;
	private JMenuItem jMenuItem48 = null;
	public sema() throws HeadlessException {
		super();
		// TODO Auto-generated constructor stub
		initialize();
	}

	public sema(GraphicsConfiguration arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
		initialize();
	}

	public sema(String arg0) throws HeadlessException {
		super(arg0);
		// TODO Auto-generated constructor stub
		initialize();
	}

	public sema(String arg0, GraphicsConfiguration arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
		initialize();
	}

	/**
	 * This method initializes jJMenuBar	
	 * 	
	 * @return javax.swing.JMenuBar	
	 */
	private JMenuBar getJJMenuBar() {
		if (jJMenuBar == null) {
			jJMenuBar = new JMenuBar();
			jJMenuBar.add(getJMenu6());
			jJMenuBar.add(getJMenu1());
			jJMenuBar.add(getJMenu2());
			jJMenuBar.add(getJMenu3());
			jJMenuBar.add(getJMenu4());
			jJMenuBar.add(getJMenu5());
			jJMenuBar.add(getJMenu9());
		}
		return jJMenuBar;
	}

	/**
	 * This method initializes jMenu1	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getJMenu1() {
		if (jMenu1 == null) {
			jMenu1 = new JMenu();
			jMenu1.setText("�REA VERDE");
			jMenu1.add(getJMenuItem());
			jMenu1.add(getJMenuItem1());
			jMenu1.add(getJMenuItem2());
		}
		return jMenu1;
	}

	/**
	 * This method initializes jMenu2	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getJMenu2() {
		if (jMenu2 == null) {
			jMenu2 = new JMenu();
			jMenu2.setText("IMPLANTA��O");
		}
		return jMenu2;
	}

	/**
	 * This method initializes jMenu3	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getJMenu3() {
		if (jMenu3 == null) {
			jMenu3 = new JMenu();
			jMenu3.setText("MANUTEN��O");
			jMenu3.add(getJMenuItem8());
			jMenu3.add(getJMenuItem9());
			jMenu3.add(getJMenu());
			jMenu3.add(getJMenuItem10());
			jMenu3.add(getJMenu7());
		}
		return jMenu3;
	}

	/**
	 * This method initializes jMenu4	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getJMenu4() {
		if (jMenu4 == null) {
			jMenu4 = new JMenu();
			jMenu4.setText("ESTOQUE (S/E)");
			jMenu4.add(getJMenu10());
			jMenu4.add(getJMenu11());
		}
		return jMenu4;
	}

	/**
	 * This method initializes jMenu5	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getJMenu5() {
		if (jMenu5 == null) {
			jMenu5 = new JMenu();
			jMenu5.setText("RECURSOS HUMANOS");
			jMenu5.add(getJMenuItem35());
			jMenu5.add(getJMenu16());
			jMenu5.add(getJMenu17());
		}
		return jMenu5;
	}

	/**
	 * This method initializes jMenuItem	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem() {
		if (jMenuItem == null) {
			jMenuItem = new JMenuItem();
			jMenuItem.setText("cadastro");
		}
		return jMenuItem;
	}

	/**
	 * This method initializes jMenuItem1	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem1() {
		if (jMenuItem1 == null) {
			jMenuItem1 = new JMenuItem();
			jMenuItem1.setText("observa��es");
		}
		return jMenuItem1;
	}

	/**
	 * This method initializes jMenu6	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getJMenu6() {
		if (jMenu6 == null) {
			jMenu6 = new JMenu();
			jMenu6.setText("CADASTRO");
			jMenu6.add(getJMenuItem3());
			jMenu6.add(getJMenuItem4());
			jMenu6.add(getJMenuItem7());
			jMenu6.add(getJMenuItem6());
			jMenu6.add(getJMenuItem5());
		}
		return jMenu6;
	}

	/**
	 * This method initializes jMenuItem2	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem2() {
		if (jMenuItem2 == null) {
			jMenuItem2 = new JMenuItem();
			jMenuItem2.setText("tipo");
		}
		return jMenuItem2;
	}

	/**
	 * This method initializes jMenuItem3	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem3() {
		if (jMenuItem3 == null) {
			jMenuItem3 = new JMenuItem();
			jMenuItem3.setText("logradouro");
		}
		return jMenuItem3;
	}

	/**
	 * This method initializes jMenuItem4	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem4() {
		if (jMenuItem4 == null) {
			jMenuItem4 = new JMenuItem();
			jMenuItem4.setText("tipo de logradouro");
		}
		return jMenuItem4;
	}

	/**
	 * This method initializes jMenuItem5	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem5() {
		if (jMenuItem5 == null) {
			jMenuItem5 = new JMenuItem();
			jMenuItem5.setText("loteamento / bairro / regional");
		}
		return jMenuItem5;
	}

	/**
	 * This method initializes jMenuItem6	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem6() {
		if (jMenuItem6 == null) {
			jMenuItem6 = new JMenuItem();
			jMenuItem6.setText("respons�vel pela regional");
		}
		return jMenuItem6;
	}

	/**
	 * This method initializes jMenuItem7	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem7() {
		if (jMenuItem7 == null) {
			jMenuItem7 = new JMenuItem();
			jMenuItem7.setText("regional");
		}
		return jMenuItem7;
	}

	/**
	 * This method initializes jMenuItem8	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem8() {
		if (jMenuItem8 == null) {
			jMenuItem8 = new JMenuItem();
			jMenuItem8.setText("di�rio");
		}
		return jMenuItem8;
	}

	/**
	 * This method initializes jMenuItem9	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem9() {
		if (jMenuItem9 == null) {
			jMenuItem9 = new JMenuItem();
			jMenuItem9.setText("servi�os");
		}
		return jMenuItem9;
	}

	/**
	 * This method initializes jMenu	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getJMenu() {
		if (jMenu == null) {
			jMenu = new JMenu();
			jMenu.setText("RELAT�RIOS");
			jMenu.add(getJMenuItem11());
			jMenu.add(getJMenuItem12());
			jMenu.add(getJMenuItem13());
			jMenu.add(getJMenuItem14());
			jMenu.add(getJMenu8());
		}
		return jMenu;
	}

	/**
	 * This method initializes jMenuItem10	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem10() {
		if (jMenuItem10 == null) {
			jMenuItem10 = new JMenuItem();
		}
		return jMenuItem10;
	}

	/**
	 * This method initializes jMenu7	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getJMenu7() {
		if (jMenu7 == null) {
			jMenu7 = new JMenu();
			jMenu7.setText("IMPRESSOS");
			jMenu7.add(getJMenuItem15());
			jMenu7.add(getJMenuItem16());
			jMenu7.add(getJMenuItem17());
		}
		return jMenu7;
	}

	/**
	 * This method initializes jMenuItem11	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem11() {
		if (jMenuItem11 == null) {
			jMenuItem11 = new JMenuItem();
			jMenuItem11.setText("servi�o/regional");
		}
		return jMenuItem11;
	}

	/**
	 * This method initializes jMenuItem12	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem12() {
		if (jMenuItem12 == null) {
			jMenuItem12 = new JMenuItem();
			jMenuItem12.setText("regional/servi�o");
		}
		return jMenuItem12;
	}

	/**
	 * This method initializes jMenuItem13	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem13() {
		if (jMenuItem13 == null) {
			jMenuItem13 = new JMenuItem();
			jMenuItem13.setText("SEMA/servi�o/regional");
		}
		return jMenuItem13;
	}

	/**
	 * This method initializes jMenuItem14	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem14() {
		if (jMenuItem14 == null) {
			jMenuItem14 = new JMenuItem();
			jMenuItem14.setText("SEMA/servi�os");
		}
		return jMenuItem14;
	}

	/**
	 * This method initializes jMenuItem15	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem15() {
		if (jMenuItem15 == null) {
			jMenuItem15 = new JMenuItem();
			jMenuItem15.setText("di�rio");
		}
		return jMenuItem15;
	}

	/**
	 * This method initializes jMenuItem16	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem16() {
		if (jMenuItem16 == null) {
			jMenuItem16 = new JMenuItem();
			jMenuItem16.setText("servi�os");
		}
		return jMenuItem16;
	}

	/**
	 * This method initializes jMenuItem17	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem17() {
		if (jMenuItem17 == null) {
			jMenuItem17 = new JMenuItem();
			jMenuItem17.setText("p/cad.de �rea verde");
		}
		return jMenuItem17;
	}

	/**
	 * This method initializes jMenu8	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getJMenu8() {
		if (jMenu8 == null) {
			jMenu8 = new JMenu();
			jMenu8.setText("�REA VERDE");
			jMenu8.add(getJMenuItem18());
			jMenu8.add(getJMenuItem19());
			jMenu8.add(getJMenuItem20());
		}
		return jMenu8;
	}

	/**
	 * This method initializes jMenuItem18	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem18() {
		if (jMenuItem18 == null) {
			jMenuItem18 = new JMenuItem();
			jMenuItem18.setText("regional/bairro/endere�o");
		}
		return jMenuItem18;
	}

	/**
	 * This method initializes jMenuItem19	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem19() {
		if (jMenuItem19 == null) {
			jMenuItem19 = new JMenuItem();
			jMenuItem19.setText("bairro/regional/endere�o");
		}
		return jMenuItem19;
	}

	/**
	 * This method initializes jMenuItem20	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem20() {
		if (jMenuItem20 == null) {
			jMenuItem20 = new JMenuItem();
			jMenuItem20.setText("nome da �rea verde");
		}
		return jMenuItem20;
	}

	/**
	 * This method initializes jMenu9	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getJMenu9() {
		if (jMenu9 == null) {
			jMenu9 = new JMenu();
			jMenu9.setText("PATRIM�NIO");
			jMenu9.add(getJMenuItem48());
			jMenu9.add(getJMenu18());
			jMenu9.add(getJMenuItem40());
			jMenu9.add(getJMenu19());
		}
		return jMenu9;
	}

	/**
	 * This method initializes jMenu10	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getJMenu10() {
		if (jMenu10 == null) {
			jMenu10 = new JMenu();
			jMenu10.setText("S/EMPENHO");
			jMenu10.add(getJMenu12());
			jMenu10.add(getJMenu13());
			jMenu10.add(getJMenu14());
			jMenu10.add(getJMenu15());
		}
		return jMenu10;
	}

	/**
	 * This method initializes jMenu11	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getJMenu11() {
		if (jMenu11 == null) {
			jMenu11 = new JMenu();
			jMenu11.setText("C/EMPENHO");
			jMenu11.add(getJMenuItem32());
			jMenu11.add(getJMenuItem33());
			jMenu11.add(getJMenuItem34());
		}
		return jMenu11;
	}

	/**
	 * This method initializes jMenu12	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getJMenu12() {
		if (jMenu12 == null) {
			jMenu12 = new JMenu();
			jMenu12.setText("CADASTRO");
			jMenu12.add(getJMenuItem21());
			jMenu12.add(getJMenuItem22());
			jMenu12.add(getJMenuItem23());
		}
		return jMenu12;
	}

	/**
	 * This method initializes jMenu13	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getJMenu13() {
		if (jMenu13 == null) {
			jMenu13 = new JMenu();
			jMenu13.setText("MOVIMENTA��O");
			jMenu13.add(getJMenuItem24());
			jMenu13.add(getJMenuItem25());
			jMenu13.add(getJMenuItem26());
		}
		return jMenu13;
	}

	/**
	 * This method initializes jMenu14	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getJMenu14() {
		if (jMenu14 == null) {
			jMenu14 = new JMenu();
			jMenu14.setText("RELAT�RIO");
			jMenu14.add(getJMenuItem27());
			jMenu14.add(getJMenuItem28());
			jMenu14.add(getJMenuItem29());
		}
		return jMenu14;
	}

	/**
	 * This method initializes jMenuItem21	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem21() {
		if (jMenuItem21 == null) {
			jMenuItem21 = new JMenuItem();
			jMenuItem21.setText("tipo de �tem");
		}
		return jMenuItem21;
	}

	/**
	 * This method initializes jMenuItem22	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem22() {
		if (jMenuItem22 == null) {
			jMenuItem22 = new JMenuItem();
			jMenuItem22.setText("�tem");
		}
		return jMenuItem22;
	}

	/**
	 * This method initializes jMenuItem23	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem23() {
		if (jMenuItem23 == null) {
			jMenuItem23 = new JMenuItem();
			jMenuItem23.setText("local X �tem");
		}
		return jMenuItem23;
	}

	/**
	 * This method initializes jMenuItem24	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem24() {
		if (jMenuItem24 == null) {
			jMenuItem24 = new JMenuItem();
			jMenuItem24.setText("entrada");
		}
		return jMenuItem24;
	}

	/**
	 * This method initializes jMenuItem25	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem25() {
		if (jMenuItem25 == null) {
			jMenuItem25 = new JMenuItem();
			jMenuItem25.setText("solicita��o");
		}
		return jMenuItem25;
	}

	/**
	 * This method initializes jMenuItem26	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem26() {
		if (jMenuItem26 == null) {
			jMenuItem26 = new JMenuItem();
			jMenuItem26.setText("baixa da solicita��o");
		}
		return jMenuItem26;
	}

	/**
	 * This method initializes jMenuItem27	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem27() {
		if (jMenuItem27 == null) {
			jMenuItem27 = new JMenuItem();
			jMenuItem27.setText("�rea verde / �tem");
		}
		return jMenuItem27;
	}

	/**
	 * This method initializes jMenuItem28	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem28() {
		if (jMenuItem28 == null) {
			jMenuItem28 = new JMenuItem();
			jMenuItem28.setText("�tem / �rea verde");
		}
		return jMenuItem28;
	}

	/**
	 * This method initializes jMenuItem29	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem29() {
		if (jMenuItem29 == null) {
			jMenuItem29 = new JMenuItem();
			jMenuItem29.setText("SEMA / �tem");
		}
		return jMenuItem29;
	}

	/**
	 * This method initializes jMenu15	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getJMenu15() {
		if (jMenu15 == null) {
			jMenu15 = new JMenu();
			jMenu15.setText("SALDO");
			jMenu15.add(getJMenuItem30());
			jMenu15.add(getJMenuItem31());
		}
		return jMenu15;
	}

	/**
	 * This method initializes jMenuItem30	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem30() {
		if (jMenuItem30 == null) {
			jMenuItem30 = new JMenuItem();
			jMenuItem30.setText("tipo de �tem");
		}
		return jMenuItem30;
	}

	/**
	 * This method initializes jMenuItem31	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem31() {
		if (jMenuItem31 == null) {
			jMenuItem31 = new JMenuItem();
			jMenuItem31.setText("nome do �tem");
		}
		return jMenuItem31;
	}

	/**
	 * This method initializes jMenuItem32	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem32() {
		if (jMenuItem32 == null) {
			jMenuItem32 = new JMenuItem();
			jMenuItem32.setText("saldo");
		}
		return jMenuItem32;
	}

	/**
	 * This method initializes jMenuItem33	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem33() {
		if (jMenuItem33 == null) {
			jMenuItem33 = new JMenuItem();
			jMenuItem33.setText("solicita��o");
		}
		return jMenuItem33;
	}

	/**
	 * This method initializes jMenuItem34	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem34() {
		if (jMenuItem34 == null) {
			jMenuItem34 = new JMenuItem();
			jMenuItem34.setText("baixa de solicita��o");
		}
		return jMenuItem34;
	}

	/**
	 * This method initializes jMenuItem35	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem35() {
		if (jMenuItem35 == null) {
			jMenuItem35 = new JMenuItem();
			jMenuItem35.setText("equipe / lider / integrantes");
		}
		return jMenuItem35;
	}

	/**
	 * This method initializes jMenu16	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getJMenu16() {
		if (jMenu16 == null) {
			jMenu16 = new JMenu();
			jMenu16.setText("TRANSFER�NCIA");
			jMenu16.add(getJMenuItem36());
			jMenu16.add(getJMenuItem37());
		}
		return jMenu16;
	}

	/**
	 * This method initializes jMenuItem36	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem36() {
		if (jMenuItem36 == null) {
			jMenuItem36 = new JMenuItem();
			jMenuItem36.setText("lider de equipe");
		}
		return jMenuItem36;
	}

	/**
	 * This method initializes jMenuItem37	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem37() {
		if (jMenuItem37 == null) {
			jMenuItem37 = new JMenuItem();
			jMenuItem37.setText("servidor ");
		}
		return jMenuItem37;
	}

	/**
	 * This method initializes jMenu17	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getJMenu17() {
		if (jMenu17 == null) {
			jMenu17 = new JMenu();
			jMenu17.setText("REGIONAL");
			jMenu17.add(getJMenuItem38());
			jMenu17.add(getJMenuItem39());
		}
		return jMenu17;
	}

	/**
	 * This method initializes jMenuItem38	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem38() {
		if (jMenuItem38 == null) {
			jMenuItem38 = new JMenuItem();
			jMenuItem38.setText("respons�veis");
		}
		return jMenuItem38;
	}

	/**
	 * This method initializes jMenuItem39	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem39() {
		if (jMenuItem39 == null) {
			jMenuItem39 = new JMenuItem();
			jMenuItem39.setText("transfer�ncia de respons�veis");
		}
		return jMenuItem39;
	}

	/**
	 * This method initializes jMenu18	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getJMenu18() {
		if (jMenu18 == null) {
			jMenu18 = new JMenu();
			jMenu18.setText("CADASTRO");
			jMenu18.add(getJMenuItem41());
			jMenu18.add(getJMenuItem42());
			jMenu18.add(getJMenuItem43());
		}
		return jMenu18;
	}

	/**
	 * This method initializes jMenuItem40	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem40() {
		if (jMenuItem40 == null) {
			jMenuItem40 = new JMenuItem();
			jMenuItem40.setText("movimenta��o");
		}
		return jMenuItem40;
	}

	/**
	 * This method initializes jMenuItem41	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem41() {
		if (jMenuItem41 == null) {
			jMenuItem41 = new JMenuItem();
			jMenuItem41.setText("patrim�nio");
		}
		return jMenuItem41;
	}

	/**
	 * This method initializes jMenuItem42	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem42() {
		if (jMenuItem42 == null) {
			jMenuItem42 = new JMenuItem();
			jMenuItem42.setText("nome");
		}
		return jMenuItem42;
	}

	/**
	 * This method initializes jMenuItem43	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem43() {
		if (jMenuItem43 == null) {
			jMenuItem43 = new JMenuItem();
			jMenuItem43.setText("grupo");
		}
		return jMenuItem43;
	}

	/**
	 * This method initializes jMenu19	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getJMenu19() {
		if (jMenu19 == null) {
			jMenu19 = new JMenu();
			jMenu19.setText("RELAT�RIO");
			jMenu19.add(getJMenuItem44());
			jMenu19.add(getJMenuItem45());
			jMenu19.add(getJMenuItem46());
			jMenu19.add(getJMenuItem47());
		}
		return jMenu19;
	}

	/**
	 * This method initializes jMenuItem44	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem44() {
		if (jMenuItem44 == null) {
			jMenuItem44 = new JMenuItem();
			jMenuItem44.setText("n� patrim�nio");
		}
		return jMenuItem44;
	}

	/**
	 * This method initializes jMenuItem45	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem45() {
		if (jMenuItem45 == null) {
			jMenuItem45 = new JMenuItem();
			jMenuItem45.setText("nm patrim�nio");
		}
		return jMenuItem45;
	}

	/**
	 * This method initializes jMenuItem46	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem46() {
		if (jMenuItem46 == null) {
			jMenuItem46 = new JMenuItem();
			jMenuItem46.setText("unidade/n� patrim�nio");
		}
		return jMenuItem46;
	}

	/**
	 * This method initializes jMenuItem47	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem47() {
		if (jMenuItem47 == null) {
			jMenuItem47 = new JMenuItem();
			jMenuItem47.setText("nm s/ n� patrimonial");
		}
		return jMenuItem47;
	}

	/**
	 * This method initializes jMenuItem48	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem48() {
		if (jMenuItem48 == null) {
			jMenuItem48 = new JMenuItem();
			jMenuItem48.setText("consulta");
		}
		return jMenuItem48;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
         sema sema=new sema ();
         sema.setVisible(true);
         
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(1025, 800);
		this.setJMenuBar(getJJMenuBar());
		this.setContentPane(getJContentPane());
		this.setTitle("SM11 - PAISAGISMO");
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
		}
		return jContentPane;
	}

}