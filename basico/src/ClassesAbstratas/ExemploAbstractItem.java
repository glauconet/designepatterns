package ClassesAbstratas;
//� Definindo uma classe abstrata 
import java.util.Vector;
public abstract class ExemploAbstractItem {
	private int ISBN;
	private String titulo;
	private String nomeResp;
	private int dtLancamento;
	private int dtEmprestimo;
	private int dtDevolucao;
	private String situacaoItem; // e - emprestado, l - livre	
	Vector <String> leitura = new Vector<String>();
	public  abstract void emprestar ();	
	public abstract void retornar ();
	public abstract boolean gravar();
	public abstract int recuperar(); 
	public void imprimir (){
			System.out.println();
			System.out.println("***********************************");
			System.out.println("ISBN: " + getISBN());
			System.out.println("Titulo: " + getTitulo());
			System.out.println("Nome respons�vel: " + getNomeResp());
			System.out.println("Data lan�amento: " + getDtLancamento());
			System.out.println("Data empr�stimo: " + getDtEmprestimo());
			System.out.println("Data devolu��o: " + getDtDevolucao());
			System.out.print("Situa��o atual da fita para empr�stimo: ");
			if (getSituacaoItem().equals("l")) {
				System.out.println("Livre");
			} else {
				System.out.println("Emprestada");
			}
		}
	public Vector<String> getLeitura() {
			return this.leitura;
	}
	public void setLeitura(Vector<String> leitura) {
			this.leitura = leitura;
	}
	public int getISBN() {
			return this.ISBN;
	}
	public void setISBN(int isbn) {
			this.ISBN = isbn;
	}
	public int getDtDevolucao() {
			return this.dtDevolucao;
	}
	public void setDtDevolucao(int dtDevolucao) {
			this.dtDevolucao = dtDevolucao;
	}
	public int getDtEmprestimo() {
			return this.dtEmprestimo;
	}
	public void setDtEmprestimo(int dtEmprestimo) {
			this.dtEmprestimo = dtEmprestimo;
	}
	public int getDtLancamento() {
			return this.dtLancamento;
	}
	public void setDtLancamento(int dtLancamento) {
			this.dtLancamento = dtLancamento;
	}
	public String getNomeResp() {
			return this.nomeResp;
		}
	public void setNomeResp(String nomeResp) {
			this.nomeResp = nomeResp;
	}
	public String getSituacaoItem() {
			return this.situacaoItem;
	}
	public void setSituacaoItem(String situacaoItem) {
			this.situacaoItem = situacaoItem;
	}
	public String getTitulo() {
			return this.titulo;
	}
	public void setTitulo(String titulo) {
			this.titulo = titulo;
	} 
}
