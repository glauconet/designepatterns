package ClassesAbstratas;

//� Usando os conceitos de heran�a e classe abstrata 
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ExemploFita extends ExemploAbstractItem {
	private int dtVencFita;

	/*
	 * Niveis para empr�stimo: 1 - funcionario terceirizado, 2 - Alunos, 3 -
	 * professores 4 - secretaria, 5 - diretores
	 */
	private int nivelEmprestimo;

	private String descFabricante;

	@Override
	public void emprestar() {
		// l - livre
		if (getSituacaoItem().equals("l")) {
			if (getNivelEmprestimo() > 3) {
				setSituacaoItem("e");// emprestado
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				setDtEmprestimo(Integer.parseInt(sdf.format(new Date())));
				setDtDevolucao(0);
			} else {
				System.out
						.println("Fita esta livre, por�m seu nivel � insuficiente para emprest�-la. Emprestimo n�o concedido.");
			}
		} else {
			System.out.println("Fita j� emprestada");
		}
	}

	@Override
	public void retornar() {
		// e - emprestado
		if (getSituacaoItem().equals("e")) {
			if (getNivelEmprestimo() > 3) {
				setSituacaoItem("l");// livre
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				setDtEmprestimo(0);
				setDtDevolucao(Integer.parseInt(sdf.format(new Date())));
			} else {
				System.out.println("Seu nivel � insuficiente");
			}
		} else {
			System.out.println("A fita n�o esta emprestada");
		}
	}

	@Override
	public void imprimir() {
		super.imprimir();
		System.out.println("Data de vencimento: " + getDtVencFita());
		System.out.println("Nivel para empr�stimo: " + getNivelEmprestimo());
		System.out.println("Descri��o do fabricante: " + getDescFabricante());
		System.out.println("***********************************");
	}

	@Override
	public boolean gravar() {
		FileWriter tArq1;
		PrintWriter tArq2;
		try {
			// Opera��o I - Abrir o aquivo
			tArq1 = new FileWriter(getISBN() + "F.txt");
			tArq2 = new PrintWriter(tArq1);
			tArq2.println(getISBN());
			tArq2.println(getTitulo());
			tArq2.println(getNomeResp());
			tArq2.println(getDtLancamento());
			tArq2.println(getDtVencFita());
			tArq2.println(getSituacaoItem()); // e - emprestado, l - livre
			tArq2.println(getDescFabricante());
			tArq2.println(getDtEmprestimo());
			tArq2.println(getDtDevolucao());
			tArq2.println(getNivelEmprestimo());
			// Opera��o II - Fechar o arquivo
			tArq2.close();
			return true;
		} catch (IOException tExcept) {
			tExcept.printStackTrace();
			return false;
		}
	}

	@Override
	public int recuperar() {
		FileReader tArq1;
		BufferedReader tArq2;
		try {
			if (new File(getISBN() + "F.txt").exists()) {
				// Opera��o I - Abrir o arquivo
				tArq1 = new FileReader(getISBN() + "F.txt");
				tArq2 = new BufferedReader(tArq1);
				// Opera��o II - Ler atributo/valor e colocar na matriz
				String temp;
				while ((temp = tArq2.readLine()) != null) {
					this.leitura.add(temp);
				}
				setISBN(Integer.parseInt(this.leitura.get(0)));
				setTitulo(this.leitura.get(1));
				setNomeResp(this.leitura.get(2));
				setDtLancamento(Integer.parseInt(this.leitura.get(3)));
				setDtVencFita(Integer.parseInt(this.leitura.get(4)));
				setSituacaoItem(this.leitura.get(5));
				setDescFabricante(this.leitura.get(6));
				setDtEmprestimo(Integer.parseInt(this.leitura.get(7)));
				setDtDevolucao(Integer.parseInt(this.leitura.get(8)));
				setNivelEmprestimo(Integer.parseInt(this.leitura.get(9)));
				// Opera��o III - Fechar o arquivo
				tArq2.close();
			} else {
				System.out.println("Arquivo n�o existe");
				return 0;
			}
		} catch (IOException tExcept) {
			tExcept.printStackTrace();
		}
		return 1;
	}

	public static ExemploFita getInstance() {
		return new ExemploFita();
	}

	public String getDescFabricante() {
		return this.descFabricante;
	}

	public void setDescFabricante(String descFabricante) {
		this.descFabricante = descFabricante;
	}

	public int getDtVencFita() {
		return this.dtVencFita;
	}

	public void setDtVencFita(int dtVencFita) {
		this.dtVencFita = dtVencFita;
	}

	public int getNivelEmprestimo() {
		return this.nivelEmprestimo;
	}

	public void setNivelEmprestimo(int nivelEmprestimo) {
		this.nivelEmprestimo = nivelEmprestimo;
	}

	public ExemploFita() {
		super();
	}
}
