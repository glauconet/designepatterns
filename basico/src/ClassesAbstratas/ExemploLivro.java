package ClassesAbstratas;
//� Usando os conceitos de heran�a e classe abstrata 
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
public class ExemploLivro extends ExemploAbstractItem {
	private String statusBloqueio; // b - bloqueado, l - livre
	public ExemploLivro() {
			super();
	}
	public void emprestar() {
			// l - livre
			if (getSituacaoItem().equals("l")) {
				// l - livre
				// Somente um livro ou periodico podem ser bloqueados.
				if (getStatusBloqueio().equals("l")) {
					setSituacaoItem("e");// emprestado
					SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
					setDtEmprestimo(Integer.parseInt(sdf.format(new Date())));
					setDtDevolucao(0);
				} else {
					System.out.println("Item disponivel, mas bloqueado");
				}
			} else {
				System.out.println("Item j� emprestado");
			}
		}
	public void retornar() {
			// e - emprestado
			if (getSituacaoItem().equals("e")) {
				// l - livre
				setSituacaoItem("l");// livre
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				setDtEmprestimo(0);
				setDtDevolucao(Integer.parseInt(sdf.format(new Date())));
			} else {
				System.out
						.println("Item n�o pode ser retornado, pois n�o foi emprestado");
			}
		}
	public void bloquearItem() {
			setStatusBloqueio("b");
		}
	public void desbloquearItem() {
			setStatusBloqueio("l");
	}
	public void imprimir() {
			super.imprimir();
			System.out.print("Status do bloqueio: ");
			if (getStatusBloqueio().equals("l")) {
				System.out.println("Livre");
			} else {
				System.out.println("Bloqueado");
			}
			System.out.println("***********************************");
			System.out.println();
		}
		public boolean gravar() {
			FileWriter tArq1;
			PrintWriter tArq2;
			try {
				// Opera��o I - Abrir o aquivo
				tArq1 = new FileWriter(getISBN() + "L.txt");
				tArq2 = new PrintWriter(tArq1);
				tArq2.println(getISBN());
				tArq2.println(getStatusBloqueio());
				tArq2.println(getTitulo());
				tArq2.println(getNomeResp());
				tArq2.println(getSituacaoItem()); // e - emprestado, l - livre
				tArq2.println(getDtEmprestimo());
				tArq2.println(getDtDevolucao());
				tArq2.println(getDtLancamento());
				// Opera��o II - Fechar o arquivo
				tArq2.close();
				return true;
			} catch (IOException tExcept) {
				tExcept.printStackTrace();
				return false;
			}
		}
	public int recuperar() {
			FileReader tArq1;
			BufferedReader tArq2;
			try {
				if (new File(getISBN() + "L.txt").exists()) {
					// Opera��o I - Abrir o arquivo
					tArq1 = new FileReader(getISBN() + "L.txt");
					tArq2 = new BufferedReader(tArq1);
					// Opera��o II - Ler atributo/valor e colocar na matriz
					String temp;
					while ((temp = tArq2.readLine()) != null) {
						this.leitura.add(temp);
					}
					setISBN(Integer.parseInt(this.leitura.get(0)));
					setStatusBloqueio(this.leitura.get(1));
					setTitulo(this.leitura.get(2));
					setNomeResp(this.leitura.get(3));
					setSituacaoItem(this.leitura.get(4));
					setDtEmprestimo(Integer.parseInt(this.leitura.get(5)));
					setDtDevolucao(Integer.parseInt(this.leitura.get(6)));
					setDtLancamento(Integer.parseInt(this.leitura.get(7)));
					// Opera��o III - Fechar o arquivo
					tArq2.close();
				} else {
					System.out.println("Arquivo n�o existe");
					return 0;
				}
			} catch (IOException tExcept) {
				tExcept.printStackTrace();
			}
			return 1;
		}
	public static ExemploLivro getInstance() {
			return new ExemploLivro();
		}
		public String getStatusBloqueio() {
			return this.statusBloqueio;
	}
		public void setStatusBloqueio(String statusBloqueio) {
			this.statusBloqueio = statusBloqueio;
		}
}
