package ClassesAbstratas;
//� Usando os conceitos de heran�a e classe abstrata 
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
public class ExemploPeriodico extends ExemploLivro {
	private int qdadeArtigos;
	@Override
	public void imprimir() {
			super.imprimir();
			System.out.println("Quantidade de artigos: " + getQdadeArtigos());
			System.out.println("***********************************");
			System.out.println();
	}
	@Override
	public boolean gravar() {
			FileWriter tArq1;
			PrintWriter tArq2;
			try {
				// Opera��o I - Abrir o aquivo
				super.gravar();
				tArq1 = new FileWriter(getISBN() + "P.txt");
				tArq2 = new PrintWriter(tArq1);
				tArq2.println(getQdadeArtigos());
				// Opera��o II - Fechar o arquivo
				tArq2.close();
				return true;
			} catch (IOException tExcept) {
				tExcept.printStackTrace();
				return false;
			}
	}
	@Override
	public int recuperar() {
			FileReader tArq1;
			BufferedReader tArq2;
			try {
				if (new File(getISBN() + "F.txt").exists()) {
				super.recuperar();
					// Opera��o I - Abrir o arquivo
					tArq1 = new FileReader(getISBN() + "P.txt");
					tArq2 = new BufferedReader(tArq1);
					// Opera��o II - Ler atributo/valor e colocar na matriz
					String temp;
					while ((temp = tArq2.readLine()) != null) {
						this.leitura.add(temp);
					}
					/*
					 * Aten��o: Devido a estarmos utilizando o mesmo atributo este j� foi alterado pelo m�todo recuperar da classe m�e. Neste
					 * momento quando executamos a opera��o add estaremos adicionando um valor a oitava posi��o do vetor.
					 */
					setQdadeArtigos(Integer.parseInt(this.leitura.get(8)));
					// Opera��o III - Fechar o arquivo
					tArq2.close();
				} else {
					System.out.println("Arquivo n�o existe");
					return 0;
				}
			} catch (IOException tExcept) {
				tExcept.printStackTrace();
			}
			return 1;
		}
	public static ExemploPeriodico getInstance() {
			return new ExemploPeriodico();
		}
	public int getQdadeArtigos() {
			return this.qdadeArtigos;
	}
	public void setQdadeArtigos(int qdadeArtigos) {
			this.qdadeArtigos = qdadeArtigos;
	}
}
