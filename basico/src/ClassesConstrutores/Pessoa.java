
				//CLASSE COM EXEMPLOS DE CONSTRUTORES 

package ClassesConstrutores; //

public class Pessoa {
		
		protected String nome; 	// atributos do objeto
		protected int idade;		// atributos do objeto
		private double estatura;	// atributos do objeto
		private double peso;		// atributos do objeto
		private char sexo;		// atributos do objeto
		public String email;
		
		
		public Endereco endereco;	
		public Telefone telefone;
		
		// o construtor tem sempre o nome da classe
		public Pessoa(){ // construtor que nao recebe nada 
		}
		//sobregarca de construtor
		public Pessoa(String nome){ // construtor que recebe o nome 
		this.nome = nome;// o this evita que haja confusao entre o atributi da classe e o nome da variavel de passagem
		}
		//sobregarca de construtor
		public Pessoa(String n, int i, char s){ // construtor recebe nome, idade e sexo 
			nome = n;
			idade = i;
			sexo = s;
		}
		//sobregarca de construtor
		public Pessoa(String n, int i, double e, double p,  char s){ // construtor passa nome, idade e sexo 
			nome = n;
			idade = i;
			peso = p;
			estatura = e;
			sexo = s;
			
		}	
		
		public void setNome(String n){
			nome = n ;
		}
		
		public String  getNome(){
			return nome; 
		}
		
		public void setIdade(int i){
			idade = i ;
		}
		
		public void setIdade(String i){// trata-se de uma sobregarca normal
			idade = Integer.parseInt(i) ;
		}
		
		public int getIdade(){
			return idade;
		}
		
		
		public void setSexo(char s){
			sexo = s ;
		}
		
		public char getSexo(){
			return sexo;
		}

		public double getEstatura() {
			return estatura;
		}

		public void setEstatura(double estatura) {
			this.estatura = estatura;
		}

		public double getPeso() {
			return peso;
		}

		public void setPeso(double peso) {
			this.peso = peso;
		}
		
} 


