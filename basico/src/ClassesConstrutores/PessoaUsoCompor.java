package ClassesConstrutores;

		//CONSTRUTORES

// lembre-se de quando voc� criar uma classe com mais de um construtor,
// voc� pode chamar o construtor que quiser

public class PessoaUsoCompor {

	public static void main(String[] args) {
		
		Pessoa p1 = new Pessoa();
		Pessoa p2 = new Pessoa("S�rgio");
		Pessoa p3 = new Pessoa("Daniele", 22, 'F');
		Pessoa p4 = new Pessoa("Adilson", 22,1.70, 68, 'M');
		
				
			
		
		p1.setNome("Oberdan");
		p1.setIdade(24);
		p1.setSexo('M');
		p1.setEstatura(1.6);
		p1.setPeso(70.5);
		p1.endereco = new Endereco(); // // isso e composicao
		p1.endereco.setLogradouro(" Rua Bingo");
		p1.endereco.setNumero(1);
		
		p2.setIdade(33);
		p2.setSexo('F');
		p2.setEstatura(1.5);
		p2.setPeso(55.5);
		
		p3.setEstatura(1.65);
		p3.setPeso(55.5);
		
		System.out.println("_____TABELA DE PESSOAS_______ ");
		System.out.println("________________________ ");
		System.out.println("Nome     	" + p1.getNome());
		System.out.println("Idade    	" + p1.getIdade());
		System.out.println("Sexo     	" + p1.getSexo());
		System.out.println("Estatura 	" + p1.getEstatura());
		System.out.println("Peso     	" + p1.getPeso());
		System.out.println("Endereco	" + p1.endereco.getLogradouro() + " n� " + p1.endereco.getNumero());
		System.out.println("________________________");
		System.out.println("Nome     	" + p2.getNome());
		System.out.println("Idade    	" + p2.getIdade());
		System.out.println("Sexo     	" + p2.getSexo());
		System.out.println("Estatura 	" + p2.getEstatura());
		System.out.println("Peso     	" + p2.getPeso());
		System.out.println("________________________ ");
		System.out.println("Nome     	" + p3.getNome());
		System.out.println("Idade    	" + p3.getIdade());
		System.out.println("Sexo     	" + p3.getSexo());
		System.out.println("Estatura 	" + p3.getEstatura());
		System.out.println("Peso     	" + p3.getPeso());
		System.out.println("________________________ ");
		System.out.println("Nome     	" + p4.getNome());
		System.out.println("Idade    	" + p4.getIdade());
		System.out.println("Sexo     	" + p4.getSexo());
		System.out.println("Estatura 	" + p4.getEstatura());
		System.out.println("Peso     	" + p4.getPeso());
	}

}
