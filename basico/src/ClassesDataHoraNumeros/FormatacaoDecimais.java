package ClassesDataHoraNumeros;
import static java.lang.System.out;
import java.text.DecimalFormat;

public class FormatacaoDecimais {
  public static void main(String[] args) {
    double numero = 1234567.123456;
    out.println("N�mero original:\t" + numero);
    
    DecimalFormat df = new DecimalFormat("#,##0.0000");
    out.println("N�mero Formatado:\t" + df.format(numero));
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/