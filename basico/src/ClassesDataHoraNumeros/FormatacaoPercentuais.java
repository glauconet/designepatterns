package ClassesDataHoraNumeros;
import static java.lang.System.out;
import java.text.NumberFormat;

public class FormatacaoPercentuais {
  public static void main(String[] args) {
    double percentual = 0.25;
    out.println("N�mero original:\t" + percentual);
    
    NumberFormat nf = NumberFormat.getPercentInstance();
    System.out.println("Forma percentual:\t" + nf.format(percentual));
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/