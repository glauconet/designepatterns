package ClassesDataHoraNumeros;
import static java.lang.System.out;
import java.util.TimeZone;

public class FusosDisponiveis {
  public static void main(String[] args) {
    String[] fusos = TimeZone.getAvailableIDs( -2 * 60 * 60 * 1000);
    
    out.println( "Fusos dispon�veis para GMT -02:00" );
    for (int i = 0; i < fusos.length; i++)
      out.println( i+1 + ")\t" + fusos[i] );
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/