package ClassesDataHoraNumeros;
import java.util.*;
public class I18NSample {

    static public void main(String[] args) {
      // String language;
      //  String country;
      //if (args.length != 2) {  language = new String("fr"); country = new String("FR");// se currentLocale receber as strings language, country ou ainda via argumento
      // } else {  language = new String(args[0]);  country = new String(args[1]);     }
    	ResourceBundle messages;
        Locale aLocale = new Locale("en","US");
        Locale  frLocale = new Locale("fr","FR");
        Locale  deLocale = new Locale("de","DE");
        Locale currentLocale = deLocale;// fois passado  AmericanLocale, mas poderia ser outro
      //currentLocale = new Locale(language, country); se currentLocale receber as strings language e country
      // currentLocale = new Locale("","");//  se currentLocale receber vazio
      //System.out.print(currentLocale); System.out.println("");
        messages = ResourceBundle.getBundle("MessagesBundle",currentLocale);
      //messages = ResourceBundle.getBundle("MessagesBundle",deLocale);
        System.out.println(messages.getString("greetings"));
        System.out.println(messages.getString("inquiry"));
        System.out.println(messages.getString("farewell"));
    }
}
