package ClassesDataHoraNumeros;
import static java.lang.System.out;
import java.util.Locale;

public class LocalEscolhido {
  public static void main(String[] args) {
    Locale local = new Locale("it","IT");
    
    out.println( "Local escolhido: " + local.getDisplayName() );
    out.print("Idioma: " +  local.getDisplayLanguage() );
    out.print(" - " +  local.getLanguage() );
    out.println(" - " +  local.getISO3Language() );
    out.print("Pa�s: " + local.getDisplayCountry() );
    out.print(" - " + local.getCountry() );
    out.println(" - " + local.getISO3Country() );
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/