package ClassesDataHoraNumeros;
import static java.lang.System.out;
import java.util.Locale;

public class LocalPadrao {
  public static void main(String[] args) {
	  
    Locale local = Locale.getDefault();
    
    out.println( "Local padrão: " + local.getDisplayName() );
    out.print("Idioma: " +  local.getDisplayLanguage() );
    out.print(" - " +  local.getLanguage() );
    out.println(" - " +  local.getISO3Language() );
    out.print("País: " + local.getDisplayCountry() );
    out.print(" - " + local.getCountry() );
    out.println(" - " + local.getISO3Country() );
  }
}

/********************************************************************
 *                                                                  *
 ********************************************************************/