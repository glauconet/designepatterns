package ClassesDataHoraNumeros;
import static java.lang.System.out;
import java.util.Locale;

public class PaisesDisponiveis {
  public static void main(String[] args) {
    String[] paises = Locale.getISOCountries();
    
    out.println( "Pa�ses dispon�veis:" );
    for (int i = 1; i <= paises.length; i++) {
      out.print( paises[i-1] + " " );
      if (i % 25 == 0) out.println(); 
    }
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/