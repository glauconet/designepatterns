

		//Exemplo de Construtor
// lembre-se de quando voc� criar uma classe com
//mais de um construtor, voc� pode chama o
//construtor que quiser

package ClassesDecomporAgregar;

import ClassesDecomporAgregar.Motor;

	
	public class Carro{
		
	private String modelo; // String � uma classe
	
	public Motor motor; // composi��o
	
		

	public Carro(String m){// construtor
		modelo = m;
		motor = new Motor();	
	}
	
	public Carro(float p){// construtor
		motor = new Motor(p);
	}	
		
	public String getModelo(){
		return modelo;
	}
	
	public double getPotencia(){
		
		return motor.getPotencia();// chamando um m�todo de outro objeto
	}
	
	
	public void setModelo(String m){
		
		modelo = m ;
	}
	
	
} 



