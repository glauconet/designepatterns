package ClassesEncapsulamento;
public class Assunto {
  private int codigo;
  private String descricao;
  
  public Assunto(int codigo, String descricao) {
    this.codigo = codigo;
    this.descricao = descricao;
  }
  
  public String toString() {
    return "Assunto " + codigo + ": " + descricao;
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/