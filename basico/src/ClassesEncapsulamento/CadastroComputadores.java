package ClassesEncapsulamento;
import javax.swing.JOptionPane;

import ClassesEncapsulamento.Computador;

public class CadastroComputadores {
  public static void main(String[] args) {
    Computador[] registros = new Computador[100];
    JOptionPane d = new JOptionPane();
    
    for (int indice = 0; indice < registros.length; indice++) {
      Computador comp = null;
      int codigo = 0;
      String str, descricao;
      String nr = "Computador " + (indice + 1)+ ": ";
      
      while (true) {
        str = d.showInputDialog(nr + "c�digo");
        if (str == null) break;
        
        try {
          codigo = Integer.parseInt(str);
        }
        catch(NumberFormatException nfe) {
          d.showMessageDialog(null,"C�digo inv�lido!","Erro",0);
          continue;
        }
        
        str = d.showInputDialog(nr + "descri��o");
        if (str == null) break;
        descricao = str;
        
        try {
          comp = new Computador(codigo, descricao);
          break;
        }
        catch(Exception ex) {
          d.showMessageDialog(null,ex.getMessage(),"Erro",0);
        }
      }
      if (str == null) break;
      
      registros[indice] = comp;
    }
    
    String relatorio = "Relat�rio de computadores:";
    for (Computador comp : registros) {
      if (comp == null) break;
      relatorio += "\n" + comp;
    }
    
    d.showMessageDialog(null,relatorio);
    System.exit(0);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/