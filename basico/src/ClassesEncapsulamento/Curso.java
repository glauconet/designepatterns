package ClassesEncapsulamento;
public class Curso {
  private int codigo;
  private String descricao;
  
  public Curso() {
    descricao = "";
  }
  
  public int getCodigo() {
    return codigo;
  }
  
  public String getDescricao() {
    return descricao;
  }
  
  public void setCodigo(int codigo) {
    this.codigo = codigo;
  }
  
  public void setDescricao(String descricao) {
    this.descricao = descricao;
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/