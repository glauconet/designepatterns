package ClassesEncapsulamento;
import javax.swing.JOptionPane;

public class Dialogo {
  public static String captar(String mensagem) {
    String str = JOptionPane.showInputDialog(null,mensagem);
    return str;
  }
  
  public static void exibir(String texto) {
    JOptionPane.showMessageDialog(null,texto,"Mensagem",1);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/