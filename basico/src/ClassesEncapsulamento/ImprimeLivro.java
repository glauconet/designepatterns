package ClassesEncapsulamento;
import javax.swing.JOptionPane;

import ClassesEncapsulamento.Livro;

public class ImprimeLivro {
  public static void main(String[] args) {
    Livro brain = new Livro();
    brain.setCodigo(1);
    brain.setTitulo("Brainstorms");
    JOptionPane.showMessageDialog(null,brain);
    
    Livro tripla_corda = new Livro();
    tripla_corda.setCodigo(2);
    tripla_corda.setTitulo("A tripla corda: mente, corpo e mundo");
    JOptionPane.showMessageDialog(null,tripla_corda);
    
    System.exit(0);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/