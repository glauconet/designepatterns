package ClassesEncapsulamento;
import javax.swing.JOptionPane;

import ClassesEncapsulamento.Conta;

public class OperadorConta {
  public static void main(String[] args) {
    JOptionPane d = new JOptionPane();
    Conta conta = null;
    String str;
    
    while (true) {
      str = "Informe o n�mero da conta";
      str = d.showInputDialog(str);
      if (str == null) System.exit(0);
      
      try {
        conta = new Conta(str);
        break;
      }
      catch(Exception ex) {
        d.showMessageDialog(null,"N�mero inv�lido!","Erro",0);
      }
    }
    
    while (true) {
      str = "Informe o limite da conta";
      str = d.showInputDialog(str);
      if (str == null) System.exit(0);
      
      try {
        conta.setLimite(str);
        break;
      }
      catch(Exception ex) {
        d.showMessageDialog(null,ex.getMessage(),"Erro",0);
      }
    }
    
    while (true) {
      str = "Que opera��o deseja realizar?" +
        "\nD = Dep�sito \nS = Saque";
      
      str = d.showInputDialog(str);
      if (str == null) System.exit(0);
      String operacao = str.trim().toUpperCase();
      
      if (!operacao.equals("D") && !operacao.equals("S")) {
        d.showMessageDialog(null,"Opera��o inv�lida!","Erro",0);
        continue;
      }
      
      str = "Informe o valor da opera��o";
      str = d.showInputDialog(str);
      if (str == null) System.exit(0);
        
      double valor = Double.parseDouble(str);
      
      if (operacao.equals("D")) conta.depositar(valor);
      
      if (operacao.equals("S"))
        try {
          conta.sacar(valor);
        }
        catch(Exception ex) {
          d.showMessageDialog(null,ex.getMessage(),"Erro",0);
        }
      
      d.showMessageDialog(null,conta);
    }
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/