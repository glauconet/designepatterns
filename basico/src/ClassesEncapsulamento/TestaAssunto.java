package ClassesEncapsulamento;
import javax.swing.JOptionPane;

import ClassesEncapsulamento.Assunto;

public class TestaAssunto {
  public static void main(String[] args) {
    Assunto alg = new Assunto(1,"Algoritmos");
    JOptionPane.showMessageDialog(null,alg);
    
    Assunto ed = new Assunto(2,"Estrutura de dados");
    JOptionPane.showMessageDialog(null,ed);
    
    System.exit(0);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/