package ClassesEncapsulamento;
import javax.swing.JOptionPane;

import ClassesEncapsulamento.Curso;

public class TestaCurso {
  public static void main(String[] args) {
    Curso cur = new Curso();
    
    String str = "Dados do curso: ";
    str += "\n" + cur.getCodigo() + ": " + cur.getDescricao();
    JOptionPane.showMessageDialog(null,str);
    
    cur.setCodigo( 1 );
    cur.setDescricao( "Sistemas de Informa��o" );
    
    str = "Dados do curso: ";
    str += "\n" + cur.getCodigo() + ": " + cur.getDescricao();
    JOptionPane.showMessageDialog(null,str);
    
    cur.setCodigo( -15 );
    cur.setDescricao( null );
    
    str = "Dados do curso: ";
    str += "\n" + cur.getCodigo() + ": " + cur.getDescricao();
    JOptionPane.showMessageDialog(null,str);
    
    System.exit(0);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/