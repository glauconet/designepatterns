package ClassesHerancaPolimorfismo;
public class CadastroCaminhao{
  public static void main(String[] args) {
    Caminhao caminhao = new Caminhao("ZAB-4613",2008,2);
    caminhao.exibirDados();
    System.exit(0);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/