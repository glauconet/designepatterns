package ClassesHerancaPolimorfismo;
public class CadastroOnibus {
  public static void main(String[] args) {
    Onibus onibus = new Onibus("ZAB-4613",2009,44);
    onibus.exibirDados();
    onibus.setPlaca("EDS-7898");
    onibus.setAno("2005");
    onibus.setAssentos(46);
    onibus.exibirDados();
    System.exit(0);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/