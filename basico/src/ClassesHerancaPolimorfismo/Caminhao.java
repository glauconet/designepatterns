package ClassesHerancaPolimorfismo;
import javax.swing.JOptionPane;

public class Caminhao extends Veiculo {
  private int eixos;
  
  public Caminhao(String placa, int ano, int eixos) {
    super(placa,ano);
    this.eixos = eixos;
  }
  
  public int getEixos() {
    return eixos;
  }
  
  public void setEixos(int eixos) {
    this.eixos = eixos;
  }
  
  public void exibirDados() {
    super.exibirDados();
    String str = "N�mero de eixos: " + eixos;
    JOptionPane.showMessageDialog(null,str,"Mensagem",1);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/