package ClassesHerancaPolimorfismo;
import javax.swing.JOptionPane;

public final class Caminhao2 extends Veiculo2 {
  private int eixos;
  
  public Caminhao2(String placa, int ano, int eixos) {
    super.setPlaca(placa);
    super.setAno(ano);
    this.eixos = eixos;
  }
  
  public int getEixos() {
    return eixos;
  }
  
  public void setEixos(int eixos) {
    this.eixos = eixos;
  }
  
  public void exibirDados() {
    String str = "Dados do �nibus:" +
      "\n\nPlaca: " + getPlaca() +
      "\nAno: " + getAno() +
      "\nEixos: " + eixos;
    JOptionPane.showMessageDialog(null,str,"Mensagem",1);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/