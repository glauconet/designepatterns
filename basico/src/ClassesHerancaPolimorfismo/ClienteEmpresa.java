package ClassesHerancaPolimorfismo;
public class ClienteEmpresa extends Cliente {
  private String IE;
  private String CNPJ;
  
  public ClienteEmpresa() {
    this("","","","");
  }
  
  public ClienteEmpresa(String nome, String fone, String IE,
    String CNPJ) {
    super(nome,fone);
    this.IE = IE;
    this.CNPJ = CNPJ;
  }
  
  public String getIE() {
    return IE;
  }
  
  public String getCNPJ() {
    return CNPJ;
  }
  
  public void setIE(String IE) {
    this.IE = IE;
  }
  
  public void setCNPJ(String CNPJ) {
    this.CNPJ = CNPJ;
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/