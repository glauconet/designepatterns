package ClassesHerancaPolimorfismo;
import javax.swing.JOptionPane;

public class ConsultaClasse{
  public static void main(String[] args) {
    Veiculo v1 = new Onibus("AAA-3388",2009,46);
    Veiculo v2 = new Caminhao("BBB-3498",2009,2);
    
    JOptionPane.showMessageDialog(null, v1.getClass().getName());
    JOptionPane.showMessageDialog(null, v2.getClass().getName());
    System.exit(0);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/