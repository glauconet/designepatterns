package ClassesHerancaPolimorfismo;
import javax.swing.JOptionPane;

public class ConversaoObjeto{
  public static void main(String[] args) {
    Veiculo v1 = new Onibus("AAA-3388",2009,46);
    Onibus bus = (Onibus)v1;
    
    String str = "Dados do �nibus: " +
      "\nPlaca: " + v1.getPlaca() +
      "\nAno: " + v1.getAno() +
      "\nAssentos: " + bus.getAssentos();
    JOptionPane.showMessageDialog(null, str);
    
    System.exit(0);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/