package ClassesHerancaPolimorfismo;

// Classe base para a cria��o de objetos
public class ExemploContaCorrenteConst {
	protected int atrib1;

	protected int atrib2;

	public ExemploContaCorrenteConst() {
		System.out.println("Estou no construtor da superclasse");
	}

	public void imprimir() {
		System.out.println("param1 - " + this.atrib1);
		System.out.println("param2 - " + this.atrib2);
	}

	public ExemploContaCorrenteConst(int param1, int param2) {
		this.atrib1 = param1;
		this.atrib2 = param2;
		System.out
				.println("Estou no construtor da superclasse com 2 par�metros");
	}
}