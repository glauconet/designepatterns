package ClassesHerancaPolimorfismo;
//� Definindo a subclasse e redefinindo o m�todo sacar()
public class ExemploContaCorrenteEspecial extends ExemploContaCorrente {
	private double limite;
	public ExemploContaCorrenteEspecial(int agencia , int conta , String nome , double saldo , double limite) {
			super( agencia ,  conta ,  nome ,  saldo);
			this.limite = 1000;
	}
	// Este m�todo sofreu override
	public void sacar(double valor) {
			System.out.println("Classe ExemploContaCorrenteEspecial, metodo sacar");
			if (valor <= (this.saldo + this.limite)){
				this.saldo = this.saldo - valor;
			}
	}
	public void depositar(double valor) {
			System.out.println("Classe ExemploContaCorrenteEspecial, metodo depositar");
	}
	public void imprimir() {
			super.imprimir(); // Executa o m�todo imprimir da superclasse
			System.out.println("Metodo imprimir da classe ExemploContaCorrenteEspecial");
			System.out.println("Limite: " + this.limite);
		}
}
