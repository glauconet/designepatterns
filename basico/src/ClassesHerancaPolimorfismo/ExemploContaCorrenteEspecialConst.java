package ClassesHerancaPolimorfismo;

// Usando o comando super
public class ExemploContaCorrenteEspecialConst extends
		ExemploContaCorrenteConst {
	private int novoParam;

	public ExemploContaCorrenteEspecialConst() {
		// executando o construtor vazio da superclasse
		super();
		System.out.println("Estou no construtor da subclasse");
	}

	public void imprimir() {
		// executando o m�todo imprimir da superclasse
		super.imprimir();

		System.out.println("param3 - " + this.novoParam);
	}

	public ExemploContaCorrenteEspecialConst(int param1, int param2,
			int terceiroParam) {
		// executando o construtor da superclasse com 2 par�metros
		super(param1, param2);
		this.novoParam = terceiroParam;
		System.out.println("Estou no construtor da subclasse com 3 par�metros");
		imprimir();
	}
}