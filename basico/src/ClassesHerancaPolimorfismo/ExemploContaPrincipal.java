package ClassesHerancaPolimorfismo;

// Classe que cont�m o m�todo main e utiliza as classes para apresentar o uso de heran�a
public class ExemploContaPrincipal {
	public static void main(String[] args) {
		// criando um objeto no formato tradicional
		ExemploContaCorrente tConta = new ExemploContaCorrente(10, 20,
				"Gleibe Zanetti", 100);
		tConta.sacar(10.0);
		// criando um objeto no formato tradicional
		ExemploContaCorrenteEspecial tContaEsp = new ExemploContaCorrenteEspecial(
				10, 20, "Carlos Xavier", 100, 900);
		tContaEsp.sacar(10.0);

		// criando apenas uma refer�ncia para ExemploContaCorrente. N�o � um
		// objeto ainda
		ExemploContaCorrente ref;
		// colocando na refer�ncia um objeto do tipo da subclasse
		ref = new ExemploContaCorrenteEspecial(10, 20, "Dennys Amauri", 100,
				900);
		ref.sacar(10.0);// ir� executar o m�todo sacar() de
						// ExemploContaCorrenteEspecial
		// executa o m�todo imprimir() da classe ExemploContaCorrenteEspecial
		tContaEsp.imprimir();
		// executa o m�todo depositar() da classe ExemploContaCorrenteEspecial
		tContaEsp.depositar(90);
		tConta.imprimir(); // executa o m�todo imprimir() da classe
							// ExemploContaCorrente
		/*
		 * esta pr�xima linha gera erro de compila��o. A classe
		 * ExemploContaCorrente n�o implementa o m�todo depositar(), e por isto
		 * nunca vai funcionar. Para que esta linha funcione ser� necess�rio
		 * criar o m�todo depositar() em ExemploContaCorrente. Isto ocorre
		 * devido ao compilador procurar na classe ExemploContaCorrente um
		 * m�todo com uma assinatura que corresponda a assinatura do m�todo
		 * depositar() utilizado neste m�todo main(). Como n�o existe o
		 * compildor aponta um erro.
		 */

		// tConta.depositar();
		/*
		 * esta pr�xima linha gera erro de compila��o tamb�m. Apesar do objeto
		 * ref ser instanciado como do tipo da classe
		 * ExemploContaCorrenteEspecial este � do tipo da classe
		 * ExemploContaCorrente. Como a classe ExemploContaCorrente n�o
		 * implementa o m�todo depositar() nem mesmo como abstrato o compilador
		 * Java ir� apresentar um erro de compila��o. Para que esta linha
		 * funcione ser� necess�rio usar downcasting, ou seja, substituir a
		 * linha existente por: ((ExemploContaCorrenteEspecial)ref).depositar();
		 */
		// ref.depositar();
	}
}