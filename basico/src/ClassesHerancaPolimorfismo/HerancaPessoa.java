package ClassesHerancaPolimorfismo;
import ClassesConstrutores.*;

public class HerancaPessoa extends Pessoa{

	public String algomais;
	
	public void meuMetodo(){
		System.out.println("Eu sou heranca de pessoa");
		System.out.println("Este e meu metodo");
	}
		
	public String  getNome(){// exemplo de sobreposicao com mais instrucao
		String s = super.getNome();
		s = "Meu nome " + s;
		return s; 
	}

	public int getIdade(){// exemplo de sobreposicao
		return idade  + 100;
	}
	
	
	public static void main(String[] args) {
		HerancaPessoa pessoa = new HerancaPessoa();
		// perceba aqui a troca aqui do qualificacao(tipo) e da instanciacao
		
		pessoa.setNome("Pessoa extendida");
		pessoa.setIdade("34"); // foi passado como string, mas usou o metodo sobregarredado
		pessoa.setSexo('M');
		pessoa.algomais = "Eu tenho algo a mais";
		System.out.println("Nome     	" + pessoa.getNome());
		System.out.println("Idade    	" + pessoa.getIdade());
		System.out.println("Sexo     	" + pessoa.getSexo());
		System.out.println(pessoa.algomais);
		pessoa.meuMetodo();
	}
}
