package ClassesHerancaPolimorfismo;

import javax.swing.JOptionPane;

public class Sobreposicao {

	
	 	public static void main(String[] args) {
		
	 		String st ;
	 		 		
	 		st =  "\n Sobreposicao";
			st = st + "\n Sobreposicao de  um metodo significa\n " +
							"implementacao de um metodo em uma SUBCLASSE\n  " +
							"de forma que anule o comportamento do metodo da superclasse.\n " +
							"\nBasta declarar o metodo na subclasse com se ela nao existisse na superclasse,\n" +
							"a escolha sera feita de modo dinamico,  com base no tipo de dado passado";
			JOptionPane.showMessageDialog(null, st);
	 		
	 		
	 		
			st =  "\n Segunda foma de Sobreposicao";
			st = st + "\n Neste caso desejamos realiza o metodo da Superclasse com \n " +
							" mais alguma coisa, devemos declarar novamente o metodo na SUBCLASSE\n, " +
							" e depois utlizar super.nomedoMetodo. e em seguida continuar com novas instrucoes\n " ;
							
			JOptionPane.showMessageDialog(null, st);
								
		}
	 // exemplo na classe HerancaPessoa
}
