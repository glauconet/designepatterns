package ClassesInnerClass;


//classe externa
public class Externa {

	public void metodoExterno() {
		System.out.println("externo - classe normal");
	}
	
	
	//classe interna
	public class Interna {
		
		public void metdodoInterno() {
			System.out.println("interno");
		}
		
	}

}
