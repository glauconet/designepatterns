package ClassesInnerClass;

// classes internas "anonymous" são declaradas sem um nome.
// são utilizadas para extender dinamicamente classe ou implementar interface. 
// são muito convenientes em algumas situações.

public class Teste {

	public static void main(String[] args) {

		Thread obj = new Thread() {
			public void run() {
				// faz algo...
			}
		};
		obj.start();
		
		new Thread() {
			public void run() {
				// faz algo...
			}
		}.start();

	}

}
