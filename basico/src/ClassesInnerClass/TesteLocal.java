package ClassesInnerClass;


public class TesteLocal {

	public static void main(String[] args) {

		Externa obj = new Externa();
		
		Externa.Interna obj2 = obj.new Interna();
	
		obj.metodoExterno();
		
		obj2.metdodoInterno();
	}

}
