package ClassesInnerClass.local;
//classes internas "local" são declaradas dentro de métodos
public class Outer {

	private int varInstance = 111;

	public void metodoOuter(int arg, final int argFinal) {
		@SuppressWarnings("unused")
		int local = 222;
		final int localFinal = 333;

		class Inner {
			void metodoInner() {
				System.out.println("inner");
				
				// não OK
				//System.out.println(arg);
				//System.out.println(local);

				// Ok!
				System.out.println(varInstance);// Ok!
				System.out.println(argFinal);// Ok!
				System.out.println(localFinal);// Ok!
			}
		}
		
		System.out.println("outer");
		Inner obj = new Inner();
		obj.metodoInner();
	}
}
