package ClassesInnerClass.member;

public interface InterfaceOuter {

	public void metodoOuter();
	
	public interface InterfaceInner {
	
		public void metodoInner();
		
	}
}
