package ClassesInnerClass.member;

// classes internas "member" são declaradas dentro de classes

public class Outer { // Top-level class

	public void metodoOuter() {
		System.out.println("outer");
	}

	// definição de classe aninhada
	public class Inner {

		public void metodoInner() {
			System.out.println("inner");
		}

	}

	// definição de classe aninhada
	public static class StaticInner {

		public void metodoStaticInner() {
			System.out.println("static inner");
		}

	}

}
