package ClassesInnerClass.member;

public class Teste {

	public static void main(String[] args) {

		Outer x1 = new Outer();
		x1.metodoOuter();

		Outer.Inner x2 = new Outer().new Inner();
		x2.metodoInner();

		Outer.StaticInner x3 = new Outer.StaticInner();
		x3.metodoStaticInner();

	}

}
