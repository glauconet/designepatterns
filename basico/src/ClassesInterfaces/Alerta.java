package ClassesInterfaces;
public interface Alerta extends Mensagem {
  String DEMORA = "Aguarde um instante ...";
  String FECHAR = "Encerre o programa da forma correta.";
    
  void exibir(String texto,int icone);
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/