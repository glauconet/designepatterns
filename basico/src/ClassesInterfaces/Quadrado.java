package ClassesInterfaces;
public class Quadrado implements AreaCalculavel {
  private double lado;
  
  public Quadrado(double lado) {
    this.lado = lado;
  }
  
  public double calcularArea() {
    return lado * lado;
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/