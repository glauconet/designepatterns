package ClassesInterfaces;
import javax.swing.JOptionPane;

public class TesteAlerta extends TesteMensagem implements Alerta {
  public void exibir(String texto, int icone) {
    JOptionPane.showMessageDialog(null,texto,"Aten��o",icone);
  }
  
  public static void main(String[] args) {
    TesteAlerta ta = new TesteAlerta();
    ta.exibir( ENTRADA );
    
    ta.exibir( FECHAR , 2);
    ta.exibir( DEMORA , 2);
    
    try{
      Thread.sleep(3000);
      }
    catch(Exception ex) {}
    
    ta.exibir( SUCESSO );
    System.exit(0);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/