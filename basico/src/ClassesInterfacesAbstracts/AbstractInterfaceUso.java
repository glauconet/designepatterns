package ClassesInterfacesAbstracts;

public class AbstractInterfaceUso{

	public static void main(String[] args) {
				
		AbstractInterface ui =  new AbstractInterface();
				
		try{
				
			ui.showMsg(Interface1.BemVindo);	
			// o objeto usa a variavel estatica da classe implementada
			ui.showMsg(Interface1.Autor);		
			// agora utilizamos o atributo chamando pelo nome da interface
			ui.showMsg2(Interface2.teste);	
			// atributo da interface 2
			ui.showMsg3(ui.teste2);			
			// vem da classe abstrata
			ui.showMsg(Interface1.Sucesso);	
			
			System.exit(0);
			
		} catch (Exception e) {
				
			//ui.showMsg(ui.Erro); // isto tambem funciona
			ui.showMsg(Interface1.Erro);
				
			System.exit(0);
		}	
	
	}	
}		