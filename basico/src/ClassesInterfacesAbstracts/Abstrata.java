package ClassesInterfacesAbstracts;

import javax.swing.JOptionPane;

public abstract class Abstrata {
	// a partir do momento que tem a palavra abstract, 
	// esta classe não poderá ser mas instaciada
		
	
	public String teste2 = "Sou uma string da classe abstrata";
	
	
	public void showMsg3(String Texto){
		
		JOptionPane.showMessageDialog(null,Texto, "Mensagem", 1);
	}
	
	public void showMsg4(String Texto){
		
		// não será obrigatório na classe que extender
		// se colocarmos a palavra abstract, esse metodo
		// não teria corpo e passaria a se obrigatorio
		// sua implementação nas classes que extenderam
		
	}
	

}
