package ClassesRecursosEspeciais;
import static java.lang.Math.*;
import static javax.swing.JOptionPane.showInputDialog;
import static javax.swing.JOptionPane.showMessageDialog;

public class ImportacaoEstatica {
  public static void main(String[] args) {
    String str = showInputDialog("Informe um n�mero");
    if (str == null) System.exit(0);
    double numero = Double.parseDouble(str);
    
    str = "N�mero: " + numero + "\nSeno: " + sin(numero) + 
      "\nCosseno: " + cos(numero);
    showMessageDialog(null, str);
    System.exit(0);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/