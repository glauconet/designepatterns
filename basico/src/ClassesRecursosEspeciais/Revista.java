package ClassesRecursosEspeciais;
public class Revista {
  private String titulo;
  private int numero;
  private int ano;
  private Meses mes;
  
  public Revista(String titulo, int numero, int ano, Meses mes) {
    this.titulo = titulo;
    this.numero = numero;
    this.ano = ano;
    this.mes = mes;
  }
  
  public String toString() {
    return titulo + " n� " + numero + " - " + mes.getAbreviado() + 
      " de " + ano;
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/