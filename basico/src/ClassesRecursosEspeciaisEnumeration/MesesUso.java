package ClassesRecursosEspeciaisEnumeration;

import static javax.swing.JOptionPane.showMessageDialog;

public class MesesUso {
	
	public static void main(String[] args) {
	
	Meses a;
	Meses b;
	Meses c;
	
	a = Meses.JANEIRO;
		
	b = Meses.FEVEREIRO;
	
	c = Meses.MARCO;
	
	showMessageDialog(null, a);
	showMessageDialog(null, a.getExtenso());
	showMessageDialog(null, a.getNumero());
	showMessageDialog(null, b);
	showMessageDialog(null, b.getExtenso());
	showMessageDialog(null, b.getNumero());
	showMessageDialog(null, c);
	showMessageDialog(null, c.getExtenso());
	showMessageDialog(null, c.getNumero());
	
	if (a == Meses.JANEIRO){

		showMessageDialog(null, a);
		showMessageDialog(null, a);
		showMessageDialog(null, a);
	 	}
    }
}
