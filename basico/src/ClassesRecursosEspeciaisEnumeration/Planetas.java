package ClassesRecursosEspeciaisEnumeration;

public enum Planetas {
    MERCURY (3, 2),
    VENUS   (4, 6),
    EARTH   (5, 6),
    MARS    (6, 3),
    JUPITER (1, 7),
    SATURN  (5, 6),
    URANUS  (8, 2),
    NEPTUNE (1, 2);

    public final double mass;   // in kilograms
    public final double radius; // in meters
    
    Planetas(double mass, double radius) {
        this.mass = mass;
        this.radius = radius;
    }
    
    public double getMassa() { 
    	return mass; 
    }
    public double getRaio() { 
    	return radius;
    }

   
    
}
