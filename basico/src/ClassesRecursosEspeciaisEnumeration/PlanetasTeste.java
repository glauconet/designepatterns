package ClassesRecursosEspeciaisEnumeration;

public class PlanetasTeste {
		

public static void main(String[] args) {
	
	UsaEnum u = new UsaEnum(Planetas.EARTH);
   
     for (Planetas p : Planetas.values()){
    	 
       System.out.print(p.name());
       System.out.print(" - " +p.getMassa());
       System.out.println(" - " +p.getRaio());
     }
     
     System.out.println(u.p);
     System.out.println(u.p.getMassa());
     System.out.println(u.p.getRaio());
     
 }
}