package ClassesRecursosEspeciaisEnumeration;

public class TV {
	    private int tamanho;
	    private int canal;
	    private int volume;
	    private boolean ligada;
	    //declaração da variável do tipo Enum Cores
	    private Cores cor;
	 
	    public TV(int tamanho, Cores cor) {
	        this.tamanho = tamanho;
	        this.canal = 0;
	        this.volume = 25;
	        this.ligada = false;
	        this.cor = cor;
	    }
	 
	    public int getTamanho() {
	        return tamanho;
	    }
	    public int getCanal() {
	        return canal;
	    }
	    public int getVolume() {
	        return volume;
	    }
	    public Cores getCor() {
	        return cor;
	    }
	 
	   
	}