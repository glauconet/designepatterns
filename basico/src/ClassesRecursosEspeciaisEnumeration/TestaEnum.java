package ClassesRecursosEspeciaisEnumeration;

public class TestaEnum {
	
	public static void main(String args[]) {
	     	    
	    Casos x = Casos.CASO3;
	    
	    System.out.println(x);
	    
	    switch(x){
	    
	    	case CASO1:
	       
	    		System.out.println("Caso 1");
	    		break;
	    	 
	    	case CASO2:
	    	
	    		System.out.println("Caso 2");
	    		break;
	    		
	    	case CASO3:
	    		
	    		System.out.println("Caso 3");
	    		break;
	    		
	    	default: System.out.println("Não é nenhum dos casos");
	    	
	    	 break;
	    }
	    
	}
	
	
	

}
