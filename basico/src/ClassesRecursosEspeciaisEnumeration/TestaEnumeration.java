package ClassesRecursosEspeciaisEnumeration;
// acessando estaticamente (diretamente) o valor PRATA do enum Cores
 
public class TestaEnumeration {

	public static void main(String args[]) {
	
    TV minhaTV21 = new TV(21, Cores.PRATA);
    
    
    System.out.println("Características da minha TV:" +
            "\nTamanho:     " + minhaTV21.getTamanho() +
            "\nCanal atual: " + minhaTV21.getCanal() +
            "\nVolume atual:" + minhaTV21.getVolume() +
            "\nCor:         " + minhaTV21.getCor());
    
    	switch(minhaTV21.getCor()){
    
    	case PRATA:
       
    		System.out.println("Características da minha TV:" + minhaTV21.getCor());
    	 
    	case BRANCO:
    	
    		System.out.println("Características da minha TV:" + minhaTV21.getCor());
    
    	}
    
    
    }
    
}