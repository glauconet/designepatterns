package ClassesRecursosEspeciaisEnumeration;

public class TestaUsaEnum {

	public static void main(String[] args) {
		
	
	UsaEnum secondDay = new UsaEnum();
	secondDay.setDay(Dia.MONDAY);// setado depois
	secondDay.EsseDia();
	
	
	UsaEnum thirdDay = new UsaEnum(Dia.TUESDAY);// passado no construtor
	thirdDay.EsseDia();
	
	UsaEnum fifthDay = new UsaEnum(Dia.WEDNESDAY);// passado no construtor
	fifthDay.EsseDia();
	
	UsaEnum fourthDay = new UsaEnum(Dia.THURSDAY);// passado no construtor
	fourthDay.EsseDia();
	
	
	UsaEnum sixthDay = new UsaEnum(Dia.FRIDAY);// passado no construtor
	sixthDay.EsseDia();
	
	UsaEnum seventhDay = new UsaEnum(Dia.SATURDAY);// passado no construtor
	seventhDay.EsseDia();
	
	UsaEnum firstDay = new UsaEnum();
	firstDay.setDay(Dia.SUNDAY);// setado depois
	firstDay.EsseDia();
	

	}	
}