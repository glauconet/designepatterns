package ClassesRecursosEspeciaisEnumeration;

public class UsaEnum {
			
		Dia day;
		Planetas p;
		
		public UsaEnum(){};
				
		public UsaEnum(Dia day) {
			this.day = day;
		}
		
		public UsaEnum(Planetas p) {
			this.p = p;
		}
			
		public void setDay(Dia day) {
			this.day = day;
		}
		
		public void EsseDia() {
			switch (day) {
			
				case MONDAY:
				System.out.println("Mondays are are bad.");
						     break;
						
				case FRIDAY: System.out.println("Fridays are better.");
						     break;
						     
				case SATURDAY:
				case SUNDAY: System.out.println("Weekends are best.");
						     break;
						     
				default:	 System.out.println("Midweek days are so-so.");
						     break;
			}
		}
}

