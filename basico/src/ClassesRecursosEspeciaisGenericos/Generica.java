package ClassesRecursosEspeciaisGenericos;

public class Generica {
	
	public <Y> void imprimir(Y s){
		
		System.out.println(s);
	}
	
   // foi criado um sé método imprimir 
  //que recebe qualquer tipo de argumento
	
   public static void main(String[] args){
	   
	   Generica g = new Generica();
	   
	   g.imprimir("teste");
	   g.imprimir(100);
   }

}
