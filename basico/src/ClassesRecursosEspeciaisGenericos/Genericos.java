package ClassesRecursosEspeciaisGenericos;
import java.util.Vector;




public class Genericos {
	public static void main(String[] args) {
			//Definimos um vetor que só poderá receber objetos do tipo Integer.
			// usamos genéricos colocando < > envolvendo um nome de uma classe
		    // a vantagem é 
			
		// se fosse vector comum seria assim
		    Vector v = new Vector(); 
		    Vector<Integer> l = new Vector<Integer>();
			Integer num1 = new Integer(1);
			Integer num2 = new Integer(2);
			Integer num4 = new Integer(4);
			l.add(num1);
			l.add(num2);
			l.add(num4);
			
			v.add("texto1");
			for (int i = 0 ; i < l.size() ; i ++) {
				// Não precisa de downcasting no uso do método get devido a ter sido definido usando o tipo Integer.
				System.out.println(l.get(i).intValue());
			}
			String str = "3"; 
			/* Não podemos adicionar uma string em um objeto do tipo Vector que foi criado apenas para receber objetos do tipo Integer.
			 l.add(str); // Esta linha precisa estar comentada, devido a um erro de compilação
			 */
			
			for (int i = 0 ; i < v.size() ; i ++) {
				// Não precisa de downcasting no uso do método get devido a ter sido definido usando o tipo Integer.
				System.out.println(v.get(i));
			}
	}
}