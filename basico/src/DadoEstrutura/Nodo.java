package DadoEstrutura;
public class Nodo <E>{
  private E objeto;
  private Nodo<E> anterior;
  private Nodo<E> proximo;
  
  public Nodo(E objeto) {
    this.objeto = objeto;
  }
  
  public E getObjeto() {
    return objeto;
  }
  
  public Nodo<E> getAnterior() {
    return anterior;
  }
  
  public Nodo<E> getProximo() {
    return proximo;
  }
  
  public void setAnterior(Nodo<E> anterior) {
    this.anterior = anterior;
  }
  
  public void setProximo(Nodo<E> proximo) {
    this.proximo = proximo;
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/