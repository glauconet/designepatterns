package DadoEstrutura;
public class PilhaInteiro {
  private long[] dados;
  private int topo;
  
  public PilhaInteiro(int maximo) {
    dados = new long[maximo];
    topo = -1;
  }
  
  public void incluir(long numero) {
    dados[++topo] = numero;
  }
  
  public long excluir () {
    return dados[topo--];
  }
  
  public long consultar() {
    return dados[topo];
  }
  
  public boolean vazia() {
    return (topo == -1);
  }
  
  public boolean cheia() {
    return (topo == dados.length - 1);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/