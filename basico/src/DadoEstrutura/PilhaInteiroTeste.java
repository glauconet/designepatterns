package DadoEstrutura;
import java.util.Random;

public class PilhaInteiroTeste {
  private static PilhaInteiro pilha;
  
  public static void main(String[] args) {
    pilha = new PilhaInteiro(5);
    Random sorteador = new Random();
    
    while (!pilha.cheia())
      pilha.incluir( sorteador.nextLong() );
    
    while(!pilha.vazia())
      System.out.println( pilha.excluir() );
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/