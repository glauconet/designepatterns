package DadoOrdenacaoPesquisa;
public class PesquisaLinear {
  public static int pesquisar(int[] dados, int qtde, int chave) {
    if (qtde == 0) return -1;
    
    int indice;
    for(indice = 0; indice < qtde; indice++)
      if(dados[indice] == chave) break;
    
    if(indice == qtde) return -1;
    else return indice;
  }
  
  public static <C extends Comparable<C>> int pesquisar(C[] dados,
    int qtde, C chave) {
    if (qtde == 0) return -1;
    
    int indice;
    for(indice = 0; indice < qtde; indice++)
      if(dados[indice].compareTo(chave) == 0) break;
    
    if(indice == qtde) return -1;
    else return indice;
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/