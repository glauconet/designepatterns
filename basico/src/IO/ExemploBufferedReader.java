package IO;
import java.io.BufferedReader;
import java.io.FileReader;

public class ExemploBufferedReader {

public static void main(String[] args) {
	
	String file = "arquivo.txt";

	try {
	BufferedReader reader = new BufferedReader(new FileReader(file));
		String line = null;
		
		while ((line = reader.readLine()) != null) {
				System.out.println(line);
		}
		
		reader.close();

	} catch (Exception e) {
			e.printStackTrace();
	}
}
}
