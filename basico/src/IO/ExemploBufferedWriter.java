package IO;

import java.io.BufferedWriter;
import java.io.FileWriter;

public class ExemploBufferedWriter {

	public static void main(String[] args) {
		String file = "arquivo.txt";

		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(file));

			writer.write("batatinha quando nasce");
			writer.newLine();
			writer.write("espalha rama pelo chão");
			writer.newLine();
			writer.write("a menina quando dorme");
			writer.newLine();
			writer.write("põe a mão no coração");
			writer.close();
			
			System.out.println("arquivo gravado");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
