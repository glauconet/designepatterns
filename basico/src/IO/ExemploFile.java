package IO;
import java.io.File;


public class ExemploFile {

	public static void main(String[] args) {
		// Cria um objeto que é um diretório
		File myDir = new File(System.getProperty("user.home"));
		System.out.println(myDir.isDirectory());
		
		// Cria um objeto que é um arquivo
		File myFile = new File(myDir, "jude.log");
		System.out.println(myFile.exists());
		System.out.println();
		
		
		
	}

}
