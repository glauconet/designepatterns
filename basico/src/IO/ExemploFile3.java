package IO;
import java.io.File;


public class ExemploFile3 {

	public static void main(String[] args) {

		File f = new File(System.getProperty("user.home"), "io");
		
		System.out.println(f.isDirectory());
		
		ExemploFileFilter filtro = new ExemploFileFilter("t");
		
		File[] lista = f.listFiles(filtro);

		for (File arq: lista) {
			System.out.println(arq.getName());
		}
	}

}
