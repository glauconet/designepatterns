package IO;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;


public class ExemploFile4 {
	public static void main(String[] args) {
		// Cria um objeto que é um diretório
		File myDir = new File(System.getProperty("user.home"));
		
		// Obtem o conteúdo do diretório
		File[] contents = myDir.listFiles();

		// Lista o conteúdo do diretório
		if (contents != null) {
			System.out.println("\nOs " + contents.length
					+ " itens no diretório " + myDir.getAbsolutePath() + " são:");
			for (File file : contents) {
				System.out.println(file.getName() + " é um "
						+ (file.isDirectory() ? "diretório" : "arquivo")
						+ " modificado em: "
						+ new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date(file.lastModified())));
			}
		}
	}

}
