package IO;
import java.io.File;
import java.io.FilenameFilter;

public class ExemploFileFilter implements FilenameFilter {

	private String letra;

	public ExemploFileFilter(String letra) {
		this.letra = letra;
	}

	@Override
	public boolean accept(File dir, String arq) {

		if (arq.startsWith(letra)) {
			return true;
		} else {
			return false;
		}
	}

}
