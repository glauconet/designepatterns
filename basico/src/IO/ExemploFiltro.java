package IO;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Date;


public class ExemploFiltro {

	public static void main(String[] args) {
		// Cria um objeto que é um diretório
		File myDir = new File(System.getProperty("user.home"), "EclipseProjects/workspaceCurso/fundamentosIO/src");
		System.out.println(myDir.getAbsolutePath() + (myDir.isDirectory() ? " é " : " não é ") + "um diretório");
		System.out.println("O pai de " + myDir.getName() + " é " + myDir.getParent());
		
		// Define um filtro para fontes java começando com E
		FilenameFilter filtro = new FileListFilter("E", "java");
		
		// Obtem o conteúdo do diretório
		File[] contents = myDir.listFiles(filtro);

		// Lista o conteúdo do diretório
		if (contents != null) {
			System.out.println("\nOs " + contents.length
					+ " itens no diretório " + myDir.getName() + " são:");
			for (File file : contents) {
				System.out.println(file + " é um "
						+ (file.isDirectory() ? "diretório" : "arquivo")
						+ " modificado em:\n"
						+ new Date(file.lastModified()));
			}
		}
	}

}
