package IO;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class ExemploObjectInputStream {

	public static void main(String[] args) {
		GameCharacter p1 = null;
		GameCharacter p2 = null;
		GameCharacter p3 = null;

		try {
			ObjectInputStream is = new ObjectInputStream(new FileInputStream("Game.ser"));
			p1 = (GameCharacter) is.readObject();
			p2 = (GameCharacter) is.readObject();
			p3 = (GameCharacter) is.readObject();

			System.out.println("Objetos deserializados de " + new File("Game.ser").getAbsolutePath());
			System.out.println("p1: " + p1);
			System.out.println("p2: " + p2);
			System.out.println("p3: " + p3);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
