package IO;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class ExemploObjectOutputStream {

	public static void main(String[] args) {
		GameCharacter p1 = new GameCharacter(50, "Elf", new String[] { "bow", "sword", "dust" });
		GameCharacter p2 = new GameCharacter(200, "Troll", new String[] { "bare hands", "big axe" });
		GameCharacter p3 = new GameCharacter(120, "Magician", new String[] { "spells", "invisibility" });

		try {
			ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream("Game.ser"));
			os.writeObject(p1);
			os.writeObject(p2);
			os.writeObject(p3);
			os.close();

			System.out.println("Objetos serializados em " + new File("Game.ser").getAbsolutePath());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
