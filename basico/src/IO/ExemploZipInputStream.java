package IO;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ExemploZipInputStream {

	public static void main(String[] args) {
		try {
			// Abre o arquivo zip
			ZipInputStream in = new ZipInputStream(new BufferedInputStream(new FileInputStream("haikai.zip")));
			ZipEntry entry = null;

			// obtem os textos
			while ((entry = in.getNextEntry()) != null) {

				System.out.println("\n" + entry.getName() + "\n");
				// Transfere bytes do zip para um buffer
				byte[] buf = new byte[1024];
				in.read(buf);
				System.out.println(new String(buf) + "\n***");

			}

			in.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
