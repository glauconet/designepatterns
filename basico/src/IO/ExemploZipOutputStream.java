package IO;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ExemploZipOutputStream {

	public static void main(String[] args) {
	    // Estes são os textos a serem incluidos no arquivo zip
	    String[] haikai = {"Esnobar\nÉ exigir café fervendo\nE deixar esfriar\n", 
	    				   "Viva o Brasil\nOnde o ano inteiro\nÉ primeiro de abril\n",
	    				   "No hall escuro\nO segurança\nMata o inseguro\n",
	    				   "E o medo que mete\nEsse espelho\nQue não reflete\n",
	    				   "Na poça da rua\nO vira-lata\nLambe a Lua\n",
	    				   "Velho no retrato novo...\nMas, no retrato velho,\nsou novo, de novo!\n",
	    				   "Olha:\nentre um pingo e outro,\na chuva não molha\n",
	    				   "Aniversário é uma festa\nPra te lembrar\nDo que resta\n",
	    				   "É um fato concreto:\nQuem inventou o alfabeto\nFoi um analfabeto\n"
	    				  };
	    
	    try {
	        // Cria o arquivo zip
	        String outFilename = "haikai.zip";
	        ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(outFilename)));
	    
	        // Comprime os textos
	        for (int i=0; i<haikai.length; i++) {
	            // inclui ZIP entry
	            out.putNextEntry(new ZipEntry("haikai." + (i+1)));
	    
	            // transfere bytes do texto para o zip
                out.write(haikai[i].getBytes());
                // termina o entry
	            out.closeEntry();
	            System.out.println("haikai." + (i+1) + " adicionado");
	        }
	    
	        // termina o arquivo zip
	        out.close();
	        System.out.println("haikai.zip terminado!");
	    } catch (IOException e) {
	    	e.printStackTrace();
	    }

	}

}
