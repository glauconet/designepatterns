package IO;
import java.io.File;
import java.io.FilenameFilter;

public class FileListFilter implements FilenameFilter {
	private String nome; 		// filtro para o nome
	private String extensao; 	// filtro para a extensão
	
	// Constructor
	public FileListFilter(String nome, String extensao) {
		this.nome = nome;
		this.extensao = extensao;
	}

	public boolean accept(File directory, String filename) {
		boolean fileOK = true;
		// Se um filtro para o nome foi especificado, verifica o nome
		if (nome != null) {
			fileOK &= filename.startsWith(nome);
		}
		// Se um filtro para a extensão foi especificado, verifica a extensão
		if (extensao != null) {
			fileOK &= filename.endsWith('.' + extensao);
		}
		return fileOK;
	}
}
