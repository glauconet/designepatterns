package IO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GameCharacter implements Serializable {

	private static final long serialVersionUID = -8649964131670343252L;
	
	int power;
	String type;
	List<String> weapons;
	
	public GameCharacter(int p, String t, String[] w) {
		power = p;
		type = t;
		weapons = new ArrayList<String>(Arrays.asList(w));
	}

	public int getPower() {
		return power;
	}

	public String getType() {
		return type;
	}
	
	@Override
	public String toString() {
		return power + ", " + type + ", " + weapons;
	}
}
