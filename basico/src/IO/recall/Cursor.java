package IO.recall;

public class Cursor {
	
	private String registro;
	private int posicao = 1;
	
	public Cursor(String registro) {
		this.registro = registro;
	}
	
	public String nextToken(int tamanho) {
		String token = registro.substring(posicao, posicao+tamanho);
		posicao += tamanho;
		return token.trim();
	}
	

}
