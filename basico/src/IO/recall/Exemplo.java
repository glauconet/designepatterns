package IO.recall;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;



public class Exemplo {

	public static void main(String[] args) {
		String file = "recall.txt";

		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String linha = null;
			while ((linha = reader.readLine()) != null) {
				Registro reg = RegistroFactory.getRegistro(linha);
				System.out.println(reg);
			}
			reader.close();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (RecallException e) {
			e.printStackTrace();
		}

	}

}
