package IO.recall;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class ExemploReader {

	public static void main(String[] args) {
		String file = "recall.txt";

		try {
			RecallReader reader = new RecallReader(new BufferedReader(new FileReader(file)));
			Registro reg = null;
			while ((reg = reader.readRegistro()) != null) {
				System.out.println(reg);
			}
			reader.close();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (RecallException e) {
			e.printStackTrace();
		}

	}

}
