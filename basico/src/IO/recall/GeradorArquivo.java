package IO.recall;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;


public class GeradorArquivo {

	public static void main(String[] args) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter("recall.txt"));

		RegistroHeader rh = new RegistroHeader();
		rh.setAnoLote("2011");
		rh.setNumeroLote("01");
		rh.setDataHoraGeracao("01032011090000");
		rh.setCnpjMontadora("12345678901234");
		writer.write(rh.gerarLinha());
		writer.newLine();

		RegistroRecall rr = new RegistroRecall();
		rr.setTipoAtualizacao('I');
		rr.setCodigoRecall("001");
		rr.setDescricao("rebimboca da parafuseta frouxa");
		writer.write(rr.gerarLinha());
		writer.newLine();

		for (int i = 0; i < 100; i++) {
			RegistroVeiculo rv = new RegistroVeiculo();
			rv.setTipoAtualizacao('I');
			rv.setCodigoRecall("001");
			rv.setNumeroChassi("123456789012345678901");
			rv.setDataServico("10022010");
			rv.setCnpjConcessionaria("12345678901234");
			rv.setNomeConcessionaria("Metralha Veiculos");
			writer.write(rv.gerarLinha());
			writer.newLine();
		}

		RegistroTrailer rt = new RegistroTrailer();
		rt.setAnoLote("2011");
		rt.setNumeroLote("01");
		rt.setQuantidadeRecall("00001");
		rt.setQuantidadeVeiculo("00001");
		writer.write(rt.gerarLinha());
		writer.newLine();
		
		writer.close();

	}

}
