package IO.recall;

public class RecallException extends Exception {

	private static final long serialVersionUID = 100142720344584865L;

	public RecallException(String msg) {
		super(msg);
	}
}
