package IO.recall;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;



public class RecallReader extends Reader {

	private BufferedReader br;
	
	public RecallReader(BufferedReader br) {
		this.br = br;
	}

	public Registro readRegistro() throws RecallException {
		Registro reg = null;
		try {
			String linha = br.readLine();
			if (linha != null) {
				reg = RegistroFactory.getRegistro(linha);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return reg;
	}

	@Override
	public void close() throws IOException {
		br.close();
	}

	@Override
	public int read(char[] cbuf, int off, int len) throws IOException {
		return br.read(cbuf, off, len);
	}

}
