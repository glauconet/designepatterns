package IO.recall;

public abstract class Registro {
	
	public static final int TAMANHO_LINHA = 100;
	
	public abstract void carregarRegistro(String linha);

	public abstract String gerarLinha();

}
