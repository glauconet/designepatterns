package IO.recall;



public class RegistroFactory {
	
	private static int contador;
	
	private RegistroFactory() {
		
	}
	
	public static Registro getRegistro(String linha) throws RecallException {
		contador++;
		if (linha.length() != Registro.TAMANHO_LINHA) {
			throw new RecallException("registro " + contador + ". tamanho incorreto: " + linha.length());
		}

		Registro reg = null;
		char tipo = linha.charAt(0);
		if (tipo == TipoRegistro.HEADER.getValor()) {
			reg = new RegistroHeader();
		} else if (tipo == TipoRegistro.RECALL.getValor()) {
			reg = new RegistroRecall();
		} else if (tipo == TipoRegistro.VEICULO.getValor()) {
			reg = new RegistroVeiculo();
		} else if (tipo == TipoRegistro.TRAILER.getValor()) {
			reg = new RegistroTrailer();
		} else {
			throw new RecallException("registro " + contador + ". tipo de registro incorreto: " + tipo);
		}
		
		reg.carregarRegistro(linha);
		return reg;
	}

	public static int getContador() {
		return contador;
	}

}
