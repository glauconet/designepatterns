package IO.recall;




public class RegistroHeader extends Registro {

	private String anoLote;
	private String numeroLote;
	private String dataHoraGeracao;
	private String cnpjMontadora;
	
	@Override
	public String toString() {
		return "Header [anoLote=" + anoLote + 
		       " numeroLote=" + numeroLote +
		       " dataHoraGeracao=" + dataHoraGeracao + 
		       " cnpjMontadora=" + cnpjMontadora + "]";
	}
	
	@Override
	public void carregarRegistro(String linha) {
		Cursor cursor = new Cursor(linha);
		anoLote = cursor.nextToken(4);
		numeroLote = cursor.nextToken(2);
		dataHoraGeracao = cursor.nextToken(14);
		cnpjMontadora = cursor.nextToken(14);
	}
	
	@Override
	public String gerarLinha() {
		StringBuilder sb = new StringBuilder(Registro.TAMANHO_LINHA);
		sb.append(TipoRegistro.HEADER.getValor());
		sb.append(anoLote);
		sb.append(numeroLote);
		sb.append(dataHoraGeracao);
		sb.append(cnpjMontadora);
		return Util.padRight(sb.toString(), Registro.TAMANHO_LINHA);
	}

	public String getAnoLote() {
		return anoLote;
	}

	public void setAnoLote(String anoLote) {
		this.anoLote = anoLote;
	}

	public String getNumeroLote() {
		return numeroLote;
	}

	public void setNumeroLote(String numeroLote) {
		this.numeroLote = numeroLote;
	}

	public String getDataHoraGeracao() {
		return dataHoraGeracao;
	}

	public void setDataHoraGeracao(String dataHoraGeracao) {
		this.dataHoraGeracao = dataHoraGeracao;
	}

	public String getCnpjMontadora() {
		return cnpjMontadora;
	}

	public void setCnpjMontadora(String cnpjMontadora) {
		this.cnpjMontadora = cnpjMontadora;
	}

}
