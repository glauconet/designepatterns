package IO.recall;



public class RegistroRecall extends Registro {
	
	private char tipoAtualizacao;
	private String codigoRecall;
	private String descricao;
	
	@Override
	public String toString() {
		return "Recall [tipoAtualizacao=" + tipoAtualizacao + 
		       " codigoRecall=" + codigoRecall + 
		       " descricao=" + descricao + "]";
	}
	
	@Override
	public void carregarRegistro(String linha) {
		Cursor cursor = new Cursor(linha);
		tipoAtualizacao = cursor.nextToken(1).charAt(0);
		codigoRecall = cursor.nextToken(3);
		descricao = cursor.nextToken(50);
	}
	
	@Override
	public String gerarLinha() {
		StringBuilder sb = new StringBuilder(Registro.TAMANHO_LINHA);
		sb.append(TipoRegistro.RECALL.getValor());
		sb.append(tipoAtualizacao);
		sb.append(codigoRecall);
		sb.append(descricao);
		return Util.padRight(sb.toString(), Registro.TAMANHO_LINHA);
	}

	public String getCodigoRecall() {
		return codigoRecall;
	}

	public void setCodigoRecall(String codigoRecall) {
		this.codigoRecall = codigoRecall;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setTipoAtualizacao(char tipoAtualizacao) {
		this.tipoAtualizacao = tipoAtualizacao;
	}

	public char getTipoAtualizacao() {
		return tipoAtualizacao;
	}
	
}
