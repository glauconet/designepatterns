package IO.recall;



public class RegistroTrailer extends Registro {

	private String anoLote;
	private String numeroLote;
	private String quantidadeRecall;
	private String quantidadeVeiculo;
	
	@Override
	public String toString() {
		return "Trailer [anoLote=" + anoLote + 
		       " numeroLote=" + numeroLote +
		       " quantidadeRecall=" + quantidadeRecall + 
		       " quantidadeVeiculo" + quantidadeVeiculo + "]";
	}

	@Override
	public void carregarRegistro(String linha) {
		Cursor cursor = new Cursor(linha);
		anoLote = cursor.nextToken(4);
		numeroLote = cursor.nextToken(2);
		quantidadeRecall = cursor.nextToken(5);
		quantidadeVeiculo = cursor.nextToken(5);
	}
	
	@Override
	public String gerarLinha() {
		StringBuilder sb = new StringBuilder(Registro.TAMANHO_LINHA);
		sb.append(TipoRegistro.TRAILER.getValor());
		sb.append(anoLote);
		sb.append(numeroLote);
		sb.append(quantidadeRecall);
		sb.append(quantidadeVeiculo);
		return Util.padRight(sb.toString(), Registro.TAMANHO_LINHA);
	}

	public String getAnoLote() {
		return anoLote;
	}

	public void setAnoLote(String anoLote) {
		this.anoLote = anoLote;
	}

	public String getNumeroLote() {
		return numeroLote;
	}

	public void setNumeroLote(String numeroLote) {
		this.numeroLote = numeroLote;
	}

	public String getQuantidadeRecall() {
		return quantidadeRecall;
	}

	public void setQuantidadeRecall(String quantidadeRecall) {
		this.quantidadeRecall = quantidadeRecall;
	}

	public String getQuantidadeVeiculo() {
		return quantidadeVeiculo;
	}

	public void setQuantidadeVeiculo(String quantidadeVeiculo) {
		this.quantidadeVeiculo = quantidadeVeiculo;
	}
	
}
