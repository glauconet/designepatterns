package IO.recall;



public class RegistroVeiculo extends Registro {

	private char tipoAtualizacao;
	private String codigoRecall;
	private String numeroChassi;
	private String dataServico;
	private String cnpjConcessionaria;
	private String nomeConcessionaria;
	
	@Override
	public String toString() {
		return "Veiculo [tipoAtualizacao=" + tipoAtualizacao + 
		       " codigoRecall=" + codigoRecall + 
			   " numeroChassi=" + numeroChassi + 
			   " dataServico=" + dataServico + 
			   " cnpjConcessionaria=" + cnpjConcessionaria + 
		       " nomeConcessionaria=" + nomeConcessionaria + "]";
	}

	@Override
	public void carregarRegistro(String linha) {
		Cursor cursor = new Cursor(linha);
		tipoAtualizacao = cursor.nextToken(1).charAt(0);
		codigoRecall = cursor.nextToken(3);
		numeroChassi = cursor.nextToken(21);
		dataServico = cursor.nextToken(8);
		cnpjConcessionaria = cursor.nextToken(14);
		nomeConcessionaria = cursor.nextToken(50);
	}
	
	@Override
	public String gerarLinha() {
		StringBuilder sb = new StringBuilder(Registro.TAMANHO_LINHA);
		sb.append(TipoRegistro.VEICULO.getValor());
		sb.append(tipoAtualizacao);
		sb.append(codigoRecall);
		sb.append(numeroChassi);
		sb.append(dataServico);
		sb.append(cnpjConcessionaria);
		sb.append(nomeConcessionaria);
		return Util.padRight(sb.toString(), Registro.TAMANHO_LINHA);
	}

	public String getCodigoRecall() {
		return codigoRecall;
	}

	public void setCodigoRecall(String codigoRecall) {
		this.codigoRecall = codigoRecall;
	}

	public String getNumeroChassi() {
		return numeroChassi;
	}

	public void setNumeroChassi(String numeroChassi) {
		this.numeroChassi = numeroChassi;
	}

	public String getCnpjConcessionaria() {
		return cnpjConcessionaria;
	}

	public void setCnpjConcessionaria(String cnpjConcessionaria) {
		this.cnpjConcessionaria = cnpjConcessionaria;
	}

	public String getNomeConcessionaria() {
		return nomeConcessionaria;
	}

	public void setNomeConcessionaria(String nomeConcessionaria) {
		this.nomeConcessionaria = nomeConcessionaria;
	}

	public String getDataServico() {
		return dataServico;
	}

	public void setDataServico(String dataServico) {
		this.dataServico = dataServico;
	}

	public void setTipoAtualizacao(char tipoAtualizacao) {
		this.tipoAtualizacao = tipoAtualizacao;
	}

	public char getTipoAtualizacao() {
		return tipoAtualizacao;
	}

}
