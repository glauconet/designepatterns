package IO.recall;

public enum TipoAtualizacao {

	INCLUSAO ('I'),
	EXCLUSAO ('E'),
	ALTERACAO ('A');
	
	private char tipo; 
	
	private TipoAtualizacao(char tipo) {
		this.tipo = tipo;
	}

	public char getValor() {
		return tipo;
	}

}
