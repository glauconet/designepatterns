package IO.recall;

public enum TipoRegistro {
	
	HEADER ('0'),
	RECALL ('1'),
	VEICULO ('2'),
	TRAILER ('9');
	
	private char tipo; 
	
	private TipoRegistro(char tipo) {
		this.tipo = tipo;
	}

	public char getValor() {
		return tipo;
	}
}
