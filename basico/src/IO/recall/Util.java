package IO.recall;

public class Util {

	/**
	 * retorna campo com brancos à direita até o tamanho informado
	 * 
	 * @param campo
	 * @param tamanho
	 * @return campo preenchido
	 */
	public static String padRight(final String campo, final int tamanho) {
		return String.format("%1$-" + tamanho + "s", campo);
	}

	/**
	 * retorna campo com brancos à esquerda até o tamanho informado
	 * 
	 * @param campo
	 * @param tamanho
	 * @return campo preenchido
	 */
	public static String padLeft(final String campo, final int tamanho) {
		if (tamanho <= 0) {
			return "";
		} else {
			return String.format("%1$#" + tamanho + "s", campo);
		}
	}
}
