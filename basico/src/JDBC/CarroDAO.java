package JDBC;

import java.util.List;


public interface CarroDAO {
	
	/**
	 * Dado o id, recupera os dados do carro na tabela do banco,
	 * e cria um objeto Carro com esses dados. 
	 * 
	 * @param id
	 * @return Carro
	 */
	public Carro recuperarCarro(int id) throws CarroException;
	
	/**
	 * Recupera todos os registros da tabela carro e, para cada registro, 
	 * cria um objeto Carro com seus dados e o adiciona numa lista.
	 * Ao final da montagem retorna a lista.
	 * 
	 * @return List<Carro>
	 */
	public List<Carro> recuperarListaCarros() throws CarroException;
	
	/**
	 * Prepara e retorna um objeto Iterator para posterior navegação na 
	 * tabela carro e retorno de um objeto Carro
	 * 
	 * @return Iterator RelacaoCarro
	 */
	public RelacaoCarro recuperarRelacaoCarros() throws CarroException;
	
	/**
	 * Inclui linha na tabela carro com os campos do objeto Carro.
	 * O id gerado para a nova linha é retornado.
	 *   
	 * @param carro
	 * @return id gerado
	 */
	public long incluirCarro(Carro carro) throws CarroException;
	
	/**
	 * Dado o id, exclui a linha correspondente na tabela carro.
	 * 
	 * @param id
	 */
	public void excluirCarro(int id) throws CarroException;
	
	/**
	 * Altera a linha da tabela carro com os dados do objeto Carro.
	 * 
	 * @param carro
	 */
	public void alterarCarro(Carro carro) throws CarroException;

}
