package JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;



public class CarroDAOImpl implements CarroDAO {

	/* (non-Javadoc)
	 * @see br.gov.serpro.carros.model.dao.CarroDAO#alterarCarro(br.gov.serpro.carros.model.Carro)
	 */
	@Override
	public void alterarCarro(Carro carro) throws CarroException {
		if (carro == null) {
			throw new CarroException("recebido carro null na alteração");
		}
		
		Connection cn = null;
		PreparedStatement st = null;
		
		try {
			cn = getConnection();
			
			st = cn.prepareStatement("update carro set marca=?, modelo=? where id=?");
			st.setString(1, carro.getMarca());
			st.setString(2, carro.getModelo());
			st.setInt(3, carro.getId());
			st.executeUpdate();

		} catch (SQLException e) {
			throw new CarroException("erro na atualização", e);

		} finally {
			try {
				cn.close();
			} catch (SQLException e) { 
				// nada a fazer aqui
			}
		}

	}

	/* (non-Javadoc)
	 * @see br.gov.serpro.carros.model.dao.CarroDAO#excluirCarro(int)
	 */
	@Override
	public void excluirCarro(int id) throws CarroException {
		Connection cn = null;
		PreparedStatement st = null;
		
		try {
			cn = getConnection();
			
			st = cn.prepareStatement("delete from carro where id=?");
			st.setInt(1, id);
			st.executeUpdate();

		} catch (SQLException e) {
			throw new CarroException("erro na exclusão", e);

		} finally {
			try {
				cn.close();
			} catch (SQLException e) { 			
				// nada a fazer aqui
			}
		}
	}

	/* (non-Javadoc)
	 * @see br.gov.serpro.carros.model.dao.CarroDAO#incluirCarro(br.gov.serpro.carros.model.Carro)
	 */
	@Override
	public long incluirCarro(Carro carro) throws CarroException {
		if (carro == null) {
			throw new CarroException("recebido carro null na inclusão");
		}
		
		Connection cn = null;
		long id = -1;
		
		try {
			cn = getConnection();
			
			PreparedStatement st = cn.prepareStatement("insert into carro (marca, modelo) values(?, ?)");
			st.setString(1, carro.getMarca());
			st.setString(2, carro.getModelo());
			st.executeUpdate();
			
			ResultSet rs = st.getGeneratedKeys();
			if (rs.next()) {
				id = rs.getLong(1);
			}

		} catch (SQLException e) {
			throw new CarroException("erro na inclusão", e);

		} finally {
			try {
				cn.close();
			} catch (SQLException e) { 			
				// nada a fazer aqui
			}
		}
		return id;
	}

	/* (non-Javadoc)
	 * @see br.gov.serpro.carros.model.dao.CarroDAO#recuperarCarro(int)
	 */
	@Override
	public Carro recuperarCarro(int id) throws CarroException {
		Connection cn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		Carro carro = null;
		
		try {
			cn = getConnection();
			
			st = cn.prepareStatement("select id, marca, modelo from carro where id=?");
			st.setInt(1, id);
			rs = st.executeQuery();
		
			if (rs.next() == true) {
				carro = new Carro();
				carro.setId(rs.getInt("id"));
				carro.setMarca(rs.getString("marca"));
				carro.setModelo(rs.getString("modelo"));
			} 
			
		} catch (SQLException e) {
			throw new CarroException("erro na consulta de carro", e);

		} finally {
			try {
				cn.close();
			} catch (SQLException e) { 			
				// nada a fazer aqui
			}
		}
		
		return carro;
	}

	/* (non-Javadoc)
	 * @see br.gov.serpro.carros.model.dao.CarroDAO#recuperarListaCarros()
	 */
	@Override
	public List<Carro> recuperarListaCarros() throws CarroException {
		Connection cn = null;
		Statement st = null;
		ResultSet rs = null;
		List<Carro> carros = null;
		
		try {
			cn = getConnection();

			st = cn.createStatement();
			
			rs = st.executeQuery("select id, marca, modelo from carro");
			
			carros = new ArrayList<Carro>();
			while (rs.next()) {
				Carro carro = new Carro();
				carro.setId(rs.getInt("id"));
				carro.setMarca(rs.getString("marca"));
				carro.setModelo(rs.getString("modelo"));
				carros.add(carro);
			}

		} catch (SQLException e) {
			throw new CarroException("erro na consulta de carros", e);

		} finally {
			try {
				cn.close();
			} catch (SQLException e) { 			
				// nada a fazer aqui
			}
		}

		return carros;
	}

	/* (non-Javadoc)
	 * @see br.gov.serpro.carros.model.dao.CarroDAO#recuperarRelacaoCarros()
	 */
	@Override
	public RelacaoCarro recuperarRelacaoCarros() throws CarroException {
		Connection cn = null;
		Statement st = null;
		ResultSet rs = null;
		RelacaoCarro carros = null;
		
		try {
			cn = getConnection();

			st = cn.createStatement();
			rs = st.executeQuery("select id, marca, modelo from carro");
			
			carros = new RelacaoCarroImpl(cn, rs);

		} catch (SQLException e) {
			throw new CarroException("erro na consulta de carros", e);
		}
		// neste caso, Connection, Statement e ResultSet serão fechados pela classe RelacaoCarro

		return carros;
	}

	/**
	 * Método utilitário que retorna conexão com banco
	 *  
	 * @return
	 * @throws SQLException
	 */
	private Connection getConnection() throws SQLException {
		String url = "jdbc:h2:tcp://localhost/~/cursojava";
		String username = "sa";
		String password = "";
		
		// obtem conexão
		Connection cn = DriverManager.getConnection(url, username, password);
		return cn;
	}

}
