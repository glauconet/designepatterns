package JDBC;

public class CarroException extends Exception {
	
	private static final long serialVersionUID = -5232678432592448287L;

	public CarroException(String msg) {
		super(msg);
	}

	public CarroException(Exception e) {
		super(e);
	}
	
	public CarroException(String msg, Exception e) {
		super(msg, e);
	}

}
