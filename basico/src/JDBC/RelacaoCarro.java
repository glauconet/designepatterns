package JDBC;

import java.util.Iterator;


public interface RelacaoCarro extends Iterable<Carro>, Iterator<Carro> {

	/* (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public abstract Iterator<Carro> iterator();

	/* (non-Javadoc)
	 * @see java.util.Iterator#hasNext()
	 */
	@Override
	public abstract boolean hasNext();

	/* (non-Javadoc)
	 * @see java.util.Iterator#next()
	 */
	@Override
	public abstract Carro next();

	/* (non-Javadoc)
	 * @see java.util.Iterator#remove()
	 */
	@Override
	public abstract void remove();

	/**
	 * Fecha a conexão com banco de dados.
	 * A conexão já é fechada no método next() quando se chega ao último registro. 
	 * Este método apenas permite fechar explicitamente a conexão antes do fim da navegação com o Iterator. 
	 */
	public abstract void fecharConexao();

}