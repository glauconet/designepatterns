package JDBC;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.NoSuchElementException;



public class RelacaoCarroImpl implements RelacaoCarro {

	private Connection cn;
	private ResultSet rs;
	private boolean temRegistro;

	/**
	 * No construtor, além de reter o Connection e o ResultSet para uso futuro,
	 * já se posiciona no primeiro registro.
	 *  
	 * @param cn
	 * @param rs
	 */
	public RelacaoCarroImpl(Connection cn, ResultSet rs) {
		if (cn == null || rs == null) {
			throw new RuntimeException("recebido Connection ou ResultSet igual a null");
		}
		
		this.cn = cn;
		this.rs = rs;
		try {
			temRegistro = rs.next();
			
		} catch (SQLException e) {
			fecharConexao();
			throw new RuntimeException("erro na recuperação de carro", e);
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#finalize()
	 */
	@Override
	protected void finalize() throws Throwable {
		fecharConexao();
	}
	
	/* (non-Javadoc)
	 * @see br.gov.serpro.carros.dominio.dao.impl.RelacaoCarro#iterator()
	 */
	@Override
	public Iterator<Carro> iterator() {
		return this;
	}

	/* (non-Javadoc)
	 * @see br.gov.serpro.carros.dominio.dao.impl.RelacaoCarro#hasNext()
	 */
	@Override
	public boolean hasNext() {
		return temRegistro;
	}

	/* (non-Javadoc)
	 * @see br.gov.serpro.carros.dominio.dao.impl.RelacaoCarro#next()
	 */
	@Override
	public Carro next() {
		Carro carro = null;

		try {
			if (temRegistro) {
				carro = new Carro();
				carro.setId(rs.getInt("id"));
				carro.setMarca(rs.getString("marca"));
				carro.setModelo(rs.getString("modelo"));

				temRegistro = rs.next();

				if (!temRegistro) {
					fecharConexao();
				}

			} else {
				throw new NoSuchElementException("tentativa de acessar próximo registro inexistente");
			}
			
		} catch (SQLException e) {
			throw new RuntimeException("erro durante recuperação de registro", e);
		}

		return carro;
	}

	/* (non-Javadoc)
	 * @see br.gov.serpro.carros.dominio.dao.impl.RelacaoCarro#fecharConexao()
	 */
	@Override
	public void fecharConexao() {
		try {
			cn.close();

		} catch (SQLException e) {
			// nada a fazer
		}
	}

	/* (non-Javadoc)
	 * @see br.gov.serpro.carros.dominio.dao.impl.RelacaoCarro#remove()
	 */
	@Override
	public void remove() {
		throw new UnsupportedOperationException("método remove não suportado");
	}

}
