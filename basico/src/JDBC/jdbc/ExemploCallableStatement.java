package JDBC.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ExemploCallableStatement {

	public static void main(String[] args) throws SQLException {
		String url = "jdbc:oracle:thin:@10.50.240.62:1521:toc20a";
		String username = "testjava";
		String password = "nova";
		
		// obtem conexão
		Connection cn = DriverManager.getConnection(url, username, password);
		
		// obtém PreparedStatement
		CallableStatement st = cn.prepareCall("{ call INSERE(?, ?, ?, ?) }");
		
		// monta parâmetros
		st.setString(1, "jose");
		st.setString(2, "22/11/1966");
		st.setString(3, "ciclismo");
		st.registerOutParameter(4, java.sql.Types.INTEGER);
		
		// executa sp
		st.executeUpdate();
		
		// Recupera parâmetro OUT
		int id = st.getInt(4);
		
		// lista resultado
		System.out.println(id);
		
		// fecha conexão
		cn.close();
	}
}
