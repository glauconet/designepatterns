package JDBC.jdbc;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Types;

public class ExemploCallableStatement2 {
	public static void main(String[] args) throws SQLException {
		String sURL = "jdbc:sqlserver://10.15.26.159:1433;";
		String sUsername = "pgdarfjmeter";
		String sPassword = "pgdarf34383";
		
		// obtem conexão
		Connection cn = DriverManager.getConnection(sURL, sUsername, sPassword);
		
		// obtém PreparedStatement
		CallableStatement st = cn.prepareCall("{ call spu_consultarLimiteOrgaoPA(?, ?, ?) }");
		
		// monta parâmetros
		st.setString(1, "DF");
		st.setString(2, "200707");
		st.registerOutParameter(3, Types.DECIMAL);
		
		// executa sp
		st.executeUpdate();
		
		// Recupera parâmetro OUT
		String ret = st.getString(3);
		
		// lista resultado
		System.out.println(ret);
		
		// fecha conexão
		cn.close();
	}
}
