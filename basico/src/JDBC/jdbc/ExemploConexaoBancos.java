package JDBC.jdbc;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ExemploConexaoBancos {

	public static void main(String[] args) throws SQLException {
	
//		String url = "jdbc:derby://localhost:1527/cursojava";
//		String username = "user";
//		String password = "senha";

//		String url = "jdbc:oracle:thin:@10.50.240.62:1521:toc20a";
//		String username = "testjava";
//		String password = "nova";

//		String url = "jdbc:sqlserver://10.15.1.4:1433";
//		String username = "constel";
//		String password = "serpro";

//		String url = "jdbc:postgresql://10.15.11.7:5432/postgres";
//		String username = "postgres";
//		String password = "postgressql";

		String url = "jdbc:h2:tcp://localhost/~/cursojava";
		String username = "sa";
		String password = "";
		
		// obtem conexão
		Connection cn = DriverManager.getConnection(url, username, password);

		System.out.print("nome da classe : ");
		System.out.println(cn.getClass().getName());

		// Obtem metadados
		DatabaseMetaData dbmData = cn.getMetaData();
		if (dbmData == null) {
			System.out.println("driver não suporta metadados");
		} else {
			System.out.print("nome do driver : ");
			System.out.println(dbmData.getDriverName());
			System.out.print("suporta Select For Update : ");
			System.out.println(dbmData.supportsSelectForUpdate());
			System.out.print("suporta getGeneratedKeys : ");
			System.out.println(dbmData.supportsGetGeneratedKeys());
			System.out.print("suporta transactions : ");
			System.out.println(dbmData.supportsTransactions());
			System.out.print("suporta BatchUpdates : ");
			System.out.println(dbmData.supportsBatchUpdates());
		}
	
		cn.close();
	
	}
}
