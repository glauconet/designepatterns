package JDBC.jdbc;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

//import oracle.jdbc.OracleTypes;

public class ExemploCursorOracle {

	public static void main(String[] args) throws SQLException {
		String url = "jdbc:oracle:thin:@10.50.240.62:1521:toc20a";
		String username = "testjava";
		String password = "nova";
		
		// obtem conexão
		Connection cn = DriverManager.getConnection(url, username, password);

		// usando Statement
		Statement st = cn.createStatement();
		int quant = st.executeUpdate("insert into hobby (nome, data, hobby) values ('marquinhos', '11/15/1999', 'lego')");
		System.out.println(quant);
		
		// obtém PreparedStatement
		CallableStatement cs = cn.prepareCall("{ call ? := lista_hobbies }");
		
		// monta parâmetros
		//cs.registerOutParameter(1, OracleTypes.CURSOR);
		
		// executa sp
		cs.execute();
		
		// recupera parâmetro OUT
		ResultSet rs = (ResultSet) cs.getObject(1);

		// lista resultado
		while (rs.next()) {
		    System.out.println(rs.getInt(1) + ", " +
		        rs.getString(2) + ", " +
		        rs.getDate(3).toString() + ", " +
		        rs.getString(4));
		}		
		
		// fecha conexão
		cn.close();
	}

}
