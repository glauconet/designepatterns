package JDBC.jdbc;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ExemploDriverManager {

	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		
		String url = "jdbc:derby://localhost:1527/cursojava";
		String username = "user";
		String password = "senha";
		
		// registrar Driver
		Class.forName("org.apache.derby.jdbc.ClientDriver");
		//new org.apache.derby.jdbc.ClientDriver();
		
		// obter conexão
		Connection cn = DriverManager.getConnection(url, username, password);
			
		// mostra se conexão está válida
		System.out.println(cn.isValid(10));
			
		// fecha conexão
		cn.close();

	}

}
