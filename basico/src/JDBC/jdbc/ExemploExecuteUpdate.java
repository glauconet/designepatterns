package JDBC.jdbc;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ExemploExecuteUpdate {

	public static void main(String[] args) throws SQLException {
		String url = "jdbc:h2:tcp://localhost/~/cursojava";
		String username = "sa";
		String password = "";
		
		// obtem conexão
		Connection cn = DriverManager.getConnection(url, username, password);
		
		// usando Statement
		Statement st = cn.createStatement();
		int quant1 = st.executeUpdate("insert into hobby (nome, data, hobby) values ('marquinhos', '11/15/1999', 'lego')");
		
		// usando PreparedStatement
		PreparedStatement ps = cn.prepareStatement("insert into hobby (nome, data, hobby) values (?, ?, ?)");
		ps.setString(1, "bruninho");
		ps.setString(2, "06/27/1997");
		ps.setString(3, "futebol");
		int quant2 = ps.executeUpdate();
		
		//recupera id gerado
		ResultSet rs = ps.getGeneratedKeys();	
		if (rs.next()) {
			long id = rs.getLong(1);
			System.out.println(id);
		}
		
		// lista resultado
		System.out.println(quant1);
		System.out.println(quant2);
		
		// fecha conexão
		cn.close(); 
	}

}
