package JDBC.jdbc;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ExemploPreparedStatement {

	public static void main(String[] args) throws SQLException {
		String url = "jdbc:h2:tcp://localhost/~/cursojava";
		String username = "sa";
		String password = "";
		
		// obtem conexão
		Connection cn = DriverManager.getConnection(url, username, password);
		
		// obtém PreparedStatement
		PreparedStatement st = cn.prepareStatement("select id, nome, data, hobby from hobby where hobby = ?");
		
		// monta parâmetros
		st.setString(1, "ciclismo");
		
		// executa query
		ResultSet rs = st.executeQuery();
		
		// lista resultado
		while (rs.next()) {
			System.out.println(rs.getInt(1));
			System.out.println(rs.getString(2));
			System.out.println(rs.getDate("data"));
			System.out.println(rs.getString("hobby"));
		}
		
		// fecha conexão
		cn.close(); 
	}

}
