package JDBC.jdbc;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ExemploProperties {

	public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException {
		final String DRIVER;
		final String DB_URL;
		final String USER;
		final String PASSWORD;

		// obter dados de arquivo properties
		Properties props = new Properties();
		props.load(new FileInputStream("jdbc.properties"));
		DRIVER = props.getProperty("jdbc.driver");
		USER = props.getProperty("jdbc.username");
		DB_URL = props.getProperty("jdbc.url");
		PASSWORD = props.getProperty("jdbc.password");
			
		// registrar Driver
		Class.forName(DRIVER);
		
		// obter conexão
		Connection cn = DriverManager.getConnection(DB_URL, USER, PASSWORD);
			
		// mostra se conexão está válida
		System.out.println(cn.isValid(10));
			
		// fecha conexão
		cn.close();

	}

}
