package JDBC.jdbc;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ExemploResultSetInsert {

	public static void main(String[] args) throws SQLException {
		String url = "jdbc:h2:tcp://localhost/~/cursojava";
		String username = "sa";
		String password = "";
		
		// obtem conexão
		Connection cn = DriverManager.getConnection(url, username, password);

		// auto-commit desligado
		cn.setAutoCommit(false);
		
		// obtém Statement
		Statement st = cn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
										  ResultSet.CONCUR_UPDATABLE,
										  ResultSet.CLOSE_CURSORS_AT_COMMIT
										 );
	
		// executa query
		ResultSet rs = st.executeQuery("select id, nome, data, hobby from hobby");
		
		// Inicia inserção de nova linha
		rs.moveToInsertRow();
		
		// atualiza dados da linha
		rs.updateString(2, "mariazinha");
		rs.updateString(3, "09/22/1993");
		rs.updateString(4, "caminhada");
		
		// Inclui a nova linha ao ResultSet
		rs.insertRow();

		// Move cursor para a posição original
		rs.moveToCurrentRow();
		
		// Commit
		cn.commit();
		
		// fecha conexão
		cn.close();

	}

}
