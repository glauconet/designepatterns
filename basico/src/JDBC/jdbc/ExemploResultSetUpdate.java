package JDBC.jdbc;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ExemploResultSetUpdate {

	public static void main(String[] args) throws SQLException {
		String url = "jdbc:h2:tcp://localhost/~/cursojava";
		String username = "sa";
		String password = "";
		
		// obtem conexão
		Connection cn = DriverManager.getConnection(url, username, password);

		// auto-commit desligado
		cn.setAutoCommit(false);
		
		// obtém Statement
		Statement st = cn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
										  ResultSet.CONCUR_UPDATABLE,
										  ResultSet.CLOSE_CURSORS_AT_COMMIT
										 );
		
		// executa query
		ResultSet rs = st.executeQuery("select id, nome, data, hobby from hobby");
		
		// Move a última linha
		rs.last();
		
		// atualiza dados da linha
		rs.updateString(2, "joaozinho");
		rs.updateString(3, "06/09/1988");
		rs.updateString(4, "natação");
		
		// atualiza banco com os dados da linha
		rs.updateRow();
		
		// Commit
		cn.commit();
		
		// fecha conexão
		cn.close();
	}

}
