package JDBC.jdbc;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ExemploStatement {
	
	public static void main(String[] args) throws SQLException {
		String url = "jdbc:h2:tcp://localhost/~/cursojava";
		String username = "sa";
		String password = "";
		
		// obtem conexão
		Connection cn = DriverManager.getConnection(url, username, password);
		
		// obtém Statement
		Statement st = cn.createStatement();
		
		// executa query
		ResultSet rs = st.executeQuery("select id, nome, data, hobby from hobby");
		
		// lista resultado
		while (rs.next()) {
//			System.out.printf("%1$d  %2$20s  %3$td/%3$tm/%3$tY  %4$s \n", rs.getInt(1), rs.getString(2), rs.getDate("data"), rs.getString("hobby"));
			System.out.println(rs.getInt(1));
			System.out.println(rs.getString(2));
			System.out.println(rs.getDate("data"));
			System.out.println(rs.getString("hobby"));
		}
		
		// fecha conexão
		cn.close(); 
		
	}	
}
