package JDBC.jdbc;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;


public class ExemploUpdateBatch {

	public static void main(String[] args) throws SQLException {
		String url = "jdbc:h2:tcp://localhost/~/cursojava";
		String username = "sa";
		String password = "";
		
		// obtem conexão
		Connection cn = DriverManager.getConnection(url, username, password);

		// garante que autocommit está desligado
		cn.setAutoCommit(false);
		
		// obtem statement
		Statement st = cn.createStatement();
	
		// inclui comandos batch
		st.addBatch("insert into hobby (nome, data, hobby) values ('florzinha', '01/15/1999', 'jardinagem')");
		st.addBatch("insert into hobby (nome, data, hobby) values ('lindinha', '01/16/1999', 'pintura')");
		st.addBatch("insert into hobby (nome, data, hobby) values ('docinho', '01/17/1999', 'culinaria')");
		st.addBatch("insert into hobby (nome, data, hobby) values ('utônio', '11/09/1970', 'computador')");
		
		// executa
		int[] quants = st.executeBatch();
		
		// Commit
		cn.commit();
		
		// lista resultado
		for (int quant : quants) {
			System.out.println(quant);
		}
		
		cn.close();
		
	}

}
