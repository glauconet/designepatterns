package JDBC.jdbc;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ExemploUpdateBatchPrepared {

	public static void main(String[] args) throws SQLException {
		String url = "jdbc:h2:tcp://localhost/~/cursojava";
		String username = "sa";
		String password = "";
		
		// obtem conexão
		Connection cn = DriverManager.getConnection(url, username, password);

		// garante que autocommit está desligado
		cn.setAutoCommit(false);
		
		// obtem statement
		PreparedStatement ps = cn.prepareStatement("insert into hobby (nome, data, hobby) values (?, ?, ?)");
	
		// inclui comandos batch
		ps.setString(1, "bob esponja");
		ps.setString(2, "01/01/2000");
		ps.setString(3, "caçar água viva");
		ps.addBatch();
		
		ps.setString(1, "patrick");
		ps.setString(2, "02/12/2000");
		ps.setString(3, "ver tv");
		ps.addBatch();
		
		ps.setString(1, "lula molusco");
		ps.setString(2, "07/20/1995");
		ps.setString(3, "tocar clarinete");
		ps.addBatch();
		
		// executa
		int[] quants = ps.executeBatch();
		
		// Commit
		cn.commit();
		
		// lista resultado
		for (int quant : quants) {
			System.out.println(quant);
		}
		
		cn.close();
	}

}
