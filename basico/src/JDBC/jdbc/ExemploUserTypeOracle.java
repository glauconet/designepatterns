package JDBC.jdbc;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.sql.Date;
import java.util.Map;


public class ExemploUserTypeOracle {
	public static void main(String[] args) throws SQLException {
		String url = "jdbc:oracle:thin:@10.50.240.62:1521:toc20a";
		String username = "testjava";
		String password = "nova";
		
		// obtem conexão
		Connection cn = DriverManager.getConnection(url, username, password);
		
		// mapeando o tipo necessário
		Map<String,Class<?>> typeMaps = cn.getTypeMap();
		typeMaps.put(UserHobby.ORACLE_OBJECT_NAME, UserHobby.class);
		
		// obtém PreparedStatement
		CallableStatement cs = cn.prepareCall("{ call ? := list_hobbies2 }");

		// define a instância de TypeUser como paramêtro "usu" da procedure
		cs.setObject("usu", UserHobby.class);
		
		//todo: o resto...
		
		// monta parâmetros
		cs.setObject(1, "jose");
		cs.setString(2, "22/11/1966");
		cs.setString(3, "ciclismo");
		cs.registerOutParameter(4, java.sql.Types.INTEGER);
		
		// executa sp
		cs.executeUpdate();
		
		// Recupera parâmetro OUT
		int id = cs.getInt(4);
		
		// lista resultado
		System.out.println(id);
		
		// fecha conexão
		cn.close();
	}

}

class UserHobby implements SQLData {
	//O nome do tipo declarado no Oracle
	public static final String ORACLE_OBJECT_NAME = "OBJ_HOBBY";
	//O nome do array declarado no Oracle
	public static final String ORACLE_USER_ARRAY_NAME = "ARRAY_HOBBY";
	 
	//Os atributos
	private int id;
	private String nome;
	private String hobby;
	private Date data;
	
	public UserHobby() {
	}

	//Getter retorna o nome do tipo ao JDBC
	public String getSQLTypeName() throws SQLException {
	    return ORACLE_OBJECT_NAME;
	}
	
	@Override
	public void readSQL(SQLInput stream, String typeName) throws SQLException {
	    setId(stream.readInt());
		setNome(stream.readString());
	    setData(stream.readDate());
	    setHobby(stream.readString());
	}

	@Override
	public void writeSQL(SQLOutput stream) throws SQLException {
	    
		stream.writeInt(getId());
		stream.writeString(getNome());
	    stream.writeDate(getData() != null ?
	            new java.sql.Date(getData().getTime()) : null);
	    stream.writeString(getHobby());
	}

	//getters e setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getHobby() {
		return hobby;
	}

	public void setHobby(String hobby) {
		this.hobby = hobby;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
	 
}