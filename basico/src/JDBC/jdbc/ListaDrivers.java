package JDBC.jdbc;
import java.sql.Driver;
import java.sql.DriverManager;
import java.util.Enumeration;

public class ListaDrivers {

	public static void main(String[] args) {
		//Class.forName("org.apache.derby.jdbc.ClientDriver");
		
		// lista drivers registrados
		Enumeration<Driver> e = DriverManager.getDrivers();
		while (e.hasMoreElements()) {
			System.out.println(e.nextElement().getClass().getName());
		}

	}

}
