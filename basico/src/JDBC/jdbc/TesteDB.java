package JDBC.jdbc;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Enumeration;

public class TesteDB {

	public static void main(String[] args) throws SQLException {
		String url = "jdbc:h2:tcp://localhost/~/cursojava";
		String username = "sa";
		String password = "";
//		String url = "jdbc:oracle:thin:@10.50.240.62:1521:toc20a";
//		String username = "testjava";
//		String password = "nova";
		Connection cn = null;
		
		// Obtain a connection
		cn = DriverManager.getConnection(url, username, password);

		// lista drivers registrados
		Enumeration<Driver> e = DriverManager.getDrivers();
		while (e.hasMoreElements()) {
			System.out.println(e.nextElement().getClass().getName());
		}

		// verifica se suporta batch
		DatabaseMetaData dmd = cn.getMetaData();
		System.out.println(dmd.supportsGetGeneratedKeys());
		
		// obtem Statement
		Statement st = cn.createStatement();
		
		System.out.println(st.getClass().getName());
		
		cn.close(); // Close the connection
	}

}
