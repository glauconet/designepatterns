package modulo01;
// Exemplo de m�todos, vari�veis e atributos 
import java.util.Date;
public class ExemploMetodo {
private int  meuAtributo = 0;    // pode ser usado por qualquer m�todo
  public static void main(String[] args) {
    Date today = new Date();    // today representa uma vari�vel
    System.out.println(today);
    imprimir();   // executando o m�todo imprimir
}
  public static void imprimir() {
    // esta vari�vel poder� ser usada somente no m�todo imprimir()
    int  minhaVariavel = 0;
    System.out.println("m�todo imprimir");
  }
}




