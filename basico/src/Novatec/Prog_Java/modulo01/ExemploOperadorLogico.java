package modulo01;

// Exemplo do uso do comando if com os operadores &&, ||, &, e |
// A classe Console precisa estar dispon�vel no pacote modulo01.estudodecaso.util
import modulo01.estudodecaso.util.Console;

public class ExemploOperadorLogico {
	public static void main(String args[]) {
		if (Console.readInt("\nN�mero 1: ") > 10
				&& Console.readInt("N�mero 2: ") > 10) {
			System.out.println("Os dois n�meros s�o maiores que 10");
		} else {
			System.out
					.println("O primeiro ou o segundo n�mero n�o � maior que 10");
		}
		if (Console.readInt("\nN�mero 3: ") > 10
				|| Console.readInt("N�mero 4: ") > 10) {
			System.out.println("Um dos n�meros � maior que 10");
		} else {
			System.out
					.println("O terceiro ou o quarto n�mero n�o � maior que 10");
		}
		if (Console.readInt("\nN�mero 5: ") > 10
				& Console.readInt("N�mero 6: ") > 10) {
			System.out.println("Os dois n�meros s�o maiores que 10");
		} else {
			System.out.println("O quinto ou o sexto n�mero n�o � maior que 10");
		}
		if (Console.readInt("\nN�mero 7: ") > 10
				| Console.readInt("N�mero 8: ") > 10) {
			System.out.println("Um dos n�meros � maior que 10");
		} else {
			System.out
					.println("O s�timo ou o oitavo n�mero n�o � maior que 10");
		}
	}
}