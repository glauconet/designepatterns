package modulo01;
//� Exemplo do comando switch. Uma alternativa ao uso do comando if 
import java.io.IOException;
public class ExemploSwitch {
	public static void main(String[] args) throws IOException {
			System.out.println(" Digite uma das letras da palavra Java: ");
			// L� do teclado apenas um caracter
			int numero = System.in.read();
			switch ((char) numero) {
			case 'J':
			case 'j':
			case 'A':
			case 'a':
			case 'V':
			case 'v':
				System.out.println("Letra digitada esta correta.");
				break;
			case (char) 13: // utilizado para tratar o enter
				System.out.println("Foi digitado apenas um <enter>.");
				break;
			default:
				System.out.println("Letra digitada esta Incorreta.");
			}
	}
}
