package modulo01.exemplosbasicos;
//� Resposta do exerc�cio 11
public class Estoque {
	String nomeProduto;
	int quantidade;
	double valor;
	public void imprimir() {
			System.out.println("Nome do Produto: " + this.nomeProduto);
			System.out.println("Quantidade do Produto: " + this.quantidade);
			System.out.println("Valor do Produto: " + this.valor);
	}
	public int verificarDisponibilidade (int quant) {
			if ((this.quantidade > 0) && (this.quantidade >= quant)) {
				return 1;
			}
			return 0;
	}
	public int removerProdutos (int quant) {
			int ret = verificarDisponibilidade(quant);
			if (ret == 1) {
				this.quantidade = this.quantidade - quant;
				return 1;
			}
			return 0;
	}
}
