package modulo01.exemplosbasicos;
// Resposta do exerc�cio 06
public class PrincipalComputador {
	public static void main(String[] args) {
			Computador novoObj = new Computador();
			novoObj.marca = "HP";
			novoObj.cor = "Preto";
			novoObj.modelo = "DV6383";
			novoObj.nSerie = 987654312;
			novoObj.preco = 3000;
			System.out.println("Imprimindo os dados inicializados");
			novoObj.imprimir();
			novoObj.calcularValor();
			System.out.println();
			System.out.println("Imprimindo os dados ap�s a execu��o do m�todo calcularValor");
			novoObj.imprimir();
			Computador novoObj01 = new Computador();
			novoObj01.marca = "IBM";
			novoObj01.cor = "Branco";
			novoObj01.modelo = "IBM583";
			novoObj01.nSerie = 9873312;
			novoObj01.preco = 4000;
			novoObj01.calcularValor();
			System.out.println();
			System.out.println("Imprimindo dados ap�s a execu��o do m�todo calcularValor");
			novoObj01.imprimir();
			int ret = novoObj01.alterarValor(2000);
			if (ret > 0) {
				System.out.println("Valor alterado");
			} else {
				System.out.println("Valor N�O alterado");
			}
			System.out.println();
			System.out.println("Imprimindo dados ap�s a execu��o do m�todo alterarValor");
			novoObj01.imprimir();
			System.out.println();
			System.out.println("Executando o m�todo alterarValor com valor negativo");
			ret = novoObj01.alterarValor(-1300);
			if (ret > 0) {
				System.out.println("Valor alterado");
			} else {
				System.out.println("Valor N�O alterado");
			}
			System.out.println();
			System.out.println("Imprimindo dados ap�s a execu��o do m�todo alterarValor");
			novoObj01.imprimir();
	}
}
