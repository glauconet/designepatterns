package modulo01.exemplosbasicos;
//� Resposta do exerc�cio 08
import java.util.Scanner;
public class PrincipalContaCorrente {
	ContaCorrente cc = new ContaCorrente();
	public static void main(String[] args) {
			PrincipalContaCorrente obj = new PrincipalContaCorrente();
			int op = 0;
			while (op != 9) {
				Scanner sc = new Scanner(System.in);
				System.out.println("1 - Cadastrar");
				System.out.println("2 - Saque");
				System.out.println("3 - Deposito");
				System.out.println("4 - Consultar Saldo");
				System.out.println("9 - Sair");
				System.out.println("Entre com uma op��o: ");
				op = sc.nextInt();
				switch (op) {
				case 1:
					obj.execCadastrar();
					break;
				case 2:
					obj.execSaque();
					break;
				case 3:
					obj.execDeposito();
					break;
				case 4:
					obj.execConsulta();
					break;
				}
			}
	}
	public void execDeposito() {
			Scanner sc = new Scanner(System.in);
			System.out.println("Entre com o valor para o deposito: ");
			double valor = sc.nextDouble();
			this.cc.depositar(valor);
			System.out.println("Deposito realizado");
		}
	public void execSaque() {
			Scanner sc = new Scanner(System.in);
			System.out.println("Entre com o valor para o saque: ");
			double valor = sc.nextDouble();
			int ret = this.cc.sacar(valor);
			if (ret == 1) {
				System.out.println("Saque realizado");
			} else {
				System.out.println("Saque N�O realizado");
			}
	}
	public void execConsulta() {
			this.cc.imprimir();
	}
	public void execCadastrar() {
			// Para permitir que seja feita a leitura de um nome composto
			Scanner sc = new Scanner(System.in).useDelimiter("\r\n");
			System.out.println("Entre com o nome do cliente: ");
			this.cc.nomeCliente = sc.nextLine();
			System.out.println("Entre com o n�mero da ag�ncia: ");
			this.cc.agencia = sc.nextInt();
			System.out.println("Entre com o n�mero da conta: ");
			this.cc.conta = sc.nextInt();
			System.out.println("Entre com o saldo do cliente: ");
			this.cc.saldo = sc.nextDouble();
	}
}
