package modulo02;

// Exemplo utilizando for avan�ado com um vetor de strings
public class ExemploForAvancado {
	public static void main(String[] args) {
		String[] string = { "www.", "viva", "o", "java", ".com", ".br", "\n" };
		for (String i : string) {
			System.out.print(i);
		}
	}
}