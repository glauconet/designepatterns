package modulo02;

// Usando o comando for avan�ado e for tradicional
import java.util.Scanner;

public class ExemploForAvancadoTradicional {
	public static void main(String arg[]) {
		Scanner sc = new Scanner(System.in);
		System.out.print("N�mero de elementos: ");
		int tamanho = sc.nextInt();
		int vetor[] = new int[tamanho];
		if (tamanho > 0) {
			// for tradicional realizando a leitura dos dados via teclado
			for (int i = 0; i < tamanho; i++) {
				System.out.print("Elemento[" + i + "]? ");
				vetor[i] = sc.nextInt();
			}
			int soma = 0;
			for (int elemento : vetor) {
				soma += elemento; // soma os elementos
			}
			System.out.println("M�dia = " + soma / tamanho);
		} else {
			System.out.println("Valor lido � inv�lido.");
		}
	}
}