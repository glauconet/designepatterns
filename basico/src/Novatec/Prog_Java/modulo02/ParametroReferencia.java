package modulo02;
// Exemplo da passagem de par�metros usando tipos refer�ncia
public class ParametroReferencia {
int meuAtributo = 0;
// m�todo construtor sem argumentos. Utilizado na cria��o do objeto
public ParametroReferencia() {}
// m�todo construtor com um argumento do tipo inteiro
public ParametroReferencia(int arg) {
this.meuAtributo = arg;
}
public void alterarValor(int arg) {
this.meuAtributo = arg;
}
public String toString() {
// a classe Integer � uma classe wrapper abordado no cap�tulo 4
return Integer.toString(this.meuAtributo);
}
public void alterar01() {
ParametroReferencia y = new ParametroReferencia(1);
System.out.println(y.toString());
alterar02(y);
System.out.println(y.toString());
}
public void alterar02(ParametroReferencia param) {
// este m�todo est� alterando o valor do atributo da classe
param.alterarValor(2);
System.out.println(param.toString());
}
public static void main(String[] args) {
ParametroReferencia obj = new ParametroReferencia();
obj.alterar01();
}
}