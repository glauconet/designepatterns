package modulo02.estudodecaso.model;

// Resposta do Laborat�rio 2. Classe Lab02ContaCorrente
public class Lab02ContaCorrente {
	public int numAge = 0;

	public int numConta = 0;

	public String nome = "";

	public double saldo = 0.0;

	public int sacar(double p_valor) {
		if (this.saldo < p_valor)
			return 0; // saque insuficiente
		this.saldo -= p_valor;
		return 1; // saque suficiente
	}

	public void depositar(double p_valor) {
		this.saldo += p_valor;
	}

	public Lab02ContaCorrente() {
	}

	public void imprimir() {
		System.out.println("N�mero da ag�ncia: " + this.numAge);
		System.out.println("N�mero da conta: " + this.numConta);
		System.out.println("Nome do cliente: " + this.nome);
		System.out.println("Saldo do cliente: " + this.saldo);
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getNumAge() {
		return this.numAge;
	}

	public void setNumAge(int numAge) {
		this.numAge = numAge;
	}

	public int getNumConta() {
		return this.numConta;
	}

	public void setNumConta(int numConta) {
		this.numConta = numConta;
	}

	public double getSaldo() {
		return this.saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
}