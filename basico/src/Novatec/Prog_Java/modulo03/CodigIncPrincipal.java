package modulo03;

// Classe com o m�todo main
import java.util.Scanner;

public class CodigIncPrincipal {
	public static void main(String[] args) {
		CodigoIncContaCorrente[] contaVet = new CodigoIncContaCorrente[3];
		String nome;
		int conta;
		int agencia;
		double saldo;
		Scanner sc = new Scanner(System.in).useDelimiter("\r\n");
		for (int i = 0; i < contaVet.length; i++) {
			System.out.println("--------------------------------");
			System.out.println("Digite o n�mero de uma conta:");
			conta = sc.nextInt();
			System.out.println("Digite o n�mero de uma ag�ncia:");
			agencia = sc.nextInt();
			System.out.println("Digite o nome do cliente:");
			nome = sc.next();
			System.out.println("Digite o saldo do cliente:");
			saldo = sc.nextDouble();
			System.out.println("--------------------------------");
			contaVet[i] = new CodigoIncContaCorrente(conta, agencia, nome,
					saldo);
		}
		System.out.println("\nListando as contas cadastradas");
		for (int i = 0; i < contaVet.length; i++) {
			contaVet[i].imprimirConta();
		}
	}
}