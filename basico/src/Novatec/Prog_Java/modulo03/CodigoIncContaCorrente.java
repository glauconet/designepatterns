package modulo03;

// Classe de neg�cio
public class CodigoIncContaCorrente {
	int agencia;

	int conta;

	String nome;

	double saldo;

	int numeroSequencial;

	public CodigoIncContaCorrente(int conta, int agencia, String nome,
			double saldo) {
		this.numeroSequencial = CodigoIncNumeroUnico.obterNumeroUnico();
		this.agencia = agencia;
		this.conta = conta;
		this.nome = nome;
		this.saldo = saldo;
	}

	public void imprimirConta() {
		System.out.println("------------------------------------");
		System.out.println("N�mero seq�encial: " + this.numeroSequencial);
		System.out.println("Ag�ncia: " + this.agencia);
		System.out.println("Conta: " + this.conta);
		System.out.println("Nome: " + this.nome);
		System.out.println("Saldo: " + this.saldo);
	}
}