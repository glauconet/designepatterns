package modulo03;

public class ExemploConstrutorPrivado {

	public static ExemploConstrutorPrivado objeto;

	private ExemploConstrutorPrivado() {
		super();
	}

	public static ExemploConstrutorPrivado obtemObjeto() {
		if (objeto == null) {
			System.out.println("Criando objeto");
			objeto = new ExemploConstrutorPrivado();
		}
		return objeto;
	}

	public void imprimir() {
		System.out.println("Metodo imprimir");
	}
}
