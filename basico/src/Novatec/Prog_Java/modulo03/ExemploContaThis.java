package modulo03;

// Exemplo do uso da vari�vel this
public class ExemploContaThis {
	private int agencia = 0;

	private int conta = 0;

	public ExemploContaThis(int agencia, int conta) {
		// apesar do atributo e par�metros serem iguais o this resolve esta
		// ambig�idade
		this.agencia = agencia;
		this.conta = conta;
	}

	// construtor vazio
	public ExemploContaThis() {
		// executando o construtor com assinatura igual a dois par�metros
		// inteiros
		this(0, 0);
	}

	public static void main(String[] args) {
		ExemploContaThis obj1 = new ExemploContaThis();
		System.out.println("----------------------------------------");
		System.out.println("Ag�ncia e conta com valores zerados.");
		System.out.println("Ag�ncia: " + obj1.getAgencia());
		System.out.println("Conta: " + obj1.getConta());
		System.out.println("----------------------------------------");
		System.out.println("Ag�ncia e conta com valores.");
		ExemploContaThis obj2 = new ExemploContaThis(3, 963210);
		System.out.println("Ag�ncia: " + obj2.getAgencia());
		System.out.println("Conta: " + obj2.getConta());
	}

	public int getAgencia() {
		return this.agencia;
	}

	public int getConta() {
		return this.conta;
	}
}