package modulo03;

// Exemplo do uso do conceito de overload
public class ExemploOverload {
	public static void main(String args[]) {
		System.out.println("Apresentando o conceito de overload.");
	}

	int imprimir() {
		return 0;
	}

	int imprimir(int x, float y) {
		return 0;
	}

	boolean imprimir(int x) {
		return true;
	}

	// Isto gera um erro. Assinatura igual ao pr�ximo m�todo.
	int imprimir(float x, int y) {
		return 0;
	}
	/*
	 * m�todo com assinatura id�ntica ao m�todo int imprimir(float x, int y).
	 * Vai gerar erro de compila��o caso o coment�rio seja retirado.
	 */
	/*
	 * boolean imprimir(float x, int y) { return true; }
	 */
}