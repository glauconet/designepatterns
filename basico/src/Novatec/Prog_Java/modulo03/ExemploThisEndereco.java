package modulo03;

// Exemplo do uso da vari�vel this com retorno de um refer�ncia � Classe ExemploThisEndereco
public class ExemploThisEndereco {
	String cidade;

	String complemento;

	String estado;

	String rua;

	int numero;

	int cep;

	public ExemploThisEndereco(String rua, int numero, String cidade,
			String estado) {
		this(rua, numero, "", cidade, estado, 0);
	}

	public ExemploThisEndereco(String rua, int numero, String complemento,
			String cidade, String estado, int cep) {
		this.complemento = complemento;
		this.cidade = cidade;
		this.estado = estado;
		this.cep = cep;
		this.rua = rua;
		this.numero = numero;
	}

	ExemploThisEndereco definirrCep(int cep) {
		this.cep = cep;
		return this;
	}

	ExemploThisEndereco definirComplemento(String complemento) {
		this.complemento = complemento;
		return this;
	}

	void imprimir() {
		System.out.println("Rua: " + this.rua);
		System.out.println("N�mero: " + this.numero);
		System.out.println("Complemento: " + this.complemento);
		System.out.println("Cep: " + this.cep);
		System.out.println("Cidade: " + this.cidade);
		System.out.println("Estado: " + this.estado);
	}
}