package modulo03;

public class ExemploThisPrincipal {
	public static void main(String[] args) {
		ExemploThisEndereco objEndereco1;
		ExemploThisEndereco objEndereco2;
		ExemploThisEndereco objEndereco3;
		System.out.println("\nCriando objetos de Endere�o");
		objEndereco1 = new ExemploThisEndereco("Rua. S�o Jos�", 27, "Apto 201",
				"Curitiba", "PR", 80076540);
		objEndereco2 = new ExemploThisEndereco("Av. Kennedy", 1974,
				"Arauc�ria", "PR");
		objEndereco3 = new ExemploThisEndereco("Av. Luz", 9, "Jandaia do Sul",
				"PR");
		System.out.println("\nImprimindo os valores dos objetos");
		System.out.println("***********************************");
		objEndereco1.imprimir();
		System.out.println("-----------------------------------");
		objEndereco2.imprimir();
		System.out.println("-----------------------------------");
		objEndereco3.imprimir();
		System.out.println("***********************************");
		// usando m�todos encadeados. Isto ocorre devido ao m�todo definirCep
		// retornar this
		objEndereco2.definirComplemento("Cristo Rei").definirrCep(80050352);
		objEndereco3.definirComplemento("�gua Verde");
		objEndereco3.definirrCep(85030980);
		System.out
				.println("\nImprimindo os valores dos objetos ap�s complementos");

		System.out.println("***********************************");
		objEndereco1.imprimir();
		System.out.println("-----------------------------------");
		objEndereco2.imprimir();
		System.out.println("-----------------------------------");
		objEndereco3.imprimir();
		System.out.println("***********************************");
	}
}