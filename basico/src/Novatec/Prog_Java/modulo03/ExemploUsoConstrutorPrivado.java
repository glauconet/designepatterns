package modulo03;

public class ExemploUsoConstrutorPrivado {
	public static void main(String args[]) {
		ExemploConstrutorPrivado obj = ExemploConstrutorPrivado.obtemObjeto();
		obj.imprimir();
		ExemploConstrutorPrivado obj1 = ExemploConstrutorPrivado.obtemObjeto();
		obj1.imprimir();
	}
}
