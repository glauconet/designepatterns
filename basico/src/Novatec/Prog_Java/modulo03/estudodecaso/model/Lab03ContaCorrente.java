/*
 * Created on 06/07/2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package modulo03.estudodecaso.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.util.Locale;

public class Lab03ContaCorrente {
	private int numAge = 0;
	private int numConta = 0;
	private String nome = "";
	private double saldo = 0.0;
	public int sacar(double pValor) {
		if (this.saldo < pValor)
			return 0;
		this.saldo -= pValor;
		return 1;
	}
	public void depositar(double pValor) {
		this.saldo += pValor;
	}
	public Lab03ContaCorrente() {
		super();
	}
	public Lab03ContaCorrente(int pNumAge, int pNumConta) {
		setNumAge(pNumAge);
		setNumConta(pNumConta);
		// Carregar a conta corrente de um arquivo texto
		recuperar();
	}
	/* Implementado devido ao laboratório 04. Desconsiderá-lo no laboratório 3 */
	public boolean removerArquivo() {
		File tArq1;
		tArq1 = new File(this.numAge + "." + this.numConta + ".dat");
		tArq1.delete();
		tArq1 = new File(this.numAge + "." + this.numConta + ".hist");
		tArq1.delete();
		return true;
	}
	public boolean gravar() {
		FileWriter tArq1;
		PrintWriter tArq2;
		try {
			// abrir o aquivo
			tArq1 = new FileWriter(getNumAge() + "." + getNumConta() + ".dat");
			tArq2 = new PrintWriter(tArq1);
			tArq2.println(getNumAge());
			tArq2.println(getNumConta());
			tArq2.println(getNome());
			tArq2.println(getSaldo());
			// fechar o arquivo
			tArq2.close();
			return true;
		}
		catch (IOException tExcept) {
			tExcept.printStackTrace();
			return false;
		}
	}
	private void recuperar() {
		FileReader tArq1 = null;
		BufferedReader tArq2 = null;
		int tQtde = 4;
		try {
			// abrir o arquivo
			tArq1 = new FileReader(getNumAge() + "." + getNumConta() + ".dat");
			tArq2 = new BufferedReader(tArq1);

			// ler atributo/valor e colocar na matriz
			String[] tLinha = new String[tQtde];
			for (int i = 0; i < tQtde; i++) {
				tLinha[i] = tArq2.readLine();
			}
			// fechar o arquivo
			tArq2.close();
			setNumAge(Integer.parseInt(tLinha[0]));
			setNumConta(Integer.parseInt(tLinha[1]));
			setNome(tLinha[2]);
			setSaldo(Double.parseDouble(tLinha[3]));
		} catch (IOException tExcept) {
			tExcept.printStackTrace();
		}
	}
	public Lab03ContaCorrente(int pNumAge, int pNumConta, String pNome,
			double pSaldo) {
		setNumAge(pNumAge);
		setNumConta(pNumConta);
		setNome(pNome);
		setSaldo(pSaldo);
	}
	public void imprimir() {
		System.out.println("------------------------------------------");
		System.out.println("Agencia : " + getNumAge());
		System.out.println("Conta   : " + getNumConta());
		System.out.println("Nome    : " + getNome());
		NumberFormat formatter;
		formatter = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
		formatter.setMinimumFractionDigits(2);
		System.out.println("Saldo   : " + formatter.format(this.saldo));

	}
	public String getNome() {
		return this.nome;
	}
	public double getSaldo() {
		return this.saldo;
	}
	public void setNome(String string) {
		this.nome = string;
	}
	public void setNumAge(int i) {
		if ((i < 0) || (i > 9999))
			System.out.println("Numero da agencia invalido");
		this.numAge = i;
	}
	public void setNumConta(int i) {
		if ((i < 0) || (i > 9999999))
			System.out.println("Numero da conta invalido");
		this.numConta = i;
	}
	public void setSaldo(double d) {
		this.saldo = d;
	}
	public int getNumAge() {
		return this.numAge;
	}
	public int getNumConta() {
		return this.numConta;
	}
}
