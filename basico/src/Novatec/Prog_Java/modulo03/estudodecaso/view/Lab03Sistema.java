package modulo03.estudodecaso.view;

//– Resposta do laboratório 3. Classe Lab03Sistema
import modulo01.estudodecaso.util.Console;
import modulo03.estudodecaso.model.Lab03ContaCorrente;
public class Lab03Sistema {
	int numAge;
	int numConta;
	double valor;
	private void leValores() {
			do {
				this.numAge = Console.readInt("Numero da Agencia : ");
			} while (this.numAge <= 0);
			do {
				this.numConta = Console.readInt("Numero da Conta   : ");
			} while (this.numConta <= 0);
			do {
				this.valor = Console.readDouble("Valor             : ");
			} while (this.valor <= 0.0);
	}
	public void execCadastramento() {
			String nome;
			char opcao;
			leValores();
			do {
				nome = Console.readString("Nome do Cliente   : ");
			} while (nome.equals(""));
			opcao = Console.readChar("Confirma cadastramento (S/N) : ");
			if (opcao != 'S' && opcao != 's') {
				return;
			}
			Lab03ContaCorrente cc1 = new Lab03ContaCorrente(this.numAge, this.numConta, nome, this.valor);
			cc1.gravar();
	}

	public void execSaque() {
			char opcao;
			leValores();

			opcao = Console.readChar("Confirma saque (S/N) : ");
			if (opcao != 'S' && opcao != 's')
				return;
			Lab03ContaCorrente cc1 = new Lab03ContaCorrente(this.numAge, this.numConta);
			if (cc1.sacar(this.valor) == 0) {
				System.out.println("Saldo Indisponivel");
			} else {
				cc1.gravar(); // persiste o saque
				System.out.println("Saque Efetuado com Sucesso");
			}
		}
	public void execDeposito() {
			char opcao;
			leValores();
			opcao = Console.readChar("Confirma deposito (S/N) : ");
			if (opcao != 'S' && opcao != 's')
				return;
			Lab03ContaCorrente cc1 = new Lab03ContaCorrente(this.numAge, this.numConta);
			cc1.depositar(this.valor);
			cc1.gravar(); // grava um novo arquivo contendo o valor atualizado do saldo.
		}
	public void execConsulta() {
			do {
				this.numAge = Console.readInt("Numero da Agencia : ");
			} while (this.numAge <= 0);
			do {
				this.numConta = Console.readInt("Numero da Conta   : ");
			} while (this.numConta <= 0);
			Lab03ContaCorrente cc1 = new Lab03ContaCorrente(this.numAge, this.numConta);
			cc1.imprimir();
		}
	public static void main(String[] args) {
			char opcao;
			Lab03Sistema obj = new Lab03Sistema();
			while (true) {
				System.out.println("Entre com a opcao desejada");
				System.out.println("1 - Cadastramento");
				System.out.println("2 - Saque");
				System.out.println("3 - Deposito");
				System.out.println("4 - Consulta");
				System.out.println("9 - Fim");
				// System.out.println("Opcao : ");
				opcao = Console.readChar("Opcao : ");
				if (opcao == '9')
					break;
				switch (opcao) {
				case '1':
					obj.execCadastramento();
					break;
				case '2':
					obj.execSaque();
					break;
				case '3':
					obj.execDeposito();
					break;
				case '4':
					obj.execConsulta();
					break;
				default:
					System.out.println("Opcao invalida. Reentre.");
				}
			}
	}
	public Lab03Sistema() {
			super();
	}
}
