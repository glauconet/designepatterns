package modulo03.exercicio;

public class ContaCorrente {
	int conta, agencia;

	double saldo;

	String nomeCliente;

	public ContaCorrente(int conta, int agencia, double saldo,
			String nomeCliente) {
		this.conta = conta;
		this.agencia = agencia;
		this.saldo = saldo;
		this.nomeCliente = nomeCliente;
	}

	public int sacar(double valor) {
		if (this.saldo >= valor) {
			this.saldo = this.saldo - valor;
			return 1;
		}
		return 0;
	}

	public void depositar(double valor) {
		this.saldo = this.saldo + valor;
	}

	public void imprimir() {
		System.out.println("N�mero da conta: " + this.conta);
		System.out.println("N�mero da ag�ncia: " + this.agencia);
		System.out.println("Saldo da conta corrente: " + this.saldo);
		System.out.println("Nome do cliente: " + this.nomeCliente);
	}

	public int getAgencia() {
		return this.agencia;
	}

	public void setAgencia(int agencia) {
		this.agencia = agencia;
	}

	public int getConta() {
		return this.conta;
	}

	public void setConta(int conta) {
		this.conta = conta;
	}

	public String getNomeCliente() {
		return this.nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	public double getSaldo() {
		return this.saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
}