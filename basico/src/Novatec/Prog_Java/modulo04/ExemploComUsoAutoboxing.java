package modulo04;

// Usando autoboxing
public class ExemploComUsoAutoboxing {
	public static void main(String args[]) {
		// usando autoboxing para converter o n�mero 15 para um objeto da classe
		// Integer
		Integer temp = 15;
		int k = temp;
		System.out.println(k);
	}
}