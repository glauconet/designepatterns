package modulo04;

// Uso da sobrecarga do m�todo equals
public class ExemploContaEqualsPrincipal {
	public static void main(String args[]) {
		// executando o construtor com dois par�metros
		ExemploContaEquals obj1 = new ExemploContaEquals(10, 20);

		ExemploContaEquals obj2 = new ExemploContaEquals(10, 21);
		if (obj1.equals(obj2)) {
			System.out.println("Contas iguais");
		} else {
			System.out.println("Contas diferentes");
		}
		// executando o construtor com dois par�metros
		ExemploContaEquals obj3 = new ExemploContaEquals(10, 50);
		ExemploContaEquals obj4 = new ExemploContaEquals(20, 50);
		if (obj3.equals(obj4)) {
			System.out.println("Contas iguais");
		} else {
			System.out.println("Contas diferentes");
		}
	}
}