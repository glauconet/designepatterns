package modulo04;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
public class ExemploDateFormat {
	public static void main(String[] args) {
			DateFormat dataPadrao;
			DateFormat data01;
			DateFormat data02;
			DateFormat data03;
			DateFormat data04;
			Date variavelDate = new Date();
			dataPadrao = DateFormat.getDateInstance(DateFormat.DEFAULT);
			data01 = DateFormat.getDateInstance(DateFormat.SHORT);
			data02 = DateFormat.getDateInstance(DateFormat.MEDIUM);
			data03 = DateFormat.getDateInstance(DateFormat.LONG);
			data04 = DateFormat.getDateInstance(DateFormat.FULL);

			System.out.println("Apresentando as datas nos formatos dispon�veis. ");
			System.out.println("Data no formato Padr�o   : " + dataPadrao.format(variavelDate));
			System.out.println("Data no formato SHORT    : " + data01.format(variavelDate));
			System.out.println("Data no formato MEDIUM   : " + data02.format(variavelDate));
			System.out.println("Data no formato LONG     : " + data03.format(variavelDate));
			System.out.println("Data no formato FULL     : " + data04.format(variavelDate));

			dataPadrao = DateFormat.getTimeInstance(DateFormat.DEFAULT);
			data01 = DateFormat.getTimeInstance(DateFormat.SHORT);
			data02 = DateFormat.getTimeInstance(DateFormat.MEDIUM);
			data03 = DateFormat.getTimeInstance(DateFormat.LONG);
			data04 = DateFormat.getTimeInstance(DateFormat.FULL);

			System.out.println("Apresentando as horas nos formatos dispon�veis. ");
			System.out.println("Hora no formato Padr�o   : " + dataPadrao.format(variavelDate));
			System.out.println("Hora no formato SHORT    : " + data01.format(variavelDate));
			System.out.println("Hora no formato MEDIUM   : " + data02.format(variavelDate));
			System.out.println("Hora no formato LONG     : " + data03.format(variavelDate));
			System.out.println("Hora no formato FULL     : " + data04.format(variavelDate));

			//FULL para data e FULL para hora.
			dataPadrao = DateFormat.getDateTimeInstance(DateFormat.FULL , DateFormat.FULL , Locale.UK);
			System.out.println("Apresentando um objeto do tipo dateTime no formato da Inglaterra. ");
			System.out.println("Hora no formato Padr�o   : " + dataPadrao.format(variavelDate));
		}
}
