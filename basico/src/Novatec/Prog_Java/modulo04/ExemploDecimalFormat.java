package modulo04;
import java.text.DecimalFormat;

public class ExemploDecimalFormat {
	public static void main(String[] args){
			int 	valorInteiro1 = 27091974;
			long 	valorInteiro2 = -12042004;
			double	valorReal = 2709.1974;

			DecimalFormat objDecFormat = new DecimalFormat ();

			System.out.println("\nValores sem aplica��o da m�scara: Modelo padr�o. ");
			System.out.println("valorInteiro1: 	" + objDecFormat.format(valorInteiro1));
			System.out.println("valorInteiro2: 	" + objDecFormat.format(valorInteiro2));
			System.out.println("valorReal: 	" + objDecFormat.format(valorReal));
		
			objDecFormat.applyPattern("000,000.00");
			System.out.println("Valores Formatados com a m�scara: 000,000.00");
			System.out.println("valorInteiro1: 	" + objDecFormat.format(valorInteiro1));
			System.out.println("valorInteiro2: 	" + objDecFormat.format(valorInteiro2));
			System.out.println("valorReal: 	" + objDecFormat.format(valorReal));

			objDecFormat.applyPattern("###,##0.00");
			System.out.println("Valores Formatados com a m�scara: ###,##0.00");
			System.out.println("valorInteiro1: 	" + objDecFormat.format(valorInteiro1));
			System.out.println("valorInteiro2: 	" + objDecFormat.format(valorInteiro2));
			System.out.println("valorReal:	" + objDecFormat.format(valorReal));

			// Neste caso a m�scara ser� aplicada com o mesmo conte�do para n�meros positivos e negativos
			objDecFormat.applyPattern("#,##0.0000;(-#,##0.0000)");
			System.out.println("Valores Formatados com a m�scara: #,##0.000;(-#,##0.000)");
			System.out.println("valorInteiro1: 	" + objDecFormat.format(valorInteiro1));
			System.out.println("valorInteiro2: 	" + objDecFormat.format(valorInteiro2));
			System.out.println("valorReal:	" + objDecFormat.format(valorReal));
		
			objDecFormat.applyPattern("R$ 0,000.0000");
			System.out.println("Valores Formatados com a m�scara: 0,000.0000");
			System.out.println("valorInteiro1: 	" + objDecFormat.format(valorInteiro1));
			System.out.println("valorInteiro2: 	" + objDecFormat.format(valorInteiro2));
			System.out.println("valorReal:	" + objDecFormat.format(valorReal));
		}
}
