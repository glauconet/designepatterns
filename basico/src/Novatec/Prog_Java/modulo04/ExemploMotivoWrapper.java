package modulo04;
import java.util.Scanner;
//� Usando os valores m�ximos e m�nimos do tipo primitivo int 
public class ExemploMotivoWrapper {
	public static void main(String[] args) {
			int m�nimo = Integer.MAX_VALUE;
			int m�ximo = Integer.MIN_VALUE;
			Scanner var = new Scanner(System.in);
			int num = 0;
			for (int i = 0; i < 5; i++) {
				System.out.print("Digite um numero inteiro: ");
				num = var.nextInt(); // declara e inicia vari�vel
				if (num < m�nimo) {
					m�nimo = num;
				}
				if (num > m�ximo) {
					m�ximo = num;
				}
			}
			System.out.println("O menor numero eh: " + m�nimo);
			System.out.println("O maior numero eh: " + m�ximo);
		}
}
