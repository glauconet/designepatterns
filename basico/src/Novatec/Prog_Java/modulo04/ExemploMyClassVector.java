package modulo04;

public class ExemploMyClassVector {
	private String nome;

	private String endereco;

	private int telefone;

	public String getEndereco() {
		return this.endereco;
	}

	public String getNome() {
		return this.nome;
	}

	public int getTelefone() {
		return this.telefone;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setTelefone(int i) {
		this.telefone = i;
	}
}