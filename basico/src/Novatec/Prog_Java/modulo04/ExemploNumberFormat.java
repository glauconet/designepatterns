package modulo04;
import java.text.NumberFormat;
import java.util.Locale;
public class ExemploNumberFormat {
  public static void main(String[] args) {
    double valorReal = 270919.1204;
    float valorPercentual = 0.27f;
    int valorInteiro = 27091974;
    Locale loc = new Locale("pt", "BR");
    NumberFormat padrao = NumberFormat.getInstance(loc);
    NumberFormat numero = NumberFormat.getNumberInstance(loc);
    NumberFormat dinheiro = NumberFormat.getCurrencyInstance(loc);
    NumberFormat percentual = NumberFormat.getPercentInstance(loc);
    System.out.println("\nUsando o m�todo getCurrencyInstance()");
    System.out.println("Valor atual:    " + valorReal);
    System.out.println("Valor formatado  " + dinheiro.format(valorReal));
    System.out.println("\nUsando o m�todo getPercentInstance()");
    System.out.println("Valor atual:      " + valorPercentual);
    System.out.println("Valor formatado:    " + percentual.format(valorPercentual));
    System.out.println("\nUsando o m�todo getInstance()");
    System.out.println("Valor atual:      " + valorInteiro);
    System.out.println("Valor formatado:    " + padrao.format(valorInteiro));
    System.out.println("\nUsando o m�todo getNumberInstance(), valor inteiro");
    System.out.println("Valor atual:      " + valorInteiro);
    System.out.println("Valor formatado:    " + numero.format(valorInteiro));
    System.out.println("\nUsando o m�todo getNumberInstance(), valor real");
    System.out.println("Valor atual:      " + valorReal);
    System.out.println("Valor formatado:    " + numero.format(valorReal));
    // para alguns pa�ses existem constantes
    NumberFormat dinheiroUS = NumberFormat.getCurrencyInstance(Locale.US);
    System.out.println("\nUsando o m�todo getCurrencyInstance() para Estados Unidos");
    System.out.println("Valor atual:      " + valorReal);
    System.out.println("Valor formatado:    " + dinheiroUS.format(valorReal));
    NumberFormat dinheiroUK = NumberFormat.getCurrencyInstance(Locale.UK);
    System.out.println("\nUsando o m�todo getCurrencyInstance() para Inglaterra");
    System.out.println("Valor atual:      " + valorReal);
    System.out.println("Valor formatado:    " + dinheiroUK.format(valorReal));
  }
}
