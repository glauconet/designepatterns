package modulo04;

// Programa n�o usando autoboxing
public class ExemploSemUsoAutoboxing {
	public static void main(String args[]) {
		Integer temp = new Integer(15);
		// atribuindo o valor 15 para a vari�vel k
		int k = temp.intValue();
		System.out.println(k);
	}
}