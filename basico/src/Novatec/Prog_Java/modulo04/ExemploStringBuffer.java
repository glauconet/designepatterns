package modulo04;

// Usando a classe StringBuffer
public class ExemploStringBuffer {
	public static void main(String args[]) {
		// uso do m�todo append
		StringBuffer sb = new StringBuffer();
		sb.append("novo texto");
		System.out.println(sb);
		// uso do m�todo insert
		sb.insert(4, " e extraordin�rio");
		System.out.println(sb);
		// usando o m�todo length para retornar o tamanho da string
		System.out.println(sb.length());
	}
}