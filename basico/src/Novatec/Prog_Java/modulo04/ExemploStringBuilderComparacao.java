package modulo04;

// Compara��o da performance entre as classes String, StringBuffer e StringBuilder
public class ExemploStringBuilderComparacao {
	public static void main(String[] args) {
		StringBuffer strBuffer = new StringBuffer("");
		StringBuilder strBuilder = new StringBuilder("");
		String str = new String();
		long i = System.currentTimeMillis();
		long f = 0;
		// calcula o tempo para o objeto do tipo da classe StringBuffer
		for (int q = 0; q < 1000000; q++) {
			strBuffer.append("aaaaa");
			f = System.currentTimeMillis();
		}
		System.out.println("Tempo StringBuffer: " + (f - i));
		i = System.currentTimeMillis();
		f = 0;
		// calcula o tempo para o objeto do tipo da classe StringBuilder
		for (int q = 0; q < 1000000; q++) {
			strBuilder.append("aaaaa");
			f = System.currentTimeMillis();
		}
		System.out.println("Tempo StringBuilder: " + (f - i));
		i = System.currentTimeMillis();
		f = 0;

		// estamos executando uma quantidade menor de vezes. Por�m � mais lento
		// sempre.
		for (int q = 0; q < 20000; q++) {
			str += "aaaaa";
			f = System.currentTimeMillis();
		}
		System.out.println("Tempo String: " + (f - i));
	}
}