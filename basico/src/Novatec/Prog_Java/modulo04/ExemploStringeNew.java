package modulo04;

// Criando uma string a partir de um vetor de caracteres
public class ExemploStringeNew {
	public static void main(String args[]) {
		char[] varArray = { 'J', 'a', 'v', 'a', ' ', '1', '.', '5' };
		String varString = new String(varArray);
		System.out.println("varString: " + varString);
	}
}