package modulo04;
//� Usando a classe Vector 
import java.util.Vector;
public class ExemploVector {
	public static void main(String[] args) {
		/* Estamos criando com 5 elementos e quando alcan�ar o limite de 5 ser�o realocados mais 30 elementos. Se souber de antem�o a quantidade prov�vel inform�-la na cria��o, pois isto ir� reduzir a realoca��o desnecess�ria.
			 */
			Vector <Integer> vet = new Vector <Integer>(5, 30);
			/*
			 * A seguinte linha apresenta erro na vers�o 1.4 ou menor, por n�o possu�rem o conceito de autoboxing dispon�vel
			 */
			vet.add(10); // 10 representa um tipo primitivo
			Integer temp = 2;
			vet.add(temp);
			vet.add(new Integer(3));
			vet.add(new Integer(4));
			vet.add(5);
			vet.add(6);
			/*
			 * Tamanho ser� 6, devido a ter sido executado 6 vezes o comando add. Capacidade ser� de 36, pois ao alcan�ar o valor 5, o pr�prio 
				compilador Java ir� automaticamente adicionar mais 30. 
			 */
			System.out.println ("Tamanho do Vector: " + vet.size());
			System.out.println ("Capacidade do Vector: " + vet.capacity());
			Integer x = (Integer) vet.get(0);
			System.out.println("Valor: "+ x.intValue());
			// ou
			System.out.println("Valor na posi��o 0: "+ ((Integer) vet.get(0)).intValue());
		}
}
