package modulo04;

import java.util.Vector;

public class ExemploVector01 {
	public static void main(String args[]) {
		ExemploMyClassVector obj = new ExemploMyClassVector();
		Vector<ExemploMyClassVector> meuVetor = new Vector<ExemploMyClassVector>(
				5, 50);
		obj.setNome("Larissa Costa");
		obj.setEndereco("Av. S�o Jose");
		obj.setTelefone(33333333);
		meuVetor.add(obj);
		obj = new ExemploMyClassVector();
		obj.setNome("Ronei Franceschi");
		obj.setEndereco("Av. Cristo Rei");
		obj.setTelefone(36224545);
		meuVetor.add(obj);
		/*
		 * torna-se obrigat�rio o uso de cast ((ExemploMyClassVector)) quando
		 * usamos o m�todo get sem o conceito de gen�rico. Isto ocorre devido ao
		 * m�todo get retornar um objeto do tipo Object.
		 */
		obj = (ExemploMyClassVector) meuVetor.get(0);
		System.out.println(obj.getNome());
		System.out.println(obj.getEndereco());
		System.out.println(obj.getTelefone());
		System.out.println(((ExemploMyClassVector) meuVetor.get(0)).getNome());
		System.out.println(((ExemploMyClassVector) meuVetor.get(0))
				.getEndereco());
		System.out.println(((ExemploMyClassVector) meuVetor.get(0))
				.getTelefone());
		System.out.println(((ExemploMyClassVector) meuVetor.get(1)).getNome());
		System.out.println(((ExemploMyClassVector) meuVetor.get(1))
				.getEndereco());
		System.out.println(((ExemploMyClassVector) meuVetor.get(1))
				.getTelefone());
	}
}