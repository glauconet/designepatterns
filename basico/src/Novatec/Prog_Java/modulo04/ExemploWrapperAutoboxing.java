package modulo04;

// Exemplo do uso dos m�todos das classes Wrappers usufruindo do conceito de autoboxing
public class ExemploWrapperAutoboxing {
	public static void main(String[] args) {
		String strNumReal = "100.0";
		/*
		 * o m�todo valueOf retorna um objeto do tipo Double, mas devido ao
		 * autoboxing a convers�o ser� feita automaticamente para o tipo
		 * primitivo double.
		 */
		double numReal = Double.valueOf(strNumReal);
		// retorna um tipo primitivo do tipo double
		double numRealParse = Double.parseDouble(strNumReal);
		System.out.println("numReal: " + numReal);
		System.out.println("numRealParse: " + numRealParse);
		String strNum = "50";
		/*
		 * o m�todo valueOf retorna um objeto do tipo Integer, mas devido ao
		 * autoboxing a convers�o ser� feita automaticamente para o tipo
		 * primitivo int.
		 */
		int num = Integer.valueOf(strNum);
		// retorna um tipo primitivo do tipo int
		int numParse = Integer.parseInt(strNum);
		System.out.println("num: " + num);
		System.out.println("numParse: " + numParse);
	}
}