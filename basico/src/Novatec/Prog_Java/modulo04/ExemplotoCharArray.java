package modulo04;

// Usando o m�todo toCharArray()
public class ExemplotoCharArray {
	public static void main(String[] args) {
		String variavel = "Livro de Java com �nfase em Orienta��o a Objetos.";
		char[] vetorCaracteres = variavel.toCharArray();
		for (int i = variavel.length() - 1; i >= 0; i--) {
			// imprime a frase ao contr�rio
			System.out.print(vetorCaracteres[i]);
		}
	}
}