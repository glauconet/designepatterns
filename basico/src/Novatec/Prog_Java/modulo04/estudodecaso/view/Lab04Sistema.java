package modulo04.estudodecaso.view;
//– Resposta do laboratório 4 – Classe Lab04Sistema 
import modulo01.estudodecaso.util.Console;
import modulo03.estudodecaso.model.Lab03ContaCorrente;
import modulo04.estudodecaso.model.Lab04Historico;
public class Lab04Sistema {
	int numAge;
	int numConta;
	double valor;
	private void leValores() {
			do {
				this.numAge = Console.readInt("Numero da Agencia : ");
			} while (this.numAge <= 0);
			do {
				this.numConta = Console.readInt("Numero da Conta   : ");
			} while (this.numConta <= 0);
			do {
				this.valor = Console.readDouble("Valor             : ");
			} while (this.valor <= 0.0);
	}
	private void leValoresAgCta() {
			do {
				this.numAge = Console.readInt("Numero da Agencia : ");
			} while (this.numAge <= 0);
			do {
				this.numConta = Console.readInt("Numero da Conta   : ");
			} while (this.numConta <= 0);
	}
	public void execCadastramento() {
			String nome;
			char opcao;
			leValores();
			do {
				nome = Console.readString("Nome do Cliente   : ");
			} while (nome.equals(""));
			opcao = Console.readChar("Confirma cadastramento (S/N) : ");
			if (Character.toLowerCase(opcao) != 's')
				return;
			Lab03ContaCorrente cc1 = new Lab03ContaCorrente(this.numAge, this.numConta, nome,this.valor);
			cc1.gravar();
	}
	public void execSaque() {
			char opcao;
			leValores();
			opcao = Console.readChar("Confirma saque (S/N) : ");
			if (Character.toLowerCase(opcao) != 's')
				return;
			Lab03ContaCorrente cc1 = new Lab03ContaCorrente(this.numAge, this.numConta);
			if (cc1.sacar(this.valor) == 0)
				System.out.println("Saldo Indisponivel");
			else {
				System.out.println("Saque Efetuado com Sucesso");
				cc1.gravar(); // persiste o saque
			}
			Lab04Historico hist = new Lab04Historico(this.numAge, this.numConta);
			hist.gravar(1, this.valor);
		}
	public void execDeposito() {
			char opcao;
			leValores();
			opcao = Console.readChar("Confirma deposito (S/N) : ");
			if (Character.toLowerCase(opcao) != 's')
				return;
			Lab03ContaCorrente cc1 = new Lab03ContaCorrente(this.numAge, this.numConta);
			cc1.depositar(this.valor);
			cc1.gravar();
			Lab04Historico hist = new Lab04Historico(this.numAge, this.numConta);
			hist.gravar(2, this.valor);
	}
	public void execExtrato() {
			leValoresAgCta();
			Lab03ContaCorrente cc1 = new Lab03ContaCorrente(this.numAge, this.numConta);
			Lab04Historico hist = new Lab04Historico(this.numAge, this.numConta);
			cc1.imprimir();
			hist.imprimir();
	}
	public void execConsulta() {
			leValoresAgCta();
			Lab03ContaCorrente cc1 = new Lab03ContaCorrente(this.numAge, this.numConta);
			cc1.imprimir();
	}
	public void execRemoverContaCorrente() {
			leValoresAgCta();
			Lab03ContaCorrente cc1 = new Lab03ContaCorrente(this.numAge, this.numConta);
			if (cc1.removerArquivo() == true) {
				System.out.println("Conta Corrente removida com sucesso");
			}
	}
	public static void main(String[] args) {
			char opcao;
			Lab04Sistema obj = new Lab04Sistema();
			while (true) {
				System.out.println("\n\n\n");
				System.out.println("Entre com a opcao desejada");
				System.out.println("1 - Cadastramento");
				System.out.println("2 - Saque");
				System.out.println("3 - Deposito");
				System.out.println("4 - Consulta");
				System.out.println("5 - Extrato");
				System.out.println("8 - Remover Conta Corrente");
				System.out.println("9 - Fim");
				// System.out.println("Opcao : ");
				opcao = Console.readChar("Opcao : ");
				if (opcao == '9')
					break;
				switch (opcao) {
				case '1':
					obj.execCadastramento();
					break;
				case '2':
					obj.execSaque();
					break;
				case '3':
					obj.execDeposito();
					break;
				case '4':
					obj.execConsulta();
					break;
				case '5':
					obj.execExtrato();
					break;
				case '8':
					obj.execRemoverContaCorrente();
					break;
				default:
					System.out.println("Opcao invalida. Reentre.");
				}
			}
	}
	public Lab04Sistema() {
			super();
	}
}
