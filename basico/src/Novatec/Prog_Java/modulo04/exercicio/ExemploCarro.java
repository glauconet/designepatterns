package modulo04.exercicio;

// Classe base para a cria��o de objetos
public class ExemploCarro {
	private int chassi;

	private String marca;

	private String fabricante;

	private String dtFabricacao;

	public int getChassi() {
		return this.chassi;
	}

	public void setChassi(int chassi) {
		this.chassi = chassi;
	}

	public String getDtFabricacao() {
		return this.dtFabricacao;
	}

	public void setDtFabricacao(String dtFabricacao) {
		this.dtFabricacao = dtFabricacao;
	}

	public String getFabricante() {
		return this.fabricante;
	}

	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}

	public String getMarca() {
		return this.marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}
}