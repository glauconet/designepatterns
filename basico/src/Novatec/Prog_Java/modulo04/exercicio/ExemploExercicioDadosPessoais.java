package modulo04.exercicio;

// Classe base para a cria��o de objetos
public class ExemploExercicioDadosPessoais {
	private String nome;

	private String telefone;

	private int idade;

	public int getIdade() {
		return this.idade;
	}

	public void setIdade(int idade) {
		if ((idade <= 0) || (idade > 110)) {
			System.out.println("Idade n�o compat�vel.");
		} else {
			this.idade = idade;
		}
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return this.telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
}
