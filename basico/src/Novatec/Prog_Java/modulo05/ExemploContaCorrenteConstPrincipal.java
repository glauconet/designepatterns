package modulo05;

// Criando objetos e executando construtores
public class ExemploContaCorrenteConstPrincipal {
	public static void main(String[] args) {
		ExemploContaCorrenteEspecialConst tContaEsp = new ExemploContaCorrenteEspecialConst(
				10, 20, 90);
		tContaEsp.imprimir();
		ExemploContaCorrenteEspecialConst tContaEsp1 = new ExemploContaCorrenteEspecialConst();
		tContaEsp1.imprimir();
	}
}