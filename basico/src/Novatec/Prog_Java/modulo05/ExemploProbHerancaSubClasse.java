package modulo05;

// Usando construtor e executando m�todo sobrescrito dentro dele
public class ExemploProbHerancaSubClasse extends ExemploProbHerancaSuperClasse {
	public ExemploProbHerancaSubClasse() {
		// executando o construtor vazio da superclasse
		super();
		overrideMetodo();
	}

	protected void overrideMetodo() {
		System.out.println("M�todo da subclasse");
	}
}