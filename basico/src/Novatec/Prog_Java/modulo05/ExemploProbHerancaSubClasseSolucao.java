package modulo05;

public class ExemploProbHerancaSubClasseSolucao extends
		ExemploProbHerancaSuperClasseSolucao {
	public ExemploProbHerancaSubClasseSolucao() {
		// executando o construtor vazio da superclasse
		super();
		overrideMetodo();
	}

	protected void overrideMetodo() {
		// executando o m�todo overrideMetodo() da superclasse
		super.overrideMetodo();
		System.out.println("M�todo da subclasse");
	}
}