package modulo05;

// Usando construtor e executando m�todo sobrescrito dentro dele
public class ExemploProbHerancaSuperClasse {
	public ExemploProbHerancaSuperClasse() {
		// mesmo for�ando n�o ir� executar conforme esperado
		((ExemploProbHerancaSuperClasse) this).overrideMetodo();
	}

	protected void overrideMetodo() {
		System.out.println("M�todo da superclasse");
	}
}