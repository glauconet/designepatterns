package modulo05.estudodecaso.model;
//� Resposta do laborat�rio 5 � Classe Lab05ContaCorrenteEspecial 
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import modulo03.estudodecaso.model.Lab03ContaCorrente;
public class Lab05ContaCorrenteEspecial extends Lab03ContaCorrente {
	private double limite = 0.0;
	public Lab05ContaCorrenteEspecial() {
			super();	
	}
	public int  sacar (double pValor)	{
			if ( (getSaldo() + getLimite()) < pValor) {
				// Saldo insuficiente
				return 0;
			}
			setSaldo(getSaldo() - pValor);
			return 1; // Saldo suficiente
		}
	public void imprimir(){
			super.imprimir();		
			System.out.println ("__________________________________________");
			System.out.println ("Limite : " + this.limite);
			System.out.println ("------------------------------------------");	  
	}
	public Lab05ContaCorrenteEspecial(int pNumAge, int pNumConta) {
			super(pNumAge, pNumConta);
			recuperarContaEspecial();
	}
	public boolean gravar(){
			FileWriter      tArq1;
			PrintWriter     tArq2;
			try{
				// Opera��o I - Abrir o aquivo
				tArq1 = new FileWriter (getNumAge() + "." + getNumConta() + ".esp");
				tArq2 = new PrintWriter (tArq1);
				tArq2.println (getLimite());
				// Opera��o II - Fechar o arquivo
				tArq2.close();
				super.gravar(); // Grava a conta corrente
				return true;
			}
			catch (IOException tExcept){
				tExcept.printStackTrace();
				return false;
			}
		}
	public boolean removerArquivo() {
			super.removerArquivo();
			File      tArq1;
			tArq1 = new File (getNumAge() + "." + getNumConta() + ".esp");
			tArq1.delete();
			return true;
		}
		private void recuperarContaEspecial(){
			FileReader     tArq1;
			BufferedReader tArq2;
			try{
				// Opera��o I - Abrir o arquivo
				tArq1 = new FileReader (getNumAge() + "." + getNumConta() + ".esp");
				tArq2 = new BufferedReader (tArq1);
				// Opera��o II - Ler atributo/valor e colocar na matriz
				String tLinha= null;
				tLinha = tArq2.readLine();
				// Opera��o III - Fechar o arquivo
				tArq2.close();
				this.limite = Double.valueOf(tLinha).doubleValue();
			}
			catch (IOException tExcept){
				tExcept.printStackTrace();
			}
		}
		public Lab05ContaCorrenteEspecial(
			int pNumAge,
			int pNumConta,
			String pNome,
			double pSaldo,
			double pLimite) {
			super(pNumAge, pNumConta, pNome, pSaldo);
			setLimite(pLimite);
		}
		public double getLimite() {
			return this.limite;
		}
		public void setLimite(double d) {
			this.limite = d;
		}
}	
