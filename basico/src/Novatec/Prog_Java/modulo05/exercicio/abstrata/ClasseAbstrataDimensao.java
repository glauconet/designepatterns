package modulo05.exercicio.abstrata;

// Classe abstrata
public abstract class ClasseAbstrataDimensao {
	protected final int TAMANHO = 2; // valor constante

	public abstract void adicionar(int valor);

	public abstract void imprimir();
}
