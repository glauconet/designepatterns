package modulo05.exercicio.abstrata;
//� Classe abstrata Pessoa 
public abstract class Pessoa {
	String nome;
	public abstract void imprimir();
	public Pessoa() {
			super();
	}
	public Pessoa(String nome) {
			super();
			this.nome = nome;
	}
	public String getNome() {
			return this.nome;
	}
	public void setNome(String nome) {
			this.nome = nome;
	}
}
