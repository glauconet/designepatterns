package modulo05.exercicio.abstrata;
//� Classe PessoaJuridica 
public class PessoaJuridica extends Pessoa {
	private int cnpj;
	private int inscricaoEstadual;
	private String nomefantasia;
	private String razaoSocial;
	public void imprimir() {
			System.out.println("Nome: " + getNome() );
			System.out.println("CNPJ: " + getCnpj());
			System.out.println("Inscri��o Estadual: " + getInscricaoEstadual() );
			System.out.println("Nome Fantasia: " + getNomefantasia() );
			System.out.println("Raz�o Social: " + getRazaoSocial());
	}
	public int getCnpj() {
			return this.cnpj;
	}
	public void setCnpj(int cnpj) {
			this.cnpj = cnpj;
	}
	public int getInscricaoEstadual() {
			return this.inscricaoEstadual;
	}
	public void setInscricaoEstadual(int inscricaoEstadual) {
			this.inscricaoEstadual = inscricaoEstadual;
	}
	public String getNomefantasia() {
			return this.nomefantasia;
	}
	public void setNomefantasia(String nomefantasia) {
			this.nomefantasia = nomefantasia;
	}
	public String getRazaoSocial() {
			return this.razaoSocial;
	}
	public void setRazaoSocial(String razaoSocial) {
			this.razaoSocial = razaoSocial;
	}
	public PessoaJuridica(String nome,int cnpj, int inscricaoEstadual, String nomefantasia, String razaoSocial) {
			super(nome);
			this.cnpj = cnpj;
			this.inscricaoEstadual = inscricaoEstadual;
			this.nomefantasia = nomefantasia;
			this.razaoSocial = razaoSocial;
	}
}
