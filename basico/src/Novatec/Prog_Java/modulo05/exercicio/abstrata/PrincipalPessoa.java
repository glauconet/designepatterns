package modulo05.exercicio.abstrata;
//� Classe PrincipalPessoa 
import java.util.Scanner;
public class PrincipalPessoa {
	Pessoa pessoa = null;
	public static void main(String[] args) {
			PrincipalPessoa obj = new PrincipalPessoa();
			int opcao = 0;
			Scanner sc = new Scanner(System.in);
			while (true) {
				System.out.println("1 - Cadastrar");
				System.out.println("2 - Imprimir");
				System.out.println("9 - Fim");
				System.out.print("Entre com uma op��o: ");
				opcao = sc.nextInt();
				switch (opcao) {
				case 1:
					obj.cadastrar();
					break;
				case 2:
					obj.imprimir();
					break;
				case 9:
						System.exit(0);
						break;
				default:
					System.out.println("Op��o inv�lida.");
				}
			}
	}
	private void imprimir() {
			if (this.pessoa == null) {
				System.out.println("Nenhuma pessoa foi cadastrada.");
			}
			else{
				this.pessoa.imprimir();
			}
	}
	private void cadastrar() {
			int opcao = 0;
			String nome = null;
			Scanner sc = new Scanner(System.in).useDelimiter("\r\n");
			System.out.println("1 - Pessoa Fisica");
			System.out.println("2 - Pessoa Juridica");
			System.out.print("Entre com uma op��o: ");
			opcao = sc.nextInt();
			switch (opcao) {
			case 1:
				System.out.print("Entre com o nome: ");
				nome = sc.next();
				String dtNasc;
				do {
					do {
						System.out
								.print("Entre com a data de nascimento (AAAA/MM/DD): ");
						dtNasc = sc.next();
					} while (dtNasc.length() != 10);
				} while ((dtNasc.toCharArray())[4] != '/'
						|| (dtNasc.toCharArray())[7] != '/');
				System.out.print("Entre com o cpf: ");
				int cpf = sc.nextInt();
				// Executando o construtor passando as vari�veis como par�metro.
				this.pessoa = new PessoaFisica(nome, cpf, dtNasc);
				break;
			case 2:
				System.out.print("Entre com o nome: ");
				nome = sc.next();
				System.out.print("Entre com o cnpj: ");
				int cnpj = sc.nextInt();
				System.out.print("Entre com a inscri��o estadual: ");
				int inscricaoEstadual = sc.nextInt();
				System.out.print("Entre com o nome fantasia: ");
				String nomefantasia = sc.next();
				System.out.print("Entre com a raz�o social: ");
				String razaoSocial = sc.next();
				this.pessoa = new PessoaJuridica(nome, cnpj, inscricaoEstadual,
						nomefantasia, razaoSocial);
				break;
			default:
				System.out.println("Op��o inv�lida.");
			}
		}
}	
