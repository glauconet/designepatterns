package modulo05.exercicio.heranca;

//Classe MatrizBidimensional que estende a classe VetorUnidimensional. Com o comando extends estamos aplicando o conceito de Heran�a
public class MatrizBidimensional extends VetorUnidimensional {
	private int[][] dim2;
	private static int coluna;
	public MatrizBidimensional() {
			// TAMANHO esta dispon�vel nesta classe devido ter sido criado como protected e a classe MatrizBidimensional estender a classe VetorUnidimensional.
			this.dim2 = new int[this.TAMANHO][this.TAMANHO];
		}
		public int[][] getDim2() {
			return this.dim2;
		}
		public MatrizBidimensional(int pLinha, int pColuna) {
			if (((pLinha <= 0) || (pLinha > 2000000)) || ((pColuna <= 0) || (pColuna > 2000000))) {
				System.out.println("Par�metro pLinha ou pColuna fora dos limites.");
			} else {
				this.dim2 = new int[pLinha][pColuna];
			}
		}
	 //Aqui estamos aplicando override. O m�todo adicionar foi tamb�m definido na superclasse com a mesma assinatura (Nome + Par�metros).
		public void adicionar(int valor) {
			this.dim2[linha][coluna] = valor;
			if (linha < this.dim2.length) {
				coluna++;
			}
			// utiliza-se [0] para identificar a quantidade de colunas de cada linha
			if (coluna == this.dim2[linha].length) {
				linha++;
				coluna = 0;
			}
			if (linha == this.dim2.length) {
				linha = 0;
				coluna = 0;
				System.out.println("Matriz foi excedida. O pr�ximo valor ser� colocado na posi��o linha 0 e coluna 0 da matriz. Recome�ando.");
			}
		}
		public void imprimir() {
			// dim2.length - retorna a quantidade de linhas.
			for (int i = 0; i < this.dim2.length; i++) {
				// dim2[0].length - retorna a quantidade de colunas.
				for (int j = 0; j < this.dim2[linha].length; j++) {
					System.out.println("Elemento " + i + " : " + this.dim2[i][j]);
				}
			}
		}
}
