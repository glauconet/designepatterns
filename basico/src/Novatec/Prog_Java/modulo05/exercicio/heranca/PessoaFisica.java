package modulo05.exercicio.heranca;

public class PessoaFisica extends Pessoa {
	private String cpf;
	private String rg;

	public void imprimir(){
		System.out.println("Nome..: " + getNome());
		System.out.println("End...: " + getEndereco());
		System.out.println("C.P.F.: " + this.cpf);
		System.out.println("R.G...: " + this.rg);
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}
	
	
}
