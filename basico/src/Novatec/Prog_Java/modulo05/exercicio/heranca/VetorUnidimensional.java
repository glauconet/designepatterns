package modulo05.exercicio.heranca;

// Classe VetorUnidimensional. Controla o uso de um vetor
public class VetorUnidimensional {
	private int[] dim1;

	protected int linha;

	protected final int TAMANHO = 2; // valor constante

	public VetorUnidimensional() {
		this.dim1 = new int[this.TAMANHO];
	}

	public VetorUnidimensional(int param) {
		if ((param <= 0) || (param > 2000000)) {
			System.out.println("Tamanho para o vetor recebido inv�lido.");
		} else {
			this.dim1 = new int[param];
		}
	}

	public int[] getDim1() {
		return this.dim1;
	}

	public void adicionar(int valor) {
		this.dim1[this.linha] = valor;
		this.linha++;
		if (this.linha == this.dim1.length) {
			System.out
					.println("Vetor foi excedido. O pr�ximo valor ser� colocado na posi��o 0 do vetor. Recome�ando.");
			this.linha = 0;
		}
	}

	public void imprimir() {
		for (int i = 0; i < this.dim1.length; i++) {
			System.out.println("Elemento " + i + ": " + this.dim1[i]);
		}
	}
}