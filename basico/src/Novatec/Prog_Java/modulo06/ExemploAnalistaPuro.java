package modulo06;

import java.util.Scanner;

public class ExemploAnalistaPuro extends ExemploEmpregadoPuro {
	public double definirBeneficios(double total) {
		double economia = 0.0;
		Scanner scan = new Scanner(System.in).useDelimiter("\r\n");
		System.out.println("Entre com o valor da economia do projeto: ");
		economia = scan.nextInt();
		total = total + (0.10 * economia);
		return total;
	}
}