package modulo06;
//–  Implemantação da ExemploClassePrincipal 
public class ExemploClassePrincipal {
	final static int TAM_VETOR = 4;
	public void executarVetor(ExemploClasseAnimal[] p_objAnimal) {
			for (int i = 0; i < TAM_VETOR; i++) {
				p_objAnimal[i].emitirSom();
			}
	}
	public static void main(String[] args) {
			ExemploClassePrincipal pObj = new ExemploClassePrincipal();
			ExemploClasseAnimal objAnimal[] = new ExemploClasseAnimal[TAM_VETOR];
			objAnimal[0] = new ExemploClasseAnimal();
			objAnimal[1] = new ExemploClasseFelinos();
			objAnimal[2] = new ExemploClasseCaninos();
			objAnimal[3] = new ExemploClasseAves();
			pObj.executarVetor(objAnimal);
	}
}
