package modulo06;

// Exemplo do uso de downcasting
/* Aten��o para que as classes importadas neste programa sejam criadas dentro dos respectivos pacotes.
 */
import modulo05.ExemploContaCorrente;
import modulo05.ExemploContaCorrenteEspecial;

public class ExemploContaPrincipalDowncast {
	public static void main(String[] args) {
		ExemploContaCorrente tConta = new ExemploContaCorrente(30, 50,
				"Davi Mendes", 2000);
		tConta.sacar(10);
		// executando o construtor com cinco par�metros
		ExemploContaCorrenteEspecial tContaEsp = new ExemploContaCorrenteEspecial(
				30, 50, "Flora Fernandes", 2000, 4000);
		tContaEsp.sacar(10);
		/*
		 * a vari�vel ref representa apenas uma refer�ncia para a classe
		 * ExemploContaCorrente. Sem a execu��o do operador new a vari�vel ref
		 * n�o representa um objeto.
		 */
		ExemploContaCorrente ref;
		// agora a vari�vel ref representa um objeto
		ref = new ExemploContaCorrenteEspecial(30, 50, "Milton Candido Mendes",
				2000, 9000);
		ref.sacar(10);// ir� executar o m�todo sacar da classe
						// ExemploContaCorrenteEspecial
		tContaEsp.imprimir(); // executa o m�todo imprimir da classe
								// ExemploContaCorrenteEspecial
		// executa o m�todo depositar da classe ExemploContaCorrenteEspecial
		tContaEsp.depositar(10);
		tConta.imprimir(); // executa o m�todo imprimir da classe
							// ExemploContaCorrente
		/*
		 * esta pr�xima linha gera erro de compila��o. A classe
		 * ExemploContaCorrente n�o implementa o m�todo depositar. A classe
		 * ExemploContaCorrente n�o implementa o m�todo depositar, e por isto
		 * vai gerar erro de compila��o. Para que esta linha funcione ser�
		 * necess�rio criar o m�todo depositar em ExemploContaCorrente.
		 */

		// tConta.depositar();
		/*
		 * esta pr�xima linha gera erro de compila��o tamb�m. Apesar do objeto
		 * ref ser instanciado como do tipo da classe
		 * ExemploContaCorrenteEspecial este � do tipo da classe
		 * ExemploContaCorrente. Como a classe ExemploContaCorrente n�o
		 * implementa o m�todo depositar o compilador Java ir� apresentar um
		 * erro de compila��o. Para que esta linha funcione ser� necess�rio usar
		 * downcasting e substituir a linha existente por:
		 * ((ExemploContaCorrenteEspecial)ref).depositar(90); conforme
		 * realizado.
		 */
		// ref.depositar(90);
		((ExemploContaCorrenteEspecial) ref).depositar(90);
	}
}