package modulo06;

// Forma mais eficiente de programar
import java.util.Scanner;

public class ExemploFolhaPagamentoPuro {
	public double calcularFolha(ExemploEmpregadoPuro emp) {
		double total = 0.0;
		total = emp.definirSalario();
		total = emp.definirBeneficios(total);
		return total;
	}

	public static void main(String arg[]) {
		ExemploFolhaPagamentoPuro pr = new ExemploFolhaPagamentoPuro();
		ExemploEmpregadoPuro emp = null;
		while (true) {
			System.out.println("1 - Programador");
			System.out.println("2 - Gerente");
			System.out.println("3 - Analista");
			System.out.println("4 - Fim");
			System.out.println("Escolha o empregado: ");

			Scanner scan = new Scanner(System.in).useDelimiter("\r\n");
			int opcao = scan.nextInt();
			if (opcao == 4)
				System.exit(0);
			switch (opcao) {
			case 1:
				emp = new ExemploProgramadorPuro();
				break;
			case 2:
				emp = new ExemploGerentePuro();
				break;
			case 3:
				emp = new ExemploAnalistaPuro();
				break;
			default:
				System.out.println("Op��o inv�lida");
			}
			System.out.println("Sal�rio total: " + pr.calcularFolha(emp));
		}
	}
}