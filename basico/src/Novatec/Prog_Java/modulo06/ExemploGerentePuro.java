package modulo06;

import java.util.Scanner;

public class ExemploGerentePuro extends ExemploEmpregadoPuro {
	public double definirBeneficios(double total) {
		double bonus = 0.0;
		Scanner scan = new Scanner(System.in).useDelimiter("\r\n");
		System.out.println("Entre com o valor do b�nus: ");
		bonus = scan.nextInt();
		total += bonus;
		return total;
	}
}