package modulo06;

// Subclasse ExemploProgramador da superclasse ExemploEmpregado
import java.util.Scanner;

public class ExemploProgramador extends ExemploEmpregado {
	public double definirBeneficios() {
		double produtividade = 0.0;
		Scanner scan = new Scanner(System.in).useDelimiter("\r\n");
		System.out.println("Entre com o valor da produtividade: ");
		produtividade = scan.nextInt();
		return produtividade;
	}
}