package modulo06;

import java.util.Scanner;

public class ExemploProgramadorPuro extends ExemploEmpregadoPuro {
	public double definirBeneficios(double total) {
		double produtividade = 0.0;
		Scanner scan = new Scanner(System.in).useDelimiter("\r\n");
		System.out.println("Entre com o valor da produtividade: ");
		produtividade = scan.nextInt();
		total = total + (total * produtividade) / 100;
		return total;
	}
}