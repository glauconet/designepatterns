package modulo06;

/* Subclasse que evid�ncia o problema com o uso de polimorfismo, construtor e m�todos
 sobrecarregados. */
public class ExemploSubClasse extends ExemploSuperClasse {
	int atributo = 1;

	// m�todo sobrecarregado
	void imprimir() {
		System.out.println("M�todo imprimir da subClasse: " + this.atributo);
	}

	public ExemploSubClasse(int v) {
		// executando o construtor vazio da superclasse
		super();
		System.out
				.println("In�cio do construtor vazio da subClasse: in�cio constru��o ");
		imprimir();
		this.atributo = v;
		imprimir();
		System.out
				.println("Fim do construtor vazio da subClasse: fim constru��o ");
	}

	public static void main(String[] args) {
		new ExemploSubClasse(10);
	}
}






