package modulo06;

// Classe abstrata. Representa a superclasse do exerc�cio
import java.util.Scanner;

public abstract class ExemploVeiculo {
	protected String nomeFabricante;

	protected int dtFabricacao;

	protected double preco;

	protected String cor;

	public ExemploVeiculo() {
		System.out.println("Executando o construtor da Classe ExemploVeiculo ");
	}

	public abstract void gravar();

	public void cadastrar() {
		Scanner scan = new Scanner(System.in).useDelimiter("\r\n");
		System.out.println("Iniciar cadastramanento:");
		System.out.println("Entre com o nome do fabricante: ");
		this.nomeFabricante = scan.nextLine();
		System.out.println("Entre com a cor: ");
		this.cor = scan.nextLine();
		System.out.println("Entre com o data de fabrica��o: ");
		this.dtFabricacao = scan.nextInt();
		System.out.println("Entre com o pre�o: ");
		this.preco = scan.nextDouble();
	}

	public void imprimir() {
		System.out.println("Imprimindo dados");
		System.out.println("Fabricante: " + this.nomeFabricante);
		System.out.println("Data de fabrica��o: " + this.dtFabricacao);
		System.out.println("Pre�o: " + this.preco);
		System.out.println("Cor: " + this.cor);
	}
}