package modulo06.estudodecaso.model;
//– Resposta do laboratório 7 – Classe Lab07ContaRemunerada 
import modulo05.estudodecaso.model.Lab05ContaCorrenteEspecial;
public class Lab07ContaRemunerada extends Lab05ContaCorrenteEspecial implements Lab07ContaCorrenteInterface {
	public void calcularJuros() {
			setSaldo(getSaldo() * TAXA_JUROS);
	}
	public Lab07ContaRemunerada() {
			super();
	}
	public Lab07ContaRemunerada(int p_age, int p_conta) {
			super (p_age, p_conta);
	}
}

