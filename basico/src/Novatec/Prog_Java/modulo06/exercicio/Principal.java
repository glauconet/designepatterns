package modulo06.exercicio;
//� Solu��o do exerc�cio 1. Classe Principal, implementando o m�todo main 
import java.util.Scanner;
public class Principal {
	// Refer�ncia para a interface. N�o � um objeto ainda.
	private InterfaceDimensao dimensao;
	private void executar() {
			int opcao = 0;
			Scanner sc = new Scanner(System.in);
			while (true) {
				System.out.println("**************************************");
				System.out.println("1 - Vetor");
				System.out.println("2 - Matriz");
				System.out.println("3 - Sair");
				System.out.println("Entre com uma op��o: ");
				opcao = sc.nextInt();
				switch (opcao) {
				case 1:
					vetorMenu();
					break;
				case 2:
					matrizMenu();
					break;
				default:
					if (opcao == 3){
						System.exit(0);						
					}
					System.out.println("Op��o inv�lida.");
				}
			}
		}
	private void vetorMenu() {
			int opcao = 0;
			Scanner sc = new Scanner(System.in);
			boolean loop = true;
			while (loop) {
				System.out.println("**************************************");
				System.out.println("1 - Adicionar Vetor Tamanho Padr�o.");
				System.out.println("2 - Adicionar Vetor Tamanho Especificado via teclado.");
				System.out.println("3 - Imprimir vetor criado.");
				System.out.println("4 - Sair.");
				System.out.println("Entre com uma op��o: ");
				opcao = sc.nextInt();
				switch (opcao) {
				case 1:
					vetorAdicionarPadrao();
					break;
				case 2:
					vetorAdicionarTamEspecifico();
					break;
				case 3:
					imprimir();
					break;
				case 4:
					loop = false;
					break;
				default:
					System.out.println("Op��o inv�lida.");
				}
			}
	}
	private void vetorAdicionarPadrao() {
			// Vai executar o construtor sem par�metros.
		this.dimensao = new VetorUnidimensional();
			adicionar();
		}
		private void vetorAdicionarTamEspecifico()  {
			Scanner sc = new Scanner(System.in);
			// Vai executar o construtor com um par�metro inteiro.
			int tam = 0;
			do{
				System.out.println("**************************************");
				System.out.println("Entre com o tamanho do vetor: ");
				tam = sc.nextInt();
				this.dimensao = new VetorUnidimensional(tam);	
				// Estamos usando downcasting
			}while (((VetorUnidimensional)this.dimensao).getDim1() == null);
			adicionar();
		}
	private void matrizMenu() {
			int opcao = 0;
			Scanner sc = new Scanner(System.in);
			boolean loop = true;
			while (loop) {
				System.out.println("**************************************");
				System.out.println("1 - Adicionar Matriz Tamanho Padr�o.");
				System.out.println("2 - Adicionar Matriz Tamanho Especificado via teclado.");
				System.out.println("3 - Imprimir matriz criada.");
				System.out.println("4 - Sair.");
				System.out.println("Entre com uma op��o: ");
				opcao = sc.nextInt();
				switch (opcao) {
				case 1:
					matrizAdicionarPadrao();
					break;
				case 2:
					matrizAdicionarTamEspecifico();
					break;
				case 3:
					imprimir();
					break;
				case 4:
					loop = false;
					break;
				default:
					System.out.println("Op��o inv�lida.");
				}
			}
		}
		private void matrizAdicionarPadrao() {
			// Vai executar o construtor sem par�metros.
			this.dimensao = new MatrizBidimensional();
			adicionar();
		}
		private void matrizAdicionarTamEspecifico() {
			Scanner sc = new Scanner(System.in);
			do{
				System.out.println("**************************************");
				System.out.println("Entre com a quantidade de linhas: ");
				int tamLinha = sc.nextInt();
				System.out.println("Entre com a quantidade de colunas: ");
				int tamColuna = sc.nextInt();
				// Vai executar o construtor com dois par�metros inteiro.
				this.dimensao = new MatrizBidimensional(tamLinha, tamColuna);		// Estamos usando downcasting
			}while (((MatrizBidimensional)this.dimensao).getDim2() == null);
			adicionar();
		}
		private void adicionar() {
			int valor = 0;
			Scanner sc = new Scanner(System.in);
			while (true) {
				System.out.println("**************************************");
				System.out.println("Entre com o valor para ser inserido <0 - para finalizar>: ");
				valor = sc.nextInt();
				if (valor == 0) {
					break;
				}
				// Aqui estamos fazendo uso de polimorfismo.
				this.dimensao.adicionar(valor);
			}
	}
	private void imprimir() {
			if (this.dimensao != null) {
				this.dimensao.imprimir();
			}
		}
		public static void main(String[] args) {
			Principal x = new Principal();
			x.executar();
		}
}
