package modulo06.exercicio;
//� Solu��o do exerc�cio 1. Defini��o da classe VetorUnidimensional 
public class VetorUnidimensional implements InterfaceDimensao {
	private int dim1[] = null;
	protected int linha;
	public void adicionar(int valor) {
		this.dim1[this.linha] = valor;
			this.linha++;
			if (this.linha == this.dim1.length) {
				this.linha = 0;
				System.out.println("Vetor foi excedido. Recome�ando");
			}
	}
	public void imprimir() {
			for (int i = 0 ; i < this.dim1.length ; i ++) {
				System.out.println("Posi��o: " + i + " - " + this.dim1[i]);
			}
	}
	public VetorUnidimensional() {
			super();
			this.dim1 = new int[InterfaceDimensao.TAMANHO];
		}
		public VetorUnidimensional(int tamanho) {
			if ((tamanho <= 0) || (tamanho >= 2000000)) {
				System.out.println("Tamanho do vetor inv�lido.");
			} else {
				this.dim1 = new int[tamanho];
			}
		}
		public int[] getDim1() {
			return this.dim1;
		}
}

