package modulo06.interfaces;
//� Classe que ir� usar a interface 
import java.util.Scanner;
import java.util.Vector;
public class ExemploClasseDVD implements ExemploInterfaceDVD {
	private Vector <ExemploEstruturaDVD> dvds = new Vector <ExemploEstruturaDVD>();
	public void acrescentarLista(ExemploEstruturaDVD obj) {
			this.dvds.add(obj);
	}
	public void imprimirLista() {
			System.out.println("Lista de DVDs");
			for (int i = 0; i < this.dvds.size(); i++) {
				System.out.println();
				System.out.print("Nome do Cliente: ");
				System.out.println(((ExemploEstruturaDVD) this.dvds.get(i)).getNomeCliente());
				System.out.print("Preco do DVD: ");
				System.out.println(((ExemploEstruturaDVD) this.dvds.get(i)).getPreco());
			}
			System.out.println("****************************");
			System.out.println();
	}
	public void cadastrarDVD(ExemploEstruturaDVD obj) {
			Scanner scan = new Scanner(System.in).useDelimiter("\r\n");
			System.out.println("Nome do Cliente: ");
			obj.setNomeCliente(scan.nextLine());
			System.out.println("Valor do DVD: ");
			obj.setPreco(scan.nextDouble());
		}
	public static void main(String[] args) {
			ExemploClasseDVD obj = new ExemploClasseDVD();
			ExemploEstruturaDVD objEst;
			while (true) {
				objEst = new ExemploEstruturaDVD();
				System.out.println("1 - Cadastrar DVD");
				System.out.println("2 - Imprimir lista cadastrada");
				System.out.println("3 - Cadastrar DVD com desconto");
				System.out.println("4 - Fim");
				System.out.println("Entre com a opcao desejada: ");
				Scanner scan = new Scanner(System.in).useDelimiter("\r\n");
				int opcao = scan.nextInt();
				if (opcao == 4)
					break;
				switch (opcao) {
				case 1:
					obj.cadastrarDVD(objEst);
					obj.acrescentarLista(objEst);
					break;
				case 2:
					obj.imprimirLista();
					break;
				case 3:
					obj.cadastrarDVD(objEst);
					System.out.print("Valor do DVD com promocao de:  ");
					System.out.println(ExemploInterfaceDVD.PROMOCAODIA);
					double valorDescontado = objEst.getPreco() - ( objEst.getPreco() * ExemploInterfaceDVD.PROMOCAODIA)/100; 
					objEst.setPreco(valorDescontado);
					obj.acrescentarLista(objEst);
					System.out.println();System.out.println();
					break;
				default:
					System.out.println("Op��o inv�lida");
				}
			}
	}
}
