package modulo06.interfaces;

public class ExemploEstruturaDVD {
	private double preco;

	private String nomeCliente;

	public String getNomeCliente() {
		return nomeCliente;
	}

	public double getPreco() {
		return preco;
	}

	public void setNomeCliente(String string) {
		nomeCliente = string;
	}

	public void setPreco(double d) {
		preco = d;
	}

}
