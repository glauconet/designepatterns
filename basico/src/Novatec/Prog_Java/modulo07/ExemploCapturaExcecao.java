package modulo07;

// Tratando exce��es como um m�todo de retorno alternativo
import java.util.Scanner;

public class ExemploCapturaExcecao {
	public static void main(String[] args) {
		double nota = 0.0;
		System.out.println("Entre com uma nota: ");
		Scanner s = new Scanner(System.in);
		nota = s.nextInt();

		try {
			if (nota >= 70) {
				// lan�ando a exce��o
				throw new Exception("Aprovado");
			} else if ((nota >= 40) && (nota < 70)) {
				throw new Exception("Exame");
			} else {
				throw new Exception("Reprovado");
			}
			// capturando a exce��o
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}