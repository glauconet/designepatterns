package modulo07;

// Usando o comando if para o tratamento do erro
import java.util.Scanner;

public class ExemploUsandoIF {
	public static void main(String args[]) {
		ExemploUsandoIF obj = new ExemploUsandoIF();
		int ret = obj.lerNota();
		if (ret == 1000) {
			System.out.println("Erro na leitura dos dados");
		}
		if (ret == 100) {
			System.out.println("Aprovado");
		}
		if (ret == 200) {
			System.out.println("Exame");
		}
		if (ret == 300) {
			System.out.println("Reprovado");
		}
	}

	private int lerNota() {
		double nota = 0.0;
		System.out.println("Entre com uma nota: ");
		Scanner s = new Scanner(System.in);
		nota = s.nextInt();
		if ((nota < 0) || (nota > 100)) {
			return 1000; // retorna um c�digo de erro
		}
		return calcularNota(nota);
	}

	private int calcularNota(double nota) {
		if (nota >= 70) {
			return 100;
		} else if ((nota >= 40) && (nota < 70)) {
			return 200;
		} else {
			return 300;
		}
	}
}