package modulo07.estudodecaso.model;
//� Resposta do laborat�rio 7 � Classe Lab08ContaCorrente 
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.util.Locale;
//criar a classe MyClassException no respective pacote. Caso contr�rio um erro de compila��o ser� gerado
import modulo07.MyClassException;
public class Lab08ContaCorrente{
	private int    numAge = 0 ;
	private	int    numConta = 0 ;
	private	String nome = "";
	private	double saldo = 0.0 ;
	String []	   tLinha = null; // Contem o conteudo do arquivo	
	public void  sacar (double pValor) throws MyClassException{
			if (pValor < 1) {
				MyClassException obj = new MyClassException ("Saque de valor negativo ou zerado");
				obj.setMensagem("Saque com valor negativo ou zerado");
				obj.setMetodo("public void  saque (double p_valor)");
				obj.setClasse("Lab08ContaCorrente");
				throw obj;
			}
			if (this.saldo < pValor) {
				MyClassException obj = new MyClassException ("Saldo Indisponivel" + "\nSaldo Atual: " + getSaldo() + "\n" + "Valor pretendido: " + pValor);
				obj.setMensagem("Saldo Indisponivel" + "\nSaldo Atual: " + getSaldo() + "\n" + "Valor pretendido: " + pValor);
				obj.setMetodo("public void  saque (double p_valor)");
				obj.setClasse("Lab08ContaCorrente");
				throw obj;
			}
			setSaldo(getSaldo() - pValor);
			MyClassException obj = new MyClassException();
			obj.setMensagem("Transacao de saque OK" + "\nSaldo Atual: " + getSaldo() + "\n" + "Valor pretendido: " + pValor);
			obj.setMetodo("public void  saque (double p_valor)");
			obj.setClasse("Lab08ContaCorrente");	
		}
		public void depositar (double pValor) {
			this.saldo += pValor;
		}
		public Lab08ContaCorrente() {
			super();
		}
		public Lab08ContaCorrente (int pNumAge, int pNumConta) throws MyClassException{
			setNumAge( pNumAge);
			setNumConta(pNumConta);
			// carregar a conta corrente do arquivo ou banco de dados
			this.recuperar();
		}	
		public void gravar() throws MyClassException{
			FileWriter      tArq1;
			PrintWriter     tArq2;
			try	{
				// abrir o aquivo
				tArq1 = new FileWriter (this.numAge + "." + this.numConta + ".dat");
				tArq2 = new PrintWriter (tArq1);
				tArq2.println (this.numAge);
				tArq2.println (this.numConta);
				tArq2.println (this.nome);
				tArq2.println (this.saldo);
				// fechar o arquivo
				tArq2.close();
			}
			catch (IOException tExcept) {
				MyClassException myObj = new MyClassException ("Problemas na grava��o do arquivo: ");
				myObj.setClasse(this.getClass().toString());
				myObj.setMensagem(tExcept.getMessage());
				myObj.setPacote(this.getClass().getPackage().toString());
				myObj.setMetodo("gravar()");
				throw myObj;	
			}
		}
		// implementado devido ao laborat�rio 4. Desconsider�-lo no laborat�rio 3
		public boolean removerArquivo() {
			File tArq1;
			tArq1 = new File(this.numAge + "." + this.numConta + ".dat");
			tArq1.delete();
			tArq1 = new File(this.numAge + "." + this.numConta + ".hist");
			tArq1.delete();
			return true;
		}
		private void recuperar() throws MyClassException{
			FileReader     tArq1 = null;
			BufferedReader tArq2;
			int            tQtde = 4;
			try{
				// abrir o arquivo
				tArq1 = new FileReader (this.numAge + "." + this.numConta + ".dat");
				tArq2 = new BufferedReader (tArq1);
				// ler atributo/valor e colocar na matriz
				this.tLinha = new String [tQtde];
				for (int i = 0; i < tQtde; i++) {
					this.tLinha [i] = tArq2.readLine();
				}
				// fechar o arquivo
				tArq2.close();
				setaAtributos();
			}
			catch (IOException tExcept) {
				MyClassException myObj = new MyClassException ("Problemas na leitura do arquivo: ");
				myObj.setClasse(this.getClass().toString());
				myObj.setMensagem(tExcept.getMessage());
				myObj.setPacote(this.getClass().getPackage().toString());
				myObj.setMetodo("Recupera");
				throw myObj;			
			}
		}
		public void setaAtributos() {
			this.numAge   = Integer.parseInt(this.tLinha [0]);
			this.numConta = Integer.parseInt(this.tLinha [1]);
			this.nome      = this.tLinha [2];
			this.saldo     = Double.parseDouble(this.tLinha [3]);		
		}
		public Lab08ContaCorrente (int pNumAge, int pNumConta, String pNome, double pSaldo) throws MyClassException{
			setNumAge( pNumAge);
			setNumConta(pNumConta);
			setNome(pNome);
			setSaldo(pSaldo);
		}
		public void imprimir() {
			System.out.println ("------------------------------------------");
			System.out.println ("Agencia : " + this.numAge );
			System.out.println ("Conta   : " + this.numConta );
			System.out.println ("Nome    : " + this.nome );
			NumberFormat formatter;
			formatter = NumberFormat.getCurrencyInstance(new Locale("pt","BR"));
			formatter.setMinimumFractionDigits(2);
			System.out.println ("Saldo   : " + formatter.format(this.saldo));
	}
	public String getNome() {
			return this.nome;
	}
	public int getNumAge() {
			return this.numAge;
		}
	public int getNumConta() {
			return this.numConta;
	}
	public double getSaldo() {
			return this.saldo;
	}
	public String[] getTLinha() {
			return this.tLinha;
	}
	public void setNome(String string) {
		this.nome = string;
	}
	public void setNumAge(int i) throws MyClassException{
			if ((i < 0) || (i > 9999))
				throw new MyClassException ("Numero da agencia invalido");
			this.numAge = i;
		}
		public void setNumConta(int i) throws MyClassException{
			if ((i < 0) || (i > 9999999))
				throw new MyClassException ("Numero da conta invalida");
			this.numConta = i;
		}
		public void setSaldo(double d) {
			this.saldo = d;
		}
}
