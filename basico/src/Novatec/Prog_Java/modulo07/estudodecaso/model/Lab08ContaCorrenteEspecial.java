package modulo07.estudodecaso.model;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import modulo07.MyClassException;
public class Lab08ContaCorrenteEspecial extends Lab08ContaCorrente {
	private double limite = 0.0;
	public Lab08ContaCorrenteEspecial() {
		super();	
	}
	public void  sacar (double pValor) throws MyClassException{
		if ( (getSaldo() + getLimite()) < pValor){
			MyClassException obj = new MyClassException ();
			obj.setMensagem("Saldo Indisponivel" + "\nSaldo Atual: " + 
							getSaldo() + "\n" + "Valor pretendido: " + 
							pValor + "\nLimite: " + getLimite());
			obj.setMetodo("public void  saque (double p_valor)");
			obj.setClasse("Lab08ContaCorrenteEspecial");
			throw obj;
		} 
		setSaldo(getSaldo() - pValor);
		MyClassException obj = new MyClassException ();
		obj.setMensagem("Transacao de saque OK" + "\nSaldo Atual: " + 
						getSaldo() + "\n" + "Valor pretendido: "    + 
						pValor + "\nLimite: " + getLimite());
		obj.setMetodo("public void  saque (double p_valor)");
		obj.setClasse("Lab08ContaCorrenteEspecial");
		throw obj;
	}
	public void imprimir(){
	  super.imprimir();		
	  System.out.println ("__________________________________________");
	  System.out.println ("Limite : " + this.limite);
	  System.out.println ("------------------------------------------");	  
	}
	public Lab08ContaCorrenteEspecial(int p_num_age, int p_num_conta) throws MyClassException{
		super(p_num_age, p_num_conta);
		recuperaContaEspecial();
	}
	public void gravar () throws MyClassException{
		FileWriter      tArq1;
		PrintWriter     tArq2;
		try	{
			// abrir o aquivo
			tArq1 = new FileWriter (getNumAge() + "." + getNumConta() + ".esp");
			tArq2 = new PrintWriter (tArq1);
			tArq2.println (this.limite);
			// fechar o arquivo
			tArq2.close();
			super.gravar(); // grava a conta corrente
		}
		catch (IOException tExcept){
			tExcept.printStackTrace();
			MyClassException myObj = new MyClassException ("Problemas na grava��o do arquivo: ");
			myObj.setClasse(this.getClass().toString());
			myObj.setMensagem(tExcept.getMessage());
			myObj.setPacote(this.getClass().getPackage().toString());
			myObj.setMetodo("Grava");
			throw myObj;
		}
	}
	private void recuperaContaEspecial (){
		FileReader     tArq1;
		BufferedReader tArq2;
		try{
			// abrir o arquivo
			tArq1 = new FileReader (getNumAge() + "." + getNumConta() + ".esp");
			tArq2 = new BufferedReader (tArq1);
			// ler atributo/valor e colocar na matriz
			this.tLinha = new String [1];
			this.tLinha [0] = tArq2.readLine();
			// fechar o arquivo
			tArq2.close();
			this.limite = Double.valueOf(this.tLinha[0]).doubleValue();
		}
		catch (IOException tExcept){
			tExcept.printStackTrace();
		}
	}
	public Lab08ContaCorrenteEspecial (	int pNumAge,int pNumConta,String pNome,double pSaldo,
		double p_limite) throws MyClassException {
		super(pNumAge, pNumConta, pNome, pSaldo);
		this.limite = p_limite;
	}
	public double getLimite() {
		return this.limite;
	}
	public void setLimite(double d) {
		this.limite = d;
	}
}
