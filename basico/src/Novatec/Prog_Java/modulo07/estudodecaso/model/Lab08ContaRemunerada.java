package modulo07.estudodecaso.model;
//– Resposta do laboratório 7 – Classe Lab08ContaRemunerada 
import modulo07.MyClassException;
public class Lab08ContaRemunerada extends Lab08ContaCorrenteEspecial implements Lab08ContaCorrenteInterface {
	public void calcularJuros() {
			setSaldo(getSaldo() * TAXA_JUROS);
	}
	public Lab08ContaRemunerada() {
			super();
	}
	public Lab08ContaRemunerada(int pAge, int pConta) throws MyClassException {
			super (pAge, pConta);
	}
}
