package modulo07.exercicio;
//� InterfaceDimensao 
import modulo07.MyClassException;
public interface InterfaceDimensao {
	public void adicionar (int valor) throws MyClassException;
	public void imprimir();
	public static final int TAMANHO = 2;
}
