package modulo07.exercicio;
//– Classe MatrizBidimensional 
import modulo07.MyClassException;
public class MatrizBidimensional extends VetorUnidimensional implements
		InterfaceDimensao {
	private int coluna;
	private int[][] dim2;
	public int[][] getDim2() {
		return this.dim2;
	}
	public void adicionar(int valor) throws MyClassException {
			this.dim2[this.linha][this.coluna] = valor;
			if (this.linha < this.dim2.length) {
				this.coluna++;
			}
			// utiliza-se [linha] para identificar a quantidade de colunas de cada linha
			if (this.coluna == this.dim2[linha].length) {
				this.linha++;
				this.coluna = 0;
			}
			if (this.linha == this.dim2.length) {
				this.linha = 0;
				this.coluna = 0;
				throw new MyClassException("Matriz foi excedida. Recomeçando");
			}
		}
		public void imprimir() {
			for (int i = 0; i < this.dim2.length; i++) { // contador de linhas
				for (int j = 0; j < this.dim2[linha].length; j++) { // contador de colunas
					System.out.println("Elemento " + i + " : " + this.dim2[i][j]);
				}
			}
		}
	public MatrizBidimensional() {
			this.dim2 = new int[InterfaceDimensao.TAMANHO][InterfaceDimensao.TAMANHO];
		}
		public MatrizBidimensional(int linha, int coluna) throws MyClassException {
			if ((linha <= 0) || (linha >= 2000000)) {
				throw new MyClassException(
						"Tamanho da linha da matriz viola os limites definidos");
			}
			if ((coluna <= 0) || (coluna >= 2000000)) {
				throw new MyClassException("Tamanho da coluna da matriz viola os limites definidos");
			}
			this.dim2 = new int[linha][coluna];
		}
}
