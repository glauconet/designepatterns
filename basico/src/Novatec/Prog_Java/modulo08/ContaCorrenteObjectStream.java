package modulo08;

// Usando as classes ObjectInputStream e ObjectOutputStream
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
// A classe MyClassException deve ter sido criada no pacote utilizado no comando import
import modulo07.MyClassException;

public class ContaCorrenteObjectStream implements Serializable {
	// para que o objeto seja serializado precisamos criar este n�mero
	private static final long serialVersionUID = 1L;

	private int conta, agencia;

	private double saldo;

	private String nomeCliente;

	// por serem transientes n�o ser�o gravados junto com os atributos n�o
	// transientes do objeto
	private transient double limiteFixo = 1000.0;

	// este atributo ser� salvo no arquivo de forma explicita por meio do m�todo
	// writeInt()
	private transient int qdadeCadastrada;

	public int getAgencia() {
		return this.agencia;
	}

	public void setAgencia(int agencia) {
		this.agencia = agencia;
	}

	public int getConta() {
		return this.conta;
	}

	public void setConta(int conta) {
		this.conta = conta;
	}

	public String getNomeCliente() {
		return this.nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	public double getSaldo() {
		return this.saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public void imprimir() throws MyClassException {
		recuperar();
		System.out.println("******************************");
		System.out.println("Quantidade cadastrada: " + getQdadeCadastrada());
		System.out.println("N�mero da conta: " + getConta());
		System.out.println("N�mero da ag�ncia: " + getAgencia());
		System.out.println("Saldo da conta corrente: " + getSaldo());
		System.out.println("Nome do cliente: " + getNomeCliente());
		System.out.println("Saldo com limite: "
				+ (getSaldo() + getLimiteFixo()));
		System.out.println("******************************");
	}

	public void gravar() throws MyClassException {
		try {
			/*
			 * Quando usamos ObjectOutputStream n�o podemos utilizar o modo
			 * append. Na grava��o n�o apresenta erro por�m na leitura � lan�ada
			 * a exce��o Java.io.StreamCorruptedException.
			 */
			ObjectOutputStream out = new ObjectOutputStream(
					new BufferedOutputStream(new FileOutputStream(
							"exemploobjectstream.dat")));
			// grava a quantidade j� gravada mais uma unidade
			out.writeInt(getQdadeCadastrada() + 1);
			// atualiza o atributo
			setQdadeCadastrada(getQdadeCadastrada() + 1);
			out.writeObject(this);
			out.close();
		} catch (FileNotFoundException e) {
			MyClassException exc = new MyClassException(
					"Problemas com FileNotFoundException.");
			exc.setMensagem(e.getMessage());
			throw exc;
		} catch (IOException e) {
			MyClassException exc = new MyClassException(
					"Problemas com IOException.");
			exc.setMensagem(e.getMessage());
			throw exc;
		}
	}

	public void recuperar() throws MyClassException {
		try {
			ObjectInputStream in = new ObjectInputStream(
					new BufferedInputStream(new FileInputStream(
							"exemploobjectstream.dat")));
			// l� a quantidade a partir do arquivo
			setQdadeCadastrada(in.readInt());
			ContaCorrenteObjectStream obj = (ContaCorrenteObjectStream) in
					.readObject();
			setConta(obj.getConta());
			setAgencia(obj.getAgencia());
			setSaldo(obj.getSaldo());
			setNomeCliente(obj.getNomeCliente());
			in.close();
		} catch (FileNotFoundException e) {

			MyClassException exc = new MyClassException(
					"Problemas com ClassNotFoundException.");
			exc.setMensagem(e.getMessage());
			throw exc;
		} catch (ClassNotFoundException e) {
			MyClassException exc = new MyClassException(
					"Problemas com ClassNotFoundException.");
			exc.setMensagem(e.getMessage());
			throw exc;
		} catch (IOException e) {
			e.printStackTrace();
			MyClassException exc = new MyClassException(
					"Problemas com IOException.");
			exc.setMensagem(e.getMessage());
			throw exc;
		}
	}

	public double getLimiteFixo() {
		return this.limiteFixo;
	}

	public int getQdadeCadastrada() {
		return this.qdadeCadastrada;
	}

	public void setQdadeCadastrada(int qdadeCadastrada) {
		this.qdadeCadastrada = qdadeCadastrada;
	}
}