package modulo08;

import java.io.*;
import java.util.Date;

public class ExemploBufferedStreamByte {
	public static void main(String[] args) {

		BufferedInputStream arqLeitura = null;
		BufferedOutputStream arqGravacao = null;
		String strArqSaida = "acervoOUT.txt";
		try {
			arqLeitura = new BufferedInputStream(new FileInputStream(
					"acervo.txt"));
			arqGravacao = new BufferedOutputStream(new FileOutputStream(
					strArqSaida));
			int byteLido;
			System.out.println("Bytes gravados no arquivo " + strArqSaida
					+ ":\n");
			long tempoInicial = new Date().getTime();
			while ((byteLido = arqLeitura.read()) != -1) {
				arqGravacao.write(byteLido);
				System.out.print((char) byteLido);
			}
			long tempoFinal = new Date().getTime();
			System.out.println("\n Dura��o: " + (tempoFinal - tempoInicial));
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (arqLeitura != null) {
					arqLeitura.close();
				}
				if (arqGravacao != null) {
					arqGravacao.close();
				}
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}
	}
}