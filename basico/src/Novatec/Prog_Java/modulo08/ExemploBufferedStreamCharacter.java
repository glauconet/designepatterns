package modulo08;

// Usando um buffered stream baseando-se em um character stream
import java.io.*;
import java.util.Date;

public class ExemploBufferedStreamCharacter {
	public static void main(String[] args) {
		BufferedReader arqLeitura = null;
		BufferedWriter arqGravacao = null;
		String strArqSaida = "acervoOUT.txt";

		try {
			arqLeitura = new BufferedReader(new FileReader("acervo.txt"));
			/*
			 * o construtor da classe BufferedWriter recebe como argumento um
			 * objeto da classe Writer. Isto significa que um objeto da classe
			 * BufferedWriter pode ser constru�do A partir de uma subclasse da
			 * classe Writer.
			 */
			arqGravacao = new BufferedWriter(new FileWriter(strArqSaida));
			String linha;
			System.out.println("Bytes gravados no arquivo " + strArqSaida
					+ ":\n");
			long tempoInicial = new Date().getTime();
			while ((linha = arqLeitura.readLine()) != null) {
				arqGravacao.write(linha);
				// necess�rio para que no novo arquivo seja respeitada a quebras
				// das linhas
				arqGravacao.newLine();
				System.out.print(linha + "\n");
			}
			long tempoFinal = new Date().getTime();
			System.out.println("\n Dura��o: " + (tempoFinal - tempoInicial));
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (arqLeitura != null) {
					arqLeitura.close();
				}
				if (arqGravacao != null) {
					arqGravacao.close();
				}
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}
	}
}