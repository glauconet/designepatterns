package modulo08;

// Usando objetos que manipulam dados por meio das classes DataInputStream e DataOutputStream
import java.util.Scanner;
// A classe MyClassException deve ter sido criada no pacote utilizado no comando import
import modulo07.MyClassException;

public class ExemploDataStream {
	ContaCorrenteDataStream cc = new ContaCorrenteDataStream();

	public static void main(String[] args) {
		ExemploDataStream obj = new ExemploDataStream();
		int op = 0;
		while (true) {
			Scanner sc = new Scanner(System.in);
			System.out.println("1 - Cadastrar");
			System.out.println("2 - Consultar");
			System.out.println("9 - Sair");
			System.out.println("Entre com a op��o: ");
			op = sc.nextInt();
			try {
				switch (op) {
				case 1:
					obj.execCadastrar();
					break;
				case 2:
					obj.execConsulta();
					break;
				case 9:
					System.exit(0);
				default:
					System.out.println("Op��o inv�lida.");
				}
			} catch (MyClassException e) {
				System.out.println(e.getMensagem());

				System.out.println(e.getMessage());
			}
		}
	}

	public void execConsulta() throws MyClassException {
		this.cc.imprimir();
	}

	public void execCadastrar() throws MyClassException {
		// para permitir que seja feita a leitura de um nome composto
		Scanner sc = new Scanner(System.in).useDelimiter("\r\n");
		System.out.println("Entre com o nome do cliente: ");
		this.cc.setNomeCliente(sc.nextLine());
		System.out.println("Entre com o n�mero da ag�ncia: ");
		this.cc.setAgencia(sc.nextInt());
		System.out.println("Entre com o n�mero da conta: ");
		this.cc.setConta(sc.nextInt());
		System.out.println("Entre com o saldo do cliente: ");
		this.cc.setSaldo(sc.nextDouble());
		this.cc.gravar();
	}
}