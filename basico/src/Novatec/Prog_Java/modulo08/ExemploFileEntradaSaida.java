package modulo08;

// Usando a classe File, a classe BufferedReader e a classe InputStreamReader
import java.io.BufferedReader;
import java.io.File;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

public class ExemploFileEntradaSaida {
	public static void main(String args[]) {
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(
					System.in));
			System.out.println("Digite um nome de arquivo: ");
			String linha = in.readLine();
			File nomeArquivo = new File(linha);
			if (nomeArquivo.exists()) {
				StringBuilder sb = new StringBuilder();
				sb.append(nomeArquivo.getName());
				sb.append("\n1 - existe\n");
				// usando operador tern�rio no lugar do comando if
				sb.append(nomeArquivo.isDirectory() ? "2 - � um diret�rio\n"
						: "2 - N�o � um diret�rio\n");
				// usando operador tern�rio no lugar do comando if
				sb.append(nomeArquivo.isFile() ? "3 - � um arquivo\n"
						: "3 - N�o � um arquivo\n");
				// usando operador tern�rio no lugar do comando if
				sb.append(nomeArquivo.isAbsolute() ? "4 - � caminho absoluto\n"
						: "4 - N�o � caminho absoluto\n");
				sb.append("5 - �ltima altera��o: "
						+ new Date(nomeArquivo.lastModified()));
				sb.append("\n6 - Tamanho: " + nomeArquivo.length());
				sb.append("\n7 - Caminho: " + nomeArquivo.getPath());
				sb.append("\n8 - Caminho absoluto: "
						+ nomeArquivo.getAbsolutePath());
				sb.append("\n9 - Diret�rio-pai: " + nomeArquivo.getParent());
				System.out.println(sb);
				if (nomeArquivo.isFile()) {
					try {
						BufferedReader entrada = new BufferedReader(
								new FileReader(nomeArquivo));
						StringBuffer buffer = new StringBuffer();
						String texto;
						while ((texto = entrada.readLine()) != null)
							buffer.append(texto + "\n");
						System.out.print("\n\nConte�do do arquivo:\n");
						System.out.print(buffer.toString());
					} catch (IOException e) {
						throw e;
					}
				} else if (nomeArquivo.isDirectory()) {
					String diret�rio[] = nomeArquivo.list();
					System.out.print("\n\nConte�do do diret�rio:\n");
					for (int i = 0; i < diret�rio.length; i++)
						System.out.print(diret�rio[i] + "\n");
				}
			} else
				System.out.print(nomeArquivo + " n�o existe.");
		} catch (IOException e) {
			System.err.println("Ocorreu um erro de IO: " + e);
		}
	}
}