package modulo08;

// Usando a classe Scanner com delimitador modificado
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;

public class ExemploPrintWriter {
	public static void main(String[] args) {
		PrintWriter arqGravacao = null;
		try {
			arqGravacao = new PrintWriter(new FileWriter("arqprintwriter.txt"));
			System.out
					.println("Todo o resultado deste programa ser� gravado no arquivo arqprintwriter.txt");
			int valor = 10;
			double raiz = Math.sqrt(valor);
			arqGravacao
					.println("Gravando raiz por meio dos m�todos print e println.");

			arqGravacao.print("A raiz de ");
			arqGravacao.print(valor);
			arqGravacao.print(" �: ");
			arqGravacao.print(raiz);
			arqGravacao.println(".");
			arqGravacao.println("\nGravando raiz por meio do m�todo println.");
			arqGravacao.println("A raiz de " + valor + " �: " + raiz + ".");
			// usando o m�todo format
			arqGravacao.println("\nGravando raiz por meio do m�todo format.");
			// representando o valor no padr�o americano
			arqGravacao.format(Locale.US, "1 - A raiz de %d �: |%f|%n", valor,
					raiz);
			arqGravacao.format("2 - A raiz de %d �: |%f|%n", valor, raiz);
			arqGravacao.format("3 - A raiz de %d �: |%20f|%n", valor, raiz);
			arqGravacao.format("4 - A raiz de %d �: |%.2f|%n", valor, raiz);
			arqGravacao.format("5 - A raiz de %d �: |%20.2f|%n", valor, raiz);
			arqGravacao.format("6 - A raiz de %d �: |%020.2f|%n", valor, raiz);
			arqGravacao.format("7 - A raiz de %d �: |%+020.2f|%n", valor, raiz);
			arqGravacao.format("8 - A raiz de %d �: |%-20.2f|%n", valor, raiz);
			arqGravacao.format("9 - A raiz de %d �: |%2$+020.10f|%n", valor,
					raiz);
			arqGravacao.format("10 - Valor negativo: |%(d|%n", -27);
			arqGravacao.format("11 - Valor negativo: |%(20d|%n", -9);
			arqGravacao.format("12 - Valor negativo: |%(020d|%n", -74);
			arqGravacao.format("13 - Valor negativo: |%-20d|%n", 12); // Alinhado
																		// a
																		// esquerda
			arqGravacao.format("14 - String: |%s|%n", "abcdefghijklm");
			arqGravacao.format("15 - String: |%20s|%n", "abcdefghijklm");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} finally {
			if (arqGravacao != null) {
				arqGravacao.close();
			}
		}
	}
}