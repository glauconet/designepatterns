package modulo08;

// Usando a classe Scanner com delimitador padr�o
import java.io.*;
import java.util.Scanner;

public class ExemploScannereStreamEspaco {
	public static void main(String[] args) {
		Scanner sc = null;
		int qdade = 0;
		try {
			/*
			 * usa como delimitador padr�o espa�o em branco. Como espa�o em
			 * branco temos: brancos, tabs e terminadores de linha
			 */
			try {
				sc = new Scanner(new BufferedReader(
						new FileReader("acervo.txt")));
				// ir� ler somente as 10 primeiras palavras do arquivo
				while (sc.hasNext() && qdade < 10) {
					System.out.println(sc.next());
					qdade++;
				}
			} catch (FileNotFoundException e) {
				System.out.println(e.getMessage());
			}
		} finally {
			if (sc != null) {
				sc.close();
			}
		}
	}
}