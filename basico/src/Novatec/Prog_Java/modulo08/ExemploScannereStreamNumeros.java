package modulo08;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Scanner;

public class ExemploScannereStreamNumeros {
	public static void main(String[] args) {
		Scanner sc = null;
		try {
			NumberFormat formatter = NumberFormat
					.getCurrencyInstance(new Locale("pt", "BR"));
			formatter.setMinimumFractionDigits(2);
			try {
				// neste exemplo usamos como delimitador padr�o o caractere ;
				// (ponto e v�rgula)
				sc = new Scanner(new BufferedReader(new FileReader(
						"valores.txt"))).useDelimiter(";");
				sc.useLocale(new Locale("pt", "BR"));
				while (sc.hasNext()) {
					if (sc.hasNextDouble()) {
						System.out.print(formatter.format(sc.nextDouble()));
					} else {
						System.out.print(sc.next() + ":\t ");
					}
				}
			} catch (FileNotFoundException e) {
				System.out.println(e.getMessage());
			}
		} finally {
			if (sc != null) {
				sc.close();
			}
		}
	}
}