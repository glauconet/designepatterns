package modulo09.abstracfactorymvc.model;

//� Classe abstrata FactoryFormaPrimitiva, situada na camada model 
import modulo07.MyClassException;
public abstract class FactoryFormaPrimitiva {
	public static TemaFormaPrimitiva getFabrica(String nomeFabrica)
			throws MyClassException {
			try {
				// Reflection
				Class<?> classe = Class.forName(nomeFabrica);
				TemaFormaPrimitiva fabrica = (TemaFormaPrimitiva) classe.newInstance();
				return fabrica;
			} catch (InstantiationException e) {
				throw new MyClassException("Erro na F�brica - InstantiationException", e);
			} catch (IllegalAccessException e) {
				throw new MyClassException("Erro na F�brica - IllegalAccessException", e);
			} catch (ClassNotFoundException e) {
				throw new MyClassException("Erro na F�brica - ClassNotFoundException", e);
			}
	}
}
