package modulo09.abstracfactorymvc.model;
//� Classe Quadrado, situada na camada model 
public class Quadrado implements TemaFormaPrimitiva {
	public void calcularArea(FormaVO objVO) {
			objVO.setArea(Math.abs(objVO.getX2() - objVO.getX1()) * Math.abs(objVO.getY2() - objVO.getY1()));
	}
}

