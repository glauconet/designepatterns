package modulo09.abstractfactorycarro;
//� Classe ExemploChevroletFactory 
import modulo09.factorycarro.ExemploAutomovel;
import modulo09.factorycarro.ExemploCelta;
import modulo09.factorycarro.ExemploCorsa;
public  class ExemploChevroletFactory extends AbstractFactoryMontadora {
	@Override
	public ExemploAutomovel getAutomovel(String marca) {
			if (marca == null)
				return null;
			else if (marca.equals("Celta"))
				return new ExemploCelta();
			else if (marca.equals("Corsa"))
				return new ExemploCorsa();
			else
				return null;
	}
}

