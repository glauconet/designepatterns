package modulo09.abstractfactorycarro;
//� Classe PrincipalAbstractCarro 
import java.util.Scanner;

import modulo09.factorycarro.ExemploAutomovel;
public class PrincipalAbstractCarro {
	public void execSubMenuChevrolet(AbstractFactoryMontadora factory) {
			ExemploAutomovel auto = null;
			System.out.println("1 - Corsa");
			System.out.println("2 - Celta");
			Scanner scan = new Scanner(System.in);
			System.out.print("Escolha o carro e analise seu pre�o: ");
			int iTipo = scan.nextInt();
			switch (iTipo) {
			case 1: {
				auto = factory.getAutomovel("Corsa");
				break;
			}
			case 2: {
				auto = factory.getAutomovel("Celta");
				break;
			}
			default:
				System.out.println();
				System.out.println("Op��o Inv�lida. Tecle <ENTER>.");
				break;
			}
			if (auto != null) {
				System.out.println("Pre�o do carro: " + auto.getPreco());
			}
		}
		public void execSubMenuVolkswagen(AbstractFactoryMontadora factory) {
			ExemploAutomovel auto = null;
			System.out.println("1 - Polo");
			System.out.println("2 - CrossFox");
			Scanner scan = new Scanner(System.in);
			System.out.print("Escolha o carro e analise seu pre�o: ");
			int iTipo = scan.nextInt();
			switch (iTipo) {
			case 1: {
				auto = factory.getAutomovel("Polo");
				break;
			}
			case 2: {
				auto = factory.getAutomovel("CrossFox");
				break;
			}
			default:
				System.out.println();
				System.out.println("Op��o Inv�lida. Tecle <ENTER>.");
				break;
			}
			if (auto != null) {
				System.out.println("Pre�o do carro: " + auto.getPreco());
			}
		}
		public void execMenuPrincipal() {
			int iTipo = 0;
			while (iTipo != 9) {
				AbstractFactoryMontadora fabrica = null;
				System.out.println("1 - Chevrolet");
				System.out.println("2 - Volkswagen");
				System.out.println("9 - Fim \n");
				Scanner scan = new Scanner(System.in);
				System.out.print("Escolha a montadora: ");
				iTipo = scan.nextInt();
				switch (iTipo) {
				case 1: {
					fabrica = AbstractFactoryMontadora.getFabrica("Chevrolet");
					execSubMenuChevrolet(fabrica);
					break;
				}
				case 2: {
					fabrica = AbstractFactoryMontadora.getFabrica("Volkswagen");
					execSubMenuVolkswagen(fabrica);
					break;
				}
				case 9: {
					System.exit(0);
					break;
				}
				default:
					System.out.println();
					System.out.println("Op��o Inv�lida. Tecle <ENTER>.");
					break;
				}
			}
		}
		public static void main(String[] args) {
			(new PrincipalAbstractCarro()).execMenuPrincipal();
		}
}
