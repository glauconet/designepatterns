package modulo09.command;
//� Classe abstrata ComandoAbstrato 
import java.util.Scanner;

import modulo09.abstracfactorymvc.model.FormaVO;

public abstract class ComandoAbstrato implements Comando {
	protected abstract void executarComando();
	protected abstract String getNome();
	private static FormaVO objVO ;
	public void lerCoordenadas() {
			if (objVO == null) {
				objVO = new FormaVO();
		}
		Scanner scan = new Scanner(System.in);
		System.out.println("\nEntre com as Coordenadas. \n");
		System.out.println("Coordenada x1: ");
		objVO.setX1(scan.nextInt());
		System.out.println("Coordenada x2: ");
		objVO.setX2(scan.nextInt());
		System.out.println("Coordenada y1: ");
		objVO.setY1(scan.nextInt());
		System.out.println("Coordenada y2: ");
		objVO.setY2(scan.nextInt());
	}
	public void executar() {
		System.out.println("--: " + getNome() + " :--");
		/* Aqui estamos fazendo uso do padr�o de projeto Template. O m�todo executarComando() foi implementado em cada um dos comandos definidos neste exerc�cio. */
		executarComando();
	}
	public static FormaVO getObjVO() {
		return objVO;
	}
}
