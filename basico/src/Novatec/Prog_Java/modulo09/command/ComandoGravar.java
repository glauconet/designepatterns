package modulo09.command;
//– Classe ComandoGravar 
public class ComandoGravar extends ComandoAbstrato {
	public ComandoGravar() {
			super();
	}
	@Override
	protected String getNome() {
			return "Gravar";
	}
	@Override
	protected void executarComando() {
			System.out.println("Gravando as coordenadas da forma geométrica");
	}
}
