package modulo09.contacorrentemvc.controller;

import modulo09.contacorrentemvc.model.Lab01ContaCorrenteBean;
import modulo09.contacorrentemvc.model.Lab02ContaCorrente;

public class Lab03TransacaoConta {
	private Lab03TransacaoConta() {
	}
	public static boolean cadastrarConta(Lab01ContaCorrenteBean vo) {
		Lab02ContaCorrente cc1 = new Lab02ContaCorrente(vo);
		return cc1.gravar();
	}
	public static Lab01ContaCorrenteBean obterConta(Lab01ContaCorrenteBean vo) {
		// Carregar a conta corrente do arquivo ou banco de dados
		Lab02ContaCorrente cc1 = new Lab02ContaCorrente(vo);
		return cc1.recuperar();
	}
}
