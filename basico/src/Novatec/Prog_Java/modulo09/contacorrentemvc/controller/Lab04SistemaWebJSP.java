package modulo09.contacorrentemvc.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modulo09.contacorrentemvc.model.Lab01ContaCorrenteBean;

/**
 * Servlet implementation class for Servlet: Lab05SistemaWeb
 * 
 */
public class Lab04SistemaWebJSP extends javax.servlet.http.HttpServlet
		implements javax.servlet.Servlet {
	private static final long serialVersionUID = 7926741196973668754L;

	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String action = request.getParameter("action");
		if (action != null && action.equals("cadastrar")) {
			this.getAcoesParaCadastro(request, response, action);
		}
		else if (action != null && action.equals("sacar")) {
			this.getAcoesParaSaque(request, response, action);
		} else if (action != null && action.equals("depositar")) {
			this.getAcoesParaDeposito(request, response, action);
		} else if (action != null && action.equals("consultar")) {
			this.getAcoesParaConsulta(request, response, action);
		}
	}
	private void getAcoesParaCadastro(HttpServletRequest request,
			HttpServletResponse response, String actionCliente)
			throws ServletException, IOException {
		Lab01ContaCorrenteBean vo = new Lab01ContaCorrenteBean();
		vo.setNumAge(Integer.parseInt(request.getParameter("NumAgencia")));
		vo.setNumConta(Integer.parseInt(request.getParameter("NumConta")));
		vo.setNomeCliente(request.getParameter("NomeCliente"));
		vo.setSaldo(Double.parseDouble(request.getParameter("Valor")));

		Lab03TransacaoConta.cadastrarConta(vo);
		ArrayList<String> titles = new ArrayList<String>();
		ArrayList<String> values = new ArrayList<String>();
		montarResultado(titles, values, vo);

		request.setAttribute("titles", titles);
		request.setAttribute("values", values);
		this.showPage(request, response, "resultado.jsp");
	}

	private void montarResultado(ArrayList<String> titles,
			ArrayList<String> values, Lab01ContaCorrenteBean vo) {
		titles.add("Ag�ncia:");
		titles.add("Conta:");
		titles.add("Nome:");
		titles.add("Saldo:");
		values.add(String.valueOf(vo.getNumAge()));
		values.add(String.valueOf(vo.getNumConta()));
		values.add(String.valueOf(vo.getNomeCliente()));
		values.add(String.valueOf(vo.getSaldo()));
	}

	private void getAcoesParaSaque(HttpServletRequest request,
			HttpServletResponse response, String actionCliente)
			throws ServletException, IOException {
		Lab01ContaCorrenteBean vo = new Lab01ContaCorrenteBean();
		vo.setNumAge(Integer.parseInt(request.getParameter("NumAgencia")));
		vo.setNumConta(Integer.parseInt(request.getParameter("NumConta")));
		vo.setValor(Double.parseDouble(request.getParameter("Valor")));

		Lab03TransacaoSaque.realizarSaque(vo);
		ArrayList<String> titles = new ArrayList<String>();
		ArrayList<String> values = new ArrayList<String>();
		montarResultado(titles, values, vo);
		// Particular do saque.
		titles.add("Valor do Saque:");
		values.add(request.getParameter("Valor"));

		request.setAttribute("titles", titles);
		request.setAttribute("values", values);
		this.showPage(request, response, "resultado.jsp");
	}

	private void getAcoesParaDeposito(HttpServletRequest request,
			HttpServletResponse response, String actionCliente)
			throws ServletException, IOException {

		Lab01ContaCorrenteBean vo = new Lab01ContaCorrenteBean();
		vo.setNumAge(Integer.parseInt(request.getParameter("NumAgencia")));
		vo.setNumConta(Integer.parseInt(request.getParameter("NumConta")));
		vo.setValor(Double.parseDouble(request.getParameter("Valor")));

		Lab03TransacaoDeposito.realizarDeposito(vo);
		ArrayList<String> titles = new ArrayList<String>();
		ArrayList<String> values = new ArrayList<String>();
		montarResultado(titles, values, vo);
		// Particular do saque.
		titles.add("Valor do Dep�sito:");
		values.add(request.getParameter("Valor"));

		request.setAttribute("titles", titles);
		request.setAttribute("values", values);
		this.showPage(request, response, "resultado.jsp");
	}

	private void getAcoesParaConsulta(HttpServletRequest request,
			HttpServletResponse response, String actionCliente)
			throws ServletException, IOException {
		Lab01ContaCorrenteBean vo = new Lab01ContaCorrenteBean();
		vo.setNumAge(Integer.parseInt(request.getParameter("NumAgencia")));
		vo.setNumConta(Integer.parseInt(request.getParameter("NumConta")));

		Lab03TransacaoConta.obterConta(vo);
		ArrayList<String> titles = new ArrayList<String>();
		ArrayList<String> values = new ArrayList<String>();
		montarResultado(titles, values, vo);
		request.setAttribute("titles", titles);
		request.setAttribute("values", values);
		this.showPage(request, response, "resultado.jsp");
	}

	protected void showPage(HttpServletRequest request,
			HttpServletResponse response, String page) throws ServletException,
			IOException {
		ServletContext context = getServletContext();
		RequestDispatcher rd = context.getRequestDispatcher("/" + page);
		rd.forward(request, response);
	}
}