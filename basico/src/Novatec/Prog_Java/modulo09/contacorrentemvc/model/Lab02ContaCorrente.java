package modulo09.contacorrentemvc.model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Lab02ContaCorrente {
	String[] tLinha = null; // Cont�m o conte�do do arquivo
	Lab01ContaCorrenteBean vo = null;

	public int sacar(double pValor) {
		if (this.vo.getSaldo() < pValor)
			return 0;

		this.vo.setSaldo(this.vo.getSaldo() - pValor);
		return 1;
	}

	public void depositar(double pValor) {
		this.vo.setSaldo(this.vo.getSaldo() + pValor);
	}

	public Lab02ContaCorrente(Lab01ContaCorrenteBean vo) {
		super();
		this.vo = vo;
	}

	@SuppressWarnings("unused")
	private Lab02ContaCorrente() {}
	public boolean gravar() {
		FileWriter tArq1;
		PrintWriter tArq2;

		try {
			// abrir o aquivo
			tArq1 = new FileWriter(this.vo.getNumAge() + "." + this.vo.getNumConta()+ ".txt");
			tArq2 = new PrintWriter(tArq1);

			tArq2.println(this.vo.getNumAge());
			tArq2.println(this.vo.getNumConta());
			tArq2.println(this.vo.getNomeCliente());
			tArq2.println(this.vo.getSaldo());
			// fechar o arquivo
			tArq2.close();
			tArq1.close();

			return true;
		} catch (IOException tExcept) {
			tExcept.printStackTrace();
			return false;
		}
	}
	public Lab01ContaCorrenteBean recuperar() {
		FileReader tArq1;
		BufferedReader tArq2;
		int tQtde = 4;

		try {
			// abrir o arquivo
			tArq1 = new FileReader(this.vo.getNumAge() + "." + this.vo.getNumConta()
					+ ".txt");
			tArq2 = new BufferedReader(tArq1);

			// ler atributo/valor e colocar na matriz
			this.tLinha = new String[tQtde];
			for (int i = 0; i < tQtde; i++) {
				tLinha[i] = tArq2.readLine();
			}

			// fechar o arquivo
			tArq2.close();
			this.vo.setNumAge(Integer.parseInt(this.tLinha[0]));
			this.vo.setNumConta(Integer.parseInt(this.tLinha[1]));
			this.vo.setNomeCliente(this.tLinha[2]);
			this.vo.setSaldo(Double.parseDouble(this.tLinha[3]));
			return this.vo;
		} catch (IOException tExcept) {
			tExcept.printStackTrace();
			return null;
		}
	}
}
