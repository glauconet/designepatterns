package modulo09.contacorrentemvc.view;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import modulo01.estudodecaso.util.Console;
import modulo09.contacorrentemvc.controller.Lab03TransacaoConta;
import modulo09.contacorrentemvc.controller.Lab03TransacaoDeposito;
import modulo09.contacorrentemvc.controller.Lab03TransacaoSaque;
import modulo09.contacorrentemvc.model.Lab01ContaCorrenteBean;

public class Lab01SistemaViewTexto {

	public static void main(String[] args) {
		Lab01SistemaViewTexto obj = new Lab01SistemaViewTexto();
		obj.execTelaPrincipal();
	}
	public void execCadastramento() {
		char opcao;

		Lab01ContaCorrenteBean vo = new Lab01ContaCorrenteBean();
		vo.setNumAge(Console.readInt("Numero da Agencia : "));
		vo.setNumConta(Console.readInt("Numero da Conta : "));
		vo.setNomeCliente(Console.readString("Nome do Cliente   : "));
		vo.setSaldo(Console.readFloat("Deposito Inicial  : "));
		opcao = Console.readChar("Confirma cadastramento (S/N) : ");
		if (opcao != 'S' && opcao != 's')
			return;
		if (Lab03TransacaoConta.cadastrarConta(vo) != true) {
			System.out.println("problemas no cadastro da conta corrente");
		}
	}
	public void execSaque() {
		char opcao;

		Lab01ContaCorrenteBean vo = new Lab01ContaCorrenteBean();
		vo.setNumAge(Console.readInt("Numero da Agencia : "));
		vo.setNumConta(Console.readInt("Numero da Conta : "));
		vo.setValor(Console.readFloat("Valor do Saque    : "));
		opcao = Console.readChar("Confirma saque (S/N) : ");
		if (opcao != 'S' && opcao != 's')
			return;
		if (Lab03TransacaoSaque.realizarSaque(vo) != true) {
			System.out.println("problemas no saque da conta corrente");
		}
	}

	public void execDeposito() {
		char opcao;

		Lab01ContaCorrenteBean vo = new Lab01ContaCorrenteBean();
		vo.setNumAge(Console.readInt("Numero da Agencia : "));
		vo.setNumConta(Console.readInt("Numero da Conta : "));
		vo.setValor(Console.readFloat("Valor do Deposito : "));
		opcao = Console.readChar("Confirma deposito (S/N) : ");
		if (opcao != 'S' && opcao != 's')
			return;

		if (Lab03TransacaoDeposito.realizarDeposito(vo) != true) {
			System.out.println("problemas no deposito da conta corrente");
		}
	}

	public void execConsulta() {
		Lab01ContaCorrenteBean vo = new Lab01ContaCorrenteBean();
		vo.setNumAge(Console.readInt("Numero da Agencia : "));
		vo.setNumConta(Console.readInt("Numero da Conta : "));

		vo = Lab03TransacaoConta.obterConta(vo);
		if (vo == null) {
			System.out.println("problemas na recuperacao da conta correntes");
		}
		System.out.println("------------------------------------------");
		System.out.println("Agencia : " + vo.getNumAge());
		System.out.println("Conta   : " + vo.getNumConta());
		System.out.println("Nome    : " + vo.getNomeCliente());
		NumberFormat formatter;
		formatter = DecimalFormat.getCurrencyInstance(new Locale("pt", "BR"));
		formatter.setMinimumFractionDigits(2);
		System.out.println("Saldo   : " + formatter.format(vo.getSaldo()));
	}

	public Lab01SistemaViewTexto() {
		super();
	}

	public void execTelaPrincipal() {
		char opcao;
		while (true) {
			System.out.println("Entre com a opcao desejada");
			System.out.println("1 - Cadastramento");
			System.out.println("2 - Saque");
			System.out.println("3 - Deposito");
			System.out.println("4 - Consulta");
			System.out.println("9 - Fim");
			opcao = Console.readChar("Opcao : ");
			if (opcao == '9')
				break;
			switch (opcao) {
			case '1':
				execCadastramento();
				break;
			case '2':
				execSaque();
				break;
			case '3':
				execDeposito();
				break;
			case '4':
				execConsulta();
				break;
			default:
				System.out.println("Opcao invalida. Reentre.");
			}
		}

	}
}
