package modulo09.estudodecaso.model;
//�  Interface Constantes 
public interface Constantes {
	public static final String RESOURCE_HISTORICO = "historico.properties";
	public static final String PROPERTY_HISTORICO = "historico";
	public static final String RESOURCE_FACTORY = "fabrica.properties";
	public static final String PROPERTY_FABRICA_CONTACORRENTE = "fabricacontacorrente";
	public static final String PROPERTY_FABRICA_CONTACORRENTE_ESPECIAL = "fabricacontacorrenteespecial";	
	public static final String RESOURCE_CONTA = "conta.properties";
	public static final String PROPERTY_CONTACORRENTE = "contacorrente";
	public static final String PROPERTY_CONTACORRENTE_ESPECIAL = "contacorrenteespecial";
	// Somente utilizado para o laborat�rio que trata do padr�o de projeto command
	public static final String CAMINHO_COMANDO = "modulo09.estudodecaso.viewcommand.Comando";
}

