package modulo09.estudodecaso.model.conta;
// �  Classe abstrata AbstractFactoryConta
import modulo07.MyClassException;
import modulo09.estudodecaso.model.ModoGravacao;
public abstract class AbstractFactoryConta {
	public abstract void cadastrar(ContaCorrenteVO vo ) throws MyClassException;
	// este metodo representa o padr�o de projeto template
	protected abstract String carregarProperties() throws MyClassException;
	public abstract void sacar(ContaCorrenteVO vo,double valor) throws MyClassException;
	public abstract void remunerar(ContaCorrenteVO vo) throws MyClassException;
	public abstract ContaCorrenteVO imprimir(ContaCorrenteVO vo) throws MyClassException;
	public abstract boolean removerArquivo(ContaCorrenteVO vo);
	public static ModoGravacao getInstanceModoGravacao(String qual) throws MyClassException {
			try {
				// Cria um objeto para o tipo da string recebida como par�metro.
				Class<?> classe = Class.forName(qual);
				// Obtem a referecia da classe da memoria
				ModoGravacao dao = (ModoGravacao) classe.newInstance();
				return dao;
			} catch(Exception e) {
				MyClassException myObj = new MyClassException ("Erro causado ao obter um objeto de ModoGravacao");
				myObj.setClasse("AbstractFactoryConta" );
				myObj.setMensagem(e.getMessage());
				// Se a classe n�o estiver em um pacote dar� erro
				myObj.setPacote("modulo09.estudodecaso.model.conta");
				myObj.setMetodo("getInstanceModoGravacao");
				e.printStackTrace();
				throw myObj; // Relan�ando a exce��o
			}		
		}
		public static AbstractFactoryConta getInstanceAbstractFactoryConta(String qual) throws MyClassException {
			try {
				// joga a classe para memoria
				Class<?> classe = Class.forName(qual);
				// paga a referecia da classe da memoria
				AbstractFactoryConta obj = (AbstractFactoryConta) classe.newInstance();
				return obj;
			} catch(Exception e) {
				MyClassException myObj = new MyClassException ("Erro causado ao criar objeto do tipo AbstractFactoryConta");
				myObj.setClasse("AbstractFactoryConta" );
				myObj.setMensagem(e.getMessage());
				// Se a classe n�o estiver em um pacote dar� erro
				myObj.setPacote("modulo09.estudodecaso.model.conta");
				myObj.setMetodo("getInstanceAbstractFactoryConta");
				throw myObj; // Relan�ando a exce��o
			}		
	}
	protected ModoGravacao obtemFormaGravacao () throws MyClassException{
			String objFormaGravacao = getNomeDao();
			ModoGravacao objTema  = getInstanceModoGravacao(objFormaGravacao);
			return objTema;
		}
	/*
	 * Utilizado para a cria��o dos objetos que definem a forma de grava��o.
	 * Como podemos observar a chamada do m�todo carregaProperties () segue o 
	 * padr�o de projeto template, pois ir� representar uma chamada diferente
	 * para cada f�brica. Sua implementa��o ocorre nas classes ContaCorrenteFactory e
	 * ContaCorrenteEspecialFactory.
	 */
	protected String getNomeDao() throws MyClassException {
			StringBuilder tpGravacao = new StringBuilder();
			// Uso do padr�o de projeto template com o metodo carregarProperties
			tpGravacao.append(carregarProperties());
			return tpGravacao.toString();
	}
	public void depositar(ContaCorrenteVO vo , double valor) throws MyClassException{
			ModoGravacao objTema = obtemFormaGravacao();
			vo = objTema.recuperar (vo);
			if (valor <= 0) {
				MyClassException myObj = new MyClassException ("O valor do deposito dever ser maior que zero");
				myObj.setClasse(this.getClass().toString());
				myObj.setPacote(this.getClass().getPackage().toString());
				myObj.setMetodo("depositar");
				throw myObj;
			} else {
				vo.setSaldo(vo.getSaldo() + valor);
				// implementando o conceito de template
				objTema.gravar(vo);
			}
	}
	public static boolean isContaEspecial(int agencia) {
			if (agencia >= 5000)
				return true;
			return false;
		}
}
