package modulo09.estudodecaso.model.conta;
//�  Classe PlainContaCorrenteEspecialDAO
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import modulo07.MyClassException;
import modulo09.estudodecaso.model.ModoGravacao;
public class PlainContaCorrenteEspecialDAO extends ModoGravacao {
	public void gravar(ContaCorrenteVO vo) throws MyClassException{
			PlainContaCorrenteDAO obj = new PlainContaCorrenteDAO();
			obj.gravar(vo);
			FileWriter tArq1=null;
			PrintWriter tArq2;
			try {
				// Opera��o I - Abrir o aquivo
				tArq1 = new FileWriter(vo.getAgencia() + "." + vo.getConta() + ".esp");
				tArq2 = new PrintWriter(tArq1);
				// Opera��o II - Gravar os dados
				tArq2.println(((ContaLimiteVO)vo).getLimite());
				// Opera��o III - Fechar o arquivo
				tArq2.close();
			} catch (IOException tExcept) { // exce��o - erro no processo
				MyClassException myObj = new MyClassException ("Erro causado na grava��o do arquivo Cta Especial");
				myObj.setClasse(this.getClass().toString());
				myObj.setMensagem(tExcept.getMessage());
				// Se a classe n�o estiver em um pacote dar� erro
				myObj.setPacote(this.getClass().getPackage().toString());
				myObj.setMetodo("gravar");
				throw myObj; // Relan�ando a exce��o
			}
	}
	public ContaCorrenteVO recuperar(ContaCorrenteVO vo) throws MyClassException{
			String[] tLinha;
			PlainContaCorrenteDAO cc = new PlainContaCorrenteDAO();
			vo = cc.recuperar(vo);
			FileReader tArq1 = null;
			BufferedReader tArq2;
			int tQtde = 3;
			try {
				// Opera��o I - Abrir o arquivo
				tArq1 = new FileReader(vo.getAgencia() + "." + vo.getConta() + ".esp");
				tArq2 = new BufferedReader(tArq1);
				// Opera��o III - Ler atributo/valor e colocar na matriz
				tLinha = new String[tQtde];
				for (int i = 0; i < tQtde; i++) {
					tLinha[i] = tArq2.readLine();
				}
				// Opera��o IV - Fechar o arquivo
				tArq2.close();
				((ContaLimiteVO)vo).setLimite(Double.parseDouble(tLinha[0]));
			} catch (IOException e) {
				MyClassException myObj = new MyClassException ("Conta Corrente n�o existe.");
				myObj.setClasse(this.getClass().toString());
				myObj.setMensagem(e.getMessage());
				myObj.setPacote(this.getClass().getPackage().toString());
				myObj.setMetodo("recuperar");
				throw myObj;		// relan�ando a exce��o			
			}
			return (ContaLimiteVO) vo;		
	}
}
