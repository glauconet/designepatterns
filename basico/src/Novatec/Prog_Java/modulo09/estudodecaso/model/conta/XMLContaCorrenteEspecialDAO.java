package modulo09.estudodecaso.model.conta;
// Classe XMLContaCorrenteEspecialDAO
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import modulo07.MyClassException;
import modulo09.estudodecaso.model.ModoGravacao;
import com.thoughtworks.xstream.XStream;
public class XMLContaCorrenteEspecialDAO  extends ModoGravacao {
	public void gravar(ContaCorrenteVO vo) throws MyClassException{
			try {
				XMLContaCorrenteDAO obj = new XMLContaCorrenteDAO();
				obj.gravar(vo);
				XStream xs = new XStream();	
				FileWriter tArq1 = null;
				PrintWriter fos;
				tArq1 = new FileWriter(getNomeArquivo(vo));
				fos = new PrintWriter(tArq1);
				/*
				 * No caso do arquivo xml estaremos gravando todo o objeto vo.
				 * Assim ser� gravado al�m do limite outros dados da conta
				 */
				xs.toXML(vo, fos);
			} catch (Exception tExcept) {
				MyClassException myObj = new MyClassException ("Erro causado na leitura do arquivo Conta Corrente");
				myObj.setClasse(this.getClass().toString());
				myObj.setMensagem(tExcept.getMessage());
				// se a classe utilizada como par�metro do m�todo setpacote n�o estiver em um pacote dar� erro
				myObj.setPacote(this.getClass().getPackage().toString());
				myObj.setMetodo("gravar");
				throw myObj;		// relan�ando a exce��o
			}
	}
	public ContaCorrenteVO recuperar(ContaCorrenteVO vo) throws MyClassException{
			try {	
				XStream xs = new XStream();
				File file = new File(getNomeArquivo(vo));
				FileInputStream fis = new FileInputStream(file);
				return (ContaLimiteVO) xs.fromXML(fis);
			} catch (Exception e) {
				MyClassException myObj = new MyClassException ("Erro causado na leitura do arquivo Conta Corrente");
				myObj.setClasse(this.getClass().toString());
				myObj.setMensagem(e.getMessage());
				// se a classe utilizada como par�metro do m�todo setpacote n�o estiver em um pacote dar� erro
				myObj.setPacote(this.getClass().getPackage().toString());
				myObj.setMetodo("recuperar");
				throw myObj;		// relan�ando a exce��o
			}
		}
		private String getNomeArquivo(ContaCorrenteVO vo) {
			int agencia = vo.getAgencia();
			int conta = vo.getConta();
			return getNomeArquivo(agencia, conta);
		}
		private String getNomeArquivo(int agencia, int conta) {
			return String.format("%s.%s.esp.xml", agencia, conta);
		}
}
