package modulo09.estudodecaso.model.historico;
//� Classe XMLHistoricoDAO 
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import modulo07.MyClassException;
import modulo09.estudodecaso.model.ModoGravacao;
import modulo09.estudodecaso.model.conta.ContaCorrenteVO;

import com.thoughtworks.xstream.XStream;
public class XMLHistoricoDAO extends ModoGravacao {
	public void gravar(ContaCorrenteVO vo) throws MyClassException {
			StringBuilder rLinha = new StringBuilder();
			try {
				XStream xs = new XStream();
				File tArq1 = new File(getNomeArquivo(vo));
				FileOutputStream fos = new FileOutputStream(tArq1);
				/*
				 * FileWriter tArq1 = null; PrintWriter fos; tArq1 = new FileWriter(getNomeArquivo(vo),true); fos = new
				 * PrintWriter(tArq1);
				 */
				Date hoje = new Date();
				Calendar cal = new GregorianCalendar();
				cal.setTime(hoje);
				// monta uma linha do historico de movimenta��o
				rLinha.append(vo.getAgencia() + " ");
				rLinha.append(vo.getConta() + " ");
				rLinha.append(cal.get(Calendar.DAY_OF_MONTH) + " ");
				rLinha.append(cal.get(Calendar.MONTH) + " ");
				rLinha.append(cal.get(Calendar.YEAR) + " ");
				rLinha.append(cal.get(Calendar. HOUR_OF_DAY) + " ");
				rLinha.append(cal.get(Calendar.MINUTE) + " ");
				rLinha.append(cal.get(Calendar.SECOND) + " ");
				rLinha.append(((ContaHistoricoVO) vo).getTipo() + " ");
				rLinha.append(((ContaHistoricoVO) vo).getValor() + " ");
				((ContaHistoricoVO) vo).getVetOperacoes().add(new String(rLinha.toString()));
				xs.toXML(vo, fos);
			} catch (Exception e) {
				MyClassException myObj = new MyClassException(
						"Erro causado na leitura do arquivo Cta Corrente");
				myObj.setClasse(this.getClass().toString());
				myObj.setMensagem(e.getMessage());
				// Se a classe n�o estiver em um pacote dar� erro
				myObj.setPacote(this.getClass().getPackage().toString());
				myObj.setMetodo("XMLContaCorrenteDAO.gravar");
				throw myObj; // Relan�ando a exce��o
			}
		}
		public ContaHistoricoVO recuperar(ContaCorrenteVO vo) throws MyClassException {
			try {
				XStream xs = new XStream();
				/*
				 * FileReader file = null; BufferedReader fis; // Opera��o I - Abrir
				 * o arquivo file = new FileReader(getNomeArquivo(vo)); fis = new
				 * BufferedReader(file);
				 */
				File file = new File(getNomeArquivo(vo));
				FileInputStream fis = new FileInputStream(file);
				ContaHistoricoVO obj = (ContaHistoricoVO) xs.fromXML(fis);
				return obj;
			} catch (ClassCastException tExcept) {
				MyClassException myObj = new MyClassException("Erro causado na leitura do arquivo Cta Corrente");
				myObj.setClasse(this.getClass().toString());
				myObj.setMensagem(tExcept.getMessage());
				// Se a classe n�o estiver em um pacote dar� erro
				myObj.setPacote(this.getClass().getPackage().toString());
				myObj.setMetodo("XMLContaCorrenteDAO.recuperar");
				tExcept.printStackTrace();
				throw myObj; // Relan�ando a exce��o
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				MyClassException myObj = new MyClassException("Erro na abertura do arquivo ");
				myObj.setClasse(this.getClass().toString());
				myObj.setMensagem(e.getMessage());
				// Se a classe n�o estiver em um pacote dar� erro
				myObj.setPacote(this.getClass().getPackage().toString());
				myObj.setMetodo("XMLContaCorrenteDAO.recuperar");
				throw myObj; // Relan�ando a exce��o
			}
		}
	private String getNomeArquivo(ContaCorrenteVO vo) {
			int agencia = vo.getAgencia();
			int conta = vo.getConta();
			return getNomeArquivo(agencia, conta);
	}
	private String getNomeArquivo(int agencia, int conta) {
			return String.format("%s.%s.hist.xml", agencia, conta);
	}
}
