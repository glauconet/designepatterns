package modulo09.estudodecaso.viewcommand;

//� Classe ComandoCalcularJuros. Op��o do menu 
import modulo07.MyClassException;
import modulo09.estudodecaso.controller.GerenteContaCorrente;
public class ComandoCalcularJuros extends ComandoAbstrato {
	public ComandoCalcularJuros() throws MyClassException {
			super();
	}
	@Override
	protected String getNome() {
			return "Atualizar Saldo";
		}
		@Override
		protected void lerDados() {
			lerAgenciaConta();
		}
	@Override
	protected void executarComando() throws MyClassException {
			GerenteContaCorrente.execAtualizarSaldo(this.vo);
			System.out.println("Juros calculado.");
	}
}
