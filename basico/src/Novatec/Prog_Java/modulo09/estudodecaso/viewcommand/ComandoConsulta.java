package modulo09.estudodecaso.viewcommand;
//� Classe ComandoConsulta. Op��o do menu 
import java.util.Map;

import modulo07.MyClassException;
import modulo09.estudodecaso.controller.GerenteContaCorrente;
import modulo09.estudodecaso.model.conta.ContaCorrenteVO;
public class ComandoConsulta extends ComandoAbstrato {
	public ComandoConsulta() throws MyClassException {
			super();
		}
	@Override
	protected String getNome() {
			return "Consulta";
	}
	@Override
		protected void lerDados() {
			lerAgenciaConta();
		}
	@Override
	protected void executarComando() throws MyClassException {
			Map<String, ContaCorrenteVO> mapa = GerenteContaCorrente.execConsulta(this.vo);
			apresentarConsulta(mapa);
	}
}
