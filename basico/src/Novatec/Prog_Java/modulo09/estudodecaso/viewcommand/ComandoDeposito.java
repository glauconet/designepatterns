package modulo09.estudodecaso.viewcommand;
//� Classe ComandoDeposito. Op��o do menu 
import modulo07.MyClassException;
import modulo09.estudodecaso.controller.GerenteContaCorrente;
public class ComandoDeposito extends ComandoAbstrato {
	private double valor ;
	public double getValor() {
			return this.valor;
	}
	public void setValor(double valor) {
			this.valor = valor;
	}
	public ComandoDeposito() throws MyClassException {
			super();
	}
	@Override
	protected String getNome() {
			return "Dep�sito";
	}
	@Override
	protected void lerDados() {
			lerAgenciaConta();
			lerSaldo();
	}
	@Override
	protected void executarComando() throws MyClassException {
			GerenteContaCorrente.execDeposito(this.vo);
			System.out.println("Dep�sito realizado.");
	}
}
