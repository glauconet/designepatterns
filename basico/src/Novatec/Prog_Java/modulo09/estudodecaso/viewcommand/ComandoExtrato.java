package modulo09.estudodecaso.viewcommand;

import java.util.Map;

import modulo07.MyClassException;
import modulo09.estudodecaso.controller.GerenteContaCorrente;
import modulo09.estudodecaso.model.conta.ContaCorrenteVO;
public class ComandoExtrato extends ComandoAbstrato {
	public ComandoExtrato() throws MyClassException {
		super();
	}
	@Override
	protected String getNome() {
		return "Extrato";
	}
	@Override
	protected void lerDados() {
		lerAgenciaConta();
	}
	@Override
	protected void executarComando() throws MyClassException {
		Map<String, ContaCorrenteVO> impressaoMapa = GerenteContaCorrente.execExtrato(this.vo);
		apresentarConsulta(impressaoMapa);
		apresentarExtrato(impressaoMapa);
	}
}