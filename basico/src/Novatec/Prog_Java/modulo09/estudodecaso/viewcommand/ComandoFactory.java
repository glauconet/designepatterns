package modulo09.estudodecaso.viewcommand;
//� Classe ComandoFactory 
import modulo07.MyClassException;
import modulo09.estudodecaso.model.Constantes;
public class ComandoFactory {
	private static ComandoFactory instance = new ComandoFactory();
	private ComandoFactory() {
			super();
	}
	public static ComandoFactory getInstance() {
			return instance;
	}
	public Comando criarComando(String nomeComando) throws MyClassException {
			try {
				Class<?> classe = getClasseComando(nomeComando);
				return (Comando) classe.newInstance();
			} catch (Exception e) {
				MyClassException myObj = new MyClassException("Comando inexistente: ");
				myObj.setClasse(this.getClass().toString());
				myObj.setPacote(this.getClass().getPackage().toString());
				myObj.setMetodo("criarComando");
				throw myObj;
			}
	}
	private Class<?> getClasseComando(String comando)
				throws ClassNotFoundException {
			return Class.forName(String.format(Constantes.CAMINHO_COMANDO + "%s", comando));
		}
}
