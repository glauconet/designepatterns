package modulo09.estudodecaso.viewcommand;
//� Classe ValidadorDeContaCorrente 
import java.util.ArrayList;
import java.util.List;

import modulo07.MyClassException;
import modulo09.estudodecaso.model.conta.ContaCorrenteVO;
import modulo09.estudodecaso.model.conta.ContaLimiteVO;
public class ValidadorDeContaCorrente {
	private List<String> erros = new ArrayList<String>();
	ContaCorrenteVO vo = new ContaCorrenteVO(); 
	private boolean eParaValidarAgencia = false;
	private boolean eParaValidarConta = false;
	private boolean eParaValidarCliente = false;
	private boolean eParaValidarValor = false;
	private boolean eParaValidarLimite = false;
	public ValidadorDeContaCorrente() {
			super();
	}
	public ValidadorDeContaCorrente(ContaCorrenteVO vo) {
			super();
			this.vo = vo;
			this.eParaValidarAgencia = true;
			this.eParaValidarConta = true;
			if (vo.getNome() != null)
				this.eParaValidarCliente = true;
			this.eParaValidarValor = true;
			if (vo instanceof ContaLimiteVO) {
				if (((ContaLimiteVO)vo).getLimite() != 0)
					this.eParaValidarLimite = true;
			}
		}
	public boolean isAgenciaValida(int agencia) {
			boolean result = true;
			if (agencia <= 0) {
				result = false;
				this.erros.add("N�mero da Ag�ncia n�o pode ser zero ou negativo");
			} else if (agencia > 9999) {
				result = false;
				this.erros.add("N�mero da Ag�ncia n�o pode ser maior que 9999.");
			}
			return result;
		}
	public boolean isContaValida(int conta) {
			boolean result = true;
			if (conta <= 0) {
				result = false;
				this.erros.add("N�mero da Conta n�o pode ser zero ou negativo.");
			} else if (conta > 9999999) {
				result = false;
				this.erros.add("N�mero da Ag�ncia n�o pode ser maior que 9999999.");
			}
			return result;
		}
		public boolean isClienteValido(String cliente) {
			boolean result = true;
			if (cliente == null) {
				result = false;
				this.erros.add("Cliente n�o pode ser nulo.");
			} else if (cliente.trim().length() == 0) {
				result = false;
				this.erros.add("Cliente n�o pode estar vazio ou conter apenas espa�os.");
			}
			return result;
		}
		public boolean isValorValido(double valor) {
			boolean result = true;
			if (valor <= 0) {
				result = false;
				this.erros.add("Valor n�o pode ser zero ou negativo.");
			}
			return result;
		}
		public boolean isLimiteValido(double limite) {
			boolean result = true;
			if (limite <= 0) {
				result = false;
				this.erros.add("Limite n�o pode ser zero ou negativo.");
			}
			return result;
		}
		public void validar() throws MyClassException {
			boolean result = true;
			if (this.eParaValidarAgencia)
				result = isAgenciaValida(this.vo.getAgencia()) && result;
			if (this.eParaValidarConta)
				result = isContaValida(this.vo.getConta()) && result;
			if (this.eParaValidarCliente)
				result = isClienteValido(this.vo.getNome()) && result;
			if (this.eParaValidarLimite)
				result = isLimiteValido(((ContaLimiteVO)this.vo).getLimite()) && result;
			if (!result)
				throw new MyClassException(this.erros.toString());
		}
	public List<String> getErros() {
			return this.erros;
		}
		public void clear() {
			this.erros.clear();
		}
}
