package modulo09.factoryContaCorrente;
//� ContaCorrenteFactory 
import java.util.HashMap;
import java.util.Map;

import modulo07.MyClassException;
public class ContaCorrenteFactory {
		// Uso do padr�o de projeto Lazy Instantiation
		public static Map<String, ContaCorrenteDAO> mapaFabrica = new HashMap<String, ContaCorrenteDAO>();
		private ContaCorrenteFactory() {
			super();
	}
		public static ContaCorrenteDAO getContaCorrente(String qualClasseConcreta) throws MyClassException {
			ContaCorrenteDAO dao = mapaFabrica.get(qualClasseConcreta);
			if (dao == null) {
				// Usando a API Reflection
				Class<?> classe;
				try {
					classe = Class.forName(qualClasseConcreta);
					dao = (ContaCorrenteDAO) classe.newInstance();
				} catch (InstantiationException e) {
					throw new MyClassException(
							"Erro na F�brica - Instantiation", e);
				} catch (IllegalAccessException e) {
					throw new MyClassException(
							"Erro na F�brica - IllegalAccess", e);
				} catch (ClassNotFoundException e) {
					throw new MyClassException(
							"Erro na F�brica - ClassNotFound", e);
				}
				mapaFabrica.put(qualClasseConcreta, dao);
			}
			return dao;
		}
}
