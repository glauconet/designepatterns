package modulo09.factoryContaCorrente;

// Classe PrincipalContaCorrente
import java.util.Scanner;
// A classe MyClassException deve ter sido criada no pacote utilizado no comando import
import modulo07.MyClassException;

public class PrincipalContaCorrente {
	public static void main(String[] args) {
		final String classes[] = {
				"modulo09.factoryContaCorrente.AccessContaCorrente",
				"modulo09.factoryContaCorrente.PlainContaCorrente",
				"modulo09.factoryContaCorrente.XMLContaCorrente" };
		try {
			int op = 0;
			Scanner sc = new Scanner(System.in);
			while (true) {
				System.out.println("1 - Banco de dados");
				System.out.println("2 - Arquivo texto");
				System.out.println("3 - XML");
				System.out.println("9 - Fim");
				System.out.println("Entre com a op��o: ");
				op = sc.nextInt();
				switch (op) {
				case 1:
					// cria um objeto da classe AccessContaCorrente
					apresentaResultado(classes[0]);
					break;
				case 2:
					// cria um objeto da classe PlainContaCorrente
					apresentaResultado(classes[1]);
					break;

				case 3:
					// cria um objeto da classe XMLContaCorrente
					apresentaResultado(classes[2]);
					break;
				default:
					if (op == 9)
						System.exit(0);
					else
						System.out.println("Op��o inv�lida.");
				}
			}
		} catch (MyClassException e) {
			System.out.println(e.toString());
			System.out.println();
		}
	}

	public static void apresentaResultado(String conta) throws MyClassException {
		ContaCorrenteDAO dao = ContaCorrenteFactory.getContaCorrente(conta);
		Scanner sc = new Scanner(System.in);
		int op = 0;
		System.out.println("1 - Inserir dados");
		System.out.println("2 - Recuperar contas por ag�ncia");
		System.out.println("3 - Recuperar contas");
		System.out.println("Entre com a op��o: ");
		op = sc.nextInt();
		switch (op) {
		case 1:
			dao.insere(null);
			break;
		case 2:
			dao.recuperarContasByAgencia(0);
			break;
		case 3:
			dao.recuperarConta(0, 0);
			break;
		default:
			if (op == 9)
				System.exit(0);
			else
				System.out.println("op��o inv�lida.");
		}
	}
}