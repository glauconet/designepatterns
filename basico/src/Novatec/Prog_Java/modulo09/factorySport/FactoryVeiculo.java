package modulo09.factorySport;
//� Classe abstrata FactoryVeiculo � Constr�i objetos usando a API Reflection 
import modulo07.MyClassException;
public abstract class FactoryVeiculo {
	public static final String NOME_PACOTE = "modulo09.factorySport.";
	public static SportRadical getVeiculoRadical(String tipoVeiculo) throws MyClassException {
			if (tipoVeiculo == null) {
				throw new MyClassException("Tipo do Veiculo igual a null");
			}
			try {
				// Usando Reflection para criar um objeto do tipo da classe indicada pelo par�metro tipoVeiculo.
				Class<?> classe = Class.forName(NOME_PACOTE + tipoVeiculo);
				return (SportRadical) classe.newInstance();
			} catch (Exception e) {
				throw new MyClassException("Tipo de sport para "+ tipoVeiculo + " incorreto");
			}
		}
}
