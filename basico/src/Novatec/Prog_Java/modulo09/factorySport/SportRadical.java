package modulo09.factorySport;
//� Interface SportRadical � Define o tema para o padr�o de projeto factory 
/* Neste exemplo o tema comum entre as f�bricas ser� esta interface */
public interface SportRadical {
	public void cadastrarPiloto();
	public void cadastrarProva();
	public void cadastrarCalendario();
	public void imprimirResultadoProva();
	public void imprimirCalendario();
}

