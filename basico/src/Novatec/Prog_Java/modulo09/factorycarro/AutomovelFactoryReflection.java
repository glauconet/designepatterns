package modulo09.factorycarro;

// Classe AutomovelFactoryReflection
// A classe MyClassException deve ter sido criada no pacote utilizado no comando import
import modulo07.MyClassException;

public class AutomovelFactoryReflection {
	public static ExemploAutomovel getAutomovel(String tipoCarro)
			throws MyClassException {
		ExemploAutomovel obj = null;
		try {
			/*
			 * o s�mbolo ? significa um tipo desconhecido. Este s�mbolo foi
			 * inserido por meio do conceito de gen�ricos presente na plataforma
			 * Java a partir da vers�o 1.5 do J2SE. Estamos tamb�m utilizando
			 * Reflection para a cria��o do objeto.
			 */
			Class<?> classe = Class.forName(tipoCarro);
			obj = (ExemploAutomovel) classe.newInstance();
		} catch (ClassNotFoundException e) {
			MyClassException myObj = new MyClassException(
					"Problemas com ClassNotFoundException");
			myObj.setMensagem(e.getMessage());
			myObj.setMetodo("getAutomovel()");
			throw myObj;
		} catch (InstantiationException e) {
			MyClassException myObj = new MyClassException(
					"Problemas com InstantiationException");
			myObj.setMensagem(e.getMessage());
			myObj.setMetodo("getAutomovel()");
			throw myObj;

		} catch (IllegalAccessException e) {
			MyClassException myObj = new MyClassException(
					"Problemas com IllegalAccessException");
			myObj.setMensagem(e.getMessage());
			myObj.setMetodo("getAutomovel()");
			throw myObj;
		}
		return obj;
	}
}