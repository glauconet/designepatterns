package modulo09.factorycarro;

// Classe PrincipalFactoryCarro
import java.util.Scanner;

public class PrincipalFactoryCarro {
	public void execMenuPrincipal() {
		int iTipo = 0;
		final String valor[] = { "Corsa", "Celta", "Polo", "CrossFox" };
		while (iTipo != 9) {
			ExemploAutomovel auto = null;
			System.out.println("1 - " + valor[0].toString());
			System.out.println("2 - " + valor[1].toString());
			System.out.println("3 - " + valor[2].toString());
			System.out.println("4 - " + valor[3].toString());
			System.out.println("9 - Fim \n");
			Scanner scan = new Scanner(System.in);
			System.out.print("Escolha o carro e analise seu pre�o: ");

			iTipo = scan.nextInt();
			switch (iTipo) {
			case 1: {
				auto = AutomovelFactory.getAutomovel(valor[0]);
				break;
			}
			case 2: {
				auto = AutomovelFactory.getAutomovel(valor[1]);
				break;
			}
			case 3: {
				auto = AutomovelFactory.getAutomovel(valor[2]);
				break;
			}
			case 4: {
				auto = AutomovelFactory.getAutomovel(valor[3]);
				break;
			}
			case 9: {
				System.exit(0);
				break;
			}
			default:
				System.out.println();
				System.out.println("Op��o inv�lida. Tecle <ENTER>.");
				break;
			}
			if (auto != null) {
				// m�todo getPreco() ser� executado para o objeto escolhido
				System.out.println("Pre�o do carro: " + auto.getPreco());
			}
		}
	}

	public static void main(String[] args) {
		(new PrincipalFactoryCarro()).execMenuPrincipal();
	}
}