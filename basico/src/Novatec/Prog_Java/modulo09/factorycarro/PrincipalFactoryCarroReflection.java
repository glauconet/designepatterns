package modulo09.factorycarro;

// Classe PrincipalFactoryCarroReflection
import java.util.Scanner;
// A classe MyClassException deve ter sido criada no pacote utilizado no comando import
import modulo07.MyClassException;

public class PrincipalFactoryCarroReflection {
	public void execMenuPrincipal() {
		int iTipo = 0;
		final String valor[] = { "modulo09.factorycarro.ExemploCorsa",
				"modulo09.factorycarro.ExemploCelta",
				"modulo09.factorycarro.ExemploPolo",
				"modulo09.factorycarro.ExemploCrossFox" };
		while (iTipo != 9) {
			ExemploAutomovel auto = null;
			System.out.println("1 - Corsa");
			System.out.println("2 - Celta");
			System.out.println("3 - Polo");
			System.out.println("4 - CrossFox");
			System.out.println("9 - Fim \n");
			Scanner scan = new Scanner(System.in);
			System.out.print("Escolha o carro e analise seu pre�o: ");
			iTipo = scan.nextInt();
			try {
				switch (iTipo) {
				case 1: {
					auto = AutomovelFactoryReflection.getAutomovel(valor[0]);
					break;
				}
				case 2: {
					auto = AutomovelFactoryReflection.getAutomovel(valor[1]);
					break;
				}
				case 3: {
					auto = AutomovelFactoryReflection.getAutomovel(valor[2]);
					break;
				}

				case 4: {
					auto = AutomovelFactoryReflection.getAutomovel(valor[3]);
					break;
				}
				case 9: {
					System.exit(0);
					break;
				}
				default:
					System.out.println();
					System.out.println("Op��o inv�lida. Tecle <ENTER>.");
					break;
				}
			} catch (MyClassException e) {
				System.out.println("Erro: " + e.getMessage());
				System.out.println("Mensagem: " + e.getMensagem());
			}
			if (auto != null) {
				// o m�todo getPreco() ser� executado para o objeto escolhido
				System.out.println("Pre�o do carro: " + auto.getPreco());
			}
		}
	}

	public static void main(String[] args) {
		(new PrincipalFactoryCarroReflection()).execMenuPrincipal();
	}
}