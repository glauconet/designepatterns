package modulo09.singleton;
//� Classe ExemploUsoSingleton 
public class ExemploUsoSingleton {
	public static void main(String args[]) {
			ExemploSingleton obj = ExemploSingleton.getInstance();
			obj.imprimirRecurso("nome");
			obj.imprimirRecurso("email");
			System.out.println("***********************");
			/* O objeto ser� criado apenas uma vez e poder� ser utilizado por todo o programa */
			ExemploSingleton obj1 = ExemploSingleton.getInstance();
			obj1.imprimirRecurso("nome");
			obj1.imprimirRecurso("email");
	}
}

