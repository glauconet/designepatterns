package modulo10;

public final class ExemploClassEmpregado {
	public String nome;

	public String sobreNome;

	public int salario;

	public ExemploClassEmpregado() {
		this("Edilson", "Zanetti", 50000);
	}

	public ExemploClassEmpregado(String nome, String sobreNome, int salario) {
		this.nome = nome;
		this.sobreNome = sobreNome;
		this.salario = salario;
	}
}