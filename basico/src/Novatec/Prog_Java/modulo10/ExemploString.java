package modulo10;

import java.lang.reflect.Method;

public class ExemploString {
	public static void main(String[] args) {
		try {
			Class c = Class.forName("java.io.InputStream");
			/*
			 * o m�todo getDeclaredMethods retorna o nome de todos os m�todos da
			 * classe InputStream.
			 */
			Method m[] = c.getDeclaredMethods();
			for (int i = 0; i < m.length; i++) {
				System.out.println(m[i].toString());
			}
		} catch (ClassNotFoundException e) {
			System.err.println(e);
		}
	}
}