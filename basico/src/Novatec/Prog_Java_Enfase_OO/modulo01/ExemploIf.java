package modulo01;
// Exemplo do comando if 
public class ExemploIf {
  public static void main(String args[]) {
    int var1 = 20;
    int var2 = 10;
    // modo de uso: if (condicao)
    if (var1 > var2) {
      // bloco de comandos do if
      System.out.println("var1 � maior que var2");
    } 
  }
}
