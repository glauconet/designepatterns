package modulo01;
// Exemplo comando if e do comando else
public class ExemploIfElse {
  public static void main(String args[]) {
    int var1 = 10;
    int var2 = 20;
    // modo de uso: if (condicao)
    if (var1 > var2) {
      // bloco de comandos do if
      System.out.println("var1 � maior que var2");
    } else { // condi��o avaliada como falso
      // bloco de comandos do else
      System.out.println("var1 � menor que var2");
    }
  }
}
