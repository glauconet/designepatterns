package modulo01;

// Exemplo da classe Scanner lendo dados do teclado
import java.util.Scanner;

public class ExemploScanner {
	public static void main(String args[]) {
		System.out.print("Digite um n�mero inteiro: ");
		Scanner var = new Scanner(System.in);
		int numero = var.nextInt(); // declara e inicia a vari�vel
		System.out.println("Valor digitado = " + numero);
		System.out.print("Digite uma string composta: ");
		/*
		 * definindo que o delimitador de leitura do objeto sc � o enter. Para
		 * formalizar que o delimitador � o enter usamos os caracteres especiais
		 * \r\n. O caracter padr�o do comando Scanner � o espa�o em branco.
		 */
		Scanner sc = new Scanner(System.in).useDelimiter("\r\n");
		/*
		 * como o delimitador do objeto sc � igual a enter podemos usar o m�todo
		 * next() para ler strings compostas.
		 */
		String nome = sc.next();
		System.out.println("String digitada = " + nome);
		System.out.print("Digite uma string composta: ");
		// criando um novo objeto Scanner, sem altera��o do delimitador padr�o
		Scanner sc1 = new Scanner(System.in);
		// para o objeto sc1 ler uma string composta precisamos do m�todo
		// nextLine
		String nome1 = sc1.nextLine(); // usando o m�todo nextLine
		System.out.println("String digitada = " + nome1);
		/*
		 * situa��o onde um problema ser� encontrado. Quando n�o modificamos o
		 * delimitador padr�o apesar de ser lida uma string composta ser�
		 * apresentada na tela apenas a primeira parte da string at� o espa�o em
		 * branco.
		 */
		System.out.print("Digite uma nova String composta: ");
		nome1 = sc1.next(); // usando o m�todo next
		System.out.println("String digitada = " + nome1);
	}
}