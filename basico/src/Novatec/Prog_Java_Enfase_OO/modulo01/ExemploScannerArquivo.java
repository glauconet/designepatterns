package modulo01;
//� Exemplo da classe Scanner lendo dados de um arquivo 
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ExemploScannerArquivo {
	public static void main(String args[]) throws FileNotFoundException {
			System.out.println("N�meros contidos no arquivo: ");
			// o arquivo deve ficar dentro do pacote junto com a classe
			Scanner sc = new Scanner(new File(".\\modulo01\\numeros.txt"));
			while (sc.hasNextLong()) {
				long aLong = sc.nextLong();
				System.out.println ("Numero: " + aLong);
			}
	}
}

