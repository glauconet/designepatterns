package modulo01;

public class ExemploVariaveis {
	int atributoNaoEstatico;

	static int atributoEstatico;

	public static void main(String[] args) {
		int variavelMetodo = 50;
		ExemploVariaveis obj = new ExemploVariaveis();
		obj.atributoNaoEstatico = 10;
		obj.atributoEstatico = 20;
		obj.imprime();

		ExemploVariaveis obj1 = new ExemploVariaveis();
		obj1.atributoNaoEstatico = 160;
		obj1.atributoEstatico = 320;
		obj1.imprime();
		System.out.println("-------------------");
		obj.imprime(); // ir� imprimir o valor que obj2 atribuiu
	}

	public void imprime() {
		System.out.println("Atributo n�o estatico: " + atributoNaoEstatico);
		System.out.println("Atributo estatico: " + atributoEstatico);
	}

}
