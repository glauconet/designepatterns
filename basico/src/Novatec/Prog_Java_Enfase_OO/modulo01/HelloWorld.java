package modulo01;

// Classe HelloWorld – Tem como objetivo imprimir na tela a frase Hello World e os parâmetros recebidos.
public class HelloWorld {
	public static void main(String args[]) {
		System.out.println("Parametro 01: " + args [0]);
		System.out.println("Parametro 02: " + args [1]);
		System.out.println("Hello World");
	}
}
