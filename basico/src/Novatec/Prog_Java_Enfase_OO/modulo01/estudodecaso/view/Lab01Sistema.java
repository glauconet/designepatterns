package modulo01.estudodecaso.view;
// Resposta do Laborat�rio 1
import java.util.Scanner;
// A classe Console precisa estar dispon�vel no pacote modulo01.estudodecaso.util.
import modulo01.estudodecaso.util.Console;
public class Lab01Sistema {
int numAge;
int numConta;
double valor;
private void leValores() {
do {
this.numAge = Console.readInt("N�mero da ag�ncia: ");
} while (this.numAge <= 0);
do {
this.numConta = Console.readInt("N�mero da conta: ");
} while (this.numConta <= 0);
do {
this.valor = Console.readDouble("Valor: ");
} while (this.valor <= 0.0);
}
public void execCadastramento() {
String nome = null;
// usando a classe Scanner como uma op��o de leitura
Scanner sc = new Scanner(System.in);
char opcao;
leValores();
do {
System.out.println("Nome do cliente: ");
nome = sc.nextLine();
} while (nome.equals(""));
opcao = Console.readChar("Confirma cadastramento (S/N): ");
if ((opcao == 'S') || ((opcao == 's'))) {
System.out.println("Cadastramento realizado com sucesso.");
} else {
System.out.println("Cadastramento n�o realizado.");
}
}
public void execSaque() {
char opcao;
leValores();
opcao = Console.readChar("Confirma saque (S/N): ");
if ((opcao == 'S') || ((opcao == 's'))) {
System.out.println("Saque efetuado com sucesso.");
} else {
System.out.println("Saque n�o realizado.");
}
}

public void execDeposito() {
char opcao;
leValores();
opcao = Console.readChar("Confirma dep�sito (S/N): ");
if ((opcao == 'S') || ((opcao == 's'))) {
System.out.println("Dep�sito efetuado com sucesso.");
} else {
System.out.println("Dep�sito n�o realizado.");
}
}
public static void main(String[] args) {
char opcao;
Lab01Sistema obj = new Lab01Sistema();
while (true) {
System.out.println("Entre com a op��o desejada");
System.out.println("1 - Cadastramento");
System.out.println("2 - Saque");
System.out.println("3 - Dep�sito");
System.out.println("9 - Fim");
opcao = Console.readChar("Op��o: ");
if (opcao == '9')
break;
switch (opcao) {
case '1':
obj.execCadastramento();
break;
case '2':
obj.execSaque();
break;
case '3':
obj.execDeposito();
break;
default:
System.out.println("Op��o inv�lida. Reentre.");
}
}
}
}