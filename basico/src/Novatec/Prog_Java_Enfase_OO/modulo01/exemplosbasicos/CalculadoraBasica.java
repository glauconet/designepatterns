package modulo01.exemplosbasicos;

/*
 * Para executar atrav�s do emulador DOS.
 * Para gerar o executavel:
 * 1 - Criar uma  folder chamada META-INF abaixo do projeto
 * 2 - Clicar no projeto  - File - Export - JAR File
 * 3 - Dar um nome para o jar (teste.jar) -  next - next -
 * 4 - Escolher o radio buttom Generate manifest file
 * 5 - Deixar marcado save the manifest in the workspace
 * 6 - Em Main Class escolher Browse
 * 7 - Escolher a classe que contenha o m�todo main
 * 8 - finished
 * Para executar como um arquivo no emulador do DOS
 * 		C:\temp>java -jar teste.jar
 */
public class CalculadoraBasica {
	public static void main(String[] args) {
		// sinal /
		if ( args.length == 0 || args.length < 2 ){
			System.out.println("Passar par�metros");
			System.out.println("Ex.: java -jar teste.jar 12 4");
			System.exit(10);
		}
		int x = Integer.parseInt(args [0]);
		int y = Integer.parseInt(args [1]);
		int res = 0;
		while (x > 1 ){
			x = x - y;
			res++;
		}
		System.out.println("Divisao inteira: " + Integer.parseInt(args [0]) + " / " + Integer.parseInt(args [1]) + " = " +  res);

		// sinal *
		res = 0;
		x = Integer.parseInt(args [0]);
		y = Integer.parseInt(args [1]);		
		for (int i = 0 ; i < y ; i ++){
			res = x + res;
		}
		System.out.println("Multiplicacao: " + Integer.parseInt(args [0]) + " * " + Integer.parseInt(args [1]) + " = " +  res);

		// sinal pot (x,y)
		res = 0;
		x = Integer.parseInt(args [0]);
		y = Integer.parseInt(args [1]);
		int pot = x;
		for (int j = 1 ; j < y ; j ++){
			for (int i = 0 ; i < x ; i ++){
				res = res + pot;
			}
			pot = res;
			res = 0;
		
		}
		System.out.println("Exponenciacao: pot(" + Integer.parseInt(args [0]) + "," + Integer.parseInt(args [1]) + ") = " +  pot);

	} 

}
