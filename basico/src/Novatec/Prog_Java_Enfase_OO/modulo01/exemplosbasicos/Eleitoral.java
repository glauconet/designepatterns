package modulo01.exemplosbasicos;
//� Resposta do exerc�cio 09
public class Eleitoral {
	String nome;
	int idade;
	public void imprimir() {
			System.out.println("Nome do eleitor: " + this.nome);
			System.out.println("idade do eleitor: " + this.idade);
			// Executar o m�todo verificar para que este imprima seu resultado na tela.
			verificar();
	}
	public void verificar() {
			if (this.idade < 16) {
				System.out.println("Eleitor n�o pode votar");
			}
			if ((this.idade >= 16) && (this.idade <= 65)) {
				System.out.println("Eleitor deve votar");
			}
			if (this.idade > 65) {
				System.out.println("Voto facultativo");
			}
	}
}
