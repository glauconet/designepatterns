package modulo01.exemplosbasicos;
// Resposta do exerc�cio 4
public class PrincipalAcampamento {
public static void main(String[] args) {
Acampamento novoObj = new Acampamento();
novoObj.idade = 22;
novoObj.nome = "Rafael Zanetti";
// n�o precisamos atribuir valor para equipe, pois ela ser� definida por meio da idade
System.out.println("Imprimindo os dados inicializados");
novoObj.imprimir();
novoObj.separarGrupo();
System.out.println();
System.out.println("Imprimindo os dados ap�s a execu��o do m�todo separarGrupo");
novoObj.imprimir();
}
}