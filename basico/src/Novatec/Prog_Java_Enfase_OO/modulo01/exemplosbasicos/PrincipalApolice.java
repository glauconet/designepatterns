package modulo01.exemplosbasicos;
//� Resposta do exerc�cio 02
public class PrincipalApolice {
	public static void main(String[] args) {
			Apolice novoObj = new Apolice();
			novoObj.idade = 25;
			novoObj.nome = "Gustavo Baravieira Costa";
			novoObj.valorPremio = 500;
			System.out.println();
			System.out.println("Imprimindo os dados inicializados");
			novoObj.imprimir();
			novoObj.calcularPremioApolice();
			System.out.println();
			System.out.println("Imprimindo os dados apos a execu��o do m�todo calcularPremioApolice");
			novoObj.imprimir();
			novoObj.oferecerDesconto("Curitiba");
			System.out.println();
			System.out.println("Imprimindo os dados apos a execu��o do m�todo oferecerDesconto");
			novoObj.imprimir();
	}
}
