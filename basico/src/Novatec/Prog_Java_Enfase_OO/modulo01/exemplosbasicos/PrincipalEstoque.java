package modulo01.exemplosbasicos;
//� Resposta do exerc�cio 12
public class PrincipalEstoque {
	public static void main(String[] args) {
			//objeto 01
			Estoque es1 = new Estoque();
			es1.nomeProduto = "Mochilas";
			es1.quantidade = 100;
			es1.valor = 10;
			es1.imprimir();
			int ret = es1.verificarDisponibilidade(100);
			if (ret == 1) {
				System.out.println("produto na quantidade informada dispon�vel");
			}
			else{
				System.out.println("produto na quantidade informada N�O dispon�vel");
			}
			ret = es1.verificarDisponibilidade(500);
			if (ret == 1) {
				System.out.println("produto na quantidade informada dispon�vel");
			}
			else{
				System.out.println("produto na quantidade informada N�O dispon�vel");
			}
			//objeto 02
			Estoque es2 = new Estoque();
			es2.nomeProduto = "Pastas";
			es2.quantidade = 50;
			es2.valor = 250;
			//objeto 03
			Estoque es3 = new Estoque();
			es3.nomeProduto = "Telefones";
			es3.quantidade = 150;
			es3.valor = 59;
			ret = es3.verificarDisponibilidade(120);
			if (ret == 1) {
				System.out.println("produto na quantidade informada dispon�vel");
			}
			else{
				System.out.println("produto na quantidade informada N�O dispon�vel");
			}
	}
}
