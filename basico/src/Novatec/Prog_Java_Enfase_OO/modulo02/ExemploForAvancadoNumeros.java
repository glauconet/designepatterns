package modulo02;

// Usando o comando for avan�ado com um vetor de inteiros
public class ExemploForAvancadoNumeros {
	public static void main(String[] args) {
		int j = 0;
		int[] squares = { 0, 1, 4, 9, 16, 25 };
		for (int i : squares) {
			System.out.printf("%d ao quadrado � %d.\n", j++, i);
		}
	}
}