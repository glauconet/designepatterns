package modulo02;

// Exemplo utilizando for tradicional com um vetor de strings
public class ExemploForTradicional {
	public static void main(String[] args) {
		String[] string = { "www.", "viva", "o", "java", ".com", ".br", "\n" };
		for (int i = 0; i < string.length; i++) {
			System.out.print(string[i]);
		}
	}
}