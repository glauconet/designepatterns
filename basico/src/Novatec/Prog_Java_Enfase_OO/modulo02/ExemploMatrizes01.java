package modulo02;

// Usando uma matriz com a inicialização feita pela leitura de dados do teclado
public class ExemploMatrizes01 {
	public static void main(String args[]) {
		double matriz[][] = new double[3][4]; // cria uma matriz com 3 linhas
												// e 4 colunas
		for (int l = 0; l < matriz.length; l++) { // carregando a matriz
			for (int c = 0; c < matriz[l].length; c++) {
				matriz[l][c] = l * matriz[l].length + c;
			}
		}
		for (int l = 0; l < matriz.length; l++) {
			// utilizando a linha (matriz[l].length) para identificar o tamanho
			// da coluna
			for (int c = 0; c < matriz[l].length; c++) {
				System.out.print(matriz[l][c] + "\t"); // \t representa o tab
			}
			System.out.println();
		}
	}
}