package modulo02;

// Exemplo do uso de vetores
public class ExemploVetor01 {
	public static void main(String[] args) {
		int[] vetor; // Definindo o vetor, sendo apenas uma refer�ncia
		vetor = new int[5]; // alocando 5 posi��es inteiras para o vetor
		vetor[0] = 10;
		vetor[1] = 20;
		vetor[2] = 30;
		vetor[3] = 40;
		vetor[4] = 50;
		for (int i = 0; i < vetor.length; i++) {
			System.out.println("Elemento - vetor [" + i + "] = " + vetor[i]);
		}
		int vetorInt[] = { 30, 4, 9 };
		for (int i = 0; i < vetorInt.length; i++) {
			System.out.println("Elemento - vetorInt [" + i + "] = "
					+ vetorInt[i]);
		}
	}
}