package modulo02;

// Exemplo do uso de vetores
import java.util.Scanner;

public class ExemploVetor02 {
	public static void main(String args[]) {
		// declara e aloca espa�o com 10 bytes do tipo primitivo int
		int var1[] = new int[10];
		Scanner s = new Scanner(System.in);
		System.out.print("Informe os valores \n");
		for (int i = 0; i < var1.length; i++) {
			System.out.print("var1[" + i + "]? ");
			var1[i] = s.nextInt();
		}
		int soma = 0;
		int cont = 0;
		for (cont = 0; cont < var1.length; cont++) {
			// soma os valores lidos
			soma += var1[cont];
		}
		System.out.println("M�dia = " + soma / cont);
	}
}