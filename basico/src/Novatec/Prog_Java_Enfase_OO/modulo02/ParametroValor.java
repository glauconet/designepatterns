package modulo02;
// Exemplo da passagem de par�metros usando tipos primitivos
public class ParametroValor {
public void alterar1() {
int y = 1;
System.out.println(y);

// passando o valor de y como par�metro para o m�todo alterar2()
alterar2(y);
System.out.println(y);
}
public void alterar2(int param) {
// mesmo alterando o valor de param para 2 esta altera��o n�o ir� refletir na vari�vel y
param = 2;
System.out.println(param);
}
public static void main(String[] args) {
ParametroValor obj = new ParametroValor();
obj.alterar1();
}
}