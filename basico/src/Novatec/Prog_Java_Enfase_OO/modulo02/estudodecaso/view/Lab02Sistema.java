package modulo02.estudodecaso.view;

// Resposta do Laborat�rio 2. Classe Lab02Sistema
import java.util.Scanner;
import modulo02.estudodecaso.model.*;
import modulo01.estudodecaso.util.*;

public class Lab02Sistema {
	Lab02ContaCorrente cc1 = new Lab02ContaCorrente();

	int numAge;

	int numConta;

	double valor;

	private void leValores() {
		do {
			this.numAge = Console.readInt("N�mero da ag�ncia: ");
		} while (this.numAge <= 0);
		do {
			this.numConta = Console.readInt("N�mero da conta: ");
		} while (this.numConta <= 0);
		do {
			this.valor = Console.readDouble("Valor: ");
		} while (this.valor <= 0.0);
	}

	public void execCadastramento() {
		String nome = null;
		// usando o Scanner como uma op��o de leitura
		Scanner sc = new Scanner(System.in);
		char opcao;
		leValores();
		do {
			System.out.println("Nome do cliente: ");
			nome = sc.nextLine();
		} while (nome.equals(""));
		opcao = Console.readChar("Confirma cadastramento (S/N): ");
		if ((opcao == 'S') || (opcao == 's')) {
			this.cc1.setNumAge(this.numAge);
			this.cc1.setNumConta(this.numConta);
			this.cc1.setNome(nome);
			this.cc1.setSaldo(this.valor);
			System.out.println("Cadastramento realizado");
		} else {
			System.out.println("Cadastramento n�o realizado");
		}
	}

	public void execSaque() {
		char opcao;
		leValores();
		opcao = Console.readChar("Confirma saque (S/N): ");
		if ((opcao == 'S') || (opcao == 's')) {
			int ret = this.cc1.sacar(this.valor);
			if (ret == 0)
				System.out.println("Saldo indispon�vel");

			else
				System.out.println("Saque efetuado com sucesso");
		} else {
			System.out.println("Saque n�o realizado");
		}
	}

	public void execDeposito() {
		char opcao;
		leValores();
		opcao = Console.readChar("Confirma dep�sito (S/N): ");
		if ((opcao == 'S') || (opcao == 's')) {
			this.cc1.depositar(this.valor);
			System.out.println("Dep�sito realizado");
		} else {
			System.out.println("Dep�sito n�o realizado");
		}
	}

	public static void main(String[] args) {
		char opcao;
		Lab02Sistema obj = new Lab02Sistema();
		while (true) {
			System.out.println("Entre com a op��o desejada");
			System.out.println("1 - Cadastramento");
			System.out.println("2 - Saque");
			System.out.println("3 - Dep�sito");
			System.out.println("4 - Imprimir");
			System.out.println("9 - Fim");
			opcao = Console.readChar("Op��o: ");
			if (opcao == '9')
				break;
			switch (opcao) {
			case '1':
				obj.execCadastramento();
				break;
			case '2':
				obj.execSaque();
				break;
			case '3':
				obj.execDeposito();
				break;
			case '4':
				obj.execConsulta();
				break;
			default:
				System.out.println("Op��o inv�lida. Reentre.");
			}
		}
	}

	public void execConsulta() {
		this.cc1.imprimir();
	}
}