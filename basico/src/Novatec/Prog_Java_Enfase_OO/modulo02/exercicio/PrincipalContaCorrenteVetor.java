package modulo02.exercicio;

import java.util.Scanner;

public class PrincipalContaCorrenteVetor {
	public final static int TAM = 3;

	ContaCorrente cc[] = new ContaCorrente[TAM];

	public static int indice = 0;

	// atributo utilizado para a pesquisa de uma conta corrente cadastrada no
	// vetor.
	int op;

	public int getOp() {
		return this.op;
	}

	public void setOp(int op) {
		/*
		 * caso op seja menor ou igual a zero e ainda maior que o maior �ndice
		 * cadastrado apresenta erro e utiliza a primeira posi��o como padr�o.
		 */
		if (op > indice || op <= 0) {
			System.out
					.println("Posi��o inv�lida. Ser� executada para a primeira posi��o.");
			this.op = 1;
		} else {
			this.op = op;
		}
	}

	public static void main(String[] args) {
		PrincipalContaCorrenteVetor obj = new PrincipalContaCorrenteVetor();
		Scanner sc = new Scanner(System.in).useDelimiter("\r\n");
		while (true) {
			System.out.println("Podemos cadastrar somente 3 contas correntes.");
			System.out.println("1 - Cadastrar");
			System.out.println("2 - Sacar");
			System.out.println("3 - Depositar");
			System.out.println("4 - Consultar");
			System.out.println("9 - Sair");
			System.out.print("Entre com uma op��o:");
			int opcaoMenu = sc.nextInt();
			switch (opcaoMenu) {
			case 1:
				obj.execCadastro();
				break;
			case 2:
				obj.execSaque();
				break;
			case 3:
				obj.execDeposito();
				break;
			case 4:
				obj.execConsulta();
				break;
			default:
				if (opcaoMenu == 9)
					System.exit(0);
				System.out.println("Op��o inv�lida.");
			}
		}
	}

	void execCadastro() {
		if (indice >= TAM) {
			System.out
					.println("Todas as posi��es j� foram ocupadas. Encerrando o programa.");
			System.exit(0);
		}
		// o conte�do \r\n define que o separador entre strings � o enter. O
		// padr�o � o espa�o
		Scanner sc = new Scanner(System.in).useDelimiter("\r\n");
		this.cc[indice] = new ContaCorrente();
		System.out.println("Digitar a conta:");
		this.cc[indice].conta = sc.nextInt();
		System.out.println("Digitar a ag�ncia:");
		this.cc[indice].agencia = sc.nextInt();
		System.out.println("Digitar o nome:");
		this.cc[indice].nome = sc.next();
		System.out.println("Digitar o saldo:");
		this.cc[indice].saldo = sc.nextDouble();
		indice++; // incrementando o �ndice para o vetor
	}

	void execSaque() {
		if (indice > 0) {
			System.out.println("Qual posi��o dever� ser sacada: ");
			Scanner sc = new Scanner(System.in);
			// o m�todo setOp valida se a posi��o digitada esta dentro dos
			// limites
			setOp(sc.nextInt());
			System.out.print("Digite um valor:");
			double valor = sc.nextDouble();
			/*
			 * o m�todo getOp recupera o valor do atributo op ap�s ser validado
			 * pelo m�todo setOp.
			 */
			int ret = this.cc[getOp() - 1].sacar(valor);
			if (ret == 0) {
				System.out.println("Saque n�o realizado");
			} else {
				System.out.println("Saque realizado");
			}
		} else {
			System.out.println("Nenhuma conta foi cadastrada.");
		}
	}

	void execDeposito() {
		if (indice > 0) {
			System.out.println("Qual posi��o dever� ser depositada: ");
			Scanner sc = new Scanner(System.in);
			// o m�todo setOp valida se a posi��o digitada esta dentro dos
			// limites
			setOp(sc.nextInt());
			System.out.print("Digite um valor:");
			double valor = sc.nextDouble();
			/*
			 * o m�todo getOp recupera o valor do atributo op ap�s ser validado
			 * pelo m�todo setOp.
			 */
			this.cc[getOp() - 1].depositar(valor);
		} else {
			System.out.println("Nenhuma conta foi cadastrada.");
		}
	}

	void execConsulta() {
		if (indice > 0) {
			System.out.println("Qual posi��o dever� ser apresentada: ");
			Scanner sc = new Scanner(System.in);
			// o m�todo setOp valida se a posi��o digitada esta dentro dos
			// limites
			setOp(sc.nextInt());
			this.cc[getOp() - 1].imprimir();
		} else {
			System.out.println("Nenhuma conta foi cadastrada.");
		}
	}
}