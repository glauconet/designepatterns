package modulo03;

// Gera��o do c�digo de inicializa��o
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class CodigoIncNumeroUnico {
	private static int numeroUnico;
	static {

		System.out.println("C�digo de inicializa��o do n�mero �nico");
		try {
			// este arquivo ser� lido do diret�rio definido para o projeto
			FileReader arq1 = new FileReader("numerounico.txt");
			BufferedReader arq2 = new BufferedReader(arq1);
			numeroUnico = Integer.parseInt(arq2.readLine());
			arq2.close();
			arq1.close();
		} catch (IOException tExcept) {
			try {
				// este arquivo ser� gravado no diret�rio definido para o
				// projeto
				FileWriter arq1 = new FileWriter("numerounico.txt");
				PrintWriter arq2 = new PrintWriter(arq1);
				arq2.println(1);
				arq2.close();
				arq1.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static int obterNumeroUnico() {
		try {
			// este arquivo ser� gravado no diret�rio definido para o projeto
			FileWriter arq1 = new FileWriter("numerounico.txt");
			PrintWriter arq2 = new PrintWriter(arq1);
			arq2.println(++numeroUnico);
			arq2.close();
			return numeroUnico;
		} catch (IOException e) {
			e.printStackTrace();
			return -1;
		}
	}
}