package modulo03;

// Exemplo do uso do conceito de overload com construtores
public class PontoBidimensional {
	private double x, y;

	// construtor sem argumentos, inicializando os atributos com 0
	public PontoBidimensional() {
		this.x = 0.0;
		this.y = 0.0;
	}

	// construtor composto por dois par�metros do tipo double
	public PontoBidimensional(double px, double py) {
		this.x = px;
		this.y = py;
	}

	// m�todo toString para converter os tipos primitivos em strings e
	// apresentar na tela
	public String toString() {
		return "Ponto2D[x = " + this.x + ", y = " + this.y + "]";
	}

	public static void main(String args[]) {
		// instanciar objetos da classe Ponto2D. Ir� executar o construtor sem
		// argumentos.

		PontoBidimensional obj1 = new PontoBidimensional();
		System.out.println(obj1.toString());
		// far� a execu��o autom�tica do construtor que recebe dois par�metros
		// do tipo double
		PontoBidimensional obj2 = new PontoBidimensional(10.0, 20.0);
		System.out.println(obj2.toString());
	}
}