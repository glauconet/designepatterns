package modulo03.exercicio;
//� Classe base Pessoa 
public class Pessoa {
	int cpf;
	String nome;
	String dataNasc;
	public Pessoa() {
			super();
	}
	public Pessoa(int cpf, String nome, String dataNasc) {
			super();
			this.cpf = cpf;
			this.nome = nome;
			this.dataNasc = dataNasc;
	}
	public int getCpf() {
			return this.cpf;
	}
	public void setCpf(int cpf) {
			this.cpf = cpf;
	}
	public String getDataNasc() {
			return this.dataNasc;
	}
	public void setDataNasc(String dataNasc) {
			this.dataNasc = dataNasc;
	}
	public String getNome() {
			return this.nome;
	}
	public void setNome(String nome) {
			this.nome = nome;
	}
}
