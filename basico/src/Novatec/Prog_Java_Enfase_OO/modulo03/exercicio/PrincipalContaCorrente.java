package modulo03.exercicio;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.Scanner;
// A classe Console precisa estar dispon�vel no pacote modulo01.estudodecaso.util
import modulo01.estudodecaso.util.Console;

public class PrincipalContaCorrente {
	ContaCorrente cc1; // cc1 representa uma refer�ncia para a classe
						// ContaCorrente

	public static void main(String[] args) {
		PrincipalContaCorrente obj = new PrincipalContaCorrente();
		int opcao = 0;
		Scanner sc = new Scanner(System.in);
		while (true) {
			System.out.println("1 - Cadastrar");
			System.out.println("2 - Imprimir");
			System.out.print("Entre com a op��o: ");
			opcao = sc.nextInt();
			switch (opcao) {
			case 1:
				obj.cadastrar();
				break;
			case 2:
				obj.imprimir();
				break;
			default:
				System.out.println("Op��o inv�lida.");
			}
		}
	}

	private void cadastrar() {
		char opcao;
		int agencia;
		int conta;
		String nome;
		double saldo;
		do {
			agencia = Console.readInt("N�mero da ag�ncia: ");
		} while (agencia <= 0);

		do {
			conta = Console.readInt("N�mero da conta: ");
		} while (conta <= 0);
		do {
			saldo = Console.readDouble("Valor: ");
		} while (saldo <= 0.0);
		do {
			nome = Console.readString("Nome do cliente: ");
		} while (nome.equals(""));
		opcao = Console.readChar("Confirma cadastramento (S/N): ");
		if (Character.toLowerCase(opcao) != 's')
			return;
		this.cc1 = new ContaCorrente(conta, agencia, saldo, nome);
	}

	private void imprimir() {
		System.out.println("------------------------------------------");
		System.out.println("Ag�ncia: " + this.cc1.getAgencia());
		System.out.println("Conta: " + this.cc1.getConta());
		System.out.println("Nome: " + this.cc1.getNomeCliente());
		NumberFormat formatter;
		formatter = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
		formatter.setMinimumFractionDigits(2);
		System.out.println("Saldo: " + formatter.format(this.cc1.getSaldo()));
	}
}