package modulo03.exercicio;
//� Classe PrincipalPessoa respons�vel por executar o programa e utilizar o construtor da classe Pessoa 
import java.util.Scanner;
public class PrincipalPessoa {
	Pessoa pessoa;
	public static void main(String[] args) {
			PrincipalPessoa obj = new PrincipalPessoa();
			int opcao = 0;
			Scanner sc = new Scanner(System.in);
			while (true) {
				System.out.println("1 - Cadastrar");
				System.out.println("2 - Imprimir");
				System.out.print("Entre com uma op��o: ");
				opcao = sc.nextInt();
				switch (opcao) {
				case 1:
					obj.cadastrar();
					break;
				case 2:
					obj.imprimir(); break;
				default:
					System.out.println("Op��o inv�lida.");
				}
			}
	}
	private void cadastrar() {
			Scanner sc = new Scanner(System.in).useDelimiter("\r\n");
			System.out.print("Entre com o nome: ");
			String nome = sc.next();
			String dtNasc;
			do {
				do {
					System.out.print("Entre com a data de nascimento (AAAA/MM/DD): ");
					dtNasc = sc.next();
					
				} while (dtNasc.length() != 10);
			} while (dtNasc.charAt(4) != '/' || (dtNasc.charAt(7)) != '/');
			System.out.print("Entre com o cpf: ");
			int cpf = sc.nextInt();
			// executando o construtor passando as vari�veis como par�metro
			this.pessoa = new Pessoa (cpf,nome,dtNasc);
	}
	private void imprimir() {
			System.out.println("CPF: " + this.pessoa.getCpf());
			System.out.println("Nome: " + this.pessoa.getNome() );
			System.out.println("Data Nascimento: " + this.pessoa.getDataNasc() );
	}
}
