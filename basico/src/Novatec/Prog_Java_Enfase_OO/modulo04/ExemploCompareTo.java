package modulo04;

// Usando do m�todo compareTo()
public class ExemploCompareTo {
	public static void main(String args[]) {
		String var1 = "uva";
		String var2 = "abacaxi";
		// m�todo compareTo realiza a compara��o lexicogr�fica, ou seja, letra a
		// letra
		if (var1.compareTo(var2) == 0)
			System.out.println("As strings s�o iguais");
		else if (var1.compareTo(var2) > 0)
			System.out.println("String " + var1 + " � maior que a string "
					+ var2);
		else if (var1.compareTo(var2) < 0)
			System.out.println("String " + var1 + " � menor que a string "
					+ var2);
	}
}