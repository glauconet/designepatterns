package modulo04;

// Exemplo da sobrecarga do m�todo equals
public class ExemploContaEquals {
	private int agencia = 0;

	private int conta = 0;

	public ExemploContaEquals(int agencia, int conta) {
		// o operador this resolve o problema de ambig�idade entre atributos e
		// par�metros iguais
		this.agencia = agencia;
		this.conta = conta;
	}

	// defini��o do construtor vazio
	public ExemploContaEquals() {
		// realizando a chamada de outro construtor com outra assinatura
		this(0, 0);
	}

	public ExemploContaEquals retornarReferencia() {
		return this;
	}

	public static void main(String[] args) {
		// executa o construtor vazio, que por sua vez, ir� inicializar os
		// atributos com 0
		ExemploContaEquals obj1 = new ExemploContaEquals();
		System.out.println("Ag�ncia: " + obj1.getAgencia());
		System.out.println("Conta: " + obj1.getConta());
		// executa o construtor com dois par�metros
		ExemploContaEquals obj2 = new ExemploContaEquals(3, 963210);
		System.out.println("Ag�ncia: " + obj2.getAgencia());
		System.out.println("Conta: " + obj2.getConta());
	}

	public int getAgencia() {
		return this.agencia;
	}

	public int getConta() {
		return this.conta;
	}

	/*
	 * sobrecarregando o m�todo equals. Com esta sobrecarga podemos comparar
	 * dois objetos do tipo da classe ExemploContaEquals.
	 */
	public boolean equals(Object obj) {
		/*
		 * se o argumento for diferente de null e for uma inst�ncia da classe
		 * ExemploContaEquals a condi��o ser� verdadeira.
		 */
		if (obj != null && obj instanceof ExemploContaEquals) {
			// compara as vari�veis de inst�ncia dos dois objetos
			return (getConta() == ((ExemploContaEquals) obj).getConta());
		} else {
			return false;
		}
	}
}