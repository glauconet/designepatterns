package modulo04;

// Exemplo do uso do m�todo format
import java.text.DecimalFormat;

public class ExemploFormatarString {
	public static void main(String[] args) {
		double var1 = 1254.908;
		DecimalFormat novoFormato = null;
		novoFormato = new DecimalFormat("0.##");
		// imprime var1 = 1254,91 - Foi feito um arredondamento
		System.out.println("1 - formato: 0.## - var1 = "
				+ novoFormato.format(var1));
		// imprime var1 = 01254,91 - Foi feito um arredondamento
		novoFormato = new DecimalFormat("000000.##");
		System.out.println("2 - formato: 000000.## - var1 = "
				+ novoFormato.format(var1));
		// imprime var1 = 01254,908
		novoFormato = new DecimalFormat("000000.000");
		System.out.println("3 - formato: 000000.000 - var1 = "
				+ novoFormato.format(var1));
		// imprime var1 = 1254,91
		novoFormato = new DecimalFormat("######.##");
		System.out.println("4 - formato: ######.## - var1 = "
				+ novoFormato.format(var1));
		// imprime var1 = 1254,908
		novoFormato = new DecimalFormat("######.####");
		System.out.println("5 - formato: ######.#### - var1 = "
				+ novoFormato.format(var1));

		// imprime var1 = -1254,908
		novoFormato = new DecimalFormat("-######.####");
		System.out.println("6 - formato: -######.#### - var1 = "
				+ novoFormato.format(var1));
	}
}