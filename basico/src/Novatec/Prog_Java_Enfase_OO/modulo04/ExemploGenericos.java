package modulo04;
//� Usando a classe Vector com Gen�ricos 
import java.util.Vector;
public class ExemploGenericos {
	public static void main(String[] args) {
			//Definimos um vetor que s� poder� receber objetos do tipo Integer.
			Vector<Integer> l = new Vector<Integer>();
			Integer num1 = new Integer(1);
			Integer num2 = new Integer(2);
			Integer num4 = new Integer(4);
			l.add(num1);
			l.add(num2);
			l.add(num4);
			for (int i = 0 ; i < l.size() ; i ++) {
				// N�o precisa de downcasting no uso do m�todo get devido a ter sido definido usando o tipo Integer.
				System.out.println(l.get(i).intValue());
			}
			String str = "3"; 
			/* N�o podemos adicionar uma string em um objeto do tipo Vector que foi criado apenas para receber objetos do tipo Integer.
			 l.add(str); // Esta linha precisa estar comentada, devido a um erro de compila��o
			 */
	}
}
