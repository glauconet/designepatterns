package modulo04;

// Convertendo tipos primitivos
public class ExemploMotivoWrapper01 {
	public static void main(String[] args) {
		int num = 10;
		// convertendo o tipo primitivo int para String
		String str = Integer.valueOf(num).toString();
		System.out.println("str: " + str);
		// convertendo a vari�vel str do tipo String para o tipo primitivo short
		short num01 = Short.parseShort(str);
		System.out.println("num01: " + num01);
		// convertendo a vari�vel num do tipo primitivo int para hexadecimal
		String strHexa = Integer.toHexString(num);
		System.out.println("strHexadecimal: " + strHexa);
		// convertendo a vari�vel num do tipo primitivo int para bin�rio
		String strBinario = Integer.toBinaryString(num);
		System.out.println("strBinario: " + strBinario);
		// convertendo a vari�vel num do tipo primitivo int para octal
		String strOctal = Integer.toOctalString(num);
		System.out.println("strOctal: " + strOctal);
	}
}