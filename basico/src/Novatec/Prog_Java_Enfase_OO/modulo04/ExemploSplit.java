package modulo04;

// Usando do m�todo split()
public class ExemploSplit {
	public static void main(String[] args) {
		String variavel = "O livro constru��o de software usando Java � muito pr�tico";
		// retorna um vetor de Strings separados pelo espa�o
		String[] vetorString = variavel.split(" ");
		for (int i = vetorString.length - 1; i >= 0; i--) {
			// imprime a frase ao contr�rio
			System.out.println(vetorString[i]);
		}
	}
}