package modulo04;

// Usando os m�todos insert() e append()
public class ExemploStringBuilder {
	public static void main(String args[]) {
		StringBuilder z = new StringBuilder("Bom dia");
		z.append("Emilia Laudiceia Moreira");
		z.insert(7, " Sra. ");
		System.out.println(z);
	}
}
