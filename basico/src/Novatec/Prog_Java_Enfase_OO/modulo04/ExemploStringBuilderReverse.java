package modulo04;
//�Usando do m�todo reverse()
public class ExemploStringBuilderReverse {
	public static void main(String[] args) {
	/* Exemplo de palavras palindromas
	* a cara rajada da jararaca, marujos so juram, a mala nada na lama, a
	* grama e amarga, a miss e pessima, socorram me subi no onibus em
	* marrocos
	*/
		String palindrome = "a grama e amarga";
		StringBuilder sb = new StringBuilder(palindrome);
		sb.reverse();
		System.out.println(sb);
	}
}
