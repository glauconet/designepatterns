package modulo04;
//� Usando o operador == para comparar strings 
public class ExemploStringIgualIgual {
	public static void main(String[] args) {
			String objStr = "Antonio Carlos";
			String variavelStr = "Antonio Carlos";
			/*
			 * Somente podemos utilizar o operador == para comparar strings quando na cria��o da string usamos o operador = para atribuir valor.
			 * Neste caso criamos uma constante, ou seja, o conte�do n�o fica na �rea de heap. Neste comando if o operador == compara os onte�dos.
			 * O resultado deste if ser� verdadeiro.
			 */
			if (objStr == variavelStr) {
				System.out.println("Strings com conte�do Igual.");
			} else {
				System.out.println("Strings com conte�do Diferente.");
			}	
			String objStr1 = new String ("Antonio Carlos");
			String variavelStr1 = new String ("Antonio Carlos");
			// Neste if o operador == compara as refer�ncias.
			// O resultado deste if ser� falso.
			if (objStr1 == variavelStr1) {
				System.out.println("Strings com refer�ncias Iguais");
			} else {
				System.out.println("Strings com refer�ncias Diferentes");
			}
	}
}
