package modulo04;
//� Usando os m�todos equals e equalsIgnoreCase 
public class ExemploStringIquals {
	public static void main(String[] args) {
			String variavel = "Douglas Jose Peixoto";
			// Em ambos os comandos if as strings ser�o iguais
			if (variavel.equals("Douglas Jose Peixoto")) {
				System.out.println("Strings com conte�dos iguais");
			}
			if (variavel.equalsIgnoreCase("DOUGLAS JOSE PEIXOTO")) {
				System.out.println("Strings com conte�dos iguais");
			}
	}
}

