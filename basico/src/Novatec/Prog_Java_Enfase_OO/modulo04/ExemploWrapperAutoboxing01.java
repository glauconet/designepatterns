package modulo04;

// Usando o conceito de autoboxing
public class ExemploWrapperAutoboxing01 {
	public static void main(String[] args) {
		String strNum = "100";

		// O compilador ir� fazer uma convers�o da string strNum para um long
		long num = Long.valueOf(strNum);
		System.out.println("num: " + num);
		Double numObj = new Double(40.0);
		double numObj2 = 90.0;
		System.out.println("numObj: " + (numObj + numObj2));
	}
}