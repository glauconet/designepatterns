package modulo04.exercicio;

// Classe que utiliza a classe ExemploCarro para criar objetos
import java.util.Scanner;
import java.util.Vector;

public class ExemploCarroPrincipal {
	Vector<ExemploCarro> vet;

	public static void main(String[] args) {
		ExemploCarroPrincipal obj = new ExemploCarroPrincipal();
		int opcao = 0;
		Scanner sc = new Scanner(System.in);
		while (true) {
			System.out.println("1 - Cadastrar Carro");
			System.out.println("2 - Imprimir Carro");
			System.out.print("Entre com uma op��o: ");
			opcao = sc.nextInt();
			switch (opcao) {
			case 1:
				obj.cadastrar();
				break;
			case 2:
				obj.imprimir();
				break;
			default:
				System.out.println("Op��o inv�lida.");
			}
		}
	}

	private void cadastrar() {
		ExemploCarro carro = new ExemploCarro();
		Scanner sc = new Scanner(System.in).useDelimiter("\r\n");
		this.vet = new Vector<ExemploCarro>();
		// o n�mero do chassi informado dever� ser inteiro
		System.out.print("Entre com o n�mero do chassi: ");
		carro.setChassi(sc.nextInt());

		String dtFabricacao;
		do {
			do {
				System.out
						.print("Entre com a data de fabrica��o (AAAA/MM/DD): ");
				dtFabricacao = sc.next();
			} while (dtFabricacao.length() != 10);
		} while ((dtFabricacao.toCharArray())[4] != '/'
				|| (dtFabricacao.toCharArray())[7] != '/');
		carro.setDtFabricacao(dtFabricacao);
		System.out.print("Entre com o nome do fabricante: ");
		carro.setFabricante(sc.next());
		System.out.print("Entre com marca do carro: ");
		carro.setMarca(sc.next());
		this.vet.add(carro);
	}

	private void imprimir() {
		for (int i = 0; i < this.vet.size(); i++) {
			ExemploCarro carro = this.vet.get(i);
			System.out.println("Chassi: " + carro.getChassi());
			System.out.println("Marca: " + carro.getMarca());
			System.out.println("Fabricante: " + carro.getFabricante());
			String data[] = (carro.getDtFabricacao()).split("/");
			System.out.print("Data Fabrica��o (DD/MM/AAAA): ");
			System.out.print(data[2] + "/");
			System.out.print(data[1] + "/");
			System.out.println(data[0]);
		}
	}
}