package modulo05;

// Usando o modificador final
import java.util.Scanner;

// Esta classe n�o pode ser herdada
public final class ExemploFinal {
	public final double valorPI = 3.1416;

	public int raio;

	public static void main(String[] args) {
		(new ExemploFinal()).retornarPI();
	}

	// Este m�todo n�o pode ser sobrescrito (override)
	public final void retornarPI() {
		System.out.println("Digite um valor para o raio: ");
		Scanner scan = new Scanner(System.in).useDelimiter("\r\n");
		this.raio = scan.nextInt();
		System.out.println("Area igual: " + this.valorPI * this.raio
				* this.raio);
		// erro de compila��o, devido a vari�vel valorPI ser final
		// valorPI = 3.15;
	}
}