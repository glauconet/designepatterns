package modulo05;
//� Usando os conceitos de heran�a e classe abstrata 
import java.util.Scanner;
public class ExemploPrincipalLivroFita {
	public static void main(String[] args) {
			int opcao;
	/* Pode tamb�m ser utilizado para os objetos da classe ExemploPeriodico. Isto � poss�vel devido a ExemploPeriodico ser sub classe de ExemploLivro. */
			ExemploLivro obj; 
			ExemploFita obj1;
			while (true) {
				System.out.println("Entre com a opcao desejada");
				System.out.println("1 - Cadastrar Fita");
				System.out.println("2 - Emprestar Fita");
				System.out.println("3 - Devolver Fita");
				System.out.println("4 - Imprimir Fita");
				System.out.println("5 - Cadastrar Livro");
				System.out.println("6 - Emprestar Livro");
				System.out.println("7 - Devolver Livro");
				System.out.println("8 - Imprimir Livro");
				System.out.println("9 - Bloquear Livro");
				System.out.println("10 - Liberar Livro");
				System.out.println("11 - Cadastrar Periodico");
				System.out.println("12 - Emprestar Periodico");
				System.out.println("13 - Devolver Periodico");
				System.out.println("14 - Imprimir Periodico");
				System.out.println("15 - Bloquear Periodico");
				System.out.println("16 - Liberar Periodico");
				System.out.println("0 - Fim");
				System.out.println("Opcao : ");
				Scanner scan = new Scanner(System.in).useDelimiter("\r\n");
				opcao = scan.nextInt();
				if (opcao == 0)
					System.exit(0);
				switch (opcao) {
				// cadastrar fita
				case 1:
					obj1 = ExemploFita.getInstance();
					System.out.println("Digite o ISBN: ");
					obj1.setISBN(scan.nextInt());
					System.out.println("Digite o titulo: ");
					obj1.setTitulo(scan.next());
					System.out.println("Digite o nome do respons�vel: ");
					obj1.setNomeResp(scan.next());
					System.out.println("Digite a data de vencimento da fita (AAAAMMDD): ");
					obj1.setDtVencFita(scan.nextInt());
					System.out.println("Digite a data de lan�amento da fita (AAAAMMDD): ");
					obj1.setDtLancamento(scan.nextInt());
					System.out.println("Digite o seu n�vel: ");
					obj1.setNivelEmprestimo(scan.nextInt());
					System.out.println("Digite o nome do fabricante: ");
					obj1.setDescFabricante(scan.next());
					obj1.setSituacaoItem("l");// livre
					obj1.gravar();
					break;
				case 2:
					obj1 = ExemploFita.getInstance();
					System.out.println("Digite o ISBN: ");
					obj1.setISBN(scan.nextInt());
					if (obj1.recuperar() == 1) {
						obj1.emprestar();
						obj1.gravar();
					}
					break;
				case 3:
					obj1 = ExemploFita.getInstance();
					System.out.println("Digite o ISBN: ");
					obj1.setISBN(scan.nextInt());
					if (obj1.recuperar() == 1) {
						obj1.retornar();
						obj1.gravar();
					}
					break;
				case 4:
					obj1 = ExemploFita.getInstance();
					System.out.println("Digite o ISBN: ");
					obj1.setISBN(scan.nextInt());
					if (obj1.recuperar() == 1) {
						obj1.imprimir();
					}
					break;
				case 5:
					obj = ExemploLivro.getInstance();
					System.out.println("Digite o ISBN: ");
					obj.setISBN(scan.nextInt());
					System.out.println("Digite o titulo: ");
					obj.setTitulo(scan.next());
					System.out.println("Digite o nome do respons�vel: ");
					obj.setNomeResp(scan.next());
					System.out.println("Digite o data do lan�amento (AAAAMMDD): ");
					obj.setDtLancamento(scan.nextInt());
					obj.setStatusBloqueio("l"); // livre
					obj.setSituacaoItem("l");// livre
					obj.gravar();
					break;
				case 6:
					obj = ExemploLivro.getInstance();
					System.out.println("Digite o ISBN: ");
					obj.setISBN(scan.nextInt());
					if (obj.recuperar() == 1) {
						obj.emprestar();
						obj.gravar();
					}
					break;
				case 7:
					obj = ExemploLivro.getInstance();
					System.out.println("Digite o ISBN: ");
					obj.setISBN(scan.nextInt());
					if (obj.recuperar() == 1) {
						obj.retornar();
						obj.gravar();
					}
					break;
				case 8:
					obj = ExemploLivro.getInstance();
					System.out.println("Digite o ISBN: ");
					obj.setISBN(scan.nextInt());
					if (obj.recuperar() == 1) {
						obj.imprimir();
					}
					break;
				case 9:
					obj = ExemploLivro.getInstance();
					System.out.println("Digite o ISBN: ");
					obj.setISBN(scan.nextInt());
					if (obj.recuperar() == 1) {
						obj.bloquearItem();
						obj.gravar();
					}
					break;
				case 10:
					obj = ExemploLivro.getInstance();
					System.out.println("Digite o ISBN: ");
					obj.setISBN(scan.nextInt());
					if (obj.recuperar() == 1) {
						obj.desbloquearItem();
						obj.gravar();
					}
					break;
				case 11:
					obj = ExemploPeriodico.getInstance();
					System.out.println("Digite o ISBN: ");
					obj.setISBN(scan.nextInt());
					System.out.println("Digite o titulo: ");
					obj.setTitulo(scan.next());
					System.out.println("Digite o nome do respons�vel: ");
					obj.setNomeResp(scan.next());
					System.out.println("Digite o data do lan�amento (AAAAMMDD): ");
					obj.setDtLancamento(scan.nextInt());
					System.out.println("Digite a quantidade de artigos: ");
					// Downscasting necess�rio devido ao m�todo estar presente somente na classe filha.
					((ExemploPeriodico) obj).setQdadeArtigos(scan.nextInt());
					obj.setStatusBloqueio("l"); // livre
					obj.setSituacaoItem("l");// livre
					obj.gravar();
					break;
				case 12:
					obj = ExemploPeriodico.getInstance();
					System.out.println("Digite o ISBN: ");
					obj.setISBN(scan.nextInt());
					if (obj.recuperar() == 1) {
						obj.emprestar();
						obj.gravar();
					}
					break;
				case 13:
					obj = ExemploPeriodico.getInstance();
					System.out.println("Digite o ISBN: ");
					obj.setISBN(scan.nextInt());
					if (obj.recuperar() == 1) {
						obj.retornar();
						obj.gravar();
					}
					break;
				case 14:
					obj = ExemploPeriodico.getInstance();
					System.out.println("Digite o ISBN: ");
					obj.setISBN(scan.nextInt());
					if (obj.recuperar() == 1) {
						obj.imprimir();
					}
					break;
				case 15:
					obj = ExemploPeriodico.getInstance();
					System.out.println("Digite o ISBN: ");
					obj.setISBN(scan.nextInt());
					if (obj.recuperar() == 1) {
						obj.bloquearItem();
						obj.gravar();
					}
					break;
				case 16:
					obj = ExemploPeriodico.getInstance();
					System.out.println("Digite o ISBN: ");
					obj.setISBN(scan.nextInt());
					if (obj.recuperar() == 1) {
						obj.desbloquearItem();
						obj.gravar();
					}
					break;
				default:
					System.out.println("Opcao invalida. Reentre.");
				}
			}
		}
}
