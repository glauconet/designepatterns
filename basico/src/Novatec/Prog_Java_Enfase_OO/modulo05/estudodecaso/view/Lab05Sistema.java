package modulo05.estudodecaso.view;
//� Resposta do laborat�rio 5 � Classe Lab05Sistema 
import modulo01.estudodecaso.util.Console;
import modulo03.estudodecaso.model.Lab03ContaCorrente;
import modulo04.estudodecaso.model.Lab04Historico;
import modulo05.estudodecaso.model.Lab05ContaCorrenteEspecial;
public class Lab05Sistema {
	int numAge;
	int numConta;
	double valor;
	private void leValores() {
			do {
				this.numAge = Console.readInt("Numero da Agencia : ");
			} while (this.numAge <= 0);
			do {
				this.numConta = Console.readInt("Numero da Conta   : ");
			} while (this.numConta <= 0);
			do {
				this.valor = Console.readDouble("Valor             : ");
			} while (this.valor <= 0.0);
		}
		private void leValoresAgCta() {
			do {
				this.numAge = Console.readInt("Numero da Agencia : ");
			} while (this.numAge <= 0);
			do {
				this.numConta = Console.readInt("Numero da Conta   : ");
			} while (this.numConta <= 0);
		}
		public void execCadastramento() {
			String nome;
			double limite = 0.0;
			char opcao;
			leValores();
			do {
				nome = Console.readString("Nome do Cliente   : ");
			} while (nome.equals(""));
			if (this.numAge > 5000) {
				do {
					limite = Console.readDouble("Limite      : ");
				} while (limite <= 0.0);	
			}
			opcao = Console.readChar("Confirma cadastramento (S/N) : ");
			if (Character.toLowerCase(opcao) != 's')
				return;
			
			if (this.numAge > 5000) {
				Lab05ContaCorrenteEspecial cc1 = 
					new Lab05ContaCorrenteEspecial (this.numAge, this.numConta, nome, this.valor, limite);
				cc1.gravar();
			}
			else{
				Lab03ContaCorrente cc1 = new Lab03ContaCorrente (this.numAge, this.numConta, nome, this.valor);
				cc1.gravar();
			}		
		}
		public void execSaque() {
			char opcao;
			leValores();
			opcao = Console.readChar("Confirma saque (S/N) : ");
			if (Character.toLowerCase(opcao) != 's')
				return;
			if (this.numAge > 5000)
			{
				Lab05ContaCorrenteEspecial cc1 = 
				new Lab05ContaCorrenteEspecial (this.numAge, this.numConta);
				if (cc1.sacar (this.valor) == 0)
				{
					System.out.println ("Saldo Indisponivel");
					System.out.println ("\nSaldo Atual: " + cc1.getSaldo());
					System.out.println ("Limite: " + cc1.getLimite());
					System.out.println ("Saque m�ximo permitido: " + (cc1.getLimite() + cc1.getSaldo()) + "\n\n");
				}
				else
				{
					System.out.println ("Saque Efetuado com Sucesso");
					cc1.gravar(); //persiste o saque
					Lab04Historico hist = new Lab04Historico (this.numAge, this.numConta);
					hist.gravar(1,this.valor); 					
				}		
			}
			else 
			{	
				Lab03ContaCorrente cc1 = new Lab03ContaCorrente (this.numAge, this.numConta);
				if (cc1.sacar (this.valor) == 0)
				  System.out.println ("Saldo Indisponivel");
				else
				{
					System.out.println ("Saque Efetuado com Sucesso");
					cc1.gravar(); //persiste o saque
					Lab04Historico hist = new Lab04Historico (this.numAge, this.numConta);
					hist.gravar(1,this.valor); 				
				}	
			}
		}
		public void execDeposito() {
			char opcao;
			leValores();
			opcao = Console.readChar("Confirma deposito (S/N) : ");
			if (Character.toLowerCase(opcao) != 's')
				return;
			Lab03ContaCorrente cc1 = new Lab03ContaCorrente (this.numAge, this.numConta);			
			cc1.depositar (this.valor);
			cc1.gravar();
			Lab04Historico hist = new Lab04Historico (this.numAge, this.numConta);
			hist.gravar(2,this.valor); 	
		}
		public void execExtrato(){
			leValoresAgCta();
			if (this.numAge > 5000)	{
				Lab05ContaCorrenteEspecial cc1 = 
				new Lab05ContaCorrenteEspecial (this.numAge, this.numConta);
				cc1.imprimir();			
			}
			else{
				Lab03ContaCorrente cc1 = new Lab03ContaCorrente (this.numAge, this.numConta);
				cc1.imprimir();
			}
			Lab04Historico hist = new Lab04Historico (this.numAge, this.numConta);			
			hist.imprimir();
	}
	public void execConsulta(){
			leValoresAgCta();
			if (this.numAge > 5000){
				Lab05ContaCorrenteEspecial cc1 = 	new Lab05ContaCorrenteEspecial (this.numAge, this.numConta);
				cc1.imprimir();
			}
			else{
				Lab03ContaCorrente cc1 = new Lab03ContaCorrente (this.numAge, this.numConta);
				cc1.imprimir();
			}
	}
	public void execRemoverContaCorrente(){
			leValoresAgCta();
			if (this.numAge > 5000){
				Lab05ContaCorrenteEspecial cc1 = new Lab05ContaCorrenteEspecial (this.numAge, this.numConta);
				cc1.removerArquivo();
			}
			else{
				Lab03ContaCorrente cc1 = new Lab03ContaCorrente (this.numAge, this.numConta);
				cc1.removerArquivo();
			}
	}
	public static void main(String[] args) {
			char opcao;
			Lab05Sistema obj = new Lab05Sistema();
			while (true)	{
				System.out.println ("\n\n\n");
				System.out.println("Entre com a opcao desejada");
				System.out.println("1 - Cadastramento");
				System.out.println("2 - Saque");
				System.out.println("3 - Deposito");
				System.out.println("4 - Consulta");
				System.out.println("5 - Extrato");
				System.out.println("8 - Remover Conta Corrente");
				System.out.println("9 - Fim");
				//System.out.println("Opcao : ");
				opcao = Console.readChar("Opcao : ");
				if (opcao == '9')
					break;
				switch (opcao) {
					case '1' :
						obj.execCadastramento();
						break;
					case '2' :
						obj.execSaque();
						break;
					case '3' :
						obj.execDeposito();
						break;
					case '4' :
						obj.execConsulta();
						break;
					case '5' :
						obj.execExtrato();
						break;	
					case '8' :
						obj.execRemoverContaCorrente();
						break;					
					default :
						System.out.println("Opcao invalida. Reentre.");
				}
			}
		}
		public Lab05Sistema()	{
			super();
	}
}
