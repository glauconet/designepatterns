package modulo05.exercicio.abstrata;
//� Classe PessoaFisica 
public class PessoaFisica extends Pessoa {
	private int cpf;
	private String dataNasc;
	public int getCpf() {
			return this.cpf;
	}
	public void setCpf(int cpf) {
			this.cpf = cpf;
	}
	public String getDataNasc() {
			return this.dataNasc;
	}
	public void setDataNasc(String dataNasc) {
			this.dataNasc = dataNasc;
	}
	public PessoaFisica() {
			super();
	}
	public PessoaFisica(String nome, int cpf, String dataNasc) {
			super(nome);
			this.cpf = cpf;
			this.dataNasc = dataNasc;
	}	
	public void imprimir() {
			System.out.println("Nome: " + getNome() );
			System.out.println("CPF: " + getCpf());
			System.out.println("Data Nascimento: " + getDataNasc() );
		}
}
