package modulo05.exercicio.abstrata;

// Classe VetorUnidimensional que estende a classe abstrata
public class VetorUnidimensional extends ClasseAbstrataDimensao {
	protected int[] dim1;

	protected static int linha;

	public VetorUnidimensional() {
		// executando o construtor vazio da superclasse
		super();
		this.dim1 = new int[this.TAMANHO];
	}

	public int[] getDim1() {
		return this.dim1;
	}

	public VetorUnidimensional(int param) {
		// executando o construtor vazio da superclasse
		super();
		if ((param < 0) || (param > 2000000)) {
			System.out.println("Tamanho para o vetor recebido inv�lido.");
		} else {
			this.dim1 = new int[param];
		}
	}

	public void adicionar(int valor) {
		this.dim1[linha] = valor;
		linha++;
		if (linha == this.dim1.length) {
			System.out
					.println("Vetor foi excedido. O pr�ximo valor ser� colocado na posi��o 0 do vetor. Recome�ando.");
			linha = 0;
		}
	}

	public void imprimir() {
		for (int i = 0; i < this.dim1.length; i++) {
			System.out.println("Elemento " + i + " : " + this.dim1[i]);
		}
	}
}