package modulo05.exercicio.heranca;
//� Classe abstrata Pessoa 
public class Pessoa {
	String nome;
	String endereco;
	public Pessoa() {
			super();
	}
	public Pessoa(String nome) {
			super();
			this.nome = nome;
	}
	public String getNome() {
			return this.nome;
	}
	public void setNome(String nome) {
			this.nome = nome;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	
}
