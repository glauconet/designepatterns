package modulo05.exercicio.heranca;

public class PessoaJuridica extends Pessoa{
	private String cnpj;
	private String razaoSocial;
	
	public void imprimir(){
		System.out.println("Nome....: " + getNome());
		System.out.println("End.....: " + getEndereco());
		System.out.println("C.N.P.J.: " + this.cnpj);
		System.out.println("R.Social: " + this.razaoSocial);
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	
	
}
