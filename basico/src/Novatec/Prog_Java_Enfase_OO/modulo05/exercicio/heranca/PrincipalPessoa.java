package modulo05.exercicio.heranca;


public class PrincipalPessoa {
	public static void main(String[] args) {
		PessoaFisica pf = new PessoaFisica();
		pf.setNome("Juliano");
		pf.setEndereco("Rua A");
		pf.setCpf("031");
		pf.setRg("6074");
		pf.imprimir();
		System.out.println("----------------");
		PessoaJuridica pj = new PessoaJuridica();
		pj.setNome("JJD");
		pj.setEndereco("rua B");
		pj.setCnpj("06238");
		pj.setRazaoSocial("JJDInf");
		pj.imprimir();
		System.out.println("----------------");
		
		Pessoa p;
		p = new PessoaFisica();
		p.setNome("juliano");
		((PessoaFisica)p).imprimir();
	}
}
