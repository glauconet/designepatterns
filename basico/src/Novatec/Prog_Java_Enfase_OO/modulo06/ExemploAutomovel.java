package modulo06;

// ExemploAutomovel, subclasse de ExemploVeiculo
import java.util.Scanner;

public class ExemploAutomovel extends ExemploVeiculo {
	private int nrPortas;

	private int nrChassi;

	public ExemploAutomovel() {
		System.out.println("Construindo um objeto da Classe ExemploAutomovel");
	}

	public void gravar() {
		System.out.println("Gravando dados do Autom�vel");
	}

	public void cadastrar() {
		super.cadastrar();
		Scanner scan = new Scanner(System.in).useDelimiter("\r\n");
		System.out.println("Entre com o n�mero de portas: ");
		this.nrPortas = scan.nextInt();
		System.out.println("Entre com o n�mero do chassi: ");
		this.nrChassi = scan.nextInt();
	}

	public void imprimir() {
		super.imprimir();
		System.out.println("Quantidade de portas: " + this.nrPortas);
		System.out.println("N�mero do chassi: " + this.nrChassi);
	}
}