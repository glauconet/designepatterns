package modulo06;

// ExemploBicicleta, subclasse de ExemploVeiculo
import java.util.Scanner;

public class ExemploBicicleta extends ExemploVeiculo {
	// 1 - corrida, 2 - mountain bike, 3 - bicicross
	private int categoriaBicicleta;

	public ExemploBicicleta() {
		System.out.println("Construindo um objeto da Classe ExemploBicicleta");
	}

	public void gravar() {
		System.out.println("Gravando dados da Bicicleta");
	}

	public void cadastrar() {
		super.cadastrar();
		Scanner scan = new Scanner(System.in).useDelimiter("\r\n");
		System.out.println("Entre com a categoria da bicicleta: ");
		this.categoriaBicicleta = scan.nextInt();
	}

	public void imprimir() {
		super.imprimir();
		System.out
				.println("Categoria da bicicleta: " + this.categoriaBicicleta);
	}
}