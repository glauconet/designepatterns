package modulo06;
// Convertendo tipos primitivos 
public class ExemploDoubletoInt {
	public static void main(String s[]) {
			// primeira situa��o
			int a = 1, c = 0;
			double b = 2;
			c = a + (int) b;
			// ser� impresso 3
			System.out.println(c);
			// segunda situa��o
			b = 3.1415;
			c = a + (int) b;
			// ser� impresso 4, entretanto toda a parte decimal ser� perdida
			System.out.println(c);
		}
}


