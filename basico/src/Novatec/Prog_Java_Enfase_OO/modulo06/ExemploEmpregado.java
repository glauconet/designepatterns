package modulo06;

// Classe abstrata ExemploEmpregado
import java.util.Scanner;

public abstract class ExemploEmpregado {
	public double definirSalario() {
		double salario = 0.0;
		System.out.println("Entre com o valor do sal�rio: ");
		Scanner scan = new Scanner(System.in).useDelimiter("\r\n");
		salario = scan.nextDouble();
		return salario;
	}

	public abstract double definirBeneficios();
}