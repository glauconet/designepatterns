package modulo06;

// Usando o operador instanceOf
import java.util.Scanner;

public class ExemploFolhaPagamento {
	public double calcularFolha(ExemploEmpregado emp) {
		double total = 0.0;
		total = emp.definirSalario();
		if (emp instanceof ExemploProgramador) {
			total = total
					+ (total * ((ExemploProgramador) emp).definirBeneficios())
					/ 100;
		}
		if (emp instanceof ExemploGerente) {
			total += ((ExemploGerente) emp).definirBeneficios();
		}
		return total;
	}

	public static void main(String arg[]) {
		ExemploFolhaPagamento pr = new ExemploFolhaPagamento();
		ExemploEmpregado emp = null;
		while (true) {
			System.out.println("1 - Programador");
			System.out.println("2 - Gerente");
			System.out.println("3 - Fim");
			System.out.println("Escolha o empregado: ");
			Scanner scan = new Scanner(System.in).useDelimiter("\r\n");
			int opcao = scan.nextInt();
			if (opcao == 3)
				System.exit(0);
			switch (opcao) {
			case 1:
				emp = new ExemploProgramador();
				break;
			case 2:
				emp = new ExemploGerente();
				break;
			default:
				System.out.println("Op��o inv�lida");
			}
			System.out.println("Sal�rio total: " + pr.calcularFolha(emp));
		}
	}
}