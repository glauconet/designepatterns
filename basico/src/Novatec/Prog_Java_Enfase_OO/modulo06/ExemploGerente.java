package modulo06;

// Subclasse ExemploGerente da superclasse ExemploEmpregado
import java.util.Scanner;

public class ExemploGerente extends ExemploEmpregado {
	public double definirBeneficios() {
		double bonus = 0.0;
		Scanner scan = new Scanner(System.in).useDelimiter("\r\n");
		System.out.println("Entre com o valor do b�nus: ");
		bonus = scan.nextInt();
		return bonus;
	}
}