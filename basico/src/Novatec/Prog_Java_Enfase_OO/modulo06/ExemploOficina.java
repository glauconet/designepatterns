package modulo06;

// Classe ExemploOficina. Implementa o m�todo main()
import java.util.Scanner;

public class ExemploOficina {
	// o par�metro v � uma refer�ncia da classe ExemploVeiculo
	public static void manter(ExemploVeiculo v) {
		while (true) {
			System.out.println("1 - Cadastrar");
			System.out.println("2 - Imprimir");
			System.out.println("3 - gravar");
			System.out.println("4 - Escolher outro ve�culo");
			System.out.println("Entre com a op��o desejada: ");
			Scanner scan = new Scanner(System.in).useDelimiter("\r\n");
			int opcao = scan.nextInt();
			if (opcao == 4)
				break;
			switch (opcao) {
			case 1:
				v.cadastrar();
				break;
			case 2:
				v.imprimir();
				break;
			case 3:
				v.gravar();
				break;
			default:
				System.out.println("Op��o inv�lida");
			}
		}
	}

	public static void main(String[] args) {
		ExemploVeiculo o = null;
		while (true) {
			System.out.println("1 - Autom�vel");
			System.out.println("2 - Bicicleta");
			System.out.println("3 - Fim");
			System.out.println("Escolha o ve�culo: ");
			Scanner scan = new Scanner(System.in).useDelimiter("\r\n");
			int opcao = scan.nextInt();
			if (opcao == 3)
				System.exit(0);
			switch (opcao) {
			case 1:
				o = new ExemploAutomovel();
				break;
			case 2:
				o = new ExemploBicicleta();
				break;
			default:

				System.out.println("Op��o inv�lida");
			}
			/*
			 * na chamada do m�todo manter() ser� realizado uma atribui��o de um
			 * objeto ExemploAutomovel ou ExemploBicicleta a refer�ncia v
			 * definida como par�metro no m�todo manter(). A defini��o de qual
			 * objeto ser� enviado atrav�s do par�metro ir� depender da escolha
			 * do usu�rio. Essa atribui��o de um objeto do tipo de uma subclasse
			 * para um par�metro de uma superclasse � denominada upcasting.
			 */
			ExemploOficina.manter(o);
		}
	}
}