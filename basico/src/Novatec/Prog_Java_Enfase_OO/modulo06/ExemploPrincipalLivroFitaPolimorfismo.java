package modulo06;
//� Nova Implementa��o da classe ExemploPrincipalLivroFita 
import java.util.Scanner;

import modulo05.ExemploAbstractItem;
import modulo05.ExemploFita;
import modulo05.ExemploLivro;
public class ExemploPrincipalLivroFitaPolimorfismo {
	int opcao;
	ExemploAbstractItem obj;
	Scanner scan = new Scanner(System.in).useDelimiter("\r\n");
	public static void main(String[] args) {
			ExemploPrincipalLivroFitaPolimorfismo x = new ExemploPrincipalLivroFitaPolimorfismo();
			x.executar();
	}
	public void executar() {
			int opcao;
			while (true) {
				System.out.println("Entre com a opcao desejada");
				System.out.println("1 - Fita");
				System.out.println("2 - Livro");
				System.out.println("3 - Periodico");
				System.out.println("0 - Fim");
				System.out.println("Opcao : ");
				opcao = this.scan.nextInt();
				if (opcao == 0)
					System.exit(0);
				switch (opcao) {
				// cadastrar fita
				case 1:
					this.obj = ExemploFita.getInstance();
					break;
				case 2:
				case 3:
					this.obj = ExemploLivro.getInstance();
					break;
				default:
					System.out.println("Opcao invalida. Reentre.");
				}
				execSubMenu();
			}
		}
		public int carregarItem() {
			System.out.println("Digite o ISBN: ");
			this.obj.setISBN(this.scan.nextInt());
			if (this.obj.recuperar() == 0) {
				return 0;
			} else {
				return 1;
			}
		}
		public void lerDados() {
			System.out.println("Digite o ISBN: ");
			this.obj.setISBN(this.scan.nextInt());
			System.out.println("Digite o titulo: ");
			this.obj.setTitulo(this.scan.next());
			System.out.println("Digite o nome do respons�vel: ");
			this.obj.setNomeResp(this.scan.next());
			System.out.println("Digite a data de lan�amento da fita (AAAAMMDD): ");
			this.obj.setDtLancamento(this.scan.nextInt());
			this.obj.setSituacaoItem("l");// livre
		}
		public void execSubMenu() {
			while (true) {
				System.out.println("Entre com a opcao desejada");
				System.out.println("1 - Cadastrar ");
				System.out.println("2 - Emprestar");
				System.out.println("3 - Devolver");
				System.out.println("4 - Imprimir");
				System.out.println("5 - Bloquear Livro");
				System.out.println("6 - Liberar Livro");
				System.out.println("0 - Fim");
				System.out.println("Opcao : ");
				Scanner scan = new Scanner(System.in).useDelimiter("\r\n");
				this.opcao = scan.nextInt();
				if (this.opcao == 0)
					System.exit(0);
				switch (this.opcao) {
				// cadastrar fita
				case 1:
					lerDados();
					if (this.obj instanceof ExemploFita) {
						System.out.println("Digite a data de vencimento da fita (AAAAMMDD): ");
						((ExemploFita) this.obj).setDtVencFita(scan.nextInt());
						System.out.println("Digite o seu n�vel: ");
						((ExemploFita) this.obj).setNivelEmprestimo(scan.nextInt());
						System.out.println("Digite o nome do fabricante: ");
						((ExemploFita) this.obj).setDescFabricante(scan.next());
					}
					this.obj.gravar();
					break;
				case 2:
					if (carregarItem() == 1) {
						this.obj.emprestar();
						this.obj.gravar();
					}
					break;
				case 3:
					if (carregarItem() == 1) {
						this.obj.retornar();
						this.obj.gravar();
					}
					break;
				case 4:
					if (carregarItem() == 1) {
						this.obj.imprimir();
					}
					break;
				case 5:
					if (this.obj instanceof ExemploLivro) {
						if (carregarItem() == 1) {
							((ExemploLivro) this.obj).bloquearItem();
							this.obj.gravar();
						}
					} else {
						System.out.println("Op��o disponivel somente para livros.");
					}
					break;
				case 6:
					if (this.obj instanceof ExemploLivro) {
						if (carregarItem() == 1) {
							((ExemploLivro) this.obj).desbloquearItem();
							this.obj.gravar();
						}
					} else {
						System.out.println("Op��o disponivel somente para livros.");
					}
					break;
				default:
					System.out.println("Opcao invalida. Reentre.");
				}
			}
		}
}	
