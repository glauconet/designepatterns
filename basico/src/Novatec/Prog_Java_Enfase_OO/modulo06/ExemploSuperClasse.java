package modulo06;

/* Superclasse que evidencia o problema do uso de polimorfismo com m�todos sobrecarregados
 em um construtor. */
public class ExemploSuperClasse {
	void imprimir() {
		System.out
				.println("M�todo imprimir da superclasse que n�o ser� executado");
	}

	public ExemploSuperClasse() {
		System.out
				.println("In�cio do construtor vazio da SuperClasse: in�cio constru��o");
		/*
		 * apesar de estarmos executando este m�todo na superclasse o m�todo a
		 * ser executado neste exemplo ser� o da subclasse. Isto ocorre porque o
		 * m�todo main que inicia a execu��o deste exerc�cio utiliza um objeto
		 * do tipo da subclasse. Este � um exemplo que mostra que nem sempre o
		 * que parece que seria executado e impresso, realmente ocorre conforme
		 * o esperado. A expectativa seria a execu��o do m�todo imprimir desta
		 * classe por�m isto n�o vai ocorrer.
		 */
		imprimir();
		System.out
				.println("Fim do construtor vazio da SuperClasse: fim constru��o");
	}
}