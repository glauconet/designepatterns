package modulo06.estudodecaso.model;
//– Resposta do laboratório 7 – Classe Lab07ContaCorrenteInterface 
public interface Lab07ContaCorrenteInterface{
	// final representa constante.
	public final static double TAXA_JUROS = 1.5;	
	public int sacar (double valor);
	public void depositar (double valor);
}
