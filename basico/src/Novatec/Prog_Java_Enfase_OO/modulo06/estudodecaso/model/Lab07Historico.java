package modulo06.estudodecaso.model;
//� Resposta do laborat�rio 7 � Classe Lab07Historico 
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Vector;
public class Lab07Historico {
	private int numAge = 0;
	private int numConta = 0;
	private int dia = 0, mes = 0, ano = 0;
	private int hora = 0, min = 0, seg = 0;
	private int cod_hist = 0;
	private double valor = 0.0;
	Vector<String> vetOperacoes = new Vector<String>();
	public Lab07Historico() {
			super();
	}
	public Lab07Historico(int p_num_age, int p_num_conta) {
		this.numAge = p_num_age;
			this.numConta = p_num_conta;
	}
	public void recuperarHistorico() {
			FileReader tArq1;
			BufferedReader tArq2;
			String tLinha = null;
			try {
				// Opera��o I - Abrir o arquivo
				tArq1 = new FileReader(getNumAge() + "." + getNumConta() + ".hist");
				tArq2 = new BufferedReader(tArq1);
				// Opera��o III - Ler atributo/valor e colocar na matriz
				while (true) {
					tLinha = tArq2.readLine();
					if (tLinha == null)
						break;
					this.vetOperacoes.add(tLinha);
				}
				// Opera��o IV - Fechar o arquivo
				tArq2.close();
			}
			catch (FileNotFoundException e) {
				System.out.println("\n Conta sem movimento \n\n");
			}
			catch (IOException tExcept) {
				tExcept.printStackTrace();
			}
		}
	public boolean gravar(int p_hist, double p_valor) {
			FileWriter tArq1;
			PrintWriter tArq2;
			try {
				// Opera��o I - Abrir o aquivo
				tArq1 = new FileWriter(getNumAge() + "." + getNumConta() + ".hist",true);
				tArq2 = new PrintWriter(tArq1);
				Date hoje = new Date();
				Calendar cal = new GregorianCalendar();
				cal.setTime(hoje);
				this.dia = cal.get(Calendar.DAY_OF_MONTH);
				this.mes = cal.get(Calendar.MONTH) + 1; // Em Java o m�s come�a em 0
				this.ano = cal.get(Calendar.YEAR);
				this.hora = cal.get(Calendar.HOUR_OF_DAY);
				this.min = cal.get(Calendar.MINUTE);
				this.seg = cal.get(Calendar.SECOND);	
				tArq2.print(this.numAge   + " ");
				tArq2.print(this.numConta + " ");
				tArq2.print(this.dia		+ " ");
				tArq2.print(this.mes		+ " ");
				tArq2.print(this.ano		+ " ");
				tArq2.print(this.hora		+ " ");
				tArq2.print(this.min		+ " ");
				tArq2.print(this.seg		+ " ");
				tArq2.print(p_hist	+ " ");
				tArq2.println(p_valor);
				// Opera��o III - Fechar o arquivo
				tArq2.close();
				return true;
			} catch (IOException tExcept) {
				tExcept.printStackTrace();
				return false;
			}
		}
		public void imprimir() {
			System.out.println ("\n");
			recuperarHistorico();
			for (int i = 0; i < this.vetOperacoes.size() ; i++) {
				String tSplit[] = ((String) this.vetOperacoes.get(i)).split(" ");
				this.numAge = Integer.parseInt(tSplit[0]);
				this.numConta = Integer.parseInt(tSplit[1]);
				this.dia = Integer.parseInt(tSplit[2]);
				this.mes = Integer.parseInt(tSplit[3]);
				this.ano = Integer.parseInt(tSplit[4]);
				this.hora = Integer.parseInt(tSplit[5]);
				this.min = Integer.parseInt(tSplit[6]);
				this.seg = Integer.parseInt(tSplit[7]);
				this.cod_hist = Integer.parseInt(tSplit[8]);
				this.valor = Double.parseDouble(tSplit[9]);
				NumberFormat formatter;
				StringBuffer sb = new StringBuffer();
				formatter = new DecimalFormat("0000");				
				sb.append(String.valueOf(formatter.format (this.numAge)));
				sb.append(" ");
				formatter = new DecimalFormat("0000000");
				sb.append(String.valueOf(formatter.format (this.numConta)));
				sb.append(" ");		
				formatter = new DecimalFormat("00");
				sb.append(String.valueOf(formatter.format (this.dia)));
				sb.append("/");			
				sb.append(String.valueOf(formatter.format (this.mes)));
				sb.append("/");
				sb.append(String.valueOf(formatter.format (this.ano)));
				sb.append(" - ");
				sb.append(String.valueOf(formatter.format (this.hora)));
				sb.append(":");			
				sb.append(String.valueOf(formatter.format (this.min)));
				sb.append(":");
				sb.append(String.valueOf(formatter.format (this.seg)));
				sb.append(" - ");
				System.out.print(sb.toString());
				switch (this.cod_hist) {
					case 1 :
						System.out.print("Saque caixa        ");
						break;
					case 2 :
						System.out.print("Deposito dinheiro  ");
						break;
					case 3 :
						System.out.print("Atualiza��o do Saldo  ");
						break;					
					default :
						System.out.print("Transacao          ");
						break;
				}
				formatter = NumberFormat.getCurrencyInstance(new Locale("pt","BR"));
				formatter.setMinimumFractionDigits(2);
				System.out.println(formatter.format(this.valor));
			}
			System.out.println ("\n\n\n");
		}
		public int getNumAge() {
			return this.numAge;
		}
		public void setNum_age(int numAge) {
			this.numAge = numAge;
		}
		public int getNumConta() {
			return this.numConta;
		}
		public void setNum_conta(int numConta) {
			this.numConta = numConta;
		}
}
