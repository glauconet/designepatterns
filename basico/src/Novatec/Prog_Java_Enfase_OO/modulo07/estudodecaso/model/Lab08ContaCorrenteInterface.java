package modulo07.estudodecaso.model;
//– Resposta do laboratório 7 – Interface Lab08ContaCorrenteInterface 
import modulo07.MyClassException;
public interface Lab08ContaCorrenteInterface{
	// final representa constante.
	public final static double TAXA_JUROS = 1.5;	
	public void sacar (double valor) throws MyClassException;
	public void depositar (double valor) throws MyClassException;
}
