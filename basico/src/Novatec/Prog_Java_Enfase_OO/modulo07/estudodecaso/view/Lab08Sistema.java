package modulo07.estudodecaso.view;
//– Resposta do laboratório 7 – Classe Lab08Sistema 
import modulo01.estudodecaso.util.Console;
import modulo07.MyClassException;
import modulo07.estudodecaso.model.Lab08ContaCorrente;
import modulo07.estudodecaso.model.Lab08ContaCorrenteEspecial;
import modulo07.estudodecaso.model.Lab08ContaRemunerada;
import modulo07.estudodecaso.model.Lab08Historico;

public class Lab08Sistema {
	int numAge;
	int numConta;
	double valor;
	private void leValores() {
			do {
				this.numAge = Console.readInt("Numero da Agencia : ");
			} while (this.numAge <= 0);
			do {
				this.numConta = Console.readInt("Numero da Conta   : ");
			} while (this.numConta <= 0);
			do {
				this.valor = Console.readDouble("Valor             : ");
			} while (this.valor <= 0.0);
		}
		private void leValoresAgCta() {
			do {
				this.numAge = Console.readInt("Numero da Agencia : ");
			} while (this.numAge <= 0);
			do {
				this.numConta = Console.readInt("Numero da Conta   : ");
			} while (this.numConta <= 0);
		}
		public void execCadastramento() throws MyClassException {
			String nome;
			double limite = 0.0;
			char opcao;
			do {
				nome = Console.readString("Nome do Cliente   : ");
			} while (nome.equals(""));
			leValores();
			if (this.numAge > 5000) {
				do {
					limite = Console.readDouble("Limite      : ");
				} while (this.valor <= 0.0);
			}
			opcao = Console.readChar("Confirma cadastramento (S/N) : ");
			if (Character.toLowerCase(opcao) != 's')
				return;
			Lab08ContaCorrente cc1;
			if (this.numAge > 5000) {
				cc1 = new Lab08ContaCorrenteEspecial(this.numAge, this.numConta, nome, this.valor,limite);
			} else {
				cc1 = new Lab08ContaCorrente(this.numAge, this.numConta, nome, this.valor);
			}
			cc1.gravar();
		}
	public void execSaque() throws MyClassException {
			char opcao;
			leValores();
			opcao = Console.readChar("Confirma saque (S/N) : ");
			if (Character.toLowerCase(opcao) != 's')
				return;
			Lab08ContaCorrente cc1;
			if (this.numAge > 5000) {
				cc1 = new Lab08ContaCorrenteEspecial(this.numAge, this.numConta);
			} else {
				cc1 = new Lab08ContaCorrente(this.numAge, this.numConta);
			}
			cc1.sacar(this.valor);
			cc1.gravar(); // persiste o saque
			Lab08Historico hist = new Lab08Historico(this.numAge, this.numConta);
			hist.gravar(1, this.valor);
		}
	public void execDeposito() throws MyClassException {
			char opcao;
			leValores();
			opcao = Console.readChar("Confirma deposito (S/N) : ");
			if (Character.toLowerCase(opcao) != 's')
				return;
			Lab08ContaCorrente cc1 = new Lab08ContaCorrente(this.numAge, this.numConta);
			cc1.depositar(this.valor);
			cc1.gravar();
			Lab08Historico hist = new Lab08Historico(this.numAge, this.numConta);
			hist.gravar(2, this.valor);
		}
		public void execExtrato() throws MyClassException {
			execConsulta();
			Lab08Historico hist = new Lab08Historico(this.numAge, this.numConta);
			hist.imprimir();
		}
		public void execConsulta() throws MyClassException {
			leValoresAgCta();
			Lab08ContaCorrente cc1;
			if (this.numAge > 5000) {
				cc1 = new Lab08ContaCorrenteEspecial(this.numAge, this.numConta);
			} else {
				cc1 = new Lab08ContaCorrente(this.numAge, this.numConta);
			}
			cc1.imprimir();
		}
		public void execAtualizarSaldo() throws MyClassException {
			leValoresAgCta();
			if (this.numAge > 5000) {
				Lab08ContaRemunerada cc1 = new Lab08ContaRemunerada(this.numAge,this.numConta);
				cc1.calcularJuros();
				cc1.gravar();
				cc1.imprimir();
				Lab08Historico hist = new Lab08Historico(this.numAge, this.numConta);
				hist.gravar(3, cc1.getSaldo());
			} else {
				System.out.println("Somente contas correntes especiais podem atualizar o saldo:");
			}
		}
		public static void main(String[] args) {
			char opcao;
			Lab08Sistema obj = new Lab08Sistema();
			while (true) {
				System.out.println("\n\n\n");
				System.out.println("Entre com a opcao desejada");
				System.out.println("1 - Cadastramento");
				System.out.println("2 - Saque");
				System.out.println("3 - Deposito");
				System.out.println("4 - Consulta");
				System.out.println("5 - Extrato");
				System.out.println("6 - Atualiza Juros");
				System.out.println("8 - Remover Conta Corrente");
				System.out.println("9 - Fim");
				opcao = Console.readChar("Opcao : ");
				if (opcao == '9')
					break;
				try {
					switch (opcao) {
					case '1':
						obj.execCadastramento();
						break;
					case '2':
						obj.execSaque();
						break;
					case '3':
						obj.execDeposito();
						break;
					case '4':
						obj.execConsulta();
						break;
					case '5':
						obj.execExtrato();
						break;
					case '6':
						obj.execAtualizarSaldo();
						break;
					case '8':
						obj.execRemoverContaCorrente();
						break;
					default:
						System.out.println("Opcao invalida. Reentre.");
					}
				} catch (MyClassException e) {
					System.out.println("Classe: " + e.getClasse());
					System.out.println("Mensagem Objeto: " + e.getMessage());
					System.out.println("Mensagem Negocio: " + e.getMensagem());
					System.out.println("Metodo: " + e.getMetodo());
				}
			}
	}
	public Lab08Sistema() {
			super();
	}
	public void execRemoverContaCorrente() throws MyClassException {
			leValoresAgCta();
			if (this.numAge > 5000) {
				Lab08ContaCorrenteEspecial cc1 = new Lab08ContaCorrenteEspecial(this.numAge, this.numConta);
				cc1.removerArquivo();
			} else {
				Lab08ContaCorrente cc1 = new Lab08ContaCorrente(this.numAge, this.numConta);
				cc1.removerArquivo();
			}
	}
}
