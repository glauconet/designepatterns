package modulo07.exercicio;
//� Classe VetorUnidimensional 
import modulo07.MyClassException;
public class VetorUnidimensional implements InterfaceDimensao {
	private int dim1[];
	protected int linha;
	public void adicionar(int valor) throws MyClassException {
			this.dim1[this.linha] = valor;
			this.linha++;
			if (this.linha == this.dim1.length) {
				this.linha = 0;
				throw new MyClassException("Vetor foi excedido. Recome�ando");
			}
	}
	public void imprimir() {
			for (int i = 0 ; i < this.dim1.length ; i ++) {
				System.out.println("Posi��o: " + i + " - " + this.dim1[i]);
			}
	}
	public VetorUnidimensional() {
		this.dim1 = new int[InterfaceDimensao.TAMANHO];
		}
	public VetorUnidimensional(int tamanho) throws MyClassException {
			if ((tamanho <= 0) || (tamanho >= 2000000)) {
				throw new MyClassException("Tamanho do vetor viola os limites definidos");
			}
			this.dim1 = new int[tamanho];
		}
}
