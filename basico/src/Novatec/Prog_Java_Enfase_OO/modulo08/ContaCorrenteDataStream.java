package modulo08;

// Usando as classes DataInputStream e DataOutputStream
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
// A classe MyClassException deve ter sido criada no pacote utilizado no comando import
import modulo07.MyClassException;

public class ContaCorrenteDataStream {
	private int conta, agencia;

	private double saldo;

	private String nomeCliente;

	List<ContaCorrenteDataStream> vet = new ArrayList<ContaCorrenteDataStream>();

	public int getAgencia() {
		return this.agencia;
	}

	public void setAgencia(int agencia) {
		this.agencia = agencia;
	}

	public int getConta() {
		return this.conta;
	}

	public void setConta(int conta) {
		this.conta = conta;
	}

	public String getNomeCliente() {
		return this.nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	public double getSaldo() {
		return this.saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public void imprimir() throws MyClassException {
		recuperar();
		for (int i = 0; i < this.vet.size(); i++) {
			System.out.println("******************************");
			System.out
					.println("N�mero da conta: " + this.vet.get(i).getConta());
			System.out.println("N�mero da ag�ncia: "
					+ this.vet.get(i).getAgencia());
			System.out.println("Saldo da conta corrente: "
					+ this.vet.get(i).getSaldo());
			System.out.println("Nome do cliente: "
					+ this.vet.get(i).getNomeCliente());
			System.out.println("******************************");
		}
	}

	public void gravar() throws MyClassException {
		try {
			// o true significa que o arquivo sempre ir� gravar dados no final
			// (append)
			DataOutputStream out = new DataOutputStream(
					new BufferedOutputStream(new FileOutputStream(
							"exemplodatastream.dat", true)));
			out.writeInt(getAgencia());
			out.writeInt(getConta());
			out.writeUTF(getNomeCliente());
			out.writeDouble(getSaldo());
			out.close();
		} catch (IOException e) {
			MyClassException exc = new MyClassException(
					"Problemas com IOException.");
			exc.setMensagem(e.getMessage());
			throw exc;
		}
	}

	public void recuperar() throws MyClassException {
		try {
			DataInputStream in = new DataInputStream(new BufferedInputStream(
					new FileInputStream("exemplodatastream.dat")));
			ContaCorrenteDataStream obj;

			while (true) {
				obj = new ContaCorrenteDataStream();
				obj.setAgencia(in.readInt());
				obj.setConta(in.readInt());
				obj.setNomeCliente(in.readUTF());
				obj.setSaldo(in.readDouble());
				this.vet.add(obj);
			}
			// objetos do tipo DataInputStream lan�a esta exce��o quando alcan�a
			// o final do arquivo
		} catch (EOFException e) {
			System.out.println("Fim de arquivo.");
		} catch (IOException e) {
			MyClassException exc = new MyClassException(
					"Problemas com IOException.");
			exc.setMensagem(e.getMessage());
			throw exc;
		}
	}
}