package modulo08;

// Realizando a grava��o e leitura de dados em um arquivo usando byte stream
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

public class ExemploByteStream {
	public static void main(String[] args) {
		/*
		 * criar as seguintes vari�veis strArqSaida, arqGravacao e arqleitura
		 * fora do bloco try. Isto ir� permitir que sejam utilizados dentro do
		 * bloco finally.
		 */
		FileOutputStream arqGravacao = null;
		FileInputStream arqLeitura = null;
		String strArqSaida = "acervoOUT.txt";
		try {
			arqGravacao = new FileOutputStream(strArqSaida);
			// o arquivo que ser� lido deve estar gravado abaixo do projeto do
			// Eclipse
			arqLeitura = new FileInputStream("acervo.txt");
			int byteLido = 0;
			/*
			 * o m�todo read l� um byte do fluxo de entrada. Retorna -1 quando o
			 * comando for alcan�ar o final do arquivo.
			 */
			System.out.println("Bytes gravados no arquivo " + strArqSaida
					+ ":\n");
			long tempoInicial = new Date().getTime();
			while ((byteLido = arqLeitura.read()) != -1) {
				arqGravacao.write(byteLido);

				/*
				 * o byte lido est� no formato inteiro. Para apresentar no
				 * formato caracter fazer um cast para char.
				 */
				System.out.print((char) byteLido);
			}
			long tempoFinal = new Date().getTime();
			System.out.println("\n Dura��o: " + (tempoFinal - tempoInicial));
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (arqLeitura != null) {
					arqLeitura.close();
				}
				if (arqGravacao != null) {
					arqGravacao.close();
				}
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}
	}
}