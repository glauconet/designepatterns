package modulo08;

// Usando Character Stream para leitura e grava��o de dados
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

public class ExemploCharacterStream {
	public static void main(String[] args) {
		/*
		 * criar as seguintes vari�veis strArqSaida, arqGravacao e arqleitura
		 * fora do bloco try. Isto ir� permitir que sejam utilizados dentro do
		 * bloco finally.
		 */
		FileWriter arqGravacao = null;
		FileReader arqLeitura = null;
		String strArqSaida = "acervoOUT.txt";
		try {
			arqGravacao = new FileWriter(strArqSaida);
			// o arquivo que ser� lido deve estar gravado abaixo do projeto do
			// Eclipse
			arqLeitura = new FileReader("acervo.txt");
			int byteLido = 0;
			/*
			 * o m�todo read l� um byte do fluxo de entrada. Retorna -1 quando
			 * for alcan�ado o final do arquivo.
			 */
			System.out.println("Bytes gravados no arquivo " + strArqSaida
					+ ":\n");

			long tempoInicial = new Date().getTime();
			while ((byteLido = arqLeitura.read()) != -1) {
				arqGravacao.write(byteLido);
				/*
				 * o byte lido est� no formato inteiro. Para apresentar no
				 * formato caracter fazer um cast para char.
				 */
				System.out.print((char) byteLido);
			}
			long tempoFinal = new Date().getTime();
			System.out.println("\n Dura��o: " + (tempoFinal - tempoInicial));
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (arqLeitura != null) {
					arqLeitura.close();
				}
				if (arqGravacao != null) {
					arqGravacao.close();
				}
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}
	}
}