package modulo08;

// Usando a classe File para manipular as caracter�sticas de um arquivo
import java.io.File;
import java.util.Date;

public class ExemploFile {
	public static void main(String[] args) {
		StringBuilder var = new StringBuilder();
		File arquivo = new File("exemploobjectstreamm.dat");
		var.append("Nome: " + arquivo.getName() + "\n");
		var.append("Caminho: " + arquivo.getAbsolutePath() + "\n");
		var.append("Existe? " + arquivo.exists() + "\n");
		var.append("� um diret�rio? " + arquivo.isDirectory() + "\n");
		var.append("� um arquivo? " + arquivo.isFile() + "\n");
		var.append("Pode ser lido? " + arquivo.canRead() + "\n");
		var.append("Pode ser escrito? " + arquivo.canWrite() + "\n");
		var.append("�ltima modifica��o: " + new Date(arquivo.lastModified())
				+ "\n");
		System.out.println(var);
	}
}