package modulo08;

public class ExemploFormat {
	public static void main(String[] args) {
		int num = 15;
		double raiz = Math.sqrt(num);

		// Usando o objeto out do tipo PrintStream. 
		// Usando o m�todo print e println
		System.out.print("A raiz de: ");
		System.out.print(num);
		System.out.print(" � ");
		System.out.print(raiz);
		System.out.println(".");
		num = 23;
		raiz = Math.sqrt(num);
		System.out.println("A raiz de: " + num + " � " + raiz + ".");
		
		// Usando o m�todo format		
		System.out.format("A raiz de: %d � %f.%n", num, raiz);


	}

}
