package modulo08;
import java.util.Scanner;

import modulo07.MyClassException;
public class ExemploObjectStream {
	ContaCorrenteObjectStream cc = new ContaCorrenteObjectStream();
	public static void main(String[] args) {
			ExemploObjectStream obj = new ExemploObjectStream();
			obj.execMenu();
		}
		private void execMenu() {
			int op = 0;
			while (true) {
				Scanner sc = new Scanner(System.in);
				System.out.println("1 - Cadastrar uma conta");
				System.out.println("2 - Consultar uma conta");
				System.out.println("9 - Sair");
				System.out.println("Entre com uma op��o: ");
				op = sc.nextInt();
				try {
					switch (op) {
					case 1:
						execCadastrar();
						break;
					case 2:
						execConsulta();
						break;
					case 9:
						System.exit(0);
					default:
						System.out.println("Op��o inv�lida.");
					}
				}
				catch (MyClassException e) {
					System.out.println(e.getMensagem());
					System.out.println(e.getMessage());
				}
			}
		}
		public void execConsulta() throws MyClassException {
			this.cc.imprimir();
	}
	public void execCadastrar() throws MyClassException {
			// Para permitir que seja feita a leitura de um nome composto
			Scanner sc = new Scanner(System.in).useDelimiter("\r\n");
			System.out.println("Entre com o nome do cliente: ");
			this.cc.setNomeCliente(sc.nextLine());
			System.out.println("Entre com o n�mero da ag�ncia: ");
			this.cc.setAgencia(sc.nextInt());
			System.out.println("Entre com o n�mero da conta: ");
			this.cc.setConta(sc.nextInt());
			System.out.println("Entre com o saldo: ");
			this.cc.setSaldo(sc.nextDouble());
			this.cc.gravar();
		}
}
