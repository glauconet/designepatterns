package modulo09.abstracfactorymvc.controller;
//� Classe GerentePrimitiva, situada na camada controller 
import modulo09.abstracfactorymvc.model.FactoryFormaPrimitiva;
import modulo09.abstracfactorymvc.model.FactoryIO;
import modulo09.abstracfactorymvc.model.FormaVO;
import modulo09.abstracfactorymvc.model.TemaFormaPrimitiva;
import modulo09.abstracfactorymvc.model.TemaIO;
public class GerentePrimitiva {
	public static final String NOME_PACOTE = "modulo09.abstracfactorymvc.model.";
	/* subOpcao = 1 - Gravar
	 * subOpcao = 2 - Recuperar
	 */
	public static void executarAcao(int subOpcao, FormaVO objVO, String formaEscolhida, String modoGravacao) {
			try {
				TemaFormaPrimitiva fab = FactoryFormaPrimitiva
						.getFabrica(NOME_PACOTE + formaEscolhida);
				TemaIO forma = FactoryIO.getModoGravacao(NOME_PACOTE + modoGravacao);
				if (subOpcao == 1) {
					fab.calcularArea(objVO);
					objVO.setNomeForma(formaEscolhida);
					forma.grava(objVO);
				} else {
					forma.recupera();
				}
			} catch (Exception e) {
				System.out.println(e.toString());
			}
	}
}
