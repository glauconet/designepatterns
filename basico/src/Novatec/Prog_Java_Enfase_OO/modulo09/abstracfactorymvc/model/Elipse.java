package modulo09.abstracfactorymvc.model;
//� Classe Elipse, situada na camada model 
public class Elipse implements TemaFormaPrimitiva {
	public void calcularArea(FormaVO objVO) {
			objVO.setArea(Math.PI * (Math.abs(objVO.getX2() - objVO.getX1()) / 2) * (Math.abs(objVO.getY2() - objVO.getY1()) / 2));
	}
}

