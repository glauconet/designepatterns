package modulo09.abstracfactorymvc.model;

//� Classe abstrata FactoryIO, situada na camada model 
import modulo07.MyClassException;
public abstract class FactoryIO {
	public static TemaIO getModoGravacao(String classe) throws MyClassException {
			try {
				Class<?> objeto = Class.forName(classe);
				TemaIO fabrica = (TemaIO) objeto.newInstance();
				return fabrica;
			} catch (InstantiationException e) {
				throw new MyClassException(
						"Erro na F�brica - InstantiationException", e);
			} catch (IllegalAccessException e) {
				throw new MyClassException("Erro na F�brica - IllegalAccessException", e);
			} catch (ClassNotFoundException e) {
				throw new MyClassException("Erro na F�brica - ClassNotFoundException", e);
			}
	}
}

