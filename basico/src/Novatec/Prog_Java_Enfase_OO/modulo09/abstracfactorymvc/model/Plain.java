package modulo09.abstracfactorymvc.model;
//� Classe Plain, situada na camada model 
import java.util.Vector;
public class Plain implements TemaIO{
	static Vector <FormaVO> vet = new Vector <FormaVO>();
		public void grava(FormaVO objVO) {
			System.out.println ("Gravando em formato Plain");
			System.out.println("Forma: " + objVO.getNomeForma());
			System.out.println("Coordenada X1: " + objVO.getX1());
			System.out.println("Coordenada X2: " + objVO.getX2());
			System.out.println("Coordenada Y1: " + objVO.getY1());
			System.out.println("Coordenada Y2: " + objVO.getY2());
			System.out.println("Area: " + objVO.getArea());
			vet.add(objVO);
	}
	public void recupera() {
			System.out.println("Recuperando coordenadas no formato Texto");
			for (int i = 0 ; i < vet.size() ; i ++) {
				System.out.println("Forma: " + ((FormaVO)vet.get(i)).getNomeForma());
				System.out.println("Coordenada X1: " + ((FormaVO)vet.get(i)).getX1());
				System.out.println("Coordenada X2: " + ((FormaVO)vet.get(i)).getX2());
				System.out.println("Coordenada Y1: " + ((FormaVO)vet.get(i)).getY1());
				System.out.println("Coordenada Y2: " + ((FormaVO)vet.get(i)).getY2());
				System.out.println("Area: " + ((FormaVO)vet.get(i)).getArea());
				System.out.println();
			}
		}
}
