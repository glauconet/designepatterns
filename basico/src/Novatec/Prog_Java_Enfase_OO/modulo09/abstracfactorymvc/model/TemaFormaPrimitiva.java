package modulo09.abstracfactorymvc.model;

//� Interface TemaFormaPrimitiva, situada na camada model 
public interface TemaFormaPrimitiva {
	public void calcularArea(FormaVO objVO);
}
