package modulo09.abstracfactorymvc.model;

//� Interface TemaIO, situada na camada model 
public interface TemaIO {
	public void grava(FormaVO obj);
	public void recupera();
}

