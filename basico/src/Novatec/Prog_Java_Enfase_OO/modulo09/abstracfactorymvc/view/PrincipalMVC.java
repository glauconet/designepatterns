package modulo09.abstracfactorymvc.view;

import java.util.Scanner;

import modulo09.abstracfactorymvc.controller.GerentePrimitiva;
import modulo09.abstracfactorymvc.model.FormaVO;

public class PrincipalMVC {
	public static void main(String[] args) {
		PrincipalMVC obj = new PrincipalMVC();
		obj.execMenuPrincipal();
	}
	public void execSubOpcao(String formaEscolhida, FormaVO objVO) {
		int iTipo = 0;
		while (iTipo != 9) {
			System.out.println("1 - Gravar forma bidimensional");
			System.out.println("2 - Recuperar dados de formas gravadas");
			System.out.println("9 - Escolher outra forma geom�trica \n");
			Scanner scan = new Scanner(System.in);
			System.out.print("Entre com Op��o: ");
			iTipo = scan.nextInt();
			switch (iTipo) {
			case 1:
				execModoGravacao (1, formaEscolhida , objVO);
				break;	
			case 2: {
				execModoGravacao (2, formaEscolhida , objVO);
				break;
			}
			case 9: {
				break;
			}
			default:
				System.out.println();
				System.out.println("Op��o Inv�lida. Tecle <ENTER>.");
				break;
			}
		}
	}
	public void execModoGravacao(int opcao , String formaEscolhida, FormaVO objVO) {
		int iTipo = 0;
		System.out.println("1 - Access - Banco de dados");
		System.out.println("2 - Plain - Arquivo texto");
		System.out.println("9 - Escolher outra forma geom�trica \n");
		Scanner scan = new Scanner(System.in);
		System.out.print("Entre com Op��o: ");
		iTipo = scan.nextInt();
		switch (iTipo) {
		case 1:
			GerentePrimitiva.executarAcao(opcao, objVO, formaEscolhida , "Access");
			break;
		case 2: {
			GerentePrimitiva.executarAcao(opcao, objVO, formaEscolhida , "Plain");
			break;
		}
		case 9: {
			break;
		}
		default:
			System.out.println();
			System.out.println("Op��o Inv�lida. Tecle <ENTER>.");
			break;
		}
	}
	public FormaVO lerCoordenadas(String forma) {
		FormaVO objVO = new FormaVO();
		Scanner scan = new Scanner(System.in);
		System.out.println("Entre com as coordenadas da forma geom�trica");
		System.out.println("Coordenada x1: ");
		objVO.setX1(scan.nextInt());
		System.out.println("Coordenada x2: ");
		objVO.setX2(scan.nextInt());
		System.out.println("Coordenada y1: ");
		objVO.setY1(scan.nextInt());
		System.out.println("Coordenada y2: ");
		objVO.setY2(scan.nextInt());
		objVO.setNomeForma(forma);
		return objVO;
	}
	public void execMenuPrincipal() {
		int iTipo = 0;
		while (iTipo != 9) {
			System.out.println("1 - Elipse");
			System.out.println("2 - Quadrado");
			System.out.println("9 - Fim \n");
			Scanner scan = new Scanner(System.in);
			System.out.print("Entre com Op��o: ");
			iTipo = scan.nextInt();
			switch (iTipo) {
			case 1: {
				execSubOpcao("Elipse", lerCoordenadas("Elipse"));
				break;
			}
			case 2: {
				execSubOpcao("Quadrado", lerCoordenadas("Quadrado"));
				break;
			}
			case 9: {
				break;
			}
			default:
				System.out.println();
				System.out.println("Op��o Inv�lida. Tecle <ENTER>.");
				break;
			}
		}
	}
}
