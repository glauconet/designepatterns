package modulo09.abstractfactorycarro;
import modulo09.factorycarro.ExemploAutomovel;
import modulo09.factorycarro.ExemploCrossFox;
import modulo09.factorycarro.ExemploPolo;
public  class ExemploVolkswagenFactory extends AbstractFactoryMontadora {
	@Override
	public ExemploAutomovel getAutomovel(String marca) {
		if (marca == null)
			return null;
		else if (marca.equals("Polo"))
			return new ExemploPolo();
		else if (marca.equals("CrossFox"))
			return new ExemploCrossFox();
		else
			return null;
	}
}
