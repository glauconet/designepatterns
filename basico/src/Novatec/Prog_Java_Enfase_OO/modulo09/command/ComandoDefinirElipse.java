package modulo09.command;
//� Classe ComandoDefinirElipse 
public class ComandoDefinirElipse extends ComandoAbstrato {
	public ComandoDefinirElipse() {
			super();
	}
	@Override
	protected String getNome() {
			return "DefinirElipse";
	}
	@Override
	protected void executarComando() {
			System.out.println("Executando o comando DefinirElipse");
			lerCoordenadas();
	}
}
