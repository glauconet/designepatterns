package modulo09.command;

//� Classe ComandoDefinirQuadrado 
public class ComandoDefinirQuadrado extends ComandoAbstrato {
	public ComandoDefinirQuadrado() {
			super();
	}
	@Override
	protected String getNome() {
			return "DefinirQuadrado";
	}
	@Override
	protected void executarComando() {
			System.out.println("Executando o comando DefinirQuadrado");
			lerCoordenadas();
	}
}
