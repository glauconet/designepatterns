package modulo09.command;
//� Classe ComandoDefinirReta 
public class ComandoDefinirReta extends ComandoAbstrato {
	public ComandoDefinirReta() {
			super();
	}
	@Override
	protected String getNome() {
			return "DefinirReta";
	}
	@Override
	protected void executarComando() {
			System.out.println("Executando o comando DefinirReta");
			lerCoordenadas();
	}
}


