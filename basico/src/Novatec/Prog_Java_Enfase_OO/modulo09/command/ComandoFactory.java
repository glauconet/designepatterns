package modulo09.command;
//� Classe ComandoFactory 
import modulo07.MyClassException;
public class ComandoFactory {
	public static final String NOME_PACOTE = "modulo09.command.";
	private static ComandoFactory instance = new ComandoFactory();
	// Singleton
	private ComandoFactory() {
			super();
	}
	// Singleton
	public static ComandoFactory getInstance() {
			return instance;
	}
	public Comando criarComando(String nomeComando) throws MyClassException {
			try {
				if (nomeComando == null) {
					throw new Exception("Nome do comando nulo: ");
				}
				Class<?> classe = getClasseComando(nomeComando);
				return (Comando) classe.newInstance();
			} catch (Exception e) {
				throw new MyClassException("Comando inexistente: " + nomeComando);
			}
	}
		private Class<?> getClasseComando(String comando) throws ClassNotFoundException {
				return Class.forName(String.format(NOME_PACOTE + "Comando%s", comando));
	}
}
