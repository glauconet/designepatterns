package modulo09.command;
//– Classe ComandoRecuperar 
public class ComandoRecuperar extends ComandoAbstrato {
	public ComandoRecuperar() {
			super();
	}
	@Override
	protected String getNome() {
			return "Recuperar";
	}
	@Override
	protected void executarComando() {
			System.out.println("Recuperando as coordenadas da forma geométrica");
			System.out.println("Coordenada x1: " + getObjVO().getX1());
			System.out.println("Coordenada x2: " + getObjVO().getX2());
			System.out.println("Coordenada y1: " + getObjVO().getY1());
			System.out.println("Coordenada y2: " + getObjVO().getY2());
	}
}
