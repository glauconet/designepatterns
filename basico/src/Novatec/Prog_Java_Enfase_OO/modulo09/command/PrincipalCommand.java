package modulo09.command;

//� Classe PrincipalCommand 
import java.util.Scanner;

import modulo07.MyClassException;

public class PrincipalCommand {
	private String[] comandosAcesso = { "Gravar", "Recuperar" };

	private String[] comandosFormas = { "DefinirElipse", "DefinirQuadrado",
			"DefinirReta" };

	public static void main(String[] args) {
		PrincipalCommand obj = new PrincipalCommand();
		try {
			obj.execMenuPrincipal();
		} catch (MyClassException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}

	public void execMenuPrincipal() throws MyClassException {
		int opcao = 0;
		while (true) {
			System.out.println("1 - Elipse");
			System.out.println("2 - Quadrado");
			System.out.println("3 - Reta");
			System.out.println("9 - Fim \n");
			Scanner scan = new Scanner(System.in);
			System.out.print("Entre com Op��o: ");
			opcao = scan.nextInt();
			if (opcao >= 1 && opcao <= this.comandosFormas.length) {
				ComandoFactory fabrica = ComandoFactory.getInstance();
				Comando comando = fabrica
						.criarComando(this.comandosFormas[opcao - 1]);
				comando.executar();
			} else {
				if (opcao == 9)
					System.exit(0);
			}
			execSubOpcao();
		}
	}

	public void execSubOpcao() throws MyClassException {
		int opcao = 0;
		while (opcao != 9) {
			System.out.println("1 - Gravar forma bidimensional");
			System.out.println("2 - Recuperar dados de formas gravadas");
			System.out.println("9 - Escolher outra forma geom�trica \n");
			Scanner scan = new Scanner(System.in);
			System.out.print("Entre com Op��o: ");
			opcao = scan.nextInt();
			if (opcao >= 1 && opcao <= this.comandosAcesso.length) {
				ComandoFactory fabrica = ComandoFactory.getInstance();
				Comando comando = fabrica
						.criarComando(this.comandosAcesso[opcao - 1]);
				comando.executar();
			}
		}
	}
}
