package modulo09.contacorrentemvc.controller;

import modulo09.contacorrentemvc.model.Lab01ContaCorrenteBean;
import modulo09.contacorrentemvc.model.Lab02ContaCorrente;

public class Lab03TransacaoSaque {
	private Lab03TransacaoSaque() {
	}
	public static boolean realizarSaque(Lab01ContaCorrenteBean vo) {
		vo = Lab03TransacaoConta.obterConta(vo);
		Lab02ContaCorrente cc1 = new Lab02ContaCorrente(vo); 
		cc1.sacar(vo.getValor());
		return cc1.gravar(); // persiste o saque
	}
}
