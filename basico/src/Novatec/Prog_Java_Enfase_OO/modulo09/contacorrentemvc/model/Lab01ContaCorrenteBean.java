package modulo09.contacorrentemvc.model;

public class Lab01ContaCorrenteBean {
	private int numAge = 0;
	private int numConta = 0;
	private String nomeCliente = "";
	private double saldo = 0.0;
	private double valor = 0.0;
	
	public int getNumAge() {
		return numAge;
	}
	public void setNumAge(int numAge) {
		this.numAge = numAge;
	}
	public int getNumConta() {
		return numConta;
	}
	public void setNumConta(int numConta) {
		this.numConta = numConta;
	}
	public String getNomeCliente() {
		return nomeCliente;
	}
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}

}
