package modulo09.estudodecaso.controller;
// Classe GerenteContaCorrente 
import java.util.HashMap;
import java.util.Map;
import modulo07.MyClassException;
import modulo09.estudodecaso.model.Constantes;
import modulo09.estudodecaso.model.ModoGravacao;
import modulo09.estudodecaso.model.Recurso;
import modulo09.estudodecaso.model.conta.AbstractFactoryConta;
import modulo09.estudodecaso.model.conta.ContaCorrenteVO;
import modulo09.estudodecaso.model.historico.ContaHistoricoVO;
import modulo09.estudodecaso.model.historico.HistoricoFactory;
//a classe GerenteContaCorrente representa dentro do padr�o de projeto MVC (model view controller) a classe que representa o controller.
public class GerenteContaCorrente {
	private static void tratarHistorico(ContaCorrenteVO vo, int tipo, double valor) throws MyClassException {
			ModoGravacao modo = HistoricoFactory.getInstanceModoGravacao();
			ContaHistoricoVO objHisVO = new ContaHistoricoVO();
			objHisVO.setAgencia(vo.getAgencia());
			objHisVO.setConta(vo.getConta());
			objHisVO.setTipo(tipo);
			objHisVO.setValor(valor);
			if (tipo != 0) {
				ContaHistoricoVO objExtrato = (ContaHistoricoVO) modo.recuperar(objHisVO);
				objHisVO.setVetOperacoes(objExtrato.getVetOperacoes());
			}
			modo.gravar(objHisVO);
	}
	public static void execCadastramento(ContaCorrenteVO vo )throws MyClassException {
			AbstractFactoryConta fabrica = null;
			if (vo.getAgencia() > 5000) { // Cta Especial
				// usando o padr�o de projeto abstractfactory para obter um objeto de uma conta corrente especial
				// ir� buscar no arquivo fabrica.properties o nome da classe para que o objeto seja criado.
				fabrica = AbstractFactoryConta.getInstanceAbstractFactoryConta(getFabrica(Constantes.PROPERTY_FABRICA_CONTACORRENTE_ESPECIAL));
			} else { // Cta Normal
				// usando o padr�o de projeto abstractfactory para obter um objeto de uma conta corrente
				fabrica = AbstractFactoryConta.getInstanceAbstractFactoryConta(getFabrica(Constantes.PROPERTY_FABRICA_CONTACORRENTE));
			}
			fabrica.cadastrar(vo);
			tratarHistorico(vo, 0,vo.getSaldo());
		}
		public static void execSaque(ContaCorrenteVO vo) throws MyClassException {
			AbstractFactoryConta fabrica = null;
			if (vo.getAgencia() > 5000) { // Cta Especial
				// usando o padr�o de projeto abstractfactory para obter um objeto de uma conta corrente especial
				fabrica = AbstractFactoryConta.getInstanceAbstractFactoryConta(getFabrica(Constantes.PROPERTY_FABRICA_CONTACORRENTE_ESPECIAL));
			} else { // Cta Normal
				// usando o padr�o de projeto abstractfactory para obter um objeto de uma conta corrente
				fabrica = AbstractFactoryConta.getInstanceAbstractFactoryConta(getFabrica(Constantes.PROPERTY_FABRICA_CONTACORRENTE));
			}
			// Apos executar o saque o vo ter� ser atributo saldo alterado para o atual.
			double valor = vo.getSaldo();
			fabrica.sacar(vo, vo.getSaldo());
			tratarHistorico(vo, 1, valor);
	}
	public static void execDeposito(ContaCorrenteVO vo) throws MyClassException {
			AbstractFactoryConta fabrica = null;
			if (vo.getAgencia() > 5000) { // Cta Especial
				// usando o padr�o de projeto abstractfactory para obter um objeto de uma conta corrente especial
				fabrica = AbstractFactoryConta.getInstanceAbstractFactoryConta(getFabrica(Constantes.PROPERTY_FABRICA_CONTACORRENTE_ESPECIAL));
			} else { // Cta Normal
				// usando o padr�o de projeto abstractfactory para obter um objeto de uma conta corrente
				fabrica = AbstractFactoryConta.getInstanceAbstractFactoryConta(getFabrica(Constantes.PROPERTY_FABRICA_CONTACORRENTE));
			}
			// Apos executar o deposito o vo ter� ser atributo saldo alterado para o atual.
			double valor = vo.getSaldo();
			fabrica.depositar(vo, vo.getSaldo());
			tratarHistorico(vo, 2, valor);
	}
	public static void execAtualizarSaldo(ContaCorrenteVO vo) throws MyClassException {
			AbstractFactoryConta fabrica = null;
			if (vo.getAgencia() > 5000) { // Cta Especial
				// usando o padr�o de projeto abstractfactory para obter um objeto de uma conta corrente especial
				fabrica = AbstractFactoryConta.getInstanceAbstractFactoryConta(getFabrica(Constantes.PROPERTY_FABRICA_CONTACORRENTE_ESPECIAL));
			} else { // Cta Normal
				// usando o padr�o de projeto abstractfactory para obter um objeto de uma conta corrente
				fabrica = AbstractFactoryConta.getInstanceAbstractFactoryConta(getFabrica(Constantes.PROPERTY_FABRICA_CONTACORRENTE));
			}
			fabrica.remunerar(vo);
			tratarHistorico(vo, 3, vo.getSaldo());
	}
	public static Map<String, ContaCorrenteVO> execConsulta(ContaCorrenteVO vo) throws MyClassException {
			Map<String, ContaCorrenteVO> impressaoMapa = new HashMap<String, ContaCorrenteVO>();
			AbstractFactoryConta fabrica = null;
			if (vo.getAgencia() > 5000) { // Cta Especial
				// usando o padr�o de projeto abstractfactory para obter um objeto de uma conta corrente especial
				fabrica = AbstractFactoryConta.getInstanceAbstractFactoryConta(getFabrica(Constantes.PROPERTY_FABRICA_CONTACORRENTE_ESPECIAL));
			} else { // Cta Normal
				// usando o padr�o de projeto abstractfactory para obter um objeto de uma conta corrente
				fabrica = AbstractFactoryConta.getInstanceAbstractFactoryConta(getFabrica(Constantes.PROPERTY_FABRICA_CONTACORRENTE));
			}
			impressaoMapa.put("contacorrente", fabrica.imprimir(vo));
			return impressaoMapa;
	}
		public static Map<String, ContaCorrenteVO> execExtrato(ContaCorrenteVO vo) throws MyClassException {
			Map<String, ContaCorrenteVO> impressaoMapa = new HashMap<String, ContaCorrenteVO>();
			AbstractFactoryConta fabrica = null;
			if (vo.getAgencia() > 5000) { // Cta Especial
				// usando o padr�o de projeto abstractfactory para obter um objeto de uma conta corrente especial
				fabrica = AbstractFactoryConta.getInstanceAbstractFactoryConta(getFabrica(Constantes.PROPERTY_FABRICA_CONTACORRENTE_ESPECIAL));
			} else { // Cta Normal
				// usando o padr�o de projeto abstractfactory para obter um objeto de uma conta corrente
				fabrica = AbstractFactoryConta.getInstanceAbstractFactoryConta(getFabrica(Constantes.PROPERTY_FABRICA_CONTACORRENTE));
			}
			ContaCorrenteVO voImpressao = null;
			voImpressao = fabrica.imprimir(vo);
			impressaoMapa.put("contacorrente", voImpressao);
			ModoGravacao modo = HistoricoFactory.getInstanceModoGravacao();
			ContaHistoricoVO objHisVO = new ContaHistoricoVO();
			objHisVO.setAgencia(vo.getAgencia());
			objHisVO.setConta(vo.getConta());
			voImpressao = modo.imprimir(objHisVO);
			impressaoMapa.put("contacorrentehistorico", voImpressao);
			return impressaoMapa;
	}
	public static void execRemoverContaCorrente(ContaCorrenteVO vo) throws MyClassException {
			AbstractFactoryConta fabrica = null;
			if (vo.getAgencia() > 5000) { // Cta Especial
				// usando o padr�o de projeto abstractfactory para obter um objeto de uma conta corrente especial
				fabrica = AbstractFactoryConta.getInstanceAbstractFactoryConta(getFabrica(Constantes.PROPERTY_FABRICA_CONTACORRENTE_ESPECIAL));
			} else { // Conta Normal
				// usando o padr�o de projeto abstractfactory para obter um objeto de uma conta corrente
				fabrica = AbstractFactoryConta.getInstanceAbstractFactoryConta(getFabrica(Constantes.PROPERTY_FABRICA_CONTACORRENTE));
			}
			fabrica.removerArquivo(vo);
		}
	/* Escolhe entre as fabricas que herdam de AbstractFactoryConta.
	 * ContaCorrenteFactory e ContaCorrenteEspecialFactory */
	private static String getFabrica(String fabrica) throws MyClassException {
			return Recurso.getRecurso(Constantes.RESOURCE_FACTORY, fabrica);
	}
	// este m�todo ser� usado no laborat�rio que utiliza o padr�o de projeto command.
	public static boolean isContaEspecial(int agencia) {
		return AbstractFactoryConta.isContaEspecial(agencia);
	}
}

