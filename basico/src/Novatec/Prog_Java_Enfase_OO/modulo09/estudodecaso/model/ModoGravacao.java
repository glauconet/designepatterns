package modulo09.estudodecaso.model;
//�  Classe abstrata ModoGravacao 
import modulo07.MyClassException;
import modulo09.estudodecaso.model.conta.ContaCorrenteVO;
import modulo09.estudodecaso.model.conta.ContaLimiteVO;
import modulo09.estudodecaso.model.historico.ContaHistoricoVO;
import modulo09.estudodecaso.model.historico.HistoricoFactory;
public abstract class ModoGravacao {
	public abstract void gravar(ContaCorrenteVO vo) throws MyClassException;
	public abstract ContaCorrenteVO recuperar(ContaCorrenteVO vo) 	throws MyClassException;
	public ContaCorrenteVO imprimir(ContaCorrenteVO vo) throws MyClassException {
			vo = recuperar(vo);
			return vo;
	}
	public ContaCorrenteVO imprimir(ContaLimiteVO vo) throws MyClassException {
			vo = (ContaLimiteVO) recuperar(vo);
			return vo;
	}
	public ContaCorrenteVO imprimir(ContaHistoricoVO vo)throws MyClassException {
			ModoGravacao objTema = HistoricoFactory.getInstanceModoGravacao();
			vo = (ContaHistoricoVO) objTema.recuperar(vo);
			return vo;
	}
}
