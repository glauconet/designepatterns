package modulo09.estudodecaso.model;
//–  Classe Recurso 
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import modulo07.MyClassException;
public class Recurso {
	private Recurso() {
			super();
	}
	public static String getRecurso(String arquivo, String propriedade) throws MyClassException {
			try {
				if (arquivo == null || propriedade == null) {
					MyClassException myObj = new MyClassException("Parâmetros arquivo ou propriedade nulos");
					myObj.setClasse("Recurso");
					myObj.setPacote("modulo09.estudodecaso.model");
					myObj.setMetodo("getRecurso()");
					throw myObj;
				}
				FileInputStream fis = null;
				Properties prop = null;
				// nome do arquivo deve ficar abaixo do projeto no Eclipse
				fis = new FileInputStream(arquivo);
				prop = new Properties();
				prop.load(fis);
				return prop.getProperty(propriedade);
			} catch (NullPointerException e) {
				MyClassException myObj = new MyClassException("Problemas causados por NullPointException");
				myObj.setClasse("Recurso");
				myObj.setPacote("modulo09.estudodecaso.model");
				myObj.setMetodo("getRecurso()");
				throw myObj;
			} catch (FileNotFoundException e) {
				MyClassException myObj = new MyClassException("Problemas causados por FileNotFoundException");
				myObj.setClasse("Recurso");
				myObj.setPacote("modulo09.estudodecaso.model");
				myObj.setMetodo("getRecurso()");
				throw myObj;
			} catch (IOException e) {
				MyClassException myObj = new MyClassException("Problemas causados por IOException");
				myObj.setClasse("Recurso");
				myObj.setPacote("modulo09.estudodecaso.model");
				myObj.setMetodo("getRecurso()");
				throw myObj;
			}
	}
}
