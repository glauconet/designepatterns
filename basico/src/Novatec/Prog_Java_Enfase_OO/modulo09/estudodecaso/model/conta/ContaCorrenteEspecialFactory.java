package modulo09.estudodecaso.model.conta;
//�  Classe ContaCorrenteEspecialFactory 
import java.io.File;

import modulo07.MyClassException;
import modulo09.estudodecaso.model.Constantes;
import modulo09.estudodecaso.model.ModoGravacao;
import modulo09.estudodecaso.model.Recurso;
public class ContaCorrenteEspecialFactory extends AbstractFactoryConta{ 
	public final static double TAXA_JUROS = 1.50;
	public void sacar (ContaCorrenteVO vo, double valor) throws MyClassException{
			if (valor <= 0) {
				MyClassException myObj = new MyClassException("Valor do saque deve ser maior que zero");
				myObj.setClasse(this.getClass().toString());
				myObj.setPacote(this.getClass().getPackage().toString());
				myObj.setMetodo("saque");
				throw myObj;
			} else {
				ModoGravacao objTema = obtemFormaGravacao();
				vo = (ContaLimiteVO)objTema.recuperar (vo);
				if (valor > (((ContaLimiteVO)vo).getLimite() + vo.getSaldo())) {
					MyClassException myObj = new MyClassException("Saldo Insuficiente para o saque");
					myObj.setClasse(this.getClass().toString());
					myObj.setPacote(this.getClass().getPackage().toString());
					myObj.setMetodo("saque");
					throw myObj;
				}	else {
					vo.setSaldo(vo.getSaldo() - valor);
					objTema.gravar(vo);
				}
			}
	}	
	public void remunerar(ContaCorrenteVO vo) throws MyClassException{
			ModoGravacao objTema = obtemFormaGravacao();
			vo = (ContaLimiteVO)objTema.recuperar (vo);
			if (vo.getSaldo() <= 0) {
				MyClassException myObj = new MyClassException 
					("Saldo zerado ou negativo");
				myObj.setClasse(this.getClass().toString());
				myObj.setPacote(this.getClass().getPackage().toString());
				myObj.setMetodo("remunerar");
				throw myObj;
			} else {
				vo.setSaldo(vo.getSaldo() * TAXA_JUROS);
				objTema.gravar(vo);
			}
		}
		public void cadastrar(ContaCorrenteVO vo ) throws MyClassException{
			ModoGravacao objTema = obtemFormaGravacao();
			objTema.gravar (vo);
		}
		protected String carregarProperties() throws MyClassException {
			/*
			 * Aqui estaremos utilizando o padr�o de projeto lazy instantiation
			 */
			return Recurso.getRecurso(Constantes.RESOURCE_CONTA,
					Constantes.PROPERTY_CONTACORRENTE_ESPECIAL);
		}
		@Override
		public ContaCorrenteVO imprimir(ContaCorrenteVO vo) throws MyClassException {
				ModoGravacao objTema = obtemFormaGravacao();
				return objTema.imprimir((ContaLimiteVO)vo);
		}
		@Override
		public boolean removerArquivo(ContaCorrenteVO vo) {
				File tArq1;
				tArq1 = new File(vo.getAgencia() + "." + vo.getConta() + ".dat");
				tArq1.delete();
				tArq1 = new File(vo.getAgencia() + "." + vo.getConta() + ".hist");
				tArq1.delete();
				tArq1 = new File(vo.getAgencia() + "." + vo.getConta() + ".esp");
				tArq1.delete();
				return true;
		}
}
