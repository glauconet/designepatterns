package modulo09.estudodecaso.model.conta;
//�  Classe ContaCorrenteFactory 
import java.io.File;

import modulo07.MyClassException;
import modulo09.estudodecaso.model.Constantes;
import modulo09.estudodecaso.model.ModoGravacao;
import modulo09.estudodecaso.model.Recurso;
public class ContaCorrenteFactory extends AbstractFactoryConta {
	public final static double TAXA_JUROS = 1.25;
	public void sacar(ContaCorrenteVO vo, double valor) throws MyClassException {
			ModoGravacao objTema = obtemFormaGravacao();
			vo = objTema.recuperar(vo);
			if (valor <= 0) {
				MyClassException myObj = new MyClassException("O valor a sacar dever ser maior que zero");
				myObj.setClasse(this.getClass().toString());
				myObj.setPacote(this.getClass().getPackage().toString());
				myObj.setMetodo("saque");
				throw myObj;
			} else {
				if (valor > vo.getSaldo()) {
					MyClassException myObj = new MyClassException("Saldo insuficiente para o saque");
					myObj.setClasse(this.getClass().toString());
					myObj.setPacote(this.getClass().getPackage().toString());
					myObj.setMetodo("saque");
					throw myObj;
				} else {
					vo.setSaldo(vo.getSaldo() - valor);
					// implementando o conceito de template
					objTema.gravar(vo);
				}
			}
	}
	public void cadastrar(ContaCorrenteVO vo) throws MyClassException {
			ModoGravacao objTema = obtemFormaGravacao();
			objTema.gravar(vo);
		}
	protected String carregarProperties() throws MyClassException {
			return Recurso.getRecurso(Constantes.RESOURCE_CONTA, Constantes.PROPERTY_CONTACORRENTE);
		}
		public void remunerar(ContaCorrenteVO vo) throws MyClassException {
			ModoGravacao objTema = obtemFormaGravacao();
			vo = objTema.recuperar(vo);
			if (vo.getSaldo() <= 0) {
				MyClassException myObj = new MyClassException("Saldo zerado ou negativo");
				myObj.setClasse(this.getClass().toString());
				myObj.setPacote(this.getClass().getPackage().toString());
				myObj.setMetodo("remunerar");
				throw myObj;
			} else {
				vo.setSaldo(vo.getSaldo() * TAXA_JUROS);
				objTema.gravar(vo);
			}
		}
		@Override
		public ContaCorrenteVO imprimir(ContaCorrenteVO vo) throws MyClassException {
			ModoGravacao objTema = obtemFormaGravacao();
				return objTema.imprimir(vo);
		}
		public boolean removerArquivo(ContaCorrenteVO vo) {
				File tArq1;
				tArq1 = new File(vo.getAgencia() + "." + vo.getConta() + ".dat");
				tArq1.delete();
				tArq1 = new File(vo.getAgencia() + "." + vo.getConta() + ".hist");
				tArq1.delete();
				return true;
		}
}
