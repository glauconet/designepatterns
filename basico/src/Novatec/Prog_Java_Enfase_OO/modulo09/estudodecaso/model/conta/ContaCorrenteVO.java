package modulo09.estudodecaso.model.conta;

//�  Classe ContaCorrenteVO 
public class ContaCorrenteVO {
	private int numAgencia;
	private int numConta;
	private String sNome; 
	private double dSaldo;
	public int getAgencia() {
			return this.numAgencia;
	}
	public void setAgencia(int pAgencia) {
			this.numAgencia = pAgencia;
	}
	public int getConta() {
			return this.numConta;
	}
	public void setConta(int pConta) {
			this.numConta = pConta;
	}
	public String getNome() {
			return this.sNome;
	}
	public void setNome(String pNome) {
			this.sNome = pNome;
	}
	public double getSaldo() {
			return this.dSaldo;
	}
	public void setSaldo(double pSaldo) {
			this.dSaldo = pSaldo;
	}
}
