package modulo09.estudodecaso.model.conta;
//�  Classe PlainContaCorrenteDAO 
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import modulo07.MyClassException;
import modulo09.estudodecaso.model.ModoGravacao;
public class PlainContaCorrenteDAO extends ModoGravacao {
	public void gravar(ContaCorrenteVO vo) throws MyClassException{
			FileWriter tArq1 = null;
			PrintWriter tArq2;
			try {
				// Opera��o I - Abrir o aquivo
				tArq1 = new FileWriter(vo.getAgencia() + "." + vo.getConta() + ".dat");
				tArq2 = new PrintWriter(tArq1);
				// Opera��o II - Gravar os dados
				tArq2.println(vo.getAgencia());
				tArq2.println(vo.getConta());
				tArq2.println(vo.getNome());
				tArq2.println(vo.getSaldo());
				// Opera��o III - Fechar o arquivo
				tArq2.close();
			} catch (IOException tExcept) { // exce��o - erro no processo
				MyClassException myObj = new MyClassException 
					("Erro causado na grava��o do arquivo Cta Corrente");
				myObj.setClasse(this.getClass().toString());
				myObj.setMensagem(tExcept.getMessage());
				myObj.setPacote(this.getClass().getPackage().toString());
				myObj.setMetodo("gravar");
				throw myObj;
			}
		}
		public ContaCorrenteVO recuperar(ContaCorrenteVO vo) throws MyClassException{
			FileReader tArq1 = null;
			BufferedReader tArq2;
			int tQtde = 4;
			String[] tLinha;
			try {
				// Opera��o I - Abrir o arquivo
				tArq1 = new FileReader(vo.getAgencia() + "." + vo.getConta() + ".dat");
				tArq2 = new BufferedReader(tArq1);
				// Opera��o III - Ler atributo/valor e colocar na matriz
				tLinha = new String[tQtde];
				for (int i = 0; i < tQtde; i++) {
					tLinha[i] = tArq2.readLine();
				}
				// Opera��o IV - Fechar o arquivo
				tArq2.close();
				vo.setAgencia(Integer.parseInt(tLinha[0]));	
				vo.setConta(Integer.parseInt(tLinha[1]));
				vo.setNome(tLinha[2]);
				vo.setSaldo(Double.parseDouble(tLinha[3]));
				return vo;
			} catch (IOException tExcept) {
				MyClassException myObj = new MyClassException ("Conta Corrente n�o existe.");
				myObj.setClasse(this.getClass().toString());
				myObj.setMensagem(tExcept.getMessage());
				// Se a classe n�o estiver em um pacote dar� erro
				myObj.setPacote(this.getClass().getPackage().toString());
				myObj.setMetodo("recuperar");
				throw myObj;		// relan�ando a exce��o
			}
	}
}
