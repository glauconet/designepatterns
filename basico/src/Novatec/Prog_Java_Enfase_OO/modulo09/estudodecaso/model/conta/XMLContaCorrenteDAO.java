package modulo09.estudodecaso.model.conta;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.PrintWriter;

import modulo07.MyClassException;
import modulo09.estudodecaso.model.ModoGravacao;

import com.thoughtworks.xstream.XStream;
public class XMLContaCorrenteDAO  extends ModoGravacao {
	public void gravar(ContaCorrenteVO vo) throws MyClassException{
		try {
			XStream xs = new XStream();			
/*
			File file = new File(getNomeArquivo(vo));
			FileOutputStream fos = new FileOutputStream(file);
*/	
			FileWriter tArq1 = null;
			PrintWriter fos;
			tArq1 = new FileWriter(getNomeArquivo(vo));
			fos = new PrintWriter(tArq1);
			
			xs.toXML(vo, fos);
			
		} catch (Exception tExcept) {
			
			MyClassException myObj = new MyClassException 
			("Erro causado na grava��o do arquivo Cta Corrente");
			myObj.setClasse(this.getClass().toString());
				myObj.setMensagem(tExcept.getMessage());
				// Se a classe n�o estiver em um pacote dar� erro
				myObj.setPacote(this.getClass().getPackage().toString());
				myObj.setMetodo("gravar");
				throw myObj;		// relan�ando a exce��o
		}
	}
	public ContaCorrenteVO recuperar(ContaCorrenteVO vo) throws MyClassException{
		try {	
			XStream xs = new XStream();
			File file = new File(getNomeArquivo(vo));
			FileInputStream fis = new FileInputStream(file);
			return (ContaCorrenteVO) xs.fromXML(fis);
		} catch (Exception tExcept) {
			MyClassException myObj = new MyClassException("Conta Corrente n�o existe.");
			myObj.setClasse(this.getClass().toString());
				myObj.setMensagem(tExcept.getMessage());
				// se a classe n�o estiver em um pacote dar� erro
				myObj.setPacote(this.getClass().getPackage().toString());
				myObj.setMetodo("recuperar");
				throw myObj;		// relan�ando a exce��o
		}		
	}
	private String getNomeArquivo(ContaCorrenteVO vo) {
			int agencia = vo.getAgencia();
			int conta = vo.getConta();
			return getNomeArquivo(agencia, conta);
	}
	private String getNomeArquivo(int agencia, int conta) {
			return String.format("%s.%s.xml", agencia, conta);
	}
}
