package modulo09.estudodecaso.model.historico;
import java.util.Vector;

import modulo09.estudodecaso.model.conta.ContaCorrenteVO;
public class ContaHistoricoVO extends ContaCorrenteVO{
	private int iTipo;
	private double  valor;
	private Vector <String> vetOperacoes = new Vector <String>() ;
	public int getTipo() {
			return this.iTipo;
	}
	public void setTipo(int pTipo) {
			this.iTipo = pTipo;
	}
	public double getValor() {
			return this.valor;
	}
	public void setValor(double valor) {
			this.valor = valor;
	}
	public Vector<String> getVetOperacoes() {
			return this.vetOperacoes;
	}
	public void setVetOperacoes(Vector<String> vetOperacoes) {
			this.vetOperacoes = vetOperacoes;
	}
}
