package modulo09.estudodecaso.model.historico;
//�  Classe HistoricoFactory 
import modulo07.MyClassException;
import modulo09.estudodecaso.model.Constantes;
import modulo09.estudodecaso.model.ModoGravacao;
import modulo09.estudodecaso.model.Recurso;
public class HistoricoFactory {
	public static ModoGravacao getInstanceModoGravacao() throws MyClassException {
			try {
				String qual = (new HistoricoFactory()).carregarProperties();
				// joga a classe para memoria
				Class<?> classe = Class.forName(qual);
				// pega a referecia da classe da memoria
				ModoGravacao dao = (ModoGravacao) classe.newInstance();
				return dao;
			} catch (Exception e) {
				MyClassException myObj = new MyClassException(
						"Erro causado ao obter um objeto de ModoGravacao");
				myObj.setClasse("Historico");
				myObj.setMensagem(e.getMessage());
				// Se a classe n�o estiver em um pacote dar� erro
				myObj.setPacote("modulo06.estudodecaso.model.historico");
				myObj.setMetodo("getInstanceModoGravacao");
				e.printStackTrace();
				throw myObj; // Relan�ando a exce��o
			}
		}
	private String carregarProperties() throws MyClassException {
			try {
				return Recurso.getRecurso(Constantes.RESOURCE_HISTORICO,Constantes.PROPERTY_HISTORICO);
			} catch (Exception e) {
				MyClassException myObj = new MyClassException(Constantes.RESOURCE_HISTORICO);
				myObj.setClasse(this.getClass().toString());
				myObj.setMensagem(e.getMessage());
				// Se a classe n�o estiver em um pacote dar� erro
				myObj.setPacote(this.getClass().getPackage().toString());
				myObj.setMetodo("carregarProperties");
				throw myObj; // Relan�ando a exce��o
			}
		}
}
