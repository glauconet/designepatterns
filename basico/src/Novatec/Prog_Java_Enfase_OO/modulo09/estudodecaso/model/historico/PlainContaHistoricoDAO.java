package modulo09.estudodecaso.model.historico;
//�  Classe PlainContaHistoricoDAO 
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import modulo07.MyClassException;
import modulo09.estudodecaso.model.ModoGravacao;
import modulo09.estudodecaso.model.conta.ContaCorrenteVO;
public class PlainContaHistoricoDAO extends ModoGravacao{
	public ContaHistoricoVO  recuperar(ContaCorrenteVO vo) throws MyClassException {
			FileReader tArq1 = null;
			BufferedReader tArq2;
			String tLinha = null;
			try {
				// Opera��o I - Abrir o arquivo
				tArq1 = new FileReader (vo.getAgencia() + "." + vo.getConta() + ".hist");
				tArq2 = new BufferedReader(tArq1);
				// Opera��o III - Ler atributo/valor e colocar na matriz
				while (true) {
					tLinha = tArq2.readLine();
					if (tLinha == null)
						break;
					((ContaHistoricoVO)vo).getVetOperacoes().add(tLinha);
				}
				// Opera��o IV - Fechar o arquivo
				tArq2.close();
				return (ContaHistoricoVO)vo;
			} catch (IOException tExcept) {
				MyClassException myObj = new MyClassException 
				("Erro causado na leitura do arquivo Historico");
				myObj.setClasse(this.getClass().toString());
				myObj.setMensagem(tExcept.getMessage());
				// 	Se a classe n�o estiver em um pacote dar� erro
				myObj.setPacote(this.getClass().getPackage().toString());
				myObj.setMetodo("recuperar");
				throw myObj;		// relan�ando a exce��o
			}
		}
		public void gravar(ContaCorrenteVO vo) throws MyClassException{
			StringBuffer rLinha = new StringBuffer();
			FileWriter tArq1=null;
			PrintWriter tArq2;
			try {
				// Opera��o I - Abrir o aquivo
				tArq1 = new FileWriter(vo.getAgencia() + "." + vo.getConta() + ".hist", true);
				tArq2 = new PrintWriter(tArq1);
				Date hoje = new Date();
				Calendar cal = new GregorianCalendar();
				cal.setTime(hoje);
				// monta uma linha do historico de movimenta��o
				rLinha.append(vo.getAgencia() + " ");
				rLinha.append(vo.getConta() + " ");
				rLinha.append(cal.get(Calendar.DAY_OF_MONTH) + " ");
				rLinha.append(cal.get(Calendar.MONTH) + " ");
				rLinha.append(cal.get(Calendar.YEAR) + " ");
				rLinha.append(cal.get(Calendar. HOUR_OF_DAY) + " ");
				rLinha.append(cal.get(Calendar.MINUTE) + " ");
				rLinha.append(cal.get(Calendar.SECOND) + " ");
				rLinha.append(((ContaHistoricoVO)vo).getTipo() + " ");
				rLinha.append(((ContaHistoricoVO)vo).getValor() + " ");
				rLinha.append(vo.getSaldo());
				// grava a linha de historico no arquivo
				tArq2.println(rLinha);
				// Opera��o III - Fechar o arquivo
				tArq2.close();
			} catch (IOException tExcept) {
				MyClassException myObj = new MyClassException 
				("Erro causado na grava��o do arquivo Historico");
				myObj.setClasse(this.getClass().toString());
				myObj.setMensagem(tExcept.getMessage());
				// Se a classe n�o estiver em um pacote dar� erro
				myObj.setPacote(this.getClass().getPackage().toString());
				myObj.setMetodo("gravar");
				throw myObj; // Relan�ando a exce��o
			}
		}	
}
