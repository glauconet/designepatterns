package modulo09.estudodecaso.view;
//�  Classe Lab09Sistema 
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Map;

import modulo01.estudodecaso.util.Console;
import modulo07.MyClassException;
import modulo09.estudodecaso.controller.GerenteContaCorrente;
import modulo09.estudodecaso.model.conta.ContaCorrenteVO;
import modulo09.estudodecaso.model.conta.ContaLimiteVO;
import modulo09.estudodecaso.model.historico.ContaHistoricoVO;
public class Lab09Sistema {
	// iremos criar do tipo da subclasse para n�o gerar erro quando for necess�rio ler o limite
	ContaCorrenteVO vo = new ContaLimiteVO();
	private void leValores() throws MyClassException {
			do {
				this.vo.setAgencia(Console.readInt("Numero da Agencia : "));
			} while (this.vo.getAgencia() <= 0);
			validaNumAge(this.vo.getAgencia());
			do {
				this.vo.setConta(Console.readInt("Numero da Conta   : "));
			} while (this.vo.getConta() <= 0);
			validaNumConta(this.vo.getConta());
			do {
				this.vo.setSaldo(Console.readDouble("Valor             : "));
			} while (this.vo.getSaldo() <= 0.0);
	}
		private void leValoresAgCta() {
			do {
				this.vo.setAgencia(Console.readInt("Numero da Agencia : "));
			} while (this.vo.getAgencia() <= 0);
			do {
				this.vo.setConta(Console.readInt("Numero da Conta   : "));
			} while (this.vo.getConta() <= 0);
		}
		public static void main(String[] args) {
			Lab09Sistema obj = new Lab09Sistema();
			obj.execMenu();
		}
		public void execMenu() {
			int iOpcao = 0;
			while (iOpcao != 9) {
				try {
					System.out.println("1 - Cadastramento");
					System.out.println("2 - Saque");
					System.out.println("3 - Dep�sito");
					System.out.println("4 - Consulta");
					System.out.println("5 - Extrato");
					System.out.println("6 - Atualiza Saldo");
					System.out.println("8 - Remover Conta Corrente");
					System.out.println("9 - Fim \n");
					iOpcao = Console.readInt("Entre com Op��o:");
					switch (iOpcao) {
					case 1: {
						execCadastramento();
						break;
					}
					case 2: {
						execSaque();
						break;
					}
					case 3: {
						execDeposito();
						break;
					}
					case 4: {
						Map<String, ContaCorrenteVO> impressaoMapa = execConsulta();
						apresentarConsulta(impressaoMapa);
						break;
					}
					case 5: {
						Map<String, ContaCorrenteVO> impressaoMapa = execExtrato();
						apresentarConsulta(impressaoMapa);
						apresentarExtrato(impressaoMapa);
						break;
					}
					case 6: {
						execAtualizarSaldo();
						break;
					}
					case 8: {
						execRemoverContaCorrente();
						break;
					}
					case 9: {
						System.exit(0);
						break;
					}
					default:
						System.out.println();
						Console.readChar("Op��o Inv�lida. Tecle <ENTER>.");
						System.out.println();
						break;
					}
				} catch (MyClassException e) {
					System.out.println("Classe .............: " + e.getClasse());
					System.out.println("Mensagem do Objeto .: " + e.getMessage());
					System.out.println("Mensagem de Neg�cio.: " + e.getMensagem());
					System.out.println("Pacote .............: " + e.getPacote());
					System.out.println("M�todo .............: " + e.getMetodo());
				}
			}
	}
	public void execCadastramento() throws MyClassException {
			char opcao;
			do {
				this.vo.setNome(Console.readString("Nome do Cliente   : "));
			} while (this.vo.getNome().equals(""));
			leValores();
			if (this.vo.getAgencia() > 5000) {			
				do {
					((ContaLimiteVO)this.vo).setLimite(Console.readDouble("Limite      : "));
				} while (((ContaLimiteVO)this.vo).getLimite() <= 0.0);	
			}
			opcao = Console.readChar("Confirma cadastramento (S/N) : ");
			if (Character.toLowerCase(opcao) != 's')
				return;
			GerenteContaCorrente.execCadastramento(this.vo);
		}
	public void execSaque() throws MyClassException {
				char opcao;
				leValores();
				opcao = Console.readChar("Confirma saque (S/N) : ");
				if (Character.toLowerCase(opcao) != 's')
					return;
				GerenteContaCorrente.execSaque(this.vo);
		}
		public void execDeposito() throws MyClassException {
			char opcao;
			leValores();
			opcao = Console.readChar("Confirma deposito (S/N) : ");
			if (Character.toLowerCase(opcao) != 's')
				return;
			GerenteContaCorrente.execDeposito(this.vo);
		}
		public Map<String, ContaCorrenteVO> execExtrato() throws MyClassException {
			leValoresAgCta();
			return GerenteContaCorrente.execExtrato(this.vo);
	}
	public Map<String, ContaCorrenteVO> execConsulta() throws MyClassException {
			leValoresAgCta();
			return GerenteContaCorrente.execConsulta(this.vo);
	}
	public void execAtualizarSaldo() throws MyClassException {
			leValoresAgCta();
			GerenteContaCorrente.execAtualizarSaldo(this.vo);
	}
	private void apresentarConsulta(Map<String, ContaCorrenteVO> mapa) {
			ContaCorrenteVO vo = mapa.get("contacorrente");
			NumberFormat formatter;
			formatter = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
			formatter.setMinimumFractionDigits(2);
			System.out.println("\n");
			System.out.println("----------------------------------------");
			System.out.println("          Situa��o da Conta");
			System.out.println("----------------------------------------");
			System.out.println("Agencia :" + vo.getAgencia());
			System.out.println("Conta   :" + vo.getConta());
			System.out.println("Nome    :" + vo.getNome());
			System.out.println("Saldo   :" + formatter.format(vo.getSaldo()));
			System.out.println("Limite   :"	+ formatter.format(((ContaLimiteVO) vo).getLimite()));
			System.out.println("----------------------------------------");
		}
		private void apresentarExtrato(Map<String, ContaCorrenteVO> mapa) {
			ContaCorrenteVO voContaHist = mapa.get("contacorrentehistorico");
			int dia, mes, ano, hora, min, seg;
			double dValor;
			int codHist, i, j;
			String tLinha;
			String[] strSplit;
			NumberFormat formatter, formint, formataux;
			formatter = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
			formatter.setMinimumFractionDigits(2);
			formint = NumberFormat.getInstance();
			formint.setMinimumIntegerDigits(2);
			if (!((ContaHistoricoVO) voContaHist).getVetOperacoes().isEmpty()) {
				j = ((ContaHistoricoVO) voContaHist).getVetOperacoes().size();
				for (i = 0; i < j; i++) {
					tLinha = (String) ((ContaHistoricoVO) voContaHist)
							.getVetOperacoes().get(i);
					strSplit = null;
					strSplit = tLinha.split(" ");
					// primeira alternativa de formata��o
					dia = (Integer.parseInt(strSplit[2]));
					mes = (Integer.parseInt(strSplit[3]) + 1);
					ano = (Integer.parseInt(strSplit[4]));
					hora = (Integer.parseInt(strSplit[5]));
					min = (Integer.parseInt(strSplit[6]));
					seg = (Integer.parseInt(strSplit[7]));
					codHist = Integer.parseInt(strSplit[8]);
					dValor = Double.parseDouble(strSplit[9]);
					StringBuffer sb = new StringBuffer();
					formataux = new DecimalFormat("0000");
					sb.append(String.valueOf(formataux.format(voContaHist	.getAgencia())));
					sb.append(" ");
					formataux = new DecimalFormat("0000000");
					sb.append(String.valueOf(formataux.format(voContaHist	.getConta())));
					sb.append(" ");
					formataux = new DecimalFormat("00");
					sb.append(String.valueOf(formataux.format(dia)));
					sb.append("/");
					sb.append(String.valueOf(formataux.format(mes)));
					sb.append("/");
					formataux = new DecimalFormat("0000");
					sb.append(String.valueOf(formataux.format(ano)));
					sb.append(" - ");
					formataux = new DecimalFormat("00");
					sb.append(String.valueOf(formataux.format(hora)));
					sb.append(":");
					sb.append(String.valueOf(formataux.format(min)));
					sb.append(":");
					sb.append(String.valueOf(formataux.format(seg)));
					sb.append(" - ");
					System.out.print(sb.toString());
					switch (codHist) {
					case 0:
						System.out.print("Deposito Inicial     ");
						break;
					case 1:
						System.out.print("Saque caixa          ");
						break;
					case 2:
						System.out.print("Deposito dinheiro    ");
						break;
					case 3:
						System.out.print("Atualiza��o de Saldo ");
						break;
					default:
						System.out.print("Transa��o            ");
						break;
					}
					formatter = NumberFormat.getCurrencyInstance(new Locale("pt","BR"));
					formatter.setMinimumFractionDigits(2);
					System.out.println(formatter.format(dValor));
				}
				System.out.println("----------------------------------------");
			}
		}
	public void execRemoverContaCorrente() throws MyClassException {
			leValoresAgCta();
			GerenteContaCorrente.execRemoverContaCorrente(this.vo);
		}
		private static void validaNumAge(int pAge) throws MyClassException {
			if (pAge <= 0 || pAge > 9999) {
				MyClassException myObj = new MyClassException("Numero da Agencia Invalida");
				myObj.setMetodo("validaNumAge");
				throw myObj;
			}
	}
	private static void validaNumConta(int pConta) throws MyClassException {
			if (pConta <= 0 || pConta > 9999999) {
				MyClassException myObj = new MyClassException("Numero da Conta Corrente Invalida");
				myObj.setMetodo("validaNumConta");
				throw myObj;
			}
	}
}
