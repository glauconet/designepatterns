package modulo09.estudodecaso.viewcommand;

//� interface Comando 
import modulo07.MyClassException;
public interface Comando {
	public void executar() throws MyClassException;
}
