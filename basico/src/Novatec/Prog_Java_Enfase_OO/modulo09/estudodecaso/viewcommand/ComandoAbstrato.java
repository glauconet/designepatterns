package modulo09.estudodecaso.viewcommand;

//� Classe abstrata ComandAbstrato
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Map;

import modulo01.estudodecaso.util.Console;
import modulo07.MyClassException;
import modulo09.estudodecaso.controller.GerenteContaCorrente;
import modulo09.estudodecaso.model.conta.ContaCorrenteVO;
import modulo09.estudodecaso.model.conta.ContaLimiteVO;
import modulo09.estudodecaso.model.historico.ContaHistoricoVO;
public abstract class ComandoAbstrato implements Comando {
	protected abstract String getNome();
	protected abstract void executarComando() throws MyClassException;
	protected abstract void lerDados();
	protected ContaCorrenteVO vo;
	public ComandoAbstrato() {}
	public void executar() throws MyClassException {
			ValidadorDeContaCorrente validador;
			System.out.println("--: " + getNome() + " :--");
			lerDados();
			if (confirmarOperacao()) {
				validador = getValidador();
				validador.validar();
				/*
				 * Implementamos aqui o conceito do padr�o de projeto template
				 */
				executarComando();
			} else {
				System.out.println("Operacao [" + getNome() + "] cancelada.");
			}
		}
	protected void apresentarConsulta(Map<String, ContaCorrenteVO> mapa) {
			ContaCorrenteVO vo = mapa.get("contacorrente");
			NumberFormat formatter;
			formatter = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
			formatter.setMinimumFractionDigits(2);
			System.out.println("\n");
			System.out.println("----------------------------------------");
			System.out.println("          Situa��o da Conta");
			System.out.println("----------------------------------------");
			System.out.println("Agencia :" + vo.getAgencia());
			System.out.println("Conta   :" + vo.getConta());
			System.out.println("Nome    :" + vo.getNome());
			System.out.println("Saldo   :" + formatter.format(vo.getSaldo()));
			if (vo instanceof ContaLimiteVO) {
				System.out.println("Limite   :"		+ formatter.format(((ContaLimiteVO) vo).getLimite()));
			}
			System.out.println("----------------------------------------");
		}
		protected void apresentarExtrato(Map<String, ContaCorrenteVO> mapa) {
			ContaCorrenteVO voContaHist = mapa.get("contacorrentehistorico");
			int dia, mes, ano, hora, min, seg;
			double dValor;
			int codHist, i, j;
			String tLinha;
			String[] strSplit;
			NumberFormat formatter, formint, formataux;
			formatter = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
			formatter.setMinimumFractionDigits(2);
			formint = NumberFormat.getInstance();
			formint.setMinimumIntegerDigits(2);
			if (!((ContaHistoricoVO) voContaHist).getVetOperacoes().isEmpty()) {
				j = ((ContaHistoricoVO) voContaHist).getVetOperacoes().size();
				for (i = 0; i < j; i++) {
					tLinha = (String) ((ContaHistoricoVO) voContaHist)
							.getVetOperacoes().get(i);
					strSplit = null;
					strSplit = tLinha.split(" ");
					// primeira alternativa de formata��o
					dia = (Integer.parseInt(strSplit[2]));
					mes = (Integer.parseInt(strSplit[3]) + 1);
					ano = (Integer.parseInt(strSplit[4]));
					hora = (Integer.parseInt(strSplit[5]));
					min = (Integer.parseInt(strSplit[6]));
					seg = (Integer.parseInt(strSplit[7]));
					codHist = Integer.parseInt(strSplit[8]);
					dValor = Double.parseDouble(strSplit[9]);
					StringBuffer sb = new StringBuffer();
					formataux = new DecimalFormat("0000");
					sb.append(String.valueOf(formataux.format(voContaHist.getAgencia())));
					sb.append(" ");
					formataux = new DecimalFormat("0000000");
					sb.append(String.valueOf(formataux.format(voContaHist.getConta())));
					sb.append(" ");
					formataux = new DecimalFormat("00");
					sb.append(String.valueOf(formataux.format(dia)));
					sb.append("/");
					sb.append(String.valueOf(formataux.format(mes)));
					sb.append("/");
					formataux = new DecimalFormat("0000");
					sb.append(String.valueOf(formataux.format(ano)));
					sb.append(" - ");
					formataux = new DecimalFormat("00");
					sb.append(String.valueOf(formataux.format(hora)));
					sb.append(":");
					sb.append(String.valueOf(formataux.format(min)));
					sb.append(":");
					sb.append(String.valueOf(formataux.format(seg)));
					sb.append(" - ");
					System.out.print(sb.toString());
					switch (codHist) {
					case 0:
						System.out.print("Deposito Inicial     ");
						break;
					case 1:
						System.out.print("Saque caixa          ");
						break;
					case 2:
						System.out.print("Deposito dinheiro    ");
						break;
					case 3:
						System.out.print("Atualiza��o de Saldo ");
						break;
					default:
						System.out.print("Transa��o            ");
						break;
					}
					formatter = NumberFormat.getCurrencyInstance(new Locale("pt","BR"));
					formatter.setMinimumFractionDigits(2);
					System.out.println(formatter.format(dValor));
				}
				System.out.println("----------------------------------------");
			}
	}
	protected void lerAgenciaConta() {
			int agencia = Console.readInt("N�mero da Ag�ncia : ");
			if (GerenteContaCorrente.isContaEspecial(agencia))
				this.vo = new ContaLimiteVO();
			else
				this.vo = new ContaCorrenteVO();
			this.vo.setAgencia(agencia);
				this.vo.setConta(Console.readInt("N�mero da Conta   : "));
		}
		protected void lerCliente() {
			this.vo.setNome(Console.readString("Nome do Cliente   : "));
		}
		protected void lerSaldo() {
			this.vo.setSaldo(Console.readDouble("Valor : "));
		}
	protected void lerLimite() {
			((ContaLimiteVO) this.vo).setLimite(Console.readInt("Limite            : "));
	}
	private boolean confirmarOperacao() throws MyClassException {
			System.out.println();
			char simNao = Console.readChar("Confirma " + getNome() + " (S/N) : ");
			if (simNao == 'S' || simNao == 's')
				return true;
			if (simNao == 'N' || simNao == 'n')
				return false;
			throw new MyClassException("Op��o inv�lida!");
		}
		protected ValidadorDeContaCorrente getValidador() {
			return new ValidadorDeContaCorrente(this.vo);
		}
}
