package modulo09.estudodecaso.viewcommand;

import modulo07.MyClassException;
import modulo09.estudodecaso.controller.GerenteContaCorrente;
public class ComandoCadastramento extends ComandoAbstrato {
	public ComandoCadastramento() throws MyClassException {
		super();
	}
	@Override
	protected String getNome() {
		return "Cadastramento";
	}
	@Override
	protected void lerDados() {
		lerAgenciaConta();
		lerCliente();
		lerSaldo();
		if (GerenteContaCorrente.isContaEspecial(this.vo.getAgencia()))
			lerLimite();
	}
	@Override
	protected void executarComando() throws MyClassException {
		GerenteContaCorrente.execCadastramento(this.vo);
	}
}