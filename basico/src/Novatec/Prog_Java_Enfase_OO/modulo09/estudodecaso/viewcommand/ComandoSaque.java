package modulo09.estudodecaso.viewcommand;
//� Classe ComandoSaque. Op��o do menu 
import modulo07.MyClassException;
import modulo09.estudodecaso.controller.GerenteContaCorrente;
public class ComandoSaque extends ComandoAbstrato {
	private double valor ;
	public double getValor() {
			return this.valor;
	}
	public void setValor(double valor) {
			this.valor = valor;
	}
	public ComandoSaque() throws MyClassException {
			super();
	}
	@Override
	protected String getNome() {
			return "Saque";
	}
	@Override
	protected void lerDados() {
			lerAgenciaConta();
			lerSaldo();
	}
	@Override
	protected void executarComando() throws MyClassException {
			GerenteContaCorrente.execSaque(this.vo);
			System.out.println("Saque realizado.");
		}
}
