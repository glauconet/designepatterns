package modulo09.estudodecaso.viewcommand;
// Classe Lab09SistemaCommand 
import java.util.Date;
// A classe Console precisa estar dispon�vel no pacote modulo01.estudodecaso.util
import modulo01.estudodecaso.util.Console;
// A classe MyClassException deve ter sido criada no pacote utilizado no comando import
import modulo07.MyClassException;
public class Lab09SistemaCommand {
	/*	define as op��es do menu dispon�veis para o usu�rio. Cada um destes elementos se 
			refere 	a uma classe concreta. Ex.: ComandoSaque. */
	private String[] comandos = { "Cadastramento", "Saque", "Deposito", "Consulta", "Extrato", "CalcularJuros" };
	public static void main(String[] args) {
			System.out.println("In�cio: " + new Date());
			final Lab09SistemaCommand sistema = new Lab09SistemaCommand();
			// apresenta o menu com as op��es para o usu�rio realizar uma escolha
			sistema.execMenu();
			System.out.println("Fim: " + new Date());
		}
		public void execMenu() {
			byte opcao = 0;
			while (true) {
				System.out.println();
				System.out.println("1 - Cadastramento");
				System.out.println("2 - Saque");
				System.out.println("3 - Dep�sito");
				System.out.println("4 - Consulta");
				System.out.println("5 - Extrato");
				System.out.println("6 - Atualizar saldo");
				System.out.println("9 - Fim");
				System.out.println();
				opcao = Console.readByte("Op��o: ");
				if (opcao == 9)
					break;
				try {
					// verifica se a op��o escolhida � v�lida
					if (opcao >= 1 && opcao <= this.comandos.length) {
						ComandoFactory fabrica = ComandoFactory.getInstance();
						// obt�m um objeto referente a op��o do menu escolhida
						Comando comando = fabrica.criarComando(this.comandos[opcao - 1]);
						/*	executa o m�todo executar utilizando o objeto retornado pelo m�todo 
							criarComando(). O m�todo executar pertence as classes que representam 
							os comandos. */
						comando.executar();
					} else
						// lan�a uma exce��o indicando que a op��o escolhida � inv�lida
						throw new MyClassException("Op��o inv�lida! ");
				} catch (MyClassException e) {
					System.out.println("Classe:				" + e.getClasse());
					System.out.println("Mensagem do objeto:	" + e.getMessage());
					System.out.println("Mensagem de neg�cio:	" + e.getMensagem());
					System.out.println("Pacote:				" + e.getPacote());
					System.out.println("M�todo:				" + e.getMetodo());
				}
				System.out.println();
			}
		}
}

