package modulo09.factoryContaCorrente;
//� Interface ContaCorrenteDAO 
import java.util.List;
public interface ContaCorrenteDAO {
	void insere(ContaCorrenteVO vo);
	List <ContaCorrenteDAO> recuperarContasByAgencia(int agencia);
	ContaCorrenteVO recuperarConta(int agencia, int conta);
}

