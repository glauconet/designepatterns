package modulo09.factoryContaCorrente;
//� Classe ContaCorrenteVO
public class ContaCorrenteVO {
	private int agencia;
	private int conta;
	private double saldo;
	public int getAgencia() {
			return this.agencia;
	}
	public void setAgencia(int agencia) {
			this.agencia = agencia;
	}
	public int getConta() {
			return this.conta;
	}
	public void setConta(int conta) {
			this.conta = conta;
	}
	public double getSaldo() {
			return this.saldo;
	}
	public void setSaldo(double saldo) {
			this.saldo = saldo;
	}
}
