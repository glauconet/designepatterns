package modulo09.factorySport;
//� Classe FormulaTruck � Implementa o tema 
public class FormulaTruck implements SportRadical {
	public void cadastrarPiloto() {
			System.out.println("Classe FormulaTruck - Cadastrar piloto");
	}
	public void imprimirCalendario() {
			System.out.println("Classe FormulaTruck - Todas as provas ser�o em Fevereiro");
	}
	public void imprimirResultadoProva() {
			System.out.println("Classe FormulaTruck - Imprimindo resultado da prova");
	}
	public void cadastrarCalendario() {
			System.out.println("Classe FormulaTruck - Cadastrar Calendario");
	}
	public void cadastrarProva() {
			System.out.println("Classe FormulaTruck - Cadastrar prova");
	}
}

