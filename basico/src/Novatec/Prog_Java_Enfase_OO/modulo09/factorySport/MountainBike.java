package modulo09.factorySport;
//� Classe MountainBike  � Implementa o tema 
public class MountainBike implements SportRadical {
	public void cadastrarPiloto() {
			System.out.println("Classe MountainBike - Cadastrar piloto");
	}
	public void imprimirCalendario() {
			System.out.println("Classe MountainBike - Todas as provas ser�o em Fevereiro");
	}
	public void imprimirResultadoProva() {
			System.out.println("Classe MountainBike - Imprimindo resultado da prova");
	}
	public void cadastrarCalendario() {
			System.out.println("Classe MountainBike - Cadastrar Calendario");
	}
	public void cadastrarProva() {
			System.out.println("Classe MountainBike - Cadastrar prova");
	}
}

