package modulo09.factorySport;
// Classe PrincipalSport � Implementa o m�todo main
import java.util.Scanner;
// A classe MyClassException deve ter sido criada no pacote utilizado no comando import
import modulo07.MyClassException;

public class PrincipalSport {
	private SportRadical veiculo = null;

	public static void main(String[] args) {
		(new PrincipalSport()).execMenuPrincipal();
	}

	private void execMenuPrincipal() {
		int iTipo = 0;
		while (iTipo != 9) {
			System.out
					.println("********** Estilos para Esportes Radicais **********");
			System.out.println("1 - Autom�vel");
			System.out.println("2 - Moto");
			System.out.println("3 - Bicicleta");
			System.out.println("9 - Fim \n");
			Scanner scan = new Scanner(System.in);
			System.out.print("Escolha a montadora: ");
			iTipo = scan.nextInt();
			try {
				switch (iTipo) {
				case 1: {
					execSubMenuAutomovel();
					break;
				}

				case 2: {
					execSubMenuMoto();
					break;
				}
				case 3: {
					execSubMenuBicicleta();
					break;
				}
				case 9: {
					System.exit(0);
					break;
				}
				default:
					System.out.println();
					System.out.println("Op��o inv�lida.");
					break;
				}
				execMenuVeiculo();
			} catch (MyClassException e) {
				System.out.println("Mensagem: " + e.getMessage());
			}
		}
	}

	private void execMenuVeiculo() {
		System.out.println("1 - Cadastrar piloto ");
		System.out.println("2 - Cadastrar prova ");
		System.out.println("3 - Cadastrar calend�rio ");
		System.out.println("4 - Imprimir resultados");
		System.out.println("5 - Imprimir calend�rio ");
		System.out.print("Escolha uma opera��o: ");
		Scanner scan = new Scanner(System.in);
		int iTipo = scan.nextInt();
		switch (iTipo) {
		case 1: {
			this.veiculo.cadastrarPiloto();
			break;
		}
		case 2: {
			this.veiculo.cadastrarProva();
			break;
		}
		case 3: {
			this.veiculo.cadastrarCalendario();
			break;
		}
		case 4: {
			this.veiculo.imprimirResultadoProva();
			break;
		}
		case 5: {
			this.veiculo.imprimirCalendario();
			break;
		}

		default:
			System.out.println();
			System.out.println("Op��o inv�lida. Tecle <ENTER>.");
			break;
		}
	}

	private void execSubMenuBicicleta() throws MyClassException {
		System.out.println("1 - Bicicross");
		System.out.println("2 - MountainBike");
		Scanner scan = new Scanner(System.in);
		System.out.print("Escolha um ve�culo: ");
		int iTipo = scan.nextInt();
		switch (iTipo) {
		case 1: {
			// retorna um objeto do tipo da classe Bicicross
			this.veiculo = FactoryVeiculo.getVeiculoRadical("Bicicross");
			break;
		}
		case 2: {
			// retorna um objeto do tipo da classe MountainBike
			this.veiculo = FactoryVeiculo.getVeiculoRadical("MountainBike");
			break;
		}
		default:
			System.out.println();
			System.out.println("Op��o inv�lida. Tecle <ENTER>.");
			break;
		}
	}

	private void execSubMenuMoto() throws MyClassException {
		System.out.println("1 - MotoCross");
		Scanner scan = new Scanner(System.in);
		System.out.print("Escolha um ve�culo: ");
		int iTipo = scan.nextInt();
		switch (iTipo) {
		case 1: {
			// Retorna um objeto do tipo da classe MotoCross
			this.veiculo = FactoryVeiculo.getVeiculoRadical("MotoCross");
			break;
		}
		default:
			System.out.println();
			System.out.println("Op��o inv�lida.");
			break;
		}
	}

	private void execSubMenuAutomovel() throws MyClassException {
		System.out.println("1 - StockCar");
		System.out.println("2 - FormulaTruck");
		Scanner scan = new Scanner(System.in);
		System.out.print("Escolha um ve�culo: ");
		int iTipo = scan.nextInt();
		switch (iTipo) {

		case 1: {
			// Retorna um objeto do tipo da classe StockCar
			this.veiculo = FactoryVeiculo.getVeiculoRadical("StockCar");
			break;
		}
		case 2: {
			// Retorna um objeto do tipo da classe FormulaTruck
			this.veiculo = FactoryVeiculo.getVeiculoRadical("FormulaTruck");
			break;
		}
		default:
			System.out.println();
			System.out.println("Op��o inv�lida.");
			break;
		}
	}
}