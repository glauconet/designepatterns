package modulo09.factorycarro;

// Classe AutomovelFactory
public class AutomovelFactory {
	public static ExemploAutomovel getAutomovel(String tipoCarro) {
		final String valor[] = { "Corsa", "Celta", "Polo", "CrossFox" };
		if (tipoCarro == null)
			return null;
		else if (tipoCarro.equals(valor[0]))
			return new ExemploCorsa();
		else if (tipoCarro.equals(valor[1]))
			return new ExemploCelta();
		else if (tipoCarro.equals(valor[2]))
			return new ExemploPolo();

		else if (tipoCarro.equals(valor[3]))
			return new ExemploCrossFox();
		else
			return null;
	}
}