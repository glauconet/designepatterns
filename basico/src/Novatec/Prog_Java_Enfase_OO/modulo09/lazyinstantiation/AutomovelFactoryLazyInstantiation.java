package modulo09.lazyinstantiation;

import java.util.HashMap;
import java.util.Map;

import modulo09.factorycarro.ExemploAutomovel;
import modulo09.factorycarro.ExemploCelta;
import modulo09.factorycarro.ExemploCorsa;
import modulo09.factorycarro.ExemploCrossFox;
import modulo09.factorycarro.ExemploPolo;

public class AutomovelFactoryLazyInstantiation {
	// Construtor privado
	private AutomovelFactoryLazyInstantiation() {
		super();
	}

	// atributo do tipo mapa est�tico
	/**
	 * @uml.property   name="mapaFabrica"
	 * @uml.associationEnd   qualifier="key:java.lang.Object modulo09.factorycarro.ExemploAutomovel"
	 */
	private static Map<String, ExemploAutomovel> mapaFabrica = new HashMap<String, ExemploAutomovel>();

	// M�todo que retorna um e somente um objeto
	public static ExemploAutomovel getAutomovel(String tipoCarro) {

		if (tipoCarro == null)
			return null;

		/*
		 * Verifica no mapa se j� existe um objeto para o carro solicitado. Precisamos de uma mapa 
		 * devido a termos esta f�brica criando diferentes objetos. Por isto n�o podemos perguntar 
		 * para um tipo especifico. Ao inv�s disto consultamos o mapa.
		 */
		ExemploAutomovel auto = mapaFabrica.get(tipoCarro);
		if (auto == null) {
			if (tipoCarro.equals("Celta"))
				auto = new ExemploCelta();
			else if (tipoCarro.equals("Corsa"))
				auto = new ExemploCorsa();
			else if (tipoCarro.equals("Polo"))
				auto = new ExemploPolo();
			else if (tipoCarro.equals("CrossFox"))
				auto = new ExemploCrossFox();
			else
				return null;
			/*
			 * Ser� impresso na tela somente quando o tipo do carro ainda n�o estiver sido escolhido.
			 */
			System.out.println("\n\n *** Criando objeto *** \n\n");
			mapaFabrica.put(tipoCarro, auto);
		}
		return auto;
	}
}
