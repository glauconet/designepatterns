package modulo09.lazyinstantiation;

import java.util.Scanner;

import modulo09.factorycarro.ExemploAutomovel;

public class PrincipalLazyInstantiation {

	public void execMenuPrincipal() {
		int iTipo = 0;

		while (iTipo != 9) {
			ExemploAutomovel auto = null;
			System.out.println("1 - Corsa");
			System.out.println("2 - Celta");
			System.out.println("3 - Polo");
			System.out.println("4 - CrossFox");
			System.out.println("9 - Fim \n");
			Scanner scan = new Scanner(System.in);
			System.out.print("Escolha o carro e analise seu pre�o: ");
			iTipo = scan.nextInt();
			switch (iTipo) {
			case 1: {
				// retorna um objeto do tipo da classe ExemploCorsa
				auto = AutomovelFactoryLazyInstantiation.getAutomovel("Corsa");
				break;
			}
			case 2: {
				// retorna um objeto do tipo da classe ExemploCelta
				auto = AutomovelFactoryLazyInstantiation.getAutomovel("Celta");
				break;
			}
			case 3: {
				//retorna um objeto do tipo da classe ExemploPolo
				auto = AutomovelFactoryLazyInstantiation.getAutomovel("Polo");
				break;
			}
			case 4: {
				// retorna um objeto do tipo da classe ExemploCrossFox
				auto = AutomovelFactoryLazyInstantiation.getAutomovel("CrossFox");
				break;
			}

			case 9: {
				break;
			}
			default:
				System.out.println();
				System.out.println("Op��o Inv�lida. Tecle <ENTER>.");
				break;
			}
			if (auto != null) {
				System.out.println("Pre�o do carro: " + auto.getPreco());
			}
		}
	}

	public static void main(String[] args) {
		(new PrincipalLazyInstantiation()).execMenuPrincipal();

	}
}