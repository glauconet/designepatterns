package modulo09.singleton;
//� Classe ExemploSingleton 
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
public class ExemploSingleton {
	private static ExemploSingleton instance = null;
	private Properties prop = null;
	private FileInputStream fis = null;
	private URL recurso = null;
	private ExemploSingleton() {
			super();
			System.out.println("Criando objeto");
			this.recurso = criarRecurso();
	}
	public void imprimirRecurso(String recurso) {
			System.out.println(this.prop.getProperty(recurso));
	}
	private URL criarRecurso() {
			this.recurso = getClass().getResource("singleton.properties");
			this.prop = new Properties();
			try {
				this.fis = new FileInputStream(this.recurso.getFile());
				this.prop.load(this.fis);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return this.recurso;
	}
	public static ExemploSingleton getInstance() {
			if (instance == null) {
				instance = new ExemploSingleton();
			}
			return instance;
	}
}
