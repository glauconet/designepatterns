import javax.swing.JOptionPane;

public class BemVindo
   {
   public static void main(String[] args) 
      {
      JOptionPane.showMessageDialog(null,"Bem-vindo ao Java!");
      System.exit(0);
      }
   }

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/