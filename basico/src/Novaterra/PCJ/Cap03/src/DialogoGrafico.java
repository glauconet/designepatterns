import javax.swing.JOptionPane;

public class DialogoGrafico {
  public static void main(String[] args) {
    String nm = JOptionPane.showInputDialog(null,"Informe seu nome");
    JOptionPane.showMessageDialog(null,nm + ": seja bem-vindo!");
    System.exit(0);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/