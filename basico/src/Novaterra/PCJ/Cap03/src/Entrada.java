import java.io.IOException;


public class Entrada {
  public static void main(String[] args) throws IOException {
    System.out.print("\nInforme seu nome: ");
    byte[] bt = new byte[50];
    System.in.read(bt);
    String nome = new String(bt).trim();
    System.out.printf("%s: seja bem-vindo!\n\n", nome);
    }
  }

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/