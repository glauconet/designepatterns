public class JogosPreferidos {
  public static void main(String[] args) {
    System.out.println();
    System.out.println("Meus jogos preferidos:");
    System.out.println("1. " + args[0]);
    System.out.println("2. " + args[1]);
    System.out.println("3. " + args[2]);
    System.out.println();
    }
  }

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/