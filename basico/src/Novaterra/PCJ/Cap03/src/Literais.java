public class Literais {
  public static void main(String[] args) {
    System.out.println();
    System.out.println("Inteiro - decimal:\t\t"       + 11);
    System.out.println("Inteiro - hexadecimal:\t\t"   + 0xB);
    System.out.println("Inteiro - octal:\t\t"         + 013);
    System.out.println("Inteiro - longo:\t\t"         + 11L);
    System.out.println("Real - precis�o simples:\t"   + 24.2f);
    System.out.println("Real - precis�o dupla:\t\t"   + 24.2);
    System.out.println("Tipo l�gico:\t\t\t"           + true);
    System.out.println("Caractere:\t\t\t"             + 'H');
    System.out.println("Texto:\t\t\t\t"               + "Ana");
    System.out.println();
    }
  }

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/