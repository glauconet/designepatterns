public class SaidaFormatada {
  public static void main(String[] args) {
    System.out.println( );
    
    System.out.printf("Um caractere: %c",'A');
    System.out.println( );
    System.out.printf("Um texto: %s","Java em todos os lugares");
    System.out.println( );
    System.out.printf("Um n�mero inteiro: %d",123);
    System.out.println( );
    System.out.printf("Um n�mero decimal: %f",4.55);
    System.out.println( );
    System.out.printf("Um valor booleano: %b",true);
    System.out.println( );
    
    System.out.println( );
    }
  }

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/