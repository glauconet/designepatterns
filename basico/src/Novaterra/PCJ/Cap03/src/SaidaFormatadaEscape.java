public class SaidaFormatadaEscape {
  public static void main(String[] args) {
    System.out.println( );
    
    System.out.printf("Dois n�meros:\t\t%d e %d \n",2, 22);
    System.out.printf("O n�mero %d equivale � letra %c \n",65, 'A');
    
    System.out.println( );
    }
  }

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/