import java.util.Scanner;

public class ConversaoNumeroCaractere {
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    
    System.out.print("\nInforme um n�mero inteiro:\t");
    String texto = scan.nextLine();
    int numero = Integer.parseInt(texto);
    char caractere = (char)numero;
    
    System.out.println("Caractere correspondente:\t" + caractere);
    System.out.println();
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/