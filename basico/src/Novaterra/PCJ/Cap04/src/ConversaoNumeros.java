import javax.swing.JOptionPane;

public class ConversaoNumeros {
  public static void main(String[] args) {
    int it1 = 15635;
    long lg = it1;
    float fl = it1;
    short sh = (short)it1;
    
    double db = 24.75;
    int it2 = (int)db;
    int it3 = (int)Math.round(db);
    
    String st = "Conte�do das vari�veis:\n" +
      "it1 = " + it1 + "\n" + "lg = " + lg + "\n" + 
      "fl = " + fl + "\n" + "sh = " + sh + "\n\n" +
      "db = " + db + "\n" + "it2 = " + it2 + "\n" +
      "it3 = " + it3;
    
    JOptionPane.showMessageDialog(null,st);
    System.exit(0);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/