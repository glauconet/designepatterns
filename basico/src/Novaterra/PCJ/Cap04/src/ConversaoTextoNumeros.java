import java.util.Scanner;

public class ConversaoTextoNumeros {
  public static void main(String[] args) {
    System.out.print("\nInforme um n�mero: ");
    Scanner scan = new Scanner(System.in);
    String texto = scan.nextLine();
    
    double db = Double.parseDouble(texto);
    float fl = Float.parseFloat(texto);
    long lg = Long.parseLong(texto);
    int it = Integer.parseInt(texto);
    short sh = Short.parseShort(texto);
    byte bt = Byte.parseByte(texto);
    char ch = (char)Integer.parseInt(texto);
    
    System.out.printf("\nConte�do das vari�veis:" +
      "\n db = \t %f \n fl = \t %f \n lg = \t %d \n it = \t %d" +
      "\n sh = \t %d \n bt = \t %d \n ch = \t %c \n\n",
      db,fl,lg,it,sh,bt,ch);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/