public class NumerosInteiros {
  public static void main(String[] args) {
    byte bt;
    short sh;
    int it;
    long lg;
    
    bt = 127;
    sh = 32767;
    it = 2147483647;
    lg = 9223372036854775807L;
    
    System.out.println("\nLimite superior:");
    System.out.println("byte:\t" + bt);
    System.out.println("short:\t" + sh);
    System.out.println("int:\t" + it);
    System.out.println("long:\t" + lg);
    
    bt = -128;
    sh = -32768;
    it = -2147483648;
    lg = -9223372036854775808L;
    
    System.out.println("\nLimite inferior:");
    System.out.println("byte:\t" + bt);
    System.out.println("short:\t" + sh);
    System.out.println("int:\t" + it);
    System.out.println("long:\t" + lg);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/