import java.io.PrintStream;
import java.util.Scanner;

public class OperadoresAritmeticos {
  public static void main(String[] args) {
    PrintStream saida = System.out;
    Scanner scan = new Scanner(System.in);
    
    saida.print("\nInforme um n�mero:\t");
    double n1 = scan.nextDouble();
    
    saida.print("Informe outro n�mero:\t");
    double n2 = scan.nextDouble();
    
    saida.println("\nOpera��es aritm�ticas b�sicas:");
    saida.printf("Soma (n1 + n2):\t\t\t%f",n1 + n2);
    saida.printf("\nSubtra��o (n1 - n2):\t\t%f",n1 - n2);
    saida.printf("\nMultiplica��o (n1 * n2):\t%f",n1 * n2);
    saida.printf("\nDivis�o (n1 / n2):\t\t%f",n1 / n2);
    saida.printf("\nM�dulo (n1 %% n2):\t\t%f",n1 % n2);
    
    saida.println("\n\nIncremento e decremento:");
    saida.println("Incremento de n1: " + ++n1);
    saida.println("Decremento de n2: " + --n2);
    
    saida.println("\nOpera��es de atribui��o:");
    saida.printf("Aditiva (n1 += 2):\t\t%f",n1 += 2);
    saida.printf("\nSubtrativa (n1 -= 3):\t\t%f",n1 -= 3);
    saida.printf("\nDe multiplica��o (n1 *= 2):\t%f",n1 *= 2);
    saida.printf("\nDe divis�o (n1 /= 2):\t\t%f",n1 /= 2);
    saida.printf("\nDe m�dulo (n1 %%= 2):\t\t%f\n\n",n1 %= 2);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/