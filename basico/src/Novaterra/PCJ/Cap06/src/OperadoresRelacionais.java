import java.io.PrintStream;
import java.util.Scanner;

public class OperadoresRelacionais {
  public static void main(String[] args) {
    PrintStream saida = System.out;
    Scanner scan = new Scanner(System.in);
    
    saida.print("\nInforme um n�mero:\t");
    int n1 = scan.nextInt();
    
    saida.print("Informe outro n�mero:\t");
    int n2 = scan.nextInt();
    
    saida.println("\nCompara��es:");
    saida.println("n1 == n2:\t" + (n1 == n2));
    saida.println("n1 != n2:\t" + (n1 != n2));
    saida.println("n1 > n2:\t"  + (n1 > n2));
    saida.println("n1 < n2:\t"  + (n1 < n2));
    saida.println("n1 >= n2:\t" + (n1 >= n2));
    saida.println("n1 <= n2:\t" + (n1 <= n2) + "\n\n");
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/