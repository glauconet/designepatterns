import javax.swing.JOptionPane;

public class Interrupcao {
  public static void main(String[] args) {
    String st = "Informe seu nome: ";
    st = JOptionPane.showInputDialog(null,st);
    
    if (st == null) System.exit(0);
    if (st.length() == 0) System.exit(0);
       
    st = "Nome informado: " + st;
    JOptionPane.showMessageDialog(null,st,"Mensagem",1);
    System.exit(0);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/