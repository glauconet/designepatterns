public class DoWhile {
  public static void main(String[] args) {
    System.out.println("Primeiro do-while:");
    byte numero = 9;
    do System.out.println(++numero);
    while (numero < 5);
    
    System.out.println("");
    
    System.out.println("Segundo do-while:");
    numero = 69;
    do {
      String st = "O n�mero " + numero + " equivale ao caractere ";
      st = st + (char)numero;
      System.out.println(st);
      numero--;
    }
    while (numero >= 65);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/