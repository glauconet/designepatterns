public class For {
  public static void main(String[] args) {
    System.out.print("Primeiro la�o:\t");
    for (byte num = 1; num <= 5; num++)
      System.out.print(num + " ");
    
    System.out.print("\nSegundo la�o:\t");
    for(byte num = 5;num >= 1; num--)
      System.out.print(num + " ");
    
    System.out.println();
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/