import javax.swing.JOptionPane;

public class Tarefas {
  public static void main(String[] args) {
    String tarefas = "Lista de tarefas:";
    for (int numero = 1; numero <= 10; numero++) {
      String str = "Tarefa n�mero " + numero;
      str = JOptionPane.showInputDialog(null,str);
      if (str == null) break;
      str = str.trim();
      tarefas += "\nTarefa " + numero + ": " + str;
    }
    
    JOptionPane.showMessageDialog(null,tarefas);
    System.exit(0);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/