public class While {
  public static void main(String[] args) {
    System.out.println("Primeiro while:");
    byte bt = 0;
    while (bt < 5)
      System.out.println(++bt);
    
    System.out.println("");
    
    System.out.println("Segundo while:");
    bt = 69;
    while (bt >= 65) {
      String st = "O n�mero " + bt + " equivale ao caractere ";
      st += (char)bt;
      System.out.println(st);
      bt--;
    }
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/