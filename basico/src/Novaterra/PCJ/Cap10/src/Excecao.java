import javax.swing.JOptionPane;

public class Excecao {
  public static void main(String[] args) {
    String str = "Informe um n�mero inteiro";
    str = JOptionPane.showInputDialog(null,str,"Informe",3);
       
    int numero = Integer.parseInt(str);
    int resultado = (int)Math.pow(numero,3);
    
    str = "O cubo de " + numero + " � " + resultado;
    JOptionPane.showMessageDialog(null,str,"Mensagem",1);
    System.exit(0);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/