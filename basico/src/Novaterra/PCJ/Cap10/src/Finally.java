import javax.swing.JOptionPane;

public class Finally {
  public static void main(String[] args){
    String str = "Informe um n�mero";
    str = JOptionPane.showInputDialog(null,str);
    if (str == null) System.exit(0);
       
    try {
      int numero = Integer.parseInt(str);
      JOptionPane.showMessageDialog(null,"N�mero v�lido!");
    }
    catch (NumberFormatException nfe) {
      JOptionPane.showMessageDialog(null,"Dado inv�lido!","Erro",0);
    }
    finally {
      JOptionPane.showMessageDialog(null,"Opera��o encerrada!");
      System.exit(0);
    }
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/