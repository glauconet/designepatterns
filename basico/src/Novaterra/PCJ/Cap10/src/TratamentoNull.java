import javax.swing.JOptionPane;

public class TratamentoNull {
  public static void main(String[] args) {
    String nome = JOptionPane.showInputDialog("Informe seu nome");
    if (nome == null) System.exit(0);
    nome = nome.toUpperCase();
    JOptionPane.showMessageDialog(null,"Seu nome: " + nome);
    System.exit(0);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/