public class Gato {
  String nome;
  char sexo;
  int idade;
  double peso;
  
  Gato(String nm, char sx, int id, double ps) {
    nome = nm;
    sexo = sx;
    idade = id;
    peso = ps;
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/