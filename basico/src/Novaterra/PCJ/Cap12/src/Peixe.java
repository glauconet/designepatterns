public class Peixe {
  String nomePopular;
  String nomeCientifico;
  String familia;
  String origem;
  int comprimento;
  
  Peixe() {
    nomePopular = "";
    nomeCientifico = "";
    familia = "";
    origem = "";
  }
  
  Peixe(String nomePopular, String nomeCientifico, 
    String familia, String origem, int comprimento) {
    this.nomePopular = nomePopular;
    this.nomeCientifico = nomeCientifico;
    this.familia = familia;
    this.origem = origem;
    this.comprimento = comprimento;
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/