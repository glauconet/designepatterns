public class Atleta {
  private String nome;
  private byte sexo;
  public static final byte SEXO_MASCULINO = 1;
  public static final byte SEXO_FEMININO = 2;
  
  public Atleta(String nome, byte sexo) {
    this.nome = nome;
    this.sexo = sexo;
  }
  
  public String toString() {
    String str = nome + " - ";
    if (sexo == SEXO_MASCULINO) str += "Masculino";
    if (sexo == SEXO_FEMININO) str += "Feminino";
    return str;
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/