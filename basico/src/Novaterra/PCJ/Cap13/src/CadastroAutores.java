import javax.swing.JOptionPane;

public class CadastroAutores {
  public static void main(String[] args) {
    Autor[] registros = new Autor[100];
    JOptionPane d = new JOptionPane();
    
    for (int indice = 0; indice < registros.length; indice++) {
      Autor aut = new Autor();
      String str;
      
      while (true) {
        try {
          str = d.showInputDialog("C�digo do autor");
          if (str == null) break;
          aut.setCodigo( str );
          break;
        }
        catch(Exception ex) {
          d.showMessageDialog(null,ex.getMessage(),"Erro",0);
        }
      }
      if (str == null) break;
      
      while (true) {
        try {
          str = d.showInputDialog("Nome do autor");
          if (str == null) break;
          aut.setNome( str );
          break;
        }
        catch(Exception ex) {
          d.showMessageDialog(null,ex.getMessage(),"Erro",0);
        }
      }
      if (str == null) break;
      
      registros[indice] = aut;
    }
    
    String relatorio = "Relat�rio de autores:";
    for (Autor aut : registros) {
      if (aut == null) break;
      relatorio += "\n" + aut.getCodigo() + " - " + aut.getNome();
    }
    
    d.showMessageDialog(null,relatorio);
    System.exit(0);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/