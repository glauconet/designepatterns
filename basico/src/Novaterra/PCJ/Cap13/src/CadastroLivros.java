import javax.swing.JOptionPane;

public class CadastroLivros {
  public static void main(String[] args) {
    Livro[] registros = new Livro[100];
    JOptionPane d = new JOptionPane();
    
    for (int indice = 0; indice < registros.length; indice++) {
      Livro liv = new Livro();
      String str;
      
      while (true) {
        try {
          str = d.showInputDialog("C�digo do livro");
          if (str == null) break;
          liv.setCodigo( Integer.parseInt(str) );
          break;
        }
        catch(NumberFormatException nfe) {
          d.showMessageDialog(null,"C�digo inv�lido!","Erro",0);
        }
        catch(IllegalArgumentException iae) {
          d.showMessageDialog(null,iae.getMessage(),"Erro",0);
        }
      }
      if (str == null) break;
      
      while (true) {
        try {
          str = d.showInputDialog("T�tulo do livro");
          if (str == null) break;
          liv.setTitulo( str );
          break;
        }
        catch(RuntimeException rte) {
          d.showMessageDialog(null,rte.getMessage(),"Erro",0);
        }
      }
      if (str == null) break;
      
      registros[indice] = liv;
    }
    
    String relatorio = "Relat�rio de livros:";
    for (Livro li : registros) {
      if (li == null) break;
      relatorio += "\n" + li.getCodigo() + " - " + li.getTitulo();
    }
    
    d.showMessageDialog(null,relatorio);
    System.exit(0);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/