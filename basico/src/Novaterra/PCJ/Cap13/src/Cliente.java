public class Cliente {
  private String nome;
  private boolean inadimplente;
  
  public Cliente(String nome, boolean inadimplente) {
    this.nome = nome;
    this.inadimplente = inadimplente;
  }
  
  public String getNome() {
    return nome;
  }
  
  public boolean isInadimplente( ) {
    return inadimplente;
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/