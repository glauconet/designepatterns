public class Doenca {
  private int codigo;
  private String descricao;
  private static int quantidade;
  
  public Doenca(int codigo, String descricao) {
    this.codigo = codigo;
    this.descricao = descricao;
    quantidade++;
  }
  
  public static int getQuantidade() {
    return quantidade;
  }
  
  public String toString() {
    return "Doen�a " + codigo + ": " + descricao;
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/