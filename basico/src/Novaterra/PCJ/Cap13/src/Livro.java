public class Livro {
  private int codigo;
  private String titulo;
  
  public Livro() {
    titulo = "";
  }
  
  public int getCodigo() {
    return codigo;
  }
  
  public String getTitulo() {
    return titulo;
  }
  
  public void setCodigo(int codigo) throws IllegalArgumentException{
    if (codigo < 1)
      throw new IllegalArgumentException("C�digo inv�lido!");
      
    this.codigo = codigo;
  }
  
  public void setTitulo(String titulo) 
    throws NullPointerException, IllegalArgumentException{
    if (titulo == null) 
      throw new NullPointerException("O t�tulo n�o pode ser nulo!");
      
    titulo = titulo.trim();
    
    if (titulo.length() < 5 || titulo.length() > 50)
      throw new IllegalArgumentException("T�tulo inv�lido!");
      
    this.titulo = titulo;
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/