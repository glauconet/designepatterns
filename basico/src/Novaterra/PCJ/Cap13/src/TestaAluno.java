import javax.swing.JOptionPane;

public class TestaAluno {
  public static void main(String[] args) {
    Aluno alu = new Aluno();
    
    String str = "Dados do aluno: ";
    //str += "\n" + alu.matricula + ": " + alu.nome;//Viola��o de encapsulamento privado
    JOptionPane.showMessageDialog(null,str);
    
    //alu.matricula = -15;//Viola��o de encapsulamento privado
    //alu.nome = "";//Viola��o de encapsulamento privado
    
    str = "Dados do aluno: ";
    //str += "\n" + alu.matricula + ": " + alu.nome;//Viola��o de encapsulamento privado
    JOptionPane.showMessageDialog(null,str);
    
    System.exit(0);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/