import javax.swing.JOptionPane;

public class TestaAtleta {
  public static void main(String[] args) {
    Atleta rodrigo = new Atleta("Rodrigo P.",Atleta.SEXO_MASCULINO);
    JOptionPane.showMessageDialog(null,rodrigo);
    
    Atleta mara = new Atleta("Mara A.",Atleta.SEXO_FEMININO);
    JOptionPane.showMessageDialog(null,mara);
    
    System.exit(0);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/