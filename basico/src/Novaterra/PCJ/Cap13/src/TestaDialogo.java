//import javax.swing.JOptionPane;

public class TestaDialogo {
  public static void main(String[] args) {
    String str = Dialogo.captar("Qual � seu nome?");
    if (str != null) Dialogo.exibir("Ol� " + str + "!");
    System.exit(0);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/