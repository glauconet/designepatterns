import javax.swing.JOptionPane;

public class TestaDisciplina {
  public static void main(String[] args) {
    Disciplina dis = new Disciplina();
    
    String str = "Dados da disciplina: ";
    str += "\n" + dis.getCodigo() + ": " + dis.getDescricao();
    JOptionPane.showMessageDialog(null,str);
    
    dis.setCodigo( 1 );
    dis.setDescricao( "Programa��o III" );
    
    str = "Dados da disciplina: ";
    str += "\n" + dis.getCodigo() + ": " + dis.getDescricao();
    JOptionPane.showMessageDialog(null,str);
    
    dis.setCodigo( -15 );
    dis.setDescricao( "Pro" );
    
    str = "Dados da disciplina: ";
    str += "\n" + dis.getCodigo() + ": " + dis.getDescricao();
    JOptionPane.showMessageDialog(null,str);
    
    System.exit(0);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/