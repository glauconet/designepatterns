import javax.swing.JOptionPane;

public class TestaDoenca {
  public static void main(String[] args) {
    Doenca aids = new Doenca(10,"AIDS");
    Doenca cancer = new Doenca(11,"C�ncer");
    
    JOptionPane.showMessageDialog(null,aids);
    JOptionPane.showMessageDialog(null,cancer);
    
    String str = "Doen�as cadastradas: " + Doenca.getQuantidade();
    JOptionPane.showMessageDialog(null, str);
    System.exit(0);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/