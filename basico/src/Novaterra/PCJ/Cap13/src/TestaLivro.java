import javax.swing.JOptionPane;

public class TestaLivro {
  public static void main(String[] args) {
    Livro liv = new Livro();
    
    String str = "Dados do livro: ";
    str += "\n" + liv.getCodigo() + ": " + liv.getTitulo();
    JOptionPane.showMessageDialog(null,str);
    
    liv.setCodigo( 1 );
    liv.setTitulo( "Java na Web" );
    
    str = "Dados do livro: ";
    str += "\n" + liv.getCodigo() + ": " + liv.getTitulo();
    JOptionPane.showMessageDialog(null,str);
    
    liv.setCodigo( -15 );
    liv.setTitulo( "Java" );
    
    str = "Dados do livro: ";
    str += "\n" + liv.getCodigo() + ": " + liv.getTitulo();
    JOptionPane.showMessageDialog(null,str);
    
    System.exit(0);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/