import javax.swing.JOptionPane;

public class TestaProfessor {
  public static void main(String[] args) {
    Professor pro = new Professor();
    
    String str = "Dados do professor: ";
    str += "\n" + pro.matricula + ": " + pro.nome;
    JOptionPane.showMessageDialog(null,str);
    
    pro.matricula = -15;
    pro.nome = "";
    
    str = "Dados do professor: ";
    str += "\n" + pro.matricula + ": " + pro.nome;
    JOptionPane.showMessageDialog(null,str);
    
    System.exit(0);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/