public class Cliente {
  private String nome;
  private String fone;
  
  public Cliente() {
    this("Sem nome","(00)0000-0000");
  }
  
  public Cliente(String nome, String fone) {
    this.nome = nome;
    this.fone = fone;
  }
  
  public String getNome() {
    return nome;
  }
  
  public String getFone() {
    return fone;
  }
  
  public void setNome(String nome) {
    this.nome = nome;
  }
  
  public void setFone(String fone) {
    this.fone = fone;
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/