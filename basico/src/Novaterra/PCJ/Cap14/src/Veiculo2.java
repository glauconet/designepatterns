public abstract class Veiculo2 {
  private String placa;
  private int ano;
  
  public String getPlaca() {
    return placa;
  }
  
  public int getAno() {
    return ano;
  }
  
  public void setPlaca(String placa) {
    this.placa = placa;
  }
  
  public void setAno(int ano) {
    this.ano = ano;
  }
  
  public void setAno(String ano) {
    this.ano = Integer.parseInt(ano);
  }
  
  public abstract void exibirDados();
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/