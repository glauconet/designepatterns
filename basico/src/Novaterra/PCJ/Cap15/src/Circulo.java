public class Circulo implements AreaCalculavel {
  private double diametro;
  
  public Circulo(double diametro) {
    this.diametro = diametro;
  }
  
  public double calcularArea() {
    double raio = diametro / 2;
    return Math.PI * Math.pow(raio,2);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/