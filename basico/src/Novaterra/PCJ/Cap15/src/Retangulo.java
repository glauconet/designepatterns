public class Retangulo implements AreaCalculavel {
  private double base;
  private double altura;
  
  public Retangulo(double base, double altura) {
    this.base = base;
    this.altura = altura;
  }
  
  public double calcularArea() {
    return base * altura;
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/