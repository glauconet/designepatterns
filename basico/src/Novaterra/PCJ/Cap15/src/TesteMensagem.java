import javax.swing.JOptionPane;

public class TesteMensagem implements Mensagem {
  public void exibir(String texto) {
    JOptionPane.showMessageDialog(null,texto,"Mensagem",1);
  }
  
  public static void main(String[] args) {
    TesteMensagem tm = new TesteMensagem();
    
    String str = Mensagem.ENTRADA;
    tm.exibir( str );
    
    str = TesteMensagem.SUCESSO;
    tm.exibir( str );
    
    System.exit(0);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/