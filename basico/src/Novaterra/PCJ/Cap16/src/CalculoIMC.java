import br.pro.ruirossi.pcj.cap16.Pessoa;
import javax.swing.JOptionPane;

public class CalculoIMC {
  public static void main(String[] args) {
    String str = JOptionPane.showInputDialog("Seu peso");
    if (str == null) System.exit(0);
    double peso = Double.parseDouble(str);
    
    str = JOptionPane.showInputDialog("Sua altura");
    if (str == null) System.exit(0);
    double altura = Double.parseDouble(str);
    
    double imc = Pessoa.calcularIMC(peso,altura);
    
    JOptionPane.showMessageDialog(null,"Seu IMC: " + imc);
    System.exit(0);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/