package br.pro.ruirossi.pcj.cap16;

import javax.swing.JOptionPane;

public class TesteIMC {
  public static void main(String[] args) {
    Pessoa p1 = new Pessoa("Marcelo",72.5,1.82);
    JOptionPane.showMessageDialog(null,p1);
    Pessoa p2 = new Pessoa("Franciele",58.5,1.74);
    JOptionPane.showMessageDialog(null,p2);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/