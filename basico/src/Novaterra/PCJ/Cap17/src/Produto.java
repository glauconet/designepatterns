import java.text.NumberFormat;

public class Produto {
  private String descricao;
  private double valor;
  
  public Produto(String descricao, double valor) {
    this.descricao = descricao;
    this.valor = valor;
  }
  
 public String toString() {
   NumberFormat nf = NumberFormat.getCurrencyInstance();
   return descricao + " - " + nf.format(valor);
 }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/