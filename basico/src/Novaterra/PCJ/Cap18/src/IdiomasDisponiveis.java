import static java.lang.System.out;
import java.util.Locale;

public class IdiomasDisponiveis {
  public static void main(String[] args) {
    String[] idiomas = Locale.getISOLanguages();
    
    out.println( "Idiomas dispon�veis:" );
    for (int i = 1; i <= idiomas.length; i++) {
      out.print( idiomas[i-1] + " " );
      if (i % 25 == 0) out.println(); 
    }
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/