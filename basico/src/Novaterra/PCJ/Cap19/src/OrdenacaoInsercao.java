public class OrdenacaoInsercao {
  public static void ordenar(int[] dados, int qtde) {
    for(int out = 1; out < qtde; out++) {
      int temp = dados[out];
      int in = out;
      
      while(in > 0 && dados[in-1] >= temp) {
        dados[in] = dados[in-1]; 
        in--;
      }
      
      dados[in] = temp;
    }
  }
  
  public static void ordenar(String[] dados, int qtde) {
    for(int out = 1; out < qtde; out++) {
      String temp = dados[out];
      int in = out;
      
      while(in > 0 && dados[in-1].compareToIgnoreCase(temp) > 0) {
        dados[in] = dados[in-1]; 
        in--;
      }
      
      dados[in] = temp;
    }
  }
  
  public static <C extends Comparable<C>> void ordenar(C[] dados,
    int qtde) {
    for(int out = 1; out < qtde; out++) {
      C temp = dados[out];
      int in = out;
      
      while(in > 0 && dados[in-1].compareTo(temp) > 0) {
        dados[in] = dados[in-1]; 
        in--;
      }
      
      dados[in] = temp;
    }
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/