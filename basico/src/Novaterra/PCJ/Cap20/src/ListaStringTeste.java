import static javax.swing.JOptionPane.*;

public class ListaStringTeste {
  private static Lista<String> lista;
  
  public static void main(String[] args) throws ListaVaziaException{
    lista = new Lista<String>();
    
    while (true) {
      String str = showInputDialog("Informe um texto");
      if (str == null) break;
      lista.incluirNoFim( str );
    }
    
    String str = "Itens da lista:";
    while(!lista.vazia())
      str += "\n" + lista.excluirDoInicio();
    showMessageDialog(null, str);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/