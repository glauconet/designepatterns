public class PilhaDinamica <E>{
  private Lista<E> lista;
  
  public PilhaDinamica() {
    lista = new Lista<E>();
  }
  
  public void incluir(E objeto) {
    lista.incluirNoFim(objeto);
  }
  
  public E excluir() throws ListaVaziaException{
    return lista.excluirDoFim();
  }
  
  public E consultar() {
    return lista.getUltimo();
  }
  
  public boolean vazia() {
    return lista.vazia();
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/