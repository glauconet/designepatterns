public class PilhaGenerica <E>{
  private E[] objetos;
  private int topo;
  
  public PilhaGenerica(int maximo) {
    objetos = (E[])new Object[maximo];
    topo = -1;
  }
  
  public void incluir(E objeto) {
    objetos[++topo] = objeto;
  }
  
  public E excluir () {
    return objetos[topo--];
  }
  
  public E consultar() {
    return objetos[topo];
  }
  
  public boolean vazia() {
    return (topo == -1);
  }
  
  public boolean cheia() {
    return (topo == objetos.length - 1);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/