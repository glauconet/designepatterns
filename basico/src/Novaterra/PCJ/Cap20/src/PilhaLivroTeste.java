import static javax.swing.JOptionPane.*;

public class PilhaLivroTeste {
  private static PilhaGenerica<Livro> pilha;
  
  public static void main(String[] args) {
    pilha = new PilhaGenerica<Livro>(50);
    
    while (!pilha.cheia()) {
      Livro livro = new Livro();
      
      String str = showInputDialog("Informe o autor");
      if (str == null) break;
      livro.setAutor(str);
      
      str = showInputDialog("Informe o t�tulo");
      if (str == null) break;
      livro.setTitulo(str);
      
      pilha.incluir( livro );
    }
    
    String str = "Itens da pilha:";
    while(!pilha.vazia())
      str += "\n" + pilha.excluir();
    showMessageDialog(null, str);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/