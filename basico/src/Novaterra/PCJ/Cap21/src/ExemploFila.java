import static javax.swing.JOptionPane.*;
import java.util.LinkedList;

public class ExemploFila {
  private static LinkedList<String> fila;
  
  public static void main(String[] args) {
    fila = new LinkedList<String>();
    
    while (true) {
      String str = showInputDialog("Informe um texto");
      if (str == null) break;
      fila.add( str );
    }
    
    String str = "Itens da fila:";
    while(!fila.isEmpty())
      str += "\n" + fila.remove();
    showMessageDialog(null, str);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/