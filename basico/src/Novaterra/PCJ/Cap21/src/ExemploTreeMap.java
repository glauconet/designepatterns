import static javax.swing.JOptionPane.*;
import java.util.Map;
import java.util.TreeMap;
import java.util.Set;

public class ExemploTreeMap {
  private static Map<String,String> mapa;
  
  public static void main(String[] args) {
    mapa = new TreeMap<String,String>();
    
    while (true) {
      String x = showInputDialog("Digite uma sigla");
      if (x == null) break;
      
      String y = showInputDialog("Digite o significado de " + x);
      if (y == null) break;
      
      mapa.put(x,y);
    }
    
    Set<String> chaves = mapa.keySet();
    
    String str = "Dicion�rio (" + mapa.size() + " siglas)\n";
    for (String chave : chaves)
      str += "\n" + chave + ": " + mapa.get(chave);
    showMessageDialog(null, str);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/