import java.util.Comparator;

public class FunNomeComparator implements Comparator<Funcionario>{
  public int compare(Funcionario func1, Funcionario func2) {
    return func1.getNome().compareToIgnoreCase(func2.getNome());
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/