import java.awt.*;
import javax.swing.*;

public class Botao extends Janela {
  private JButton btGravar;
  private JButton btSair;
  
  public Botao( ){
    super("Bot�es",new Dimension(300,100));
    
    btGravar = new JButton( );
    btGravar.setText("Gravar");
    btGravar.setBounds(50,30,100,30);
    btGravar.setBackground(new Color(0,0,170));
    btGravar.setForeground(Color.YELLOW);
    btGravar.setFont(new Font("Helvetica",Font.BOLD,12));
    btGravar.setToolTipText("Gravar os dados");
    btGravar.setIcon(new ImageIcon("Gravar.png"));
    btGravar.setHorizontalAlignment(SwingConstants.CENTER);
    btGravar.setVerticalAlignment(SwingConstants.CENTER);
    btGravar.setHorizontalTextPosition(SwingConstants.RIGHT);
    btGravar.setVerticalTextPosition(SwingConstants.CENTER);
    btGravar.setEnabled(false);
    btGravar.setMnemonic('G');
    
    btSair = new JButton("Sair");
    btSair.setBounds(150,30,100,30);
    btSair.setBackground(new Color(0,0,170));
    btSair.setForeground(Color.YELLOW);
    btSair.setFont(new Font("Helvetica",Font.BOLD,12));
    btSair.setToolTipText("Encerrar o aplicativo");
    btSair.setIcon(new ImageIcon("Sair.png"));
    btSair.setHorizontalAlignment(SwingConstants.CENTER);
    btSair.setVerticalAlignment(SwingConstants.CENTER);
    btSair.setHorizontalTextPosition(SwingConstants.RIGHT);
    btSair.setVerticalTextPosition(SwingConstants.CENTER);
    btSair.setMnemonic('S');
    
    getContentPane( ).add(btGravar);
    getContentPane( ).add(btSair);
  }
  
  public static void main(String[] args) {
    new Botao( ).setVisible(true);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/