import java.awt.*;
import javax.swing.JFrame;

public class Janela extends JFrame {
  public Janela(String titulo, Dimension tamanho) {
    super();
    setTitle(titulo);
    setSize(tamanho);
    centralizar( );
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    
    getContentPane( ).setLayout(null);
    getContentPane( ).setBackground(Color.WHITE);
    
    Image imLivro;
    imLivro = Toolkit.getDefaultToolkit( ).getImage("Livros.png");
    setIconImage(imLivro);
  }
  
  public void centralizar( ) {
    Dimension dt = Toolkit.getDefaultToolkit( ).getScreenSize( );
    Dimension dj = getSize( );
    if (dj.height > dt.height) setSize(dj.width,dt.height );
    if (dj.width > dt.width) setSize(dt.width,dj.height);
    setLocation((dt.width - dj.width)/2,(dt.height - dj.height)/2);
  }
  
  public static void main(String[] args) {
    Janela jan = new Janela("SisEscola",new Dimension(300,200));
    jan.setVisible(true);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/