import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import static javax.swing.JOptionPane.showMessageDialog;

public class EventoJanela5 extends JFrame {
  public EventoJanela5( ) {
    super();
    setSize(300,200);
    setTitle("Eventos de janela");
    getContentPane().setLayout(null);
    getContentPane().setBackground(Color.CYAN);
    
    addWindowListener( new Ouvinte() );
  }
  
  public static void main(String[] args) {
    new EventoJanela5( ).setVisible(true);
  }
  
  class Ouvinte extends WindowAdapter {
    public void windowClosing(WindowEvent e) {
      showMessageDialog(EventoJanela5.this,"At� logo!");
      System.exit(0);
    }
    
    public void windowOpened(WindowEvent e){
      showMessageDialog(EventoJanela5.this,"Bem-vindo!");
    }
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/