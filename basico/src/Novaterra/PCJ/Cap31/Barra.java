import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Barra extends JFrame implements ActionListener {
  private JToolBar tbNorte;
  private JToolBar tbOeste;
  private JButton btUsuario;
  private JButton btAluno;
  private JButton btProfessor;
  private JButton btSistema;
  private JButton btSobre;
  private JButton btAjuda;
  private JButton btLogoff;
  private JButton btSair;
  private JPopupMenu pmExibir;
  private JCheckBoxMenuItem miOeste;
  private JCheckBoxMenuItem miNorte;
  
  public Barra() {
    setTitle("Exemplo de barra de ferramentas");
    setSize(350,250);
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    
    tbNorte = new JToolBar("Barra 1",JToolBar.HORIZONTAL);
    tbOeste = new JToolBar("Barra 2",JToolBar.VERTICAL);
    
    btUsuario = new JButton();
    btUsuario.setIcon(new ImageIcon("Usuario.png"));
    btUsuario.setToolTipText("Cadastro de usu�rios");
    btUsuario.setFocusable(false);
    tbOeste.add(btUsuario);
    
    btAluno = new JButton();
    btAluno.setIcon(new ImageIcon("Aluno.png"));
    btAluno.setToolTipText("Cadastro de alunos");
    btAluno.setFocusable(false);
    tbOeste.add(btAluno);
    
    btProfessor = new JButton();
    btProfessor.setIcon(new ImageIcon("Professor.png"));
    btProfessor.setToolTipText("Cadastro de alunos");
    btProfessor.setFocusable(false);
    tbOeste.add(btProfessor);
    
    btSistema = new JButton();
    btSistema.setIcon(new ImageIcon("Sistema.png"));
    btSistema.setToolTipText("Configura��es do sistema");
    btSistema.setFocusable(false);
    tbNorte.add(btSistema);
    
    btSobre = new JButton();
    btSobre.setIcon(new ImageIcon("Sobre.png"));
    btSobre.setToolTipText("Informa��es sobre o sistema");
    btSobre.setFocusable(false);
    tbNorte.add(btSobre);
    
    btAjuda = new JButton();
    btAjuda.setIcon(new ImageIcon("Ajuda.png"));
    btAjuda.setToolTipText("Ajuda sobre o sistema");
    btAjuda.setFocusable(false);
    tbNorte.add(btAjuda);
    
    btLogoff = new JButton();
    btLogoff.setIcon(new ImageIcon("Logoff.png"));
    btLogoff.setToolTipText("Efetuar Logoff");
    btLogoff.setFocusable(false);
    tbNorte.add(btLogoff);
    
    btSair = new JButton();
    btSair.setIcon(new ImageIcon("Sair.png"));
    btSair.setToolTipText("Sair do sistema");
    btSair.setFocusable(false);
    tbNorte.add(btSair);
    
    getContentPane().setLayout(new BorderLayout());
    getContentPane().add(tbOeste,BorderLayout.WEST);
    getContentPane().add(tbNorte,BorderLayout.NORTH);
    
    miOeste = new JCheckBoxMenuItem("Barra Lateral");
    miNorte = new JCheckBoxMenuItem("Barra Superior");
    
    miOeste.setSelected(true);
    miNorte.setSelected(true);
    
    miOeste.addActionListener(this);
    miNorte.addActionListener(this);
    
    pmExibir = new JPopupMenu();
    pmExibir.add(miOeste);
    pmExibir.add(miNorte);
    
    addMouseListener(new MouseAdapter( ) {
      public void mouseReleased(MouseEvent e) {
        if (e.isPopupTrigger( ))
          pmExibir.show(e.getComponent( ),e.getX( ),e.getY( ));
        }});
  }

  public void actionPerformed(ActionEvent e) {
    if (e.getSource() == miOeste)
      if (miOeste.isSelected()) add(tbOeste,BorderLayout.WEST);
      else remove(tbOeste);
    else if (e.getSource() == miNorte)
      if (miNorte.isSelected()) add(tbNorte,BorderLayout.NORTH);
      else remove(tbNorte);
    
    SwingUtilities.updateComponentTreeUI(this);
  }  
  
  public static void main(String[] args) {
    new Barra().setVisible(true);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/