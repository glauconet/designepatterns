import javax.swing.*;
import javax.swing.UIManager.LookAndFeelInfo;

public class ListaLAFs {
  public static void main(String[] args) {
    LookAndFeelInfo[] lafs = UIManager.getInstalledLookAndFeels();
    String str = "LookAndFeels dispon�veis:\n";
    for (LookAndFeelInfo info : lafs)
      str += "\n" + info.getName() + " - " + info.getClassName();
    JOptionPane.showMessageDialog(null, str);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/