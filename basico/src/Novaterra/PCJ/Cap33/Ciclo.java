import java.applet.*;
import java.awt.*;

public class Ciclo extends Applet {
  private TextArea taLog;
  private int contador;
  
  public void init() {
    contador = 0;
    
    taLog = new TextArea();
    taLog.append("Applet inicializado!\n");
    
    setLayout(new BorderLayout());
    add(taLog,BorderLayout.CENTER);
  }
  
  public void paint(Graphics g) {
    contador++;
    taLog.append("Atualizado " + contador + " vez(es).\n");
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/