import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import java.net.*;

public class GuiAwt extends Applet 
  implements ActionListener,MouseListener,FocusListener {
  private Button btRui;
  private Button btSun;
  private URL url;
  
  public void init() {
    btRui = new Button("Rui Rossi dos Santos");
    btSun = new Button("Sun Microsystems");
    btRui.setBackground(Color.LIGHT_GRAY);
    btSun.setBackground(Color.LIGHT_GRAY);
    btRui.addActionListener(this);
    btSun.addActionListener(this);
    btRui.addMouseListener(this);
    btSun.addMouseListener(this);
    btRui.addFocusListener(this);
    btSun.addFocusListener(this);
    
    setLayout(new GridLayout(2,1));
    add(btRui);
    add(btSun);
  }
  
  public void actionPerformed(ActionEvent e) {
    try {
      if (e.getSource() == btRui) 
        url = new URL("http://ruirossi.pro.br");
      else url = new URL("http://java.sun.com");
      
      getAppletContext().showDocument(url,"_blank");
    }
    catch (MalformedURLException mf) {}
  }
  
  public void mouseEntered(MouseEvent e) {
    ((Button)e.getSource()).requestFocus();
  }
  
  public void focusGained(FocusEvent e) {
    ((Button)e.getSource()).setBackground(Color.YELLOW);
  }
  
  public void focusLost(FocusEvent e) {
    ((Button)e.getSource()).setBackground(Color.LIGHT_GRAY);
  }
  
  public void mousePressed(MouseEvent e) {}
  public void mouseReleased(MouseEvent e) {}
  public void mouseClicked(MouseEvent e) {}
  public void mouseExited(MouseEvent e){}
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/