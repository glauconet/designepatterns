import java.awt.*;
import java.awt.event.*;
import java.net.*;
import javax.swing.*;

public class GuiSwing extends JApplet 
  implements ActionListener,MouseListener,FocusListener {
  private JButton btRui;
  private JButton btSun;
  private URL url;
  
  public void init() {
    btRui = new JButton("Rui Rossi dos Santos");
    btSun = new JButton("Sun Microsystems");
    btRui.setBackground(Color.LIGHT_GRAY);
    btSun.setBackground(Color.LIGHT_GRAY);
    btRui.addActionListener(this);
    btSun.addActionListener(this);
    btRui.addMouseListener(this);
    btSun.addMouseListener(this);
    btRui.addFocusListener(this);
    btSun.addFocusListener(this);
    
    getContentPane().setLayout(new GridLayout(2,1));
    getContentPane().add(btRui);
    getContentPane().add(btSun);
  }
  
  public void actionPerformed(ActionEvent e) {
    try {
      if (e.getSource() == btRui) 
        url = new URL("http://ruirossi.pro.br");
      else url = new URL("http://java.sun.com");
      
      getAppletContext().showDocument(url,"_blank");
    }
    catch (MalformedURLException mf) {}
  }
  
  public void mouseEntered(MouseEvent e) {
    ((JButton)e.getSource()).requestFocus();
  }
  
  public void focusGained(FocusEvent e) {
    ((JButton)e.getSource()).setBackground(Color.YELLOW);
  }
  
  public void focusLost(FocusEvent e) {
    ((JButton)e.getSource()).setBackground(Color.LIGHT_GRAY);
  }
  
  public void mousePressed(MouseEvent e) {}
  public void mouseReleased(MouseEvent e) {}
  public void mouseClicked(MouseEvent e) {}
  public void mouseExited(MouseEvent e){}
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/