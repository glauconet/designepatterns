import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import java.net.*;

public class Menu extends Applet 
  implements ActionListener,MouseListener {
  private URL base;
  private int qtdeBotoes;
  private String frame;
  private Color corFundo;
  private Color corBotao;
  private Color corBotaoSel;
  private Color corBotaoPress;
  private String tipoFonte;
  private String tipoFonteSel;
  private Color corFonte;
  private Color corFonteSel;
  private int tamFonte;
  private int tamFonteSel;
  
  public void init() {
    base = getDocumentBase();   
    
    String st = getParameter("QtdeBotoes");
    try{qtdeBotoes = Integer.parseInt(st);}
    catch(Exception e) {qtdeBotoes = 0;}
    
    frame = getParameter("Frame");
    if (frame == null) frame = "_self";
    
    st = getParameter("Orientacao");
    if (st == null) setLayout(new GridLayout(qtdeBotoes,1));
    else
      if (st.equals("Horizontal"))
        setLayout(new GridLayout(1,qtdeBotoes));
      else setLayout(new GridLayout(qtdeBotoes,1));
    
    st = getParameter("CorFundo");
    if (st == null) corFundo = new Color(150,150,100);
    else corFundo = new Color(Integer.parseInt(st,16));
    setBackground(corFundo);
    
    st = getParameter("CorBotao");
    if (st == null) corBotao = new Color(200,200,100);
    else corBotao = new Color(Integer.parseInt(st,16));
    
    st = getParameter("CorBotaoSel");
    if (st == null) corBotaoSel = new Color(200,250,100);
    else corBotaoSel = new Color(Integer.parseInt(st,16));
    
    st = getParameter("CorBotaoPress");
    if (st == null) corBotaoPress = new Color(255,255,0);
    else corBotaoPress = new Color(Integer.parseInt(st,16));
    
    tipoFonte = getParameter("TipoFonte");
    if (tipoFonte == null) tipoFonte = "Helvetica";
    tipoFonteSel = getParameter("TipoFonteSel");
    if (tipoFonteSel == null) tipoFonteSel = "Helvetica";
    
    st = getParameter("CorFonte");
    if (st == null) corFonte = new Color(0,0,200);
    else corFonte = new Color(Integer.parseInt(st,16));
    
    st = getParameter("CorFonteSel");
    if (st == null) corFonteSel = new Color(0,0,255);
    else corFonteSel = new Color(Integer.parseInt(st,16));
    
    st = getParameter("TamFonte");
    try {tamFonte = Integer.parseInt(st);}
    catch(Exception e) {tamFonte = 14;}
    
    st = getParameter("TamFonteSel");
    try {tamFonteSel = Integer.parseInt(st);}
    catch(Exception e) {tamFonteSel = 16;}
    
    for(int it = 1;it <= qtdeBotoes; it++)
    {
      Button bt = new Button(getParameter("rotulo"+it));
      bt.setActionCommand(getParameter("url"+it));
      bt.setFont(new Font(tipoFonte,Font.PLAIN,tamFonte));
      bt.setBackground(corBotao);
      bt.setForeground(corFonte);
      bt.addActionListener(this);
      bt.addMouseListener(this);
      bt.setFocusable(false);
      add(bt);
    }
  }
  
  public void actionPerformed(ActionEvent e) {
    Button bt = (Button)e.getSource();
    String st = bt.getActionCommand();
    if (st == null || st.length() == 0) return;
    
    try {
      URL url = new URL(base,bt.getActionCommand());
      getAppletContext().showDocument(url,frame);
    }
    catch(Exception ex) {
      getAppletContext().showStatus("Erro ocorrido!");
    }
  }
  
  public void mouseEntered(MouseEvent e) {
    Button bt = (Button)e.getSource();
    bt.setFont(new Font(tipoFonteSel,Font.BOLD,tamFonteSel));
    bt.setBackground(corBotaoSel);
    bt.setForeground(corFonteSel);
  }
  
  public void mouseExited(MouseEvent e) {
    Button bt = (Button)e.getSource();
    bt.setFont(new Font(tipoFonte,Font.PLAIN,tamFonte));
    bt.setBackground(corBotao);
    bt.setForeground(corFonte);
  }
  
  public void mousePressed(MouseEvent e) {
    Button bt = (Button)e.getSource();
    bt.setBackground(corBotaoPress);
  }
  
  public void mouseReleased(MouseEvent e) {
    Button b = (Button)e.getSource();
    b.setBackground(corBotaoSel);
  }
  
  public void mouseClicked(MouseEvent e) {}
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/