import java.applet.*;
import java.awt.*;

public class Multimidia extends Applet {
  private AudioClip som;
  private Image img;
  
  public void init() {
    img = getImage(getDocumentBase(),getParameter("imagem"));
    som = getAudioClip(getDocumentBase(),getParameter("audio"));
  }
  
  public void start() {
    som.loop();
  }
  
  public void stop() {
    som.stop();
  }
  
  public void paint(Graphics g) {
    g.drawImage(img,0,0,this);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/