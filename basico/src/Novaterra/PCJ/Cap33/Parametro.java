import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;

public class Parametro extends Applet implements MouseListener {
  private int qtdeBotoes;
  private Color corDeFundo;
  private Color corDoBotao;
  private Color corDoTexto;
  private Color corSelecao;
  
  public void init() {
    qtdeBotoes = Integer.parseInt(getParameter("QtdeBotoes"));
    
    String st = getParameter("CorDeFundo");
    corDeFundo = new Color(Integer.parseInt(st,16));
    setBackground(corDeFundo);
    
    st = getParameter("CorDoBotao");
    corDoBotao = new Color(Integer.parseInt(st,16));
    
    st = getParameter("CorDoTexto");
    corDoTexto = new Color(Integer.parseInt(st,16));
    
    st = getParameter("CorSelecao");
    corSelecao = new Color(Integer.parseInt(st,16));
    
    setLayout(new GridLayout(qtdeBotoes,1));
    for(int it = 1;it <= qtdeBotoes; it++) {
      Button bt = new Button(getParameter("Rotulo" + it));
      bt.setBackground(corDoBotao);
      bt.setForeground(corDoTexto);
      bt.addMouseListener(this);
      add(bt);
    }
  }
  
  public void mouseEntered(MouseEvent e)  {
    Button b = (Button)e.getSource();
    b.setBackground(corSelecao);
    b.requestFocus();
  }
  
  public void mouseExited(MouseEvent e) {
    Button b = (Button)e.getSource();
    b.setBackground(corDoBotao);
  }
  
  public void mousePressed(MouseEvent e) {}
  public void mouseReleased(MouseEvent e) {}
  public void mouseClicked(MouseEvent e) {}
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/