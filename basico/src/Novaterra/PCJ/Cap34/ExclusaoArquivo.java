import java.io.File;
import static javax.swing.JOptionPane.*;

public class ExclusaoArquivo {
  public static void main(String[] args) {
    while (true) {
      String arq = showInputDialog("Que arquivo deseja excluir?");
      if (arq == null) break;
      File file = new File(arq);
      if (!file.exists())
        showMessageDialog(null,"Arquivo n�o existe!","Erro",
            ERROR_MESSAGE);
      else file.delete();
    }
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/