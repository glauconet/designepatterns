import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FluxoBufferizado {
  public static void main(String[] args) throws IOException {
    BufferedReader br = null;
    BufferedWriter bw = null;
    
    try {
      br = new BufferedReader( new FileReader("Edipo.txt") );
      bw = new BufferedWriter( new FileWriter("Copia.txt") );
      
      String linha;
      while ( (linha = br.readLine()) != null)
        bw.write(linha + System.getProperty("line.separator"));
      
    } catch (FileNotFoundException e) {
      System.out.println("Arquivo n�o encontrado!");
    } catch (IOException ioe) {
      System.out.println("Erro durante leitura/escrita!");
    }
    finally {
      if (br != null) br.close();
      if (bw != null) bw.close();
    }
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/