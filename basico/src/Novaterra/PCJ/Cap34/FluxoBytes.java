import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FluxoBytes {
  public static void main(String[] args) throws IOException {
    FileInputStream fis = null;
    FileOutputStream fos = null;
    
    try {
      fis = new FileInputStream("Edipo.txt");
      fos = new FileOutputStream("Copia.txt");
      
      int bt;
      while ( (bt = fis.read()) != -1)
        fos.write(bt);
      
    } catch (FileNotFoundException e) {
      System.out.println("Arquivo n�o encontrado!");
    } catch (IOException ioe) {
      System.out.println("Erro durante leitura/escrita!");
    }
    finally {
      if (fis != null) fis.close();
      if (fos != null) fos.close();
    }
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/