import java.io.FileReader;
import java.io.FileWriter;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FluxoCaracteres {
  public static void main(String[] args) throws IOException {
    FileReader fr = null;
    FileWriter fw = null;
    
    try {
      fr = new FileReader("Edipo.txt");
      fw = new FileWriter("Copia.txt");
      
      int bt;
      while ( (bt = fr.read()) != -1)
        fw.write(bt);
      
    } catch (FileNotFoundException e) {
      System.out.println("Arquivo n�o encontrado!");
    } catch (IOException ioe) {
      System.out.println("Erro durante leitura/escrita!");
    }
    finally {
      if (fr != null) fr.close();
      if (fw != null) fw.close();
    }
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/