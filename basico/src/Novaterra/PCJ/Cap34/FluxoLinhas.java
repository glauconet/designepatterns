import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FluxoLinhas {
  public static void main(String[] args) throws IOException {
    BufferedReader br = null;
    PrintWriter pw = null;
    
    try {
      br = new BufferedReader( new FileReader("Edipo.txt") );
      pw = new PrintWriter( new FileWriter("Copia.txt") );
      
      String linha;
      while ( (linha = br.readLine()) != null)
        pw.println(linha);
      
    } catch (FileNotFoundException e) {
      System.out.println("Arquivo n�o encontrado!");
    } catch (IOException ioe) {
      System.out.println("Erro durante leitura/escrita!");
    }
    finally {
      if (br != null) br.close();
      if (pw != null) pw.close();
    }
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/