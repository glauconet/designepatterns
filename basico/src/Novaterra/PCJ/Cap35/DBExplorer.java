import static javax.swing.JOptionPane.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import javax.swing.*;
import javax.swing.event.*;

public class DBExplorer extends JFrame
  implements ListSelectionListener, ItemListener, ActionListener {
  private JList liBancos;
  private JComboBox coTabelas;
  private JComboBox coCampos;
  private JButton btConsultar;
  private JTable tbGrade;
  private JPanel pnCombos;
  private JPanel pnCentro;
  private Connection conexao;
  
  public DBExplorer() {
    setTitle("DBExplorer");
    setSize(750,250);
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    
    liBancos = new JList(new DefaultListModel());
    liBancos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    
    coTabelas = new JComboBox(new DefaultComboBoxModel());
    coCampos = new JComboBox(new DefaultComboBoxModel());
    
    btConsultar = new JButton("Consultar");
    btConsultar.setMnemonic('C');
    
    tbGrade = new JTable(new ModeloGrade());
    
    pnCombos = new JPanel(new FlowLayout(FlowLayout.LEFT));
    pnCombos.add(coTabelas);
    pnCombos.add(coCampos);
    pnCombos.add(btConsultar);
    
    pnCentro = new JPanel(new BorderLayout());
    pnCentro.add(pnCombos,BorderLayout.NORTH);
    pnCentro.add(new JScrollPane(tbGrade,
      JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
      JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS),BorderLayout.CENTER);
    
    getContentPane().setLayout(new BorderLayout());
    getContentPane().add(new JScrollPane(liBancos),
      BorderLayout.WEST);
    getContentPane().add(pnCentro,BorderLayout.CENTER);
    
    Dimension dm = Toolkit.getDefaultToolkit().getScreenSize();
    setLocation((dm.width-getWidth())/2, (dm.height-getHeight())/2);
    
    liBancos.addListSelectionListener(this);
    coTabelas.addItemListener(this);
    btConsultar.addActionListener(this);
    
    try {
      abrirConexao("");
      
      DatabaseMetaData dbmd = conexao.getMetaData();
      ResultSet bancos = dbmd.getCatalogs();
      
      DefaultListModel dlm = (DefaultListModel)liBancos.getModel();
      while(bancos.next())
        dlm.addElement(bancos.getString("TABLE_CAT"));
    }
    catch(Exception ex) {
      showMessageDialog(this,"Erro ocorrido: " + ex.getMessage(),
        "Erro",ERROR_MESSAGE);
    }
  }
  
  public void abrirConexao(String banco){
    try {
      Class.forName("com.mysql.jdbc.Driver");
      conexao = DriverManager.getConnection(
        "jdbc:mysql://localhost/" + banco,"root","root");
    }
    catch (Exception ex) {
      showMessageDialog(this,"Conex�o falhou!","Erro",ERROR_MESSAGE);
    }
  }
  
  public void valueChanged(ListSelectionEvent e) {
    try {
      String banco = liBancos.getSelectedValue().toString();
      abrirConexao(banco);
      DatabaseMetaData dbmd = conexao.getMetaData();
      ResultSet rs_tabelas = dbmd.getTables(banco, null, null, null);
      
      DefaultComboBoxModel dcm = (DefaultComboBoxModel)
        coTabelas.getModel();
      dcm.removeAllElements();
      while(rs_tabelas.next())
        dcm.addElement(rs_tabelas.getString("TABLE_NAME"));
    }
    catch(Exception ex) {
      showMessageDialog(this,"Erro ocorrido: " + ex.getMessage(),
        "Erro",ERROR_MESSAGE);
    }
  }
  
  public void itemStateChanged(ItemEvent e) {
    if (coTabelas.getSelectedIndex() == -1) return;
    
    String banco = liBancos.getSelectedValue().toString();
    String tabela = coTabelas.getSelectedItem().toString();
    
    try {
      DatabaseMetaData dbmd = conexao.getMetaData();
      ResultSet rs_campos = dbmd.getColumns(banco,null,tabela,null);
      DefaultComboBoxModel dcm = (DefaultComboBoxModel)
        coCampos.getModel();
      dcm.removeAllElements();
      while(rs_campos.next())
        dcm.addElement( rs_campos.getString("COLUMN_NAME") );
    }
    catch(Exception ex) {
      showMessageDialog(this,"Erro ocorrido: " + ex.getMessage(),
        "Erro",ERROR_MESSAGE);
    }
  }
  
  public void actionPerformed(ActionEvent e) {
    try {
      Statement stm = conexao.createStatement();
      ResultSet rs = stm.executeQuery("SELECT * FROM " + 
        coTabelas.getSelectedItem() + " ORDER BY " +
        coCampos.getSelectedItem());
      tbGrade.setModel(new ModeloGrade(rs,null));
    }
    catch (Exception ex) {
      showMessageDialog(this,"Erro ocorrido: " + ex.getMessage(),
        "Erro",ERROR_MESSAGE);
    }
  }
  
  public static void main(String[] args) {
    try {
      UIManager.setLookAndFeel(
        "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
    }
    catch(Exception e) {}
    
    new DBExplorer().setVisible(true);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/