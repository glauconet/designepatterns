import java.awt.*;
import javax.swing.*;

public class DesktopImagem extends JDesktopPane {
  private ImageIcon iiImagem;
  
  public DesktopImagem(String imagem) {
    iiImagem = new ImageIcon(imagem);
  }
  
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    iiImagem.paintIcon(this, g, 0, 0);
  }
  
  public Dimension getPreferredSize() {
    return new Dimension(iiImagem.getIconWidth(),
        iiImagem.getIconHeight());
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/