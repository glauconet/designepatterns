import static javax.swing.JOptionPane.*;

public class TesteConexao {
  public static void main(String[] args) {
    try{
      ConexaoComercio c = new ConexaoComercio();
      showMessageDialog(null,"Conex�o aberta!");
      c.fechar();
      showMessageDialog(null,"Conex�o fechada!");
    }
    catch(Exception ex) {
      showMessageDialog(null,ex.getMessage(),"Erro",ERROR_MESSAGE);
    }
    System.exit(0);
  }
}

/********************************************************************
 * Este arquivo � parte integrante do livro identificado abaixo e � *
 * protegido pela legisla��o que trata dos direitos autorais.       *
 *                                                                  *
 * T�tulo:  Programa��o de Computadores em Java                     *
 * Autor:   Rui Rossi dos Santos                                    *
 * Editora: NovaTerra Editora e Distribuidora Ltda.                 *
 * Ano:     2011                                                    *
 ********************************************************************/