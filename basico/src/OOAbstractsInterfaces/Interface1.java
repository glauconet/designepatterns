package OOAbstractsInterfaces;

public interface Interface1 {// exemplo de interface
	public String BemVindo 	= "Ola seja bem vindo";
	public String Sucesso 	= "Operacao concluida com Sucesso! ";
	public String Erro  	= "Houve um erro inesperado";
	public String Autor 	= "Feito no Serpro";
	// 	atributos sempre sao publicos na interface
	//	em uma interface, todo atributo e final e estatico
	// 	final =  constante
	// 	static = chamado direto da classe pai
	public void showMsg (String Texto);// os metodos nao tem corpo
	// voce e obrigado a declarar nas classes que implementam
}
