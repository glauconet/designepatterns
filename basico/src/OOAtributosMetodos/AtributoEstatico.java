package OOAtributosMetodos;
import OOConstrutores.Pessoa;

public class AtributoEstatico extends Pessoa{
	
	
	public static double Creditobase = 300;// use a palavra static
	// sao compatilhados  e modificados por todos os objetos
	// a mudanca no valor reflete imediatamente em outros objetos
	// pode ser acessado pelo objeto ou pelo nome da classe
	// pode ser acessado e modificado por um objeto, mais � um atributo de classe.
	public static void main(String[] args){
		
		AtributoEstatico p1 = new AtributoEstatico();
		AtributoEstatico p2 = new AtributoEstatico();
		
		p1.setNome("X");
		p2.setNome("Y");
	
		System.out.print("Nome: " + p1.getNome());
		System.out.println(" credito: " + p1.Creditobase);
		System.out.print("Nome: " + p2.getNome());
		System.out.println(" credito: " + p2.Creditobase );
		System.out.println("-------------- "  );
		
		AtributoEstatico.Creditobase = 500; // modificando pela classe
		
		System.out.print("Nome: " + p1.getNome());
		System.out.println(" credito: " + p1.Creditobase);
		System.out.print("Nome: " + p2.getNome());
		System.out.println(" credito: " + p2.Creditobase );
		System.out.println("-------------- "  );
		
		p1.Creditobase = 600; // modificando pelo objeto
		
		
		System.out.print("Nome: " + p1.getNome());
		System.out.println(" credito: " + p1.Creditobase);
		System.out.print("Nome: " + p2.getNome());
		System.out.println(" credito: " + p2.Creditobase );
		System.out.println("-------------- "  );
		System.out.print(" atributo da classe: " + AtributoEstatico.Creditobase  );
	}

}
