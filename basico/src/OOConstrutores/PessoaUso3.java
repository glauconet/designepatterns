package OOConstrutores;

		//CONSTRUTORES

// lembre-se de quando voc� criar uma classe com mais de um construtor,
// voc� pode chamar o construtor que quiser

public class PessoaUso3 {

	public static void main(String[] args) {
		
		Pessoa p1 = new Pessoa();
		Pessoa p2 = new Pessoa("S�rgio");
		Pessoa p3 = new Pessoa("Daniele",(byte) 22, 'F');
		Pessoa p4 = new Pessoa("Adilson",(byte) 22,(float)1.70,(float) 68, 'M');
		
				
			
		
		p1.setNome("Oberdan");
		p1.setIdade((byte)24);
		p1.setSexo('M');
		p1.setEstatura((float)1.6);
		p1.setPeso((float)70.5);
		p1.endereco = new Endereco(); // isso � agrega��o
		p1.telefone = new Telefone();
		p1.endereco.setLogradouro(" Rua Bingo");
		p1.endereco.setNumero(1);
		p1.telefone.setNumeroCasa("12345");
		p1.telefone.setNumeroCelular("55556666");
		
		
		p2.setIdade((byte)33);
		p2.setSexo('F');
		p2.setEstatura((float)1.5);
		p2.setPeso((float)55.5);
		
		p3.setEstatura((float)1.65);
		p3.setPeso((float)55.5);
		
		System.out.println("_____TABELA DE PESSOAS_______ ");
		System.out.println("________________________ ");
		System.out.println("Nome     	" + p1.getNome());
		System.out.println("Idade    	" + p1.getIdade());
		System.out.println("Sexo     	" + p1.getSexo());
		System.out.println("Estatura 	" + p1.getEstatura());
		System.out.println("Peso     	" + p1.getPeso());
		System.out.println("Endereco	" + p1.endereco.getLogradouro() + " n� " + p1.endereco.getNumero());
		System.out.println("Tel Celular " + p1.telefone.getNumeroCasa() + "  Tel Casa  " + p1.telefone.getNumeroCelular());
		
		
		System.out.println("________________________");
		System.out.println("Nome     	" + p2.getNome());
		System.out.println("Idade    	" + p2.getIdade());
		System.out.println("Sexo     	" + p2.getSexo());
		System.out.println("Estatura 	" + p2.getEstatura());
		System.out.println("Peso     	" + p2.getPeso());
		System.out.println("________________________ ");
		System.out.println("Nome     	" + p3.getNome());
		System.out.println("Idade    	" + p3.getIdade());
		System.out.println("Sexo     	" + p3.getSexo());
		System.out.println("Estatura 	" + p3.getEstatura());
		System.out.println("Peso     	" + p3.getPeso());
		System.out.println("________________________ ");
		System.out.println("Nome     	" + p4.getNome());
		System.out.println("Idade    	" + p4.getIdade());
		System.out.println("Sexo     	" + p4.getSexo());
		System.out.println("Estatura 	" + p4.getEstatura());
		System.out.println("Peso     	" + p4.getPeso());
	}

}
