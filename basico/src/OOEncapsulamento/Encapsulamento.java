package OOEncapsulamento;
import javax.swing.JOptionPane;

public class Encapsulamento {

	
	public static void main(String[] args) {
		
		String st = "Encapsulamento";
		
		st = st + "\n� utilizado para definir diferente n�veis de visibilidade para atributos e m�todos de uma classe.\n";
		
		
		st = st + "\n Diretiva Public";
		st = st + "\n Deixa atributos e m�todos livres parra serem acessados diretamente por qualquer classe\n";
		
		st = st + "\n Diretiva Private";
		st = st + "\n Deve-se ent�o criar metodos getters e setters publicos para intermedia a atribui��o e recupera��o";
		st = st + "\n Dentro da pr�pria classe podem ser acessados diretamente\n";
		
		st = st + "\n Diretiva Protected";
		st = st + "\n Sao acessiveis somente na propria classe, em classes especializadas e em classes do mesmo pacote";
		JOptionPane.showMessageDialog(null, st);
		
		
		

	}

}
