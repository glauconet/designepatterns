package PatternDesign.AbstractFactory;
//� Classe AbstractFactoryMontadora 
import modulo09.factorycarro.ExemploAutomovel;
public abstract class AbstractFactoryMontadora {
	protected String name;
	public abstract ExemploAutomovel getAutomovel(String marca);
	public static AbstractFactoryMontadora getFabrica(String montadora) {
			if (montadora == null) {
				return null;
			} else if (montadora.equals("Chevrolet")) {
				return new ExemploChevroletFactory();
			} else if (montadora.equals("Volkswagen")) {
				return new ExemploVolkswagenFactory();
			} else {
				return null;
			}
	}
}

