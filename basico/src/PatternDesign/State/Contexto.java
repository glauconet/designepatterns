package PatternDesign.State;

public class Contexto {
	
	 private State estado_atual;

	    public Contexto (){
	    	estado_atual = new Off();
	    }
	    
	    public void set_state(State s){
	    	estado_atual = s;
	    }
	    
	    public void pull(){
	    	   	estado_atual.pull(this);
	    }


}
