package PatternDesign.State;

public class Eletronico {
	
	private int estado_atual;

    public Eletronico() {
    	estado_atual = 0;
    }
    public void pull(){
    	
        if (estado_atual == 0){
        	
        	estado_atual = 1;
            System.out.println("baixa velocidade");
            
        }else if(estado_atual == 1){   
        	
        	estado_atual = 2;
            System.out.println("media velocidade");
            
        }else if(estado_atual == 2){
        	
        	estado_atual = 3;
            System.out.println("alta velocidade");
            
        }else{
        	
        	estado_atual = 0;
            System.out.println("turning velocidade");
        }
    }
}
