package PatternDesign.State;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class EletronicoDemo{
	public static void main(String[] args){
		
		Eletronico eletronico = new Eletronico();
        
        while (true){
        	
            System.out.print("Aperte ");
            
            get_line();
            
            eletronico.pull();
        }
    }
    static String get_line(){
    	
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
                
        String line = null;
        
        try {
            line = in.readLine();
            
        } catch (IOException ex){
        	
            ex.printStackTrace();
        }
        return line;
    }
}
