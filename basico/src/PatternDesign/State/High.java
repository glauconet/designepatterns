package PatternDesign.State;

public class High implements State {
	
	
	@Override
	public void pull(CeilingFanApertarSerie contexto) {
		
		contexto.set_state(new Off());
		
		System.out.println(" turning off");
	}

}
