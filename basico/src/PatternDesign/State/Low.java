package PatternDesign.State;

public class Low implements State {

	@Override
	public void pull(CeilingFanApertarSerie contexto) {
		
		contexto.set_state(new Medium());
		
		System.out.println(" Medium speed");
	}

}
