package PatternDesign.State;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class StateDemoApertarSerie {

	 public static void main(String[] args)
	    {
		 CeilingFanApertarSerie chain = new CeilingFanApertarSerie();
	        
		 while (true){
	            System.out.print("Press ");
	            get_line();
	            chain.pull();
	        }
	    }
	    static String get_line()
	    {
	        BufferedReader in = new BufferedReader(new InputStreamReader(System.in))
	          ;
	        String line = null;
	        try
	        {
	            line = in.readLine();
	        }
	        catch (IOException ex)
	        {
	            ex.printStackTrace();
	        }
	        return line;
	    }

	
	
}
