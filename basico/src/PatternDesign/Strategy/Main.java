package PatternDesign.Strategy;


public class Main {

	public static void main(String[] args) {

		Robot r1 = new Robot("ROBOT 1");
		Robot r2 = new Robot("ROBOT 2");
		Robot r3 = new Robot("ROBOT 3");

		r1.setBehaviour(new AgressiveBehaviour());// foi passado uma classe de comportamento
		r2.setBehaviour(new DefensiveBehaviour());
		r3.setBehaviour(new NormalBehaviour());

		r1.move();System.out.println(" ");
		r2.move();System.out.println(" ");
		r3.move();

		
		System.out.println(" ");System.out.println(" ");
		System.out.println("Novos comportamentos ");
		System.out.println(" ");System.out.println(" ");
		
		r1.setBehaviour(new DefensiveBehaviour());
		r2.setBehaviour(new AgressiveBehaviour());
		r3.setBehaviour(new DefensiveBehaviour());
		
		r1.move();System.out.println(" ");
		r2.move();System.out.println(" ");
		r3.move();
		
	}
}