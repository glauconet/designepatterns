package PatternDesign.Strategy;


public class NormalBehaviour implements IBehaviour{
	
	public int moveCommand(){
		
		System.out.println("Comportamento Normal:");
		System.out.println("Se eu encontrar outro robot, eu ignoro");
		return 0;
		
	}
}