package PatternDesign.Strategy_robo;

public class DefensiveBehaviour implements IBehaviour{

	public int moveCommand(){
		
		System.out.println("Comportamento Defensivo:");
		System.out.println("Se eu encontrar outro robot, eu fujo dele!");
		return -1;
	}

}