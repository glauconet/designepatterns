package PatternDesign.Strategy_robo;

public class Robot {
	
	IBehaviour behaviour; String name;

	public Robot(String name){
		this.name = name;
	}

	public void setBehaviour(IBehaviour behaviour){
		this.behaviour = behaviour;
	}

	public IBehaviour getBehaviour(){
		return behaviour;
	}

	public void move(){
		
		System.out.println(this.name); 
				
		int command = behaviour.moveCommand();
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}