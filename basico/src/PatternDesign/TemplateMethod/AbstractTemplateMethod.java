package PatternDesign.TemplateMethod;

public abstract class AbstractTemplateMethod {
	 
    public final void templateMethod() {
            System.out.println("AbstractClass.templateMethod() called");
            System.out.println("");
            primitiveOperation1();
            primitiveOperation2();
    }

    public abstract void primitiveOperation1();
    public abstract void primitiveOperation2();
  
    
}

