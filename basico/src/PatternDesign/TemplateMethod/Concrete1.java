package PatternDesign.TemplateMethod;

public class Concrete1 extends AbstractTemplateMethod {

	
	public void primitiveOperation1() {
		System.out.println("Concrete1.primitiveOperation1() called");
		
	}

	
	public void primitiveOperation2() {
		System.out.println("Concrete1.primitiveOperation2() called");
		
	}

}
