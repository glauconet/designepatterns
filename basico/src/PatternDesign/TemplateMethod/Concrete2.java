package PatternDesign.TemplateMethod;

public class Concrete2 extends AbstractTemplateMethod {

	
	public void primitiveOperation1() {
		System.out.println("Concrete2.primitiveOperation1() called");
		
	}

	
	public void primitiveOperation2() {
		System.out.println("Concrete2.primitiveOperation2() called");
		
	}

}
