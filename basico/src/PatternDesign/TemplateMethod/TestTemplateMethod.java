package PatternDesign.TemplateMethod;

public class TestTemplateMethod {
	
	public static void main(String[] args){
		
        System.out.println("Test TemplateMethod");
        System.out.println("-------------------------");

        AbstractTemplateMethod class1 = new Concrete1();
        AbstractTemplateMethod class2 = new Concrete2();

        class1.templateMethod();
        System.out.println("");
        class2.templateMethod();
       
	}
}
