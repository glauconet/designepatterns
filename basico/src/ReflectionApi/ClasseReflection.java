package ReflectionApi;
import java.lang.reflect.Method;

public class ClasseReflection {
		
		public static void main(String[] args) {
			try {
				Class c = Class.forName("java.io.InputStream");
				
				
				/*
				 * o método getDeclaredMethods retorna o nome de todos os métodos da
				 * classe InputStream.
				 */
				Method m[] = c.getDeclaredMethods();
				
				for (int i = 0; i < m.length; i++) {
					System.out.println(m[i].toString());
				}
				
				
				
			} catch (ClassNotFoundException e) {
				System.err.println(e);
			}
		}
}
