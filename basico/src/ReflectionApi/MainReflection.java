package ReflectionApi;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.Method;
public class MainReflection {

	
		public static void main(String[] args) {
			ExemploClassEmpregado empregado = new ExemploClassEmpregado();
			// retorna um objeto em tempo de execução do tipo da classe do objeto
			// empregado
			Class detalheClasse = empregado.getClass();
			System.out.println("Nome do pacote e da classe: "
					+ detalheClasse.getName());
			System.out.println("Nome da superclasse: "
					+ detalheClasse.getSuperclass());
			System.out.println("Nome da classe: " + detalheClasse.getSimpleName());
			int mod = detalheClasse.getModifiers();
			System.out.println("Classe é public: " + Modifier.isPublic(mod));
			System.out.println("Classe é final: " + Modifier.isFinal(mod));
			System.out.println("Classe é abstract: " + Modifier.isAbstract(mod));
			System.out.println("É interface: " + Modifier.isInterface(mod));
			System.out
					.format(
							"Lista todos os modificadores com status de true de uma classe: %n %s%n%n",
							Modifier.toString(detalheClasse.getModifiers()));
			System.out.println("Listando os atributos da classe:");
			Field atributos[] = detalheClasse.getFields();
			for (int i = 0; i < atributos.length; i++) {
				System.out.println(atributos[i]);
			}
			
		}
	}
