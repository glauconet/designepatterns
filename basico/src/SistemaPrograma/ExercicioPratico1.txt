package SistemaPrograma;

import javax.swing.JOptionPane;

public class ExercicioPratico1 {

	
	public static void main(String[] args) {
	
		
		String c, d;
		
		int a, b, e;
		
	
		c = JOptionPane.showInputDialog(null, "Digite o valor de A"); 	
		d = JOptionPane.showInputDialog(null, "Digite o valor de B"); 
		a = Integer.parseInt(c);
		b = Integer.parseInt(d);
		
		
		System.out.println("Valor de a: " + a);
		System.out.println("Valor de b: " + b);
		
		e = a;
		a = b;
		b = e;
		
		System.out.println("Valor de a: " + a);
		System.out.println("Valor de b: " + b);
		
		

	}

}



Resultado:

Valor de a: 6
Valor de b: 8
Valor de a: 8
Valor de b: 6