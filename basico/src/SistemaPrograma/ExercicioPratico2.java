package SistemaPrograma;
import javax.swing.JOptionPane;
public class ExercicioPratico2 {
	public static void main(String[] args) {
		int inicial, segundos, horas, minutos;
		horas = 0; 	minutos = 0; segundos = 0; String recebe;
		recebe = JOptionPane.showInputDialog(null, "Digite uma quantidade de segundos"); 	
		inicial = Integer.parseInt(recebe);
		segundos = inicial;
		
			if (inicial < 3600){
				minutos = segundos/60;
				segundos = segundos - (minutos*60); 
			}else{
				horas = segundos/3600;
				segundos = segundos - (horas*3600);
				
				if(segundos >= 60){
					minutos = segundos/60;
					segundos = segundos - 60;
				}
			}	
		System.out.println(inicial + " segundos equivalem a " + horas + " hora(s), " + minutos +  " minuto(s) e "+segundos + " segundo(s)");		
	}
}
