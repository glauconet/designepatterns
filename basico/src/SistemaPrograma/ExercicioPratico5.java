package SistemaPrograma;
import javax.swing.JOptionPane;

public class ExercicioPratico5{
	   int A,B,C;
	   String  a, b;
	   
	public static void main(String[] argrandeNome) {
		ExercicioPratico5 ep5 = new ExercicioPratico5();
						
		ep5.a = JOptionPane.showInputDialog(null, "Digite o valor de A"); 	
		ep5.A = Integer.parseInt(ep5.a);
		
		ep5.b = JOptionPane.showInputDialog(null, "Digite o valor de B"); 	
		ep5.B = Integer.parseInt(ep5.b);
							
		if (ep5.A%2 !=0 &&  ep5.B%2 !=0 ){
			ep5.C = ep5.A + ep5.B;
			JOptionPane.showMessageDialog(null, " Soma de A e B = " + ep5.C);
		}
		
		if (ep5.A%2 ==0 &&  ep5.B%2 ==0 ){
			ep5.C = ep5.A * ep5.B;
			JOptionPane.showMessageDialog(null, " Multiplicação de A e B = " + ep5.C);
		}
		
		if(ep5.A%2 != 0  && ep5.B%2 ==0 ){
			JOptionPane.showMessageDialog(null, " Número impar " + ep5.A);
			
		}
        if(ep5.B%2 !=0 && ep5.A%2 == 0 ) {
        	JOptionPane.showMessageDialog(null, " Número impar " + ep5.B);
			
		}
		
	}
}