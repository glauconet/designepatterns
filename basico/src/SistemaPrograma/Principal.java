package SistemaPrograma;

// Resposta do Laboratório 1
import java.util.Scanner;
// A classe Console precisa estar disponível no pacote modulo01.estudodecaso.util.

public class Principal {
	int numAge;
	
	int numConta;
	
	double valor;
	
	String cliente;
	
	private void leValores() {
		
		do {
		this.numAge = Console.readInt("Número da agência: ");
		} while (this.numAge <= 0);
		
		do {
		this.numConta = Console.readInt("Número da conta: ");
		} while (this.numConta <= 0);
		
		do {
		this.valor = Console.readDouble("Valor: ");
		} while (this.valor <= 0.0);
		
		//do {
			//this.cliente = Console.readString("Cliente: ");
		//} while (this.cliente == null);
			
	
	}

	public void execCadastramento(){
	String nome = null;
	// usando a classe Scanner como uma opção de leitura
	Scanner sc = new Scanner(System.in);
	
	char opcao;
	
	leValores();
	
		do {
		System.out.println("Nome do cliente: ");
		
		this.cliente = sc.nextLine();
		
		} while (this.cliente.equals(""));
	
	opcao = Console.readChar("Confirma cadastramento (S/N): ");
		if ((opcao == 'S') || ((opcao == 's'))) {
		System.out.println("Cadastramento realizado com sucesso.");
		} else {
		System.out.println("Cadastramento não realizado.");
		}
	}
	
	public void execSaque(){
		
	char opcao;
	
	leValores();
	
	opcao = Console.readChar("Confirma saque (S/N): ");
	
		if ((opcao == 'S') || ((opcao == 's'))) {
			System.out.println("Saque efetuado com sucesso.");
		}else{
			System.out.println("Saque não realizado.");
		}
	}

	public void execDeposito() {
	char opcao;
	leValores();
	opcao = Console.readChar("Confirma depósito (S/N): ");
		if ((opcao == 'S') || ((opcao == 's'))){
			
			System.out.println("Depósito efetuado com sucesso.");
			
		}else{
			
		System.out.println("Depósito não realizado.");
		
		}
	}
	
	public void getSaldo(){
		System.out.println(this.numAge);
		System.out.println(this.numConta);
		System.out.println(this.valor);
		System.out.println(this.cliente);
	}
	
	
	public static void main(String[] args) {
		
		
	char opcao;
	
	Principal obj = new Principal();
	
		while (true) {
			
			System.out.println("Entre com a opção desejada");
			System.out.println("1 - Cadastramento");
			System.out.println("2 - Saque");
			System.out.println("3 - Depósito");
			System.out.println("4 - Saldo");
			System.out.println("9 - Fim");
			
			opcao = Console.readChar("Opção: ");
			
			if (opcao == '9'){
				break;
			}
			switch (opcao) {
				case '1':
				obj.execCadastramento();
				break;
				case '2':
				obj.execSaque();
				break;
				case '3':
				obj.execDeposito();
				break;
				case '4':
					obj.getSaldo();
					break;
				default:
				System.out.println("Opção inválida. Reentre.");
			}
		}
	}
}