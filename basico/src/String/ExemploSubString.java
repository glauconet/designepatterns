package String;

// Exemplo do uso do m�todo substring
public class ExemploSubString {
	public static void main(String[] args) {
		String minhaVariavel = "Antonio Carlos";
		// ir� imprimir a partir do 2� byte at� o 4� byte = to
		System.out.println(minhaVariavel.substring(2, 4));
	}
}