package String;

// Exemplo do uso do m�todo est�tico valueOf
public class ExemploValueOf {
	public static void main(String[] args) {
		double valor = 100.0;
		int numero = 27;
		String strValor = String.valueOf(valor);
		String strNum = String.valueOf(numero);
		System.out.println("strValor: " + strValor);
		System.out.println("strNum: " + strNum);
	}
}