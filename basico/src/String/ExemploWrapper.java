package String;

// Exemplo do uso dos m�todos das classes Wrappers
public class ExemploWrapper {
	public static void main(String[] args) {
		String strNumReal = "100";
		// retorna um objeto do tipo Double
		Double numReal = Double.valueOf(strNumReal);
		// retorna um tipo primitivo do tipo double
		double numRealDoubleValue = Double.valueOf(strNumReal).doubleValue();
		// tamb�m retorna um tipo primitivo do tipo double
		double numRealParse = Double.parseDouble(strNumReal);
		System.out.println("numReal: " + numReal);
		System.out.println("numRealDoubleValue: " + numRealDoubleValue);
		System.out.println("numRealParse: " + numRealParse);
		String strNum = "50";
		// retorna um objeto do tipo Integer
		Integer num = Integer.valueOf(strNum);
		// rRetorna o tipo primitivo int
		int numIntValue = Integer.valueOf(strNumReal).intValue();
		// tamb�m retorna o tipo primitivo int
		int numParse = Integer.parseInt(strNum);
		System.out.println("num: " + num);
		System.out.println("numIntValue: " + numIntValue);
		System.out.println("numParse: " + numParse);
	}
}