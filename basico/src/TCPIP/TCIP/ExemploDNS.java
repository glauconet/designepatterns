package TCPIP.TCIP;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class ExemploDNS {

	public static void main(String[] args) {
		String host = "www.uol.com.br";

		try {
			InetAddress[] addressList = InetAddress.getAllByName(host);
			for (InetAddress address : addressList) {
				System.out.println(address.getHostName() + "/" + address.getHostAddress());
			}
		} catch (UnknownHostException e) {
			System.out.println("Não foi possível localizar endereço para " + host);
		}
	}

}
