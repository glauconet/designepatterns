package TCPIP.TCIP;
import java.io.*;
import java.net.*;

public class ExemploEmail {

	private static PrintWriter saida = null;
	private static BufferedReader entrada = null;
	
	public static void send(String str) throws IOException {
		saida.println(str);
		saida.flush();
		System.out.println("Enviado: " + str);
	}

	public static void receive() throws IOException {
		String readstr = entrada.readLine();
		System.out.println("Resposta: " + readstr);
	}

	public static void main(String args[]) {
		Socket smtp = null; 
		try { 
			smtp = new Socket("relayintra.bsa.serpro", 25); // 25 é a porta default de SMTP
		    saida = new PrintWriter(smtp.getOutputStream());
		    entrada = new BufferedReader(new InputStreamReader(smtp.getInputStream()));
		} catch (IOException e) {
			System.out.println("Erro conectando: " + e);
		}
		try { 
			send("HELO " + InetAddress.getLocalHost().getHostName());
			receive();
			send("MAIL From: <gisele@bundchen.net>");
			receive();
			send("RCPT To: <francisco.mendes@serpro.gov.br>");
			receive();
			send("DATA");
			receive();
			send("Subject: Oi!");
			receive();
			send("vamos almoçar?");
			send(".");
			receive();
		} catch (IOException e) {
			System.out.println("Erro ao enviar: " + e);
		}
		System.out.println("Mensagem enviada com sucesso!");
	}
}
