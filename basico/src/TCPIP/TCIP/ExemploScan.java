package TCPIP.TCIP;
import java.io.IOException;
import java.net.Socket;

public class ExemploScan {
	public static void main(String[] args) {
		String server = "localhost";
		for (int port = 0; port < 65536; port++) {
			try {
				Socket s = new Socket(server, port);
				System.out.println("Há um serviço na porta " + port + " de " + server);
				s.close();
			} catch (IOException e) {
				// O servidor remoto não está ouvindo esta porta
			}
		}
	}
}
