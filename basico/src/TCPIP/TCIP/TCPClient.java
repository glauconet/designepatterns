package TCPIP.TCIP;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class TCPClient {

	public static void main(String[] args) {
		String server = "localhost";       // nome ou ip do servidor
	    int servPort = 1313;

		try {
			// Cria socket com Servidor
			//Socket socket = new Socket(server, servPort, InetAddress.getLocalHost(), 1414);
			Socket socket = new Socket(server, servPort);
			System.out.println("Conectado ao servidor " + socket);

			// Manipula nova conexão
			BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintStream out = new PrintStream(socket.getOutputStream());

			// Envia mensagem
			out.println("hora");
			System.out.println("enviada mensagem ao servidor");
		
			// Recebe resposta
			System.out.println("resposta recebida => " + in.readLine());
		
			// Fecha socket
			socket.close();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
