package TCPIP.TCIP;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class TCPClient2 {

	public static void main(String[] args) throws UnknownHostException, IOException {
		String server = "10.15.5.88";       // nome ou ip do servidor
	    int servPort = 1313;

	    // Cria socket com Servidor
	    Socket serverSocket = new Socket(server, servPort);
	    System.out.println("Conectado ao servidor " + serverSocket);

		// Manipula nova conexão
		BufferedReader in = new BufferedReader(new InputStreamReader(serverSocket.getInputStream()));
		PrintStream out = new PrintStream(serverSocket.getOutputStream());

		// Envia mensagem
		out.println("data");
		System.out.println("enviada mensagem ao servidor");
		
		// Recebe resposta
		System.out.println("resposta recebida => " + in.readLine());

		// Envia mensagem
		out.println("hora");
		System.out.println("enviada mensagem ao servidor");
		
		// Recebe resposta
		System.out.println("resposta recebida => " + in.readLine());

		// Envia mensagem
		out.println("tchau");
		System.out.println("enviada mensagem ao servidor");
		
		// Recebe resposta
		System.out.println("resposta recebida => " + in.readLine());
		
		// Fecha socket
		serverSocket.close();
	}

}

