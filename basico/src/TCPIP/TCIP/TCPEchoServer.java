package TCPIP.TCIP;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;

public class TCPEchoServer {

	private static final int BUFSIZE = 32; // Tamanho do buffer

	public static void main(String[] args) throws IOException {

		int servPort = 1313;

		// Cria server socket para aceitar requisições de conexão de clientes
		ServerSocket serverSocket = new ServerSocket(servPort);

		int recvMsgSize; // Tamanho da mensagem recebida
		byte[] receiveBuf = new byte[BUFSIZE]; // Recebe buffer

		while (true) { // Fica em loop, aceitanto e servindo conexões
			Socket socket = serverSocket.accept(); // Obtem socket com cliente
			
			SocketAddress clientAddress = socket.getRemoteSocketAddress();
			System.out.println("Manipulando cliente em " + clientAddress);

			InputStream in = socket.getInputStream();
			OutputStream out = socket.getOutputStream();

			// Recebe até cliente fechar conexão, indicada pelo retorno -1
			while ((recvMsgSize = in.read(receiveBuf)) != -1) {
				byte[] reverseBuf = new StringBuffer(new String(receiveBuf)).reverse().toString().trim().getBytes();
				out.write(reverseBuf, 0, recvMsgSize);
			}

			socket.close(); // Fecha o socket. Trabalho terminado para esse cliente!
		}

	}

}
