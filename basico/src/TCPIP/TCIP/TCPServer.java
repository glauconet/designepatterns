package TCPIP.TCIP;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TCPServer {

	public static void main(String[] args) {
		int echoServPort = 1313;
		try {
			ServerSocket serverSocket = new ServerSocket(echoServPort);
	
			// Executa em loop, aceitando conexão e iniciando socket para comunicação
			while (true) {
				// Block esperando por conexão
				Socket socket = serverSocket.accept();
				System.out.println("atendendo cliente em " + socket);
	
				// Manipula nova conexão
				BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				PrintStream out = new PrintStream(socket.getOutputStream());
	
				String msg = in.readLine();
				System.out.println("recebida mensagem do cliente => " + msg);
	
				if (msg.contains("data")) {
					out.println(now("dd/MM/yy"));
				} else if (msg.contains("hora")) {
					out.println(now("hh:mm:ss"));
				} else {
					out.println("?");
				}
				System.out.println("resposta enviada ao cliente");
	
				socket.close();
			} 
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	private static String now(String formato) {
		return new SimpleDateFormat(formato).format(Calendar.getInstance().getTime());
	}

}
