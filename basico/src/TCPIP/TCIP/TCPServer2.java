package TCPIP.TCIP;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TCPServer2 {

	public static void main(String[] args) throws IOException {
		int echoServPort = 1313;
		ServerSocket serverSocket = new ServerSocket(echoServPort);
		System.out.println("servidor ativo...");

		// Executa em loop, aceitando conexão e iniciando thread para trata-la
		block:
		while (true) {
			// Block esperando por conexão
			Socket socket = serverSocket.accept();
			System.out.println("atendendo cliente em " + socket);

			// Manipula nova conexão
			BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintStream out = new PrintStream(socket.getOutputStream());

			boolean fimProtocolo = false;
			do {
				String msg = in.readLine();
				System.out.println("recebida mensagem do cliente => " + msg);
				
				if (msg == null) {
					continue block;
				}

				if (msg.equals("data")) {
					out.print(now("dd/MM/yy"));
					
				} else if (msg.equals("hora")) {
					out.print(now("hh:mm:ss"));
					
				} else if (msg.equals("tchau")) {
					out.print("bye");
					fimProtocolo = true;
				
				} else {
					out.print("?");
				}
				System.out.println("resposta enviada ao cliente");

			} while (!fimProtocolo);

			out.close();
			socket.close();
		}

	}

	private static String now(String formato) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(formato);
		return sdf.format(cal.getTime()) + "\n";
	}

}