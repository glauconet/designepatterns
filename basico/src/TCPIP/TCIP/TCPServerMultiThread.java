package TCPIP.TCIP;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPServerMultiThread {

	public static void main(String[] args) throws IOException {

		int serverPort = 1313;
		ServerSocket serverSocket = new ServerSocket(serverPort);
		System.out.println("servidor multithread ativo...");

		// Executa em loop, aceitando conexão e iniciando uma thread para cada uma
		while (true) {
			// Block esperando por conexão
			Socket socket = serverSocket.accept();
			
			// Inicia thread para manipular novo socket
			TCPServerWorker worker = new TCPServerWorker(socket);
			Thread thread = new Thread(worker);
			thread.start();
		}
	}
}
