package TCPIP.TCIP;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPServerSingleThread {

	public static void main(String[] args) throws IOException {

		int serverPort = 1313;
		ServerSocket serverSocket = new ServerSocket(serverPort);
		System.out.println("servidor singlethread ativo...");

		// Executa em loop, aceitando conexão e delegando o tratamento
		while (true) {
			// Block esperando por conexão
			Socket socket = serverSocket.accept();
			
			// Delega para manipular novo socket
			TCPServerWorker worker = new TCPServerWorker(socket);
			worker.trataMensagem();
		}
	}
}
