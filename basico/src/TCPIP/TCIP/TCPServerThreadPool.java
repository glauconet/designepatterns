package TCPIP.TCIP;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPServerThreadPool {

	public static void main(String[] args) throws IOException {

		int threadPoolSize = 3;
		int echoServPort = 1313;
		final ServerSocket serverSocket = new ServerSocket(echoServPort);

		// Inicia um número determinado de threads para manipular sockets
		for (int i = 0; i < threadPoolSize; i++) {
			Thread thread = new Thread() {
				public void run() {
					System.out.println("servidor threadpool " + Thread.currentThread().getName() + " ativo...");
					while (true) {
						try {
							// Block esperando por conexão
							Socket socket = serverSocket.accept();
							TCPServerWorker worker = new TCPServerWorker(socket);
							worker.trataMensagem();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			};
			thread.start();
		}
	}
}
