package TCPIP.TCIP;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class TCPServerWorker implements Runnable  {

	Socket socket = null;
	
	public TCPServerWorker(Socket socket) {
		this.socket = socket;
	}
	
	@Override
	public void run() {
		trataMensagem();
	}
	
	public void trataMensagem() {
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintStream out = new PrintStream(socket.getOutputStream());

			String get = in.readLine();

			out.println("HTTP/1.0 200 OK");
			out.println("Server: CursoJava FakeHTTP");
			out.println("Connection: close");
			out.println("Content-type: text/html");
			out.println();

			out.println("voce foi atendido por: " + Thread.currentThread().getName());
			out.println("<p>recebido: " + get);
			out.println("<p>localport: " + socket.getLocalPort());
			out.println("<p>port: " + socket.getPort());
					
			out.close();
			socket.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
