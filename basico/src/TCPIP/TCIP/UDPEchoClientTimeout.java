package TCPIP.TCIP;
import java.net.DatagramSocket;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.io.IOException;
import java.io.InterruptedIOException;

public class UDPEchoClientTimeout {

	private static final int TIMEOUT = 3000; // Timeout para reenvio (milisegundos)
	private static final int MAXTRIES = 5;   // Número máximo de retransmissões

	public static void main(String[] args) throws IOException {

		InetAddress serverAddress = InetAddress.getByName("localhost"); // Endereço do servidor
		int servPort = 1414;

		byte[] bytesToSend = "batatinha".getBytes();

		DatagramSocket socket = new DatagramSocket();
		socket.setSoTimeout(TIMEOUT); // Tempo máximo de espera (milisegundos)

		// Packet de envio
		DatagramPacket sendPacket = new DatagramPacket(bytesToSend, bytesToSend.length, serverAddress, servPort);

		// Packet de recepção
		DatagramPacket receivePacket = new DatagramPacket(new byte[bytesToSend.length], bytesToSend.length);

		int tries = 0; // Packets podem ser perdidos, portanto devemos ficar tentando
		boolean receivedResponse = false;
		do {
			socket.send(sendPacket); // Envia os dados
			try {
				socket.receive(receivePacket); // Tenta obter resposta
				
				// Verifica a origem da resposta
				if (!receivePacket.getAddress().equals(serverAddress)) { 
					throw new IOException("Recebido packet de fonte desconhecida");
				}
				receivedResponse = true;
			} catch (InterruptedIOException e) { // Se demorar demais
				tries += 1;
				System.out.println("Timed out, " + (MAXTRIES - tries) + " novas tentativas...");
			}
		} while ((!receivedResponse) && (tries < MAXTRIES));

		if (receivedResponse) {
			System.out.println("Recebido: "	+ new String(receivePacket.getData()));
		} else {
			System.out.println("Sem responta. desistindo.");
		}
		socket.close();
	}
}
