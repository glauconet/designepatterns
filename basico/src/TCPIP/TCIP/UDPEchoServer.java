package TCPIP.TCIP;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class UDPEchoServer {

  private static final int ECHOMAX = 255; // Tamanho máximo do datagrama

  public static void main(String[] args) throws IOException {

    int servPort = 1414;

    DatagramSocket socket = new DatagramSocket(servPort);
    DatagramPacket packet = new DatagramPacket(new byte[ECHOMAX], ECHOMAX);

    while (true) { // Fica em loop, recebendo e ecoando datagramas
      socket.receive(packet); // Recebe packet do cliente
	  byte[] reverseBuf = new StringBuffer(new String(packet.getData())).reverse().toString().trim().getBytes();
	  packet.setData(reverseBuf);
      System.out.println("Manipulando cliente em " + packet.getAddress().getHostAddress() + " na porta " + packet.getPort());
      socket.send(packet); // Envia pacote (inverto) de volta ao cliente
    }
  }
}

