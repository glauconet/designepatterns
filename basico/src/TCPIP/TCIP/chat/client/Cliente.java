package TCPIP.TCIP.chat.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import javax.swing.JOptionPane;

public class Cliente {
	public static void main(String[] args) {
		String IP = JOptionPane.showInputDialog("IP Destino", "localhost");
		String PORTA = JOptionPane.showInputDialog("Porta Destino", "2000");
		int porta = Integer.parseInt(PORTA);
		try {
			Socket s = new Socket(IP, porta);// 10.15.5.88
			PrintWriter socketPrinter = new PrintWriter(s.getOutputStream(), true);
			BufferedReader socketReader = new BufferedReader(new InputStreamReader(s.getInputStream()));
			String linhaUsuario;
			Scanner scanner = new Scanner(System.in);

			while (true) {
				linhaUsuario = scanner.nextLine();
				System.out.println("Cliente: " + linhaUsuario);
				socketPrinter.println(linhaUsuario);
				if (linhaUsuario.equals("bye")) {
					socketPrinter.close();
					socketReader.close();
					s.close();
					break;
				}
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
