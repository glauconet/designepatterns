package TCPIP.TCIP.chat.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerThread {
	public static void main(String[] args) {
		try {
			ServerSocket servidor = new ServerSocket(2000);
			Socket cliente;
			while (true) {
				cliente = servidor.accept();
				System.out.println("Cliente " + cliente.getInetAddress() + " entrou na sala");
				ServerWorker worker = new ServerWorker(cliente);
				Thread t = new Thread(worker);
				t.start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
