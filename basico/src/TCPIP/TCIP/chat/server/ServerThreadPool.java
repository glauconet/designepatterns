package TCPIP.TCIP.chat.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerThreadPool {

	public static void main(String[] args) throws IOException {
		int poolSize = 3;
		final ServerSocket servidor = new ServerSocket(2000);

		for (int i = 0; i < poolSize; i++) {
			Thread t = new Thread() {
				public void run() {
					while (true) {
						try {
							Socket cliente = servidor.accept();
							System.out.println("Cliente " + cliente.getInetAddress() + " entrou na sala");
							ServerWorker worker = new ServerWorker(cliente);
							worker.trataMensagem();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}

				}
			};
			t.start();
		}
	}
}