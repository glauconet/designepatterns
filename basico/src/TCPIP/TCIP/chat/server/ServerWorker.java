package TCPIP.TCIP.chat.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ServerWorker implements Runnable {

	private Socket cliente;

	public ServerWorker(Socket c) {
		this.cliente = c;
	}

	@Override
	public void run() {
		trataMensagem();
	}

	public void trataMensagem() {
		try {
			PrintWriter out;
			BufferedReader in;
			out = new PrintWriter(cliente.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(cliente.getInputStream()));
			String linha;
			while (!(linha = in.readLine()).equals("bye")) {
				System.out.println("Cliente " + cliente.getInetAddress() + ": " + linha);
			}
			System.out.println("Cliente " + cliente.getInetAddress() + " saiu da sala");
			cliente.close();
			out.close();
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
