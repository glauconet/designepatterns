package Threads.concorrencia;
public class Contador {

	private int valor;

//	public synchronized void incrementa() {
	public void incrementa() {
		int v = valor;
		System.out.println(Thread.currentThread().getName() + " pegou valor=" + v);
		valor = v + 1;
	}

	public int getValor() {
		return valor; 
	}

}
