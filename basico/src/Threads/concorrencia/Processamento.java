package Threads.concorrencia;
public class Processamento implements Runnable {

	private Contador c;

	public Processamento(Contador c) {
		this.c = c;
	}

	@Override
	public void run() {
		c.incrementa();
	}

}
