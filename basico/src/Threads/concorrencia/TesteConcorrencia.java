package Threads.concorrencia;
public class TesteConcorrencia {

	public static void main(String[] args) throws InterruptedException {
		Contador c = new Contador();

		for (int i=0; i<100; i++) {
			Processamento p = new Processamento(c);
			Thread t = new Thread(p);
			t.start();
		}
		
		Thread.sleep(1000);
		
		System.out.println("valor final=" + c.getValor());
	}
}
