package Threads.dominio;

public class Coelho extends Competidor {

	@Override
	public void run() {
		
		System.out.println(getNome() + " pronto para largada");
		getCorrida().alinhaCompetidor();
		
		while (!jaChegou()) {
			int loc = getDistanciaPercorrida();
			int rnd = getRandomico();
	
			if (rnd < 30) {
				try {
					Thread.sleep(20);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} else if (rnd < 40) {
				loc += 6;
			} else if (rnd < 60) {
				loc += 8;
			} else if (rnd < 80) {
				loc += 10;
			} else {
				loc += 12;
			}
	
			setDistanciaPercorrida(loc);
			Thread.yield();
		}
	}

	@Override
	public String toString() {
		return "O coelho ";
	}
}
