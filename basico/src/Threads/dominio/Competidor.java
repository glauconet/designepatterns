package Threads.dominio;

import java.util.Random;

public abstract class Competidor implements Runnable {

	private String nome;
	private char simbolo;
	private int distanciaPercorrida = 1;
	private int colocacaoFinal;
	private Corrida corrida = null;
	
	public boolean jaChegou() {
		return (distanciaPercorrida >= corrida.getDistancia());
	}
	
	protected int getRandomico() {
		return new Random().nextInt(99);
//		return (int) (Math.random() * 100);
	}

	public int getDistanciaPercorrida() {
		return distanciaPercorrida;
	}

	protected void setDistanciaPercorrida(int distanciaPercorrida) {
		if (distanciaPercorrida > corrida.getDistancia()) {
			distanciaPercorrida = corrida.getDistancia(); 
		}
		
		this.distanciaPercorrida = distanciaPercorrida;
		corrida.mostraMapa();
	}

	public char getSimbolo() {
		return simbolo;
	}

	public void setSimbolo(char simbolo) {
		this.simbolo = simbolo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Corrida getCorrida() {
		return corrida;
	}
	
	public void setCorrida(Corrida corrida) {
		this.corrida = corrida;
	}

	public int getColocacaoFinal() {
		return colocacaoFinal;
	}

	public void setColocacaoFinal(int colocacaoFinal) {
		this.colocacaoFinal = colocacaoFinal;
	}
}
