package Threads.dominio;

import java.util.ArrayList;
import java.util.List;

public class Corrida {

	private int distancia;
	private List<Competidor> competidores;
	private int ordemChegada = 0;
	private volatile boolean todosProntos = false;

	public Corrida(List<Competidor> competidores, int distancia) {
		this.competidores = competidores;
		this.distancia = distancia;
		
		for (Competidor competidor: this.competidores) {
			competidor.setCorrida(this);
		}
	}
	
	/**
	 *  Quando o competidor iniciar seu método run(), vai sinalizar a Corrida que está pronto.
	 *  Receberá então um wait() para aguardar a largada.
	 */
	public synchronized void alinhaCompetidor() {
		while (!todosProntos) {
			try {
				wait();
			} catch (InterruptedException e) {
			}
		}
	}
	
	public int getDistancia() {
		return distancia;
	}
	
	public void inicia() {
		// cria uma Thread para cada competidor
		List<Thread> linhas = new ArrayList<Thread>();
		for (Competidor competidor: competidores) {
			Thread t = new Thread(competidor);
			linhas.add(t);
		}
		
		// inicia cada Thread.
		for (Thread t: linhas) {
			t.start();
		}
		
		// espera um pouco para todas as threads serem inicializadas
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
		}
		
		// dá a largada!
		System.out.println("Bang!");
		System.out.println("e lá vão eles!");
		mostraMapa();
		synchronized (this) {
			todosProntos = true;
			notifyAll();
		}
		
		// interrompe a Thread principal (esta) até todas
		// as Threads de competidores terminarem
		for (Thread t: linhas) {
			try {
				t.join();
			} catch (InterruptedException e) {
			}
		}

		// mostra resultado final
		System.out.println("\n\nterminou a corrida!");
		for (Competidor competidor: competidores) {
			System.out.println(competidor + competidor.getNome() + " chegou em " 
					+ competidor.getColocacaoFinal() + "º lugar");
		}
	}

	public synchronized void mostraMapa() {
		char[] mapa = new char[distancia + 1];
		
		// monta linha base
		mapa[1] = '[';
		mapa[distancia] = ']';
		for (int i = 2; i < distancia; i++) {
			mapa[i] = '.';
		}
		for (int i = 5; i < distancia; i += 5) {
			mapa[i] = '|';
		}
		
		// monta posição de cada competidor
		for (Competidor competidor: competidores) {
			if ("[.|]".indexOf(mapa[competidor.getDistanciaPercorrida()]) == -1) {
				mapa[competidor.getDistanciaPercorrida()] = 'X';
			} else {	
				mapa[competidor.getDistanciaPercorrida()] = competidor.getSimbolo();
			}
		}
		
		// desenha linha 
		for (int i = 1; i <= distancia; i++) {
			System.out.print(mapa[i]);
		}
		System.out.print("\n");
		
		// verifica se algum competidor chegou
		for (Competidor competidor: competidores) {
			if (competidor.getColocacaoFinal() == 0 && competidor.jaChegou()) {
				System.out.println(competidor + competidor.getNome() + " chegou!");
				ordemChegada++;
				competidor.setColocacaoFinal(ordemChegada);
			}
		}
	}
}
