package Threads.dominio;

public class Pinguim extends Competidor {

	@Override
	public void run() {
		
		System.out.println(getNome() + " pronto para largada");
		getCorrida().alinhaCompetidor();

		while (!jaChegou()) {
			int loc = getDistanciaPercorrida();
			int rnd = getRandomico();
	
			if (rnd < 40) {
				loc += 1;
			} else if (rnd < 50) {
				loc += 3;
			} else if (rnd < 70) {
				loc += 5;
			} else if (rnd < 90) {
				loc += 7;
			} else {
				loc += 9;
			}
	
			setDistanciaPercorrida(loc);
			Thread.yield();
		}
	}

	@Override
	public String toString() {
		return "O pinguim ";
	}

}
