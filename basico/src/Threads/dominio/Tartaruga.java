package Threads.dominio;

public class Tartaruga extends Competidor {

	@Override
	public void run() {
		
		System.out.println(this.getNome() + " pronto para largada");
		this.getCorrida().alinhaCompetidor();

		while (!this.jaChegou()) {
			int loc = this.getDistanciaPercorrida();
			int rnd = this.getRandomico();
			
			if (rnd < 20) {
				loc += 1;
			} else if (rnd < 50) {
				loc += 2;
			} else if (rnd < 60) {
				loc += 3;
			} else if (rnd < 80) {
				loc += 4;
			} else {
				loc += 5;
			}
	
			this.setDistanciaPercorrida(loc);
			Thread.yield();
		}
	}
	
	@Override
	public String toString() {
		return "A tartaruga ";
	}

}
