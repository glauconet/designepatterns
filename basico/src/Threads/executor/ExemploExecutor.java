package Threads.executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExemploExecutor {

	public static void main(String[] args) {
		ExecutorService pool = Executors.newFixedThreadPool(5);
		
		for (int i=0; i<5 ;i++) {
			pool.execute(new FazAlgo());
		}
		
//		for (int i=0; i<5 ;i++) {
//			Thread t = new Thread(new FazAlgo());
//			t.start();
//		}
	}

}
