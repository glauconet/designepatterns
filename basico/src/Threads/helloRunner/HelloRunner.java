package Threads.helloRunner;

public class HelloRunner implements Runnable {

	private int max;
	private long tempoInicialGeral;
	
	public HelloRunner(long tempoInicialGeral, int max) {
		this.max = max;
		this.tempoInicialGeral = tempoInicialGeral;
	}
	
	@Override
	public void run() {
		long tempoInicialRunner = System.currentTimeMillis();
		for (int i=0; i < max; i++) {
			if (i % 10 == 0) {
				System.out.println("hello runner " + Thread.currentThread().getName() + ", " + i);
			}
		}
		long tempoFinalRunner = System.currentTimeMillis();
		System.out.println("hello runner " + Thread.currentThread().getName() + 
				" terminou em " + (tempoFinalRunner - tempoInicialRunner) +
				"/" + (tempoFinalRunner - tempoInicialGeral));
	}

}
