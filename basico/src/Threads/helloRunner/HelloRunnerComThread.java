package Threads.helloRunner;

public class HelloRunnerComThread {

	public static void main(String[] args) {
		long tempoInicialGeral = System.currentTimeMillis();

		HelloRunner r1 = new HelloRunner(tempoInicialGeral, 40000);
		Thread t1 = new Thread(r1);
		t1.setDaemon(true);
		t1.start();
		
		HelloRunner r2 = new HelloRunner(tempoInicialGeral, 500);
		Thread t2 = new Thread(r2);
		t2.setDaemon(true);
		t2.start();
		
		HelloRunner r3 = new HelloRunner(tempoInicialGeral, 3000);
		Thread t3 = new Thread(r3);
		t3.setDaemon(true);
		t3.start();

		try {
			t1.join();
			t2.join();
			t3.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("main terminou em " + (System.currentTimeMillis() - tempoInicialGeral));
	}

}
