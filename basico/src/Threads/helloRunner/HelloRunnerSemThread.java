package Threads.helloRunner;

public class HelloRunnerSemThread {
	
	public static void main(String[] args) {
		long tempoInicialGeral = System.currentTimeMillis();

		HelloRunner r1 = new HelloRunner(tempoInicialGeral, 40000);
		r1.run();
		
		HelloRunner r2 = new HelloRunner(tempoInicialGeral, 500);
		r2.run();
		
		HelloRunner r3 = new HelloRunner(tempoInicialGeral, 3000);
		r3.run();

		System.out.println("main terminou em " + (System.currentTimeMillis() - tempoInicialGeral));
	}
	
}
