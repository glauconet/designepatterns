package Threads.interrupt;

public class ExemploInterrupt {

	public static void main(String[] args) {

		Thread thread = new Thread(new FazAlgo());
		thread.start();
		try {
			Thread.sleep(1);
		} catch (InterruptedException e) {
		}
		thread.interrupt();

	}

}
