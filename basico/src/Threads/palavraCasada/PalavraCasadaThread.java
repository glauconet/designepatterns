package Threads.palavraCasada;
public class PalavraCasadaThread extends Thread {

	private String primeiroNome;
	private String segundoNome;
	private long espera;

	public PalavraCasadaThread(String primeiroNome, String segundoNome, long espera) {
		this.primeiroNome = primeiroNome;
		this.segundoNome = segundoNome;
		this.espera = espera;
	}

	// Metodo onde a execução da thread vai começar
	public void run() {
		try {
			while (true) { 							// Loop infinito...
				System.out.print(primeiroNome); 	// lista primeiro nome
				sleep(espera); 						// espera alguns milisegundos
				System.out.println(segundoNome); 	// lista segundo nome
			}
		} catch (InterruptedException e) { 			// Captura interrupção da thread
			System.out.println(primeiroNome + segundoNome + e); 
		}
	}

}
