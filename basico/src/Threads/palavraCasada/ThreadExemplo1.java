package Threads.palavraCasada;

import java.io.IOException;

public class ThreadExemplo1 {

	public static void main(String[] args) {
		// cria 3 threads
		Thread primeira = new PalavraCasadaThread("Queijo ", "Goiabada ", 200L);
		Thread segunda = new PalavraCasadaThread("Romeu ", "Julieta ", 300L);
		Thread terceira = new PalavraCasadaThread("Piupiu ", "Frajola ", 500L);
		System.out.println("Tecle Enter quando quiser terminar...\n");
		
		// define as threads como deamon
		primeira.setDaemon(true); 
		segunda.setDaemon(true); 
		terceira.setDaemon(true); 

		// inicia as 3 threads
		primeira.start(); 
		segunda.start(); 
		terceira.start();
		
		try {
			System.in.read(); 			// Espera até Enter ser acionado
			System.out.println("\nEnter acionado...");
		
		} catch (IOException e) { 	
			System.out.println(e); 	
		}
		
		System.out.println("Terminando main()");
		return;
	}

}
