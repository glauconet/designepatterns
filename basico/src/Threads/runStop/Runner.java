package Threads.runStop;

public class Runner implements Runnable {

	private boolean continuaExecutando = true;
	private int i = 0;
	
	@Override
	public void run() {
		while (continuaExecutando) {
			i++;
			if (i % 10 == 0) {
				System.out.println("runner executando... " + i);
			}
		}
	}
	
	public void paraRunner() {
		continuaExecutando = false;
	}

}
