package Threads.runStop;

public class ThreadController {
	
	Runner r = new Runner();
	Thread t = new Thread(r);
	
	public void iniciaThread() {
		t.start();
	}

	public void paraThread() {
		r.paraRunner();
	}
}
