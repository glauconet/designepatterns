package Threads.runStop;

public class ThreadMain {

	public static void main(String[] args) {

		ThreadController tc = new ThreadController();

		System.out.println("vai começar a thread...");
		tc.iniciaThread();

		System.out.println("vai começar meu trabalho...");
		for (int i=0; i < 1000; i++) {
			if (i % 10 == 0) {
				System.out.println("main loop " + i);
			}
		}
		
		tc.paraThread();
		
		System.out.println("terminado!");
	}

}
