package Threads.runnable;

public class TesteFazAlgo {

	public static void main(String[] args) {
		FazAlgo a = new FazAlgo();
		FazAlgo b = new FazAlgo();
		
		Thread ta = new Thread(a);
		Thread tb = new Thread(b);

		ta.start();
		tb.start();
	}

}
