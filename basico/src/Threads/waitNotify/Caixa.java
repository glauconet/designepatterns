package Threads.waitNotify;
public class Caixa {
	private String conteudo;
	private boolean cheia = false;

	public synchronized String get() {
		while (cheia == false) {
			try {
				// espera até que um produtor coloque um conteúdo.
				wait();
			} catch (InterruptedException e) {
			}
		}
		cheia = false;
		// informa a todos que a caixa foi esvaziada.
		notifyAll();
		System.out.println("notifyAll (get) emitido");
		return conteudo;
	}

	public synchronized void put(String novoConteudo) {
		while (cheia == true) {
			try {
				// espera até que um consumidor esvazie a caixa.
				wait();
			} catch (InterruptedException e) {
			}
		}
		conteudo = novoConteudo;
		cheia = true;
		// informa a todos que um novo conteúdo foi inserido.
		notifyAll();
		System.out.println("notifyAll (put) emitido");
	}
}