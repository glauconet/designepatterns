package Threads.waitNotify;

public class CaixaConsumidor implements Runnable {

	private Caixa caixa;
	
	public CaixaConsumidor(Caixa caixa) {
		this.caixa = caixa;
	}
	
	@Override
	public void run() {
		String conteudo = caixa.get();
		System.out.println("thread Consumidor pegou '" + conteudo + "'");
	}

}
