package Threads.waitNotify;

public class CaixaFornecedor implements Runnable {

	private Caixa caixa;
	
	public CaixaFornecedor(Caixa caixa) {
		this.caixa = caixa;
	}
	
	@Override
	public void run() {
		String novoConteudo = "batatinha";
		caixa.put(novoConteudo);
		System.out.println("thread Fornecedor colocou '" + novoConteudo + "'");
	}

}
