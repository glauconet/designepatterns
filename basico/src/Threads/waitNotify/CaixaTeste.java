package Threads.waitNotify;

public class CaixaTeste {

	public static void main(String[] args) throws InterruptedException {

		final Caixa caixa = new Caixa();
		
		Thread t1 = new Thread() {
			public void run() {
				String conteudo = caixa.get();
				System.out.println("thread1 pegou '" + conteudo + "'");
			}
		};
		
		Thread t2 = new Thread() {
			public void run() {
				String novoConteudo = "batatinha";
				caixa.put(novoConteudo);
				System.out.println("thread2 colocou '" + novoConteudo + "'");
			}
		};
		
		System.out.println("thread1 vai pegar conteúdo... (ainda não tem)");
		t1.start();
		
		Thread.sleep(10);
		System.out.println("thread1 está no estado " + t1.getState());
		
		System.out.println("thread2 vai colocar conteúdo...");
		t2.start();
	}

}
