package modulo04;
import java.util.Scanner;
//� Usando os valores m�ximos e m�nimos do tipo primitivo int 
public class ExemploMotivoWrapper {
	public static void main(String[] args) {
			int minimo = Integer.MAX_VALUE;
			int maximo = Integer.MIN_VALUE;
			Scanner var = new Scanner(System.in);
			int num = 0;
			for (int i = 0; i < 5; i++) {
				System.out.print("Digite um numero inteiro: ");
				num = var.nextInt(); // declara e inicia vari�vel
				if (num < minimo) {
					minimo = num;
				}
				if (num > maximo) {
					maximo = num;
				}
			}
			System.out.println("O menor numero eh: " + minimo);
			System.out.println("O maior numero eh: " + maximo);
		}
}
