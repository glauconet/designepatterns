package cnpj;


import java.io.*;
import java.util.InputMismatchException;
import java.util.Scanner;

public class ValidaCNPJ {    //
	
	
	
	public static void validaCNPJ() {
		
		Scanner ler = new Scanner(System.in); 
		
		String CNPJ; 
		
		System.out.printf("Informe um CNPJ: "); 
		
		CNPJ = ler.next(); 
		
		System.out.printf("\nResultado: "); 
		
		// usando os métodos isCNPJ() e imprimeCNPJ() da classe "ValidaCNPJ" 
		if (ValidaCNPJ.isCNPJ(CNPJ) == true) {
			System.out.printf("%s\n", ValidaCNPJ.imprimeCNPJ(CNPJ));
		
		}else { 
			
			System.out.printf("Erro, CNPJ inválido !!!\n"); 
		}
	
	} 
	
	
	
		
	public static boolean isCNPJ(String CNPJ) { 
		// considera-se erro CNPJ's formados por uma sequencia de numeros iguais 
		if (CNPJ.equals("00000000000000") || CNPJ.equals("11111111111111") || 
			CNPJ.equals("22222222222222") || CNPJ.equals("33333333333333") || 
			CNPJ.equals("44444444444444") || CNPJ.equals("55555555555555") || 
			CNPJ.equals("66666666666666") || CNPJ.equals("77777777777777") || 
			CNPJ.equals("88888888888888") || CNPJ.equals("99999999999999") || 
			(CNPJ.length() != 14)) return(false); 
		char dig13, dig14; int sm, i, r, num, peso; 
		// "try" - protege o código para eventuais erros de conversao de tipo (int) 
		try {
			// Calculo do 1o. Digito Verificador 
			sm = 0; peso = 2; 
			for (i=11; i>=0; i--) { 
				// converte o i-ésimo caractere do CNPJ em um número: 
				// por exemplo, transforma o caractere '0' no inteiro 0 
				// (48 eh a posição de '0' na tabela ASCII) 
				num = (int)(CNPJ.charAt(i) - 48); 
				sm = sm + (num * peso); peso = peso + 1; 
				if (peso == 10) 
					peso = 2; 
			}
			r = sm % 11; 
				if ((r == 0) || (r == 1)) dig13 = '0'; 
				else dig13 = (char)((11-r) + 48);
				
				
			// Calculo do 2o. Digito Verificador 
				sm = 0; 
				peso = 2; 
				for (i=12; i>=0; i--) { 
					num = (int)(CNPJ.charAt(i)- 48); 
					sm = sm + (num * peso); 
					peso = peso + 1; 
					if (peso == 10) peso = 2; 
				} 
				r = sm % 11; 
				if ((r == 0) || (r == 1)) dig14 = '0'; 
				else dig14 = (char)((11-r) + 48); 
				
				// Verifica se os dígitos calculados conferem com os dígitos informados. 
				if ((dig13 == CNPJ.charAt(12)) && (dig14 == CNPJ.charAt(13))) 
					return(true); else return(false); 
			} catch (InputMismatchException erro) { 
					
				return(false); 
			} 
		
	} 
	
	public static String imprimeCNPJ(String CNPJ) { 
			// máscara do CNPJ: 99.999.999.9999-99 
			return(CNPJ.substring(0, 2) + "." + CNPJ.substring(2, 5) + "." + CNPJ.substring(5, 8) + "." + CNPJ.substring(8, 12) + "-" + CNPJ.substring(12, 14)); 
	} 
		
	/**
	 * Calcula DV do CNPJ Novo
	 */
	public static String dvCnpjNovo(String cnpj){
		Integer[] val = new Integer[14];
		
		int i;
		
		for (i = 0; i < cnpj.length()-1; i++) {
			val[i] = Integer.parseInt(cnpj.substring(i, i+1));
		}
		    
		val[i] = Integer.parseInt(cnpj.substring(cnpj.length()-1));
		   
		
		Integer[] digitos = new Integer[13];
		System.arraycopy(val, 0, digitos, 0, 12);
	    Integer[] pesos1 = new Integer[]{5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2};
	    Integer[] pesos2 = new Integer[]{6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2};
	
	    int dv1 = calculaParteDV(digitos, pesos1);
	    digitos[12] = dv1;
	    int dv2 = calculaParteDV(digitos, pesos2);
	    
	   // return String.format("%02d", dv1 * 10 + dv2);
	    
	    return cnpj + String.format("%02d", dv1 * 10 + dv2);
	}

	private static int calculaParteDV(Integer[] digitos, Integer[] pesos) {
	    int sum = 0;
	    int i = 0;
        for (Integer peso : pesos)
            sum += peso * digitos[i++];
	    int parcial = sum % 11;
	    return parcial < 2 ? 0 : 11 - parcial;
	}
	
	public static void executarCNPJ(String arquivo) {
		  // TODO Auto-generated method stub
		  //construtor que recebe o objeto do tipo arquivo
		  try {
		   FileReader fr = new FileReader(arquivo);
		   //construtor que recebe o objeto do tipo FileReader
		   BufferedReader br = new BufferedReader(fr);
		   //equanto houver mais linhas
		   File arquivoSaida = new File("/home/09534485845/Desktop/arquivodesaida.txt");
		   
		       //verifica se o arquivo ou diretório existe
		       boolean existe = arquivoSaida.exists();
		       
		       if (!existe){
		        arquivoSaida.createNewFile();
		       }
		   //construtor que recebe o objeto do tipo arquivo
		   FileWriter fw = new FileWriter(arquivoSaida);
		   //construtor recebe como argumento o objeto do tipo FileWriter
		   BufferedWriter bw = new BufferedWriter( fw );

		      while(br.ready()){
		   //lê a proxima linha
		        String linha = br.readLine();
		        //faz algo com a linha
		        
		         
		     //escreve o conteúdo no arquivo
		        bw.write("antes"+dvCnpjNovo(linha)+"depois");
		          
		     //quebra de linha
		     bw.newLine();
		      }
		   br.close();
		   fr.close();
		  
		   //fecha os recursos
		   bw.close();
		   fw.close();

		  }  
		  catch (FileNotFoundException e) {
		   // TODO Auto-generated catch block
		   e.printStackTrace();
		  }     
		  catch (IOException e) {
		   // TODO Auto-generated catch block
		   e.printStackTrace();
		  }
		  
		 
		 }
	
	
	
	
	
}

