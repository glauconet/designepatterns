import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;


public class Encrypter{  //

    private static final String METODO_ENCRIPTACAO = "AES";
    public static final byte[] CHAVE = {85, 10, 0, -25, 68, 88, 46, 37, 107, 48, 10, -1, -37, -90, 70, -36};
    public static final byte[] CHAVE2= {51, 104, 70, 8, -45, -32, -87, 56, 51, -23, 82, 17, -103, -78, 82, 114, } ;
    public static final byte[] CHAVE3= {77, 106, -67, -32, -8, 14, 125, -10, -86, 126, -5, -112, -33, -86, -13, -123 } ;

    public static String encriptar(byte[] key, String value)
            throws EncryptorException {

        try {
            SecretKeySpec skeySpec = new SecretKeySpec(key, METODO_ENCRIPTACAO);

            Cipher cipher = Cipher.getInstance(METODO_ENCRIPTACAO);
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
            byte[] encrypted = cipher.doFinal(value.getBytes());

            return new BASE64Encoder().encode(encrypted);
        }
        catch (Exception e) {
            throw new EncryptorException("Erro ao criptografar informações " + e.getMessage());
        }
    }

    public static String decriptar(byte[] key, String encrypted)
            throws EncryptorException {

        byte[] decrypted = null;

        try {
            SecretKeySpec skeySpec = new SecretKeySpec(key, METODO_ENCRIPTACAO);

            byte[] decoded = new BASE64Decoder().decodeBuffer(encrypted);

            Cipher cipher = Cipher.getInstance(METODO_ENCRIPTACAO);
            cipher.init(Cipher.DECRYPT_MODE, skeySpec);
            decrypted = cipher.doFinal(decoded);
        }
        catch (Exception e) {
            throw new EncryptorException("Erro ao descriptografar informações " + e.getMessage());
        }

        return new String(decrypted);
    }

    public static void gerarChave() {
        try {
            KeyGenerator keyGen = KeyGenerator.getInstance(METODO_ENCRIPTACAO);

            System.out.println("Chave AES:");
            byte[] chave = keyGen.generateKey().getEncoded();

            System.out.print("{");
            for (byte b : chave) {
                System.out.print("" + b + ", ");
            }
            System.out.println("}");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
