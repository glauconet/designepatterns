/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package extratorurl.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 22393182870
 */
public class Connector {

    Connection connection = null;

    public Connection getConnection() {

        try {
            // Load the JDBC driver  
            String driverName = "oracle.jdbc.OracleDriver";
//                    "oracle.jdbc.driver.OracleDriver";
            Class.forName(driverName);

            // Create a connection to the database  
            String serverName = "10.50.240.227";
            String portNumber = "1521";
            String sid = "d0812a";
            String url = "jdbc:oracle:thin:@" + serverName + ":" + portNumber + ":" + sid;
            String username = "mpct_bc";
            String password = "nova";
            connection = DriverManager.getConnection(url, username, password);
        } catch (ClassNotFoundException e) {
           Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Erro-Connector", e);
        } catch (SQLException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Erro-Connector", e);
        } finally {
            return this.connection;
        }
    }
}
