package extratorurl;


import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Extrator {

    private static StringBuffer scriptSQLInicio = new StringBuffer();
    private static StringBuffer scriptSQLFim = new StringBuffer();

    private static Map<String, Set<String>> mapaTransacoes = new HashMap<String, Set<String>>();

    public static StringBuffer montaSQLInicio() {
        scriptSQLInicio.append("/*	@carga_url.sql	*/ \n")
                .append("/*	Cadastramento de urls e transacoes  */ \n")
                .append("set serveroutput on size 1000000 \n")
                .append("DECLARE \n")
                .append("wg_sqlcode   		number; \n")
                .append("wg_sqlerrm   		varchar2(150); \n")
                .append("wg_seq_URL_NR_SQ	urls.URL_NR_SQ%type; \n")
                .append("wg_TRAS_CD 		transacoes.TRAS_CD%type; \n")
                .append("wg_TRAS_DT_EXTINCAO	transacoes.TRAS_DT_EXTINCAO%type; \n")
                .append("exception_cancela	exception; \n")
                .append("TYPE TRANS_ARRAY IS TABLE OF URL_TRANSACOES.URTR_TRAS_CD%type INDEX BY BINARY_INTEGER; \n")
                .append("TYPE T_URL IS RECORD ( \n")
                .append("url_txt    URLS.URL_TX_DESCR%type, \n")
                .append("cd_trans   TRANS_ARRAY \n")
                .append("); \n")
                .append("TYPE T_URLS IS TABLE OF T_URL INDEX BY BINARY_INTEGER; \n")
                .append("wg_urls 		T_URLS; \n")
                .append("wg_cd_aplicacao		URLS.URL_APLC_CD%type; \n")
                .append("BEGIN \n")
                .append("dbms_output.enable(2000000); \n")
                .append("dbms_output.put_line('Inicio carga_url.sql ' || to_char(sysdate,'dd/mm/yyyy hh24:mi:ss')); \n")
                .append("wg_cd_aplicacao := 'AFIN'; \n")
                .append(" \n");

        return scriptSQLInicio;

    }

    public static StringBuffer montaSQLFim() {
        scriptSQLFim.append("/*********************************************************************************************************/ \n")
                .append("/*  AREA DE EDICAO - FIM                                                                                 */ \n")
                .append("/*********************************************************************************************************/ \n")
                .append(" FOR i IN WG_URLS.FIRST .. WG_URLS.LAST LOOP \n")
                .append(" SELECT sq_url_nr_sq.nextval \n")
                .append(" INTO wg_seq_URL_NR_SQ  \n")
                .append(" FROM dual; \n")
                .append(" dbms_output.put_line('wg_seq_URL_NR_SQ ' || wg_seq_URL_NR_SQ); \n")
                .append(" INSERT INTO URLS ( \n")
                .append(" URL_NR_SQ, \n")
                .append(" URL_APLC_CD, \n")
                .append(" URL_DT_REGISTRO, \n")
                .append(" URL_TX_DESCR, \n")
                .append(" URL_B_IN_DELECAO_LOGICA, \n")
                .append(" URL_B_DT_ATUALIZACAO, \n")
                .append(" URL_B_IN_HISTORICO, \n")
                .append(" URL_B_IN_ATUALIZACAO \n")
                .append(" ) VALUES ( \n")
                .append(" wg_seq_URL_NR_SQ, \n")
                .append(" wg_cd_aplicacao, \n")
                .append(" SYSDATE, \n")
                .append(" WG_URLS(i).url_txt, \n")
                .append(" 'N', \n ")
                .append(" SYSDATE, \n")
                .append(" 'N', \n ")
                .append(" 'N' \n ")
                .append(" ); \n ")
                .append(" FOR j IN WG_URLS(i).cd_trans.FIRST .. WG_URLS(i).cd_trans.LAST LOOP \n")
                .append(" BEGIN \n")
                .append(" select TRAS_DT_EXTINCAO \n")
                .append(" into wg_TRAS_DT_EXTINCAO \n")
                .append(" from transacoes \n")
                .append(" where TRAS_CD = WG_URLS(i).cd_trans(j); \n")
                .append(" dbms_output.put_line('wg_TRAS_DT_EXTINCAO ' || wg_TRAS_DT_EXTINCAO); \n")
                .append(" EXCEPTION \n")
                .append(" when others then \n")
                .append(" dbms_output.put_line('Erro When others select transacoes.'); \n")
                .append(" wg_sqlcode := sqlcode; \n")
                .append(" wg_sqlerrm := sqlerrm; \n")
                .append(" dbms_output.put_line('wg_sqlcode ' || wg_sqlcode); \n")
                .append(" dbms_output.put_line('wg_sqlerrm ' || wg_sqlerrm); \n")
                .append(" 	raise exception_cancela; \n")
                .append(" END; \n")
                .append(" INSERT INTO URL_TRANSACOES ( \n")
                .append(" 	URTR_TRAS_CD,  \n")
                .append(" 	URTR_URL_NR_SQ,  \n")
                .append("	URTR_NR_SQ,    \n")
                .append("	URTR_DT_INICIO,   \n")
                .append("	URTR_DT_REGISTRO,   \n")
                .append("	URTR_DT_FIM,   \n")
                .append("	URTR_B_IN_DELECAO_LOGICA,   \n")
                .append("	URTR_B_DT_ATUALIZACAO,   \n")
                .append("	URTR_B_IN_HISTORICO,   \n")
                .append("	URTR_B_IN_ATUALIZACAO   \n")
                .append(" ) VALUES (   \n")
                .append(" WG_URLS(i).cd_trans(j), \n")
                .append(" wg_seq_URL_NR_SQ,  \n")
                .append(" 0,  \n")
                .append(" SYSDATE,  \n")
                .append(" SYSDATE,  \n")
                .append(" wg_TRAS_DT_EXTINCAO,  \n")
                .append(" 'N',   \n")
                .append(" null,  \n")
                .append(" 'N',   \n")
                .append(" 'N'   \n")
                .append(" ); \n")
                .append(" END LOOP; \n")
                .append(" END LOOP; \n")
                .append(" commit; \n")
                .append(" dbms_output.put_line('Fim carga_url.sql ' || to_char(sysdate,'dd/mm/yyyy hh24:mi:ss')); \n")
                .append(" EXCEPTION \n")
                .append(" when exception_cancela then \n")
                .append(" dbms_output.put_line('Erro exception_cancela'); \n")
                .append(" rollback; \n")
                .append(" dbms_output.put_line('fim carga_url.sql com erro.' || to_char(sysdate,'dd/mm/yyyy hh24:mi:ss')); \n")
                .append(" when others then \n")
                .append(" dbms_output.put_line('Erro When others geral.'); \n")
                .append(" rollback; \n")
                .append(" wg_sqlcode := sqlcode; \n")
                .append(" wg_sqlerrm := sqlerrm; \n")
                .append(" dbms_output.put_line('wg_sqlcode ' || wg_sqlcode); \n")
                .append(" dbms_output.put_line('wg_sqlerrm ' || wg_sqlerrm); \n")
                .append(" dbms_output.put_line('fim carga_url.sql com erro.' || to_char(sysdate,'dd/mm/yyyy hh24:mi:ss')); \n ")
                .append(" END; \n ")
                .append(" / \n ");

        return scriptSQLFim;


    }

    public static void main(String[] args) {

        // validando parametros de entrada
        if (args.length != 2) {
            System.out.println("Modo de uso: passe como parametros o caminho para o arquivo faces config e um nome para o arquivo de saida a ser gerado. Ex:");
            System.out.println("java -jar Extrator /caminho/para/WEB-INF/faces-config.xml arquivoSaida");
            return;
        }

        try {
            parseFacesConfig(args[0], args[1]);
        }catch (JDOMException e) {
            Logger.getLogger(args[0] + " não está formatado corretamente: " + e.getStackTrace());
        } catch (Exception e) {
            Logger.getLogger("Erro inesperado: " + e.getStackTrace());
        }


    }

    private static void parseFacesConfig(String nomeArquivoFacesConfig, String nomeArquivoSaida) throws Exception {

        SAXBuilder builder = new SAXBuilder();

        Document doc = builder.build(nomeArquivoFacesConfig);

        Element root = doc.getRootElement();
        listChildren(root, 0, nomeArquivoSaida);

        int i = 0;

        StringBuffer queryCompleta = new StringBuffer(montaSQLInicio().toString());

        for (Map.Entry<String, Set<String>> entry : mapaTransacoes.entrySet()) {
            i++;
            String textoUrl = String.valueOf(" \n wg_urls("
                    + i + ").url_txt := '"
                    + ((String) entry.getKey()).trim() + "'; \n");
            Logger.getLogger(Extrator.class.getName()).log(Level.FINEST, textoUrl);
            queryCompleta.append(textoUrl);
            Set<String> set = entry.getValue();
            int j = 0;
            for (String string : set) {
                j++;
                String textoTransacao = String.valueOf(" \n wg_urls("
                        + i + ").cd_trans(" + j + ") := '"
                        + string + "'; \n");
                Logger.getLogger(Extrator.class.getName()).log(Level.FINEST, textoTransacao);
                queryCompleta.append(textoTransacao);
            }

        }

        queryCompleta.append(montaSQLFim().toString());

        geraArquivo(queryCompleta.toString(), nomeArquivoSaida + "ScriptSaida.sql");

    }

    public static void listChildren(Element current, int depth, String caminhoArquivo) {

        if (current.getName().equals(String.valueOf("navigation-case"))) {
            String url = current.getContent(3).getValue();
            try {
                Element transacoes = (Element) current.getContent(5);

                if (!transacoes.getName().equalsIgnoreCase("transacoes")) {
                    Logger.getLogger(Extrator.class.getName()).log(Level.SEVERE, "As transações devem ser declarados logo após a tag to-view-id. Exemplo: \n" + exemploFormatacaoCorretaTagTransacoes());
                }

                Set<String> setTransacoes = new HashSet<String>();

                if (transacoes != null) {
                    for (Iterator it = transacoes.getChildren().iterator(); it.hasNext(); ) {
                        Element transacao = (Element) it.next();
                        if (transacao != null) {
                            setTransacoes.add(transacao.getValue());
                        }

                    }
                }

                mapaTransacoes.put(url, setTransacoes);
            } catch (IndexOutOfBoundsException e) {
                Logger.getLogger(Extrator.class.getName()).log(Level.SEVERE, "Url não possui transações: " + url.trim() + "\nPor favor declare as transações desta url no faces config dentro da tag  <transacoes><transacao>...</transacao></transacoes>.");
            }

        }

        List children = current.getChildren();
        Iterator iterator = children.iterator();
        while (iterator.hasNext()) {
            Element child = (Element) iterator.next();
            listChildren(child, depth + 1, caminhoArquivo);
        }
    }

    private static void printSpaces(int n) {

        for (int i = 0; i < n; i++) {
            System.out.print(' ');
        }
    }

    private static String criaInsertUrls(String url) {

        String sqlInsertUrl = "INSERT INTO URLS (URL_NR_SQ, URL_APLC_CD, URL_DT_REGISTRO, URL_TX_DESCR, URL_B_IN_DELECAO_LOGICA,"
                + " URL_B_DT_ATUALIZACAO,URL_B_IN_HISTORICO, URL_B_IN_ATUALIZACAO) VALUES ( "
                + "sq_url_nr_sq.nextval, "
                + "'" + "AFIN" + "',"
                + "SYSDATE,"
                + "'" + url + "',"
                + "'N',"
                + "SYSDATE,"
                + "'N',"
                + "'N');";
        return sqlInsertUrl;
    }

    private static void geraArquivo(String linha, String caminhoArquivo) {

        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(caminhoArquivo, true));
            writer.append(linha);
            writer.newLine();
            writer.flush();
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(Extrator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static String exemploFormatacaoCorretaTagTransacoes() {
        return "        <navigation-case>\n" +
               "            <from-outcome>parametroTelaInicialAjuizamento</from-outcome>\n" +
               "            <to-view-id>/private/cobra/pages/avisoCobranca/atualizarParametroAvisoCobrancaAjuizamento/parametroTelaInicial.jsf\n" +
               "            </to-view-id>\n" +
               "            <transacoes>\n" +
               "               <transacao>COBIT001</transacao>\n" +
               "               <transacao>COBIT002</transacao>\n" +
               "               <transacao>COBIT003</transacao>\n" +
               "               </transacoes>\n" +
               "        </navigation-case>";
    }
}
