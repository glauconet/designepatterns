package Behavioral.State;

public class Bandeira1 implements IBandeira {

    @Override
    public double calculaValorDaCorrida(double tempo, double distancia) {
        return 5.0 + tempo * 1.5 + distancia * 1.7;
    }
}
