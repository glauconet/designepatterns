package Behavioral.State;

public class Bandeira2 implements IBandeira {

    @Override
    public double calculaValorDaCorrida(double tempo, double distancia) {
        return 10.0 + tempo * 3.0 + distancia * 4.0;
    }
}
