package Behavioral.State;

public class Taximetro{

    private IBandeira bandeira;

    public Taximetro(IBandeira bandeira) {
         this.bandeira = bandeira ;
    }

    public void setBandeira( IBandeira bandeira ) {
        this.bandeira = bandeira ;
    }

    public double calculaValorDaCorrida(double tempo, double distancia) {
        double valor = this.bandeira.calculaValorDaCorrida(tempo, distancia);
        
        return valor;
    }

}
