package Criational.Builder;



import java.util.Calendar;

public class BBBoletoBuilder implements IBoletoBuilder {

    private String sacado ;
    private String cedente ;
    private double valor ;
    private Calendar vencimento ;
    private int nossoNumero ;



    @Override
    public void buildSacado(String sacado) {
        this.sacado = sacado ;
    }

    @Override
    public void buildCedente(String cedente) {
        this.cedente = cedente ;
    }

    @Override
    public void buildValor(double valor) {
        this.valor = valor ;
    }

    @Override
    public void buildVencimento(Calendar vencimento) {
        this.vencimento = vencimento ;
    }

    @Override
    public void buildNossoNumero(int nossoNumero) {
        this.nossoNumero = nossoNumero ;
    }


    @Override
    public IBoleto getBoleto() {
        return new BBBoleto(sacado , cedente , valor , vencimento , nossoNumero );
    }


}
