package Criational.Builder;


public class GeradorDeBoleto{

    public IBoletoBuilder boletoBuilder;

    public GeradorDeBoleto (IBoletoBuilder boletoBuilder ) {
        this.boletoBuilder = boletoBuilder;
    }

    public IBoleto geraBoleto () {


         IBoleto boleto = boletoBuilder.getBoleto();

         return boleto ;

    }
}
