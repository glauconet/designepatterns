package Criational.Builder;

import java.util.Calendar;

public interface IBoleto{

    String getSacado();
    String getCedente();
    double getValor();
    Calendar getVencimento();
    int getNossoNumero();

    String toString();


}
