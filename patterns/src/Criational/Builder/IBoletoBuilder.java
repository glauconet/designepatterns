package Criational.Builder;

import java.util.Calendar;

public interface IBoletoBuilder{

    void buildSacado(String sacado);
    void buildCedente(String cedente);
    void buildValor(double valor);
    void buildVencimento(Calendar vencimento);
    void buildNossoNumero(int nossoNumero);

    IBoleto getBoleto();
}
