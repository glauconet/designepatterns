package Criational.Builder;

import java.util.Calendar;

public class TestaGeradorDeBoleto{

    public static void main (String [] args){

        IBoletoBuilder boletoBuilder = new BBBoletoBuilder();

        GeradorDeBoleto geradorDeBoleto = new GeradorDeBoleto(boletoBuilder);

        geradorDeBoleto.boletoBuilder.buildSacado("Glauco Oliveira");

        geradorDeBoleto.boletoBuilder.buildCedente ("Treinamentos");

        geradorDeBoleto.boletoBuilder.buildValor (100.54) ;

        Calendar vencimento = Calendar.getInstance ();

        vencimento.add(Calendar.DATE, 30) ;

        geradorDeBoleto.boletoBuilder.buildVencimento(vencimento);

        geradorDeBoleto.boletoBuilder.buildNossoNumero (1234) ;


        IBoleto boleto = geradorDeBoleto.geraBoleto();



        System.out.println(boleto.toString());
    }

}
