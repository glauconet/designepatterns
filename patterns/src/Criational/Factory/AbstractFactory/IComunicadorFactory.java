package Criational.Factory.AbstractFactory;


public interface IComunicadorFactory{

    IEmissor createEmissor();
    IReceptor createReceptor();

}
