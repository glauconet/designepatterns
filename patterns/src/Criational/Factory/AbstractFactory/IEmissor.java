package Criational.Factory.AbstractFactory;


public interface IEmissor{
    void envia(String mensagem);
}
