package Criational.Factory.AbstractFactory;

public interface IReceptor{

    String recebe();

}
