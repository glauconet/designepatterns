package Criational.Factory.FactoryMethod;


public class EmissorEmail implements IEmissor {


    @Override
    public void envia(String mensagem) {
        System.out.println("Enviando por email a mensagem: ");
        System.out.println(mensagem);
    }
}
