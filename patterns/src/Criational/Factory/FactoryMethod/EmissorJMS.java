package Criational.Factory.FactoryMethod;


public class EmissorJMS implements IEmissor {


    @Override
    public void envia(String mensagem) {

        System.out.println ("Enviando por JMS a mensagem: ");
        System.out.println (mensagem );


    }
}
