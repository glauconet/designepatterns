package Criational.Factory.FactoryMethod;


public class EmissorSMS implements IEmissor {


    public void envia (String message){

        System.out.println("Enviando por SMS a mensagem : ");
        System.out.println(message);
    }


}
