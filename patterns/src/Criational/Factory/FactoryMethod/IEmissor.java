package Criational.Factory.FactoryMethod;


public interface IEmissor{

    void envia(String mensagem);

}
