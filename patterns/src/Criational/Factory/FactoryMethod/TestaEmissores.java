package Criational.Factory.FactoryMethod;


public class TestaEmissores{


    public static void main( String [] args ) {

      EmissorCreator creator = new EmissorCreator();

        // SMS
        IEmissor emissor1 = creator.create(EmissorCreator.SMS);
         emissor1.envia(" Treinamento ");

         // Email
         IEmissor emissor2 = creator.create(EmissorCreator.EMAIL);
         emissor2.envia(" Treinamento ");

         // JMS
         IEmissor emissor3 = creator.create(EmissorCreator.JMS);
         emissor3.envia(" Treinamento ");


    }



}
