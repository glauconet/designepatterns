package Criational.Factory.FactorySimple;


	public abstract class Carro {  

		protected float valor; 
		
		protected String nome;
		     
		public float getPreco() { 
		    	
		      return valor;  
		}  
		
		public String getNome() { 
	    	
		      return nome;  
		}  
		 
	}  