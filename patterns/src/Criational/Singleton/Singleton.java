package Criational.Singleton;
//Early instantiation using implementation with static field.

class Singleton{// é uma classe normal que esta utilizando 
				//	atributos e metodos estáticos para garantir 
				//  o uso da instancia de uma única classe
	
	private static Singleton uma_instancia = new Singleton();

	private Singleton(){
		
		System.out.println("Criando instancia uma vez");
	}

	public static Singleton getInstance(){    
		
		return uma_instancia;
		
	}

	public void fazendoAlgo(){
		
		System.out.println("Sozinho fazendo algo!");
		
	}
}