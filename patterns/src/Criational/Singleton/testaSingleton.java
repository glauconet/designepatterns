package Criational.Singleton;



public class testaSingleton {

	public static void main(String[] args) {
		
		Singleton Sozinho 	= Singleton.getInstance();
	    Singleton Sozinha 	= Singleton.getInstance();
		Singleton Separada 	= Singleton.getInstance();
		Singleton Separado 	= Singleton.getInstance();
		
		Sozinho.fazendoAlgo();
		Sozinha.fazendoAlgo();
		Separado.fazendoAlgo();
		Separada.fazendoAlgo();

	}

}
