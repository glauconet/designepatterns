package Structural.Adapter;

public interface IMyCompanyWidget {
	
	 public String getWidgetModel();
	 public void setWidgetModel(String model);
	 public String getWidgetColor();
	 public void setWidgetColor(String color);
	
	

}
