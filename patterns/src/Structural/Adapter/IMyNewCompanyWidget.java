package Structural.Adapter;

public interface IMyNewCompanyWidget {

	public String getWidgetDataXML();
	
    public void setWidgetDataXML(String xml);
	
	
}
