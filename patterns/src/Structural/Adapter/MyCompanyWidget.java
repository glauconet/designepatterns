package Structural.Adapter;

public class MyCompanyWidget implements IMyCompanyWidget{

	private String widgetColor;
    private String widgetModel;
 
    @Override
    public String getWidgetColor()
    {
        return widgetColor;
    }
 
    @Override
    public String getWidgetModel()
    {
        return widgetModel;
    }
 
    @Override
    public void setWidgetColor(String model)
    {
        this.widgetColor = model;
 
    }
 
    @Override
    public void setWidgetModel(String model)
    {
        this.widgetModel = model;
    }
	
	
	
}
