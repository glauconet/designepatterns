package Structural.Adapter;

public class MyNewCompanyWidget implements IMyNewCompanyWidget{

	private String xmlData;
	 
    @Override
    public String getWidgetDataXML()
    {
        return xmlData;
    }
 
    @Override
    public void setWidgetDataXML(String xml)
    {
        this.xmlData = xml;
    }
	
	
	

}
