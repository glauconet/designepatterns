package Structural.Adapter;

public class OldToNewWidgetAdapter implements IMyNewCompanyWidget{

	 private String xmlData;
	    private MyCompanyWidget myCompanyWidget;
	 
	    public OldToNewWidgetAdapter(MyCompanyWidget mcw)
	    {
	        this.myCompanyWidget = mcw;
	        this.xmlData = "<xml><widget><widgetColor>" +
	        this.myCompanyWidget.getWidgetColor() + "</widgetColor><widgetModel>" +
	        this.myCompanyWidget.getWidgetModel() + "</widgetModel></widget></xml>";
	    }
	 
	    @Override
	    public String getWidgetDataXML()
	    {
	        return xmlData;
	    }
	 
	    @Override
	    public void setWidgetDataXML(String xml)
	    {
	        this.xmlData = xml;
	    }
	
	
	
	
}
