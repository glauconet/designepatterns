package Structural.Adapter;

public class RunWidgetAdapter {

	public static void main(String[] args)
    {
        // Here is the old object
        MyCompanyWidget myCompanyWidget = new MyCompanyWidget();
        myCompanyWidget.setWidgetColor("green");
        myCompanyWidget.setWidgetModel("A45TG3");
 
        // Now run the adapter
        OldToNewWidgetAdapter adapter = new OldToNewWidgetAdapter(myCompanyWidget);
        System.out.println(adapter.getWidgetDataXML());
    }
	
	
	
}
