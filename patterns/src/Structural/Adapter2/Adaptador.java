package Structural.Adapter2;

public class Adaptador implements TomadaTresPinos {
	
	private TomadaDoisPinos tomada;
	String voltagem = "110";

	public Adaptador(TomadaDoisPinos tomada) {
		
		this.tomada = tomada;
	}

	public void fornecerEnergia() {
		
		tomada.fornecerEnergia();
		
		System.out.println("Voltagem: " + voltagem);
		
	}

	

}
