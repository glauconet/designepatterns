package Structural.Adapter2;

public class AdapterMain {
	
	public static void main(String[] args) {
		
		TomadaDoisPinos tomada 	= 	new TomadaComum();
		
		PlugueTresPinos plugue 	= 	new PlugueEuropeu();
		
		Adaptador adapter 		= 	new Adaptador(tomada);
		
		plugue.conectar(adapter);
	}

}
