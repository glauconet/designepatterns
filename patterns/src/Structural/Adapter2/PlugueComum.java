package Structural.Adapter2;

public class PlugueComum implements PlugueDoisPinos{
	
	public void conectar(TomadaDoisPinos tomada) {
		System.out.println("[PLUGUE] Pegando energia elétrica de uma tomada de dois pinos.");
		tomada.fornecerEnergia();
	}
}
