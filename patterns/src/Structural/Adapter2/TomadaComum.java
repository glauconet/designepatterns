package Structural.Adapter2;

public class TomadaComum implements TomadaDoisPinos{
	
	public void fornecerEnergia() {
		System.out.println("[TOMADA] Fornecendo energia para um plugue de dois pinos!");
	}
}
