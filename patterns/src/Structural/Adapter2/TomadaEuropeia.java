package Structural.Adapter2;

public class TomadaEuropeia implements TomadaTresPinos{
	
	public void fornecerEnergia() {
		System.out.println("[TOMADA] Fornecendo energia para um plugue de três pinos!");
	}
}
