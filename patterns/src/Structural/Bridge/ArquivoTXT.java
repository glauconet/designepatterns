package Structural.Bridge;


public class ArquivoTXT implements IFormatoArquivo {


    @Override
    public void gerarArquivoSaida(String texto) {

        System.out.println("Arquivo TXT: " + texto);
    }
}
