package Structural.Bridge;

public interface IFormatoArquivo {

    public void gerarArquivoSaida(String texto);

}
