package Structural.Bridge;

public class Main {

    public static void main(String[] args) {

        IFormatoArquivo formatoArquivo  = new ArquivoHTML() ;
        IFormatoArquivo formatoArquivoTXT  = new ArquivoTXT();

       Memorando memorando = new Memorando(formatoArquivo) ;
       Recibo recibo = new Recibo(formatoArquivo);

       recibo.texto = "Texto do Recibo";
       memorando.texto = "Texto do Memorando" ;


        recibo.gerarAquivo();
        memorando.gerarAquivo();

        recibo.formatoArquivo = formatoArquivoTXT;
        memorando.formatoArquivo =   formatoArquivoTXT;
        System.out.println("Troca do formato de saida");

        recibo.gerarAquivo();
        memorando.gerarAquivo();



    }
}
