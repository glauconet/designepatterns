package Structural.Bridge;


public class Memorando implements ITipoDocumento {


    public IFormatoArquivo formatoArquivo;

    public String texto;

    public Memorando(IFormatoArquivo formatoArquivo) {
        this.formatoArquivo = formatoArquivo;
    }

    @Override
    public void gerarAquivo() {

        this.formatoArquivo.gerarArquivoSaida(texto);

    }
}
