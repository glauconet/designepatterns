package Structural.Bridge;


public class Recibo implements ITipoDocumento {

    public IFormatoArquivo formatoArquivo;

    public String texto;

    public Recibo(IFormatoArquivo formatoArquivo) {
        this.formatoArquivo = formatoArquivo;
    }

    @Override
    public void gerarAquivo() {

        this.formatoArquivo.gerarArquivoSaida(texto);

    }
}
