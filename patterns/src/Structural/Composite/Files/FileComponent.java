package Structural.Composite.Files;

public class FileComponent extends FileSystemComponent {

    private long size;

    public FileComponent(String name, long sz) {
        super(name);
        size = sz;
    }
    public long getComponentSize() {
        return size;
    }

}
