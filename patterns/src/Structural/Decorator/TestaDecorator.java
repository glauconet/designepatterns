package Structural.Decorator;

public class TestaDecorator {


    public static void main(String[] args) {


    Turma turma1 = new TurmaPadrao();
        TurmaPJ turmaPJ = new TurmaPJ(turma1);

        turmaPJ.addMatricula(new Matricula());

        turmaPJ.gerarContrato();

    }
}
