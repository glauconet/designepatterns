package Structural.Decorator;

import java.util.Collection;

public abstract class Turma{

    private Collection<Matricula> matriculas;

    public abstract void addMatricula(Matricula m);

    public abstract void cancelarMatricula(Matricula m);

    public double precoVaga() {
       return 10.0;
    }

    public Contrato gerarContrato(String... clausulas) {
        Contrato contrato = null;

        return contrato;
    }


}
