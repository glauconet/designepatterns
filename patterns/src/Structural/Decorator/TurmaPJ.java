package Structural.Decorator;

public class TurmaPJ extends Turma{

    Turma turmaDecorada;

    public TurmaPJ(Turma t) {
        turmaDecorada=t;
    }


    @Override
    public void addMatricula(Matricula m) {
       //Aqui terias as regras diferentes
        turmaDecorada.addMatricula(m);
    }

    @Override
    public void cancelarMatricula(Matricula m) {
        turmaDecorada.cancelarMatricula(m);
    }
}
