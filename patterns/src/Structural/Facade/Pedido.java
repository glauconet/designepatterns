package Structural.Facade;

public class Pedido {

    private String produto ;
    private String cliente ;
    private String enderecoDeEntrega ;
    private String notaFiscal;

    public Pedido (String produto, String cliente, String enderecoDeEntrega ) {
        this.produto = produto ;
        this.cliente = cliente ;
        this.enderecoDeEntrega = enderecoDeEntrega ;
        this.notaFiscal = "Numero da nota: 000001 ";

    }

    public String getProduto () {
         return produto ;
    }

    public String getCliente () {
         return cliente ;
    }

    public String getEnderecoDeEntrega () {
         return enderecoDeEntrega ;
    }


    public String getNotaFiscal() {
        return notaFiscal;
    }

    public void setNotaFiscal(String notaFiscal) {
        this.notaFiscal = notaFiscal;
    }
}
