package Structural.Facade;

public class PedidoFacade{

    private Estoque estoque ;
    private Financeiro financeiro ;
    private PosVenda posVenda ;

    public PedidoFacade (Estoque estoque ,Financeiro financeiro, PosVenda posVenda ) {
         this.estoque    = estoque ;
         this.financeiro = financeiro ;
         this.posVenda   = posVenda ;
    }

    public void registraPedido (Pedido p){

         System.out.println("-------------Estoque: envia produto ---------------");
         this.estoque.enviaProduto (p.getProduto() , p.getEnderecoDeEntrega ());
        System.out.println("");
         System.out.println("-------------Financeiro: faturar ------------------");
         this.financeiro.fatura(p. getCliente () , p. getNotaFiscal ());
        System.out.println("");
         System.out.println("-------------PosVenda: agendar contato ------------");
         this.posVenda.agendaContato (p. getCliente () , p. getProduto ());

    }









}
