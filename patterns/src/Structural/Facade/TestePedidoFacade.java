package Structural.Facade;

public class TestePedidoFacade{

    public static void main (String [] args ){

         Estoque estoque        = new Estoque ();
         Financeiro financeiro  = new Financeiro ();
         PosVenda posVenda      = new PosVenda ();

         PedidoFacade facade = new PedidoFacade(estoque, financeiro, posVenda);

         Pedido pedido = new Pedido("LG G2", "Glauco", "Av. João de Barros, 155, São Paulo, SP");

         facade.registraPedido(pedido);

         }


}
