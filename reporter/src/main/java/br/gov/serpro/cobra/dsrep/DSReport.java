package br.gov.serpro.cobra.dsrep;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DSReport {
    private static final UseCaseRepository repository = new UseCaseRepository();
    public static final String GRANT_TEMPLATE = "grant %s on %s to %s;";
    public static final String REVOKE_TEMPLATE = "revoke %s on %s from %s;";
    public static final Map<String, String> databases = new HashMap<String, String>();

    static {
        databases.put("doc21", "jdbc:oracle:thin:COBI/NOVA@10.50.240.70:1521:doc21a");
        databases.put("toc20", "jdbc:oracle:thin:COBI/NOVA@10.50.240.133:1521:toc20a");
        databases.put("hoc21", "jdbc:oracle:thin:COBI/NOVA@10.50.240.242:1521:hoc21a");
    }

    public static void main(String[] args) throws SQLException, IOException {
        if (args.length < 1) {
            System.out.println("Choose an option \"compare\", \"report\", or \"check\"");
            return;
        }
        String option = args[0];

        if (option.equals("compare")) {
            if (args.length < 2) {
                System.out.println("Choose a target database as the first parameter: \"doc21\", \"toc20\", or \"hoc21\"");
                return;
            }
            String targetDBName = args[1];

            if (args.length < 3) {
                System.out.println("Choose a template database as the second parameter: \"doc21\", \"toc20\", or \"hoc21\"");
                return;
            }
            String templateDBName = args[2];
            compare(targetDBName, templateDBName);
        } else if (option.equals("report")) {
            if (args.length < 2) {
                System.out.println("Choose a database to report: \"doc21\", \"toc20\", or \"hoc21\"");
                return;
            }
            String dbName = args[1];
            report(dbName);
        } else if (option.equals("check")) {
            if (args.length < 2) {
                System.out.println("Choose a target database as the first parameter: \"doc21\", \"toc20\", or \"hoc21\"");
                return;
            }
            String dbName = args[1];

            if (args.length < 3) {
                System.out.println("Choose a template json file as the second parameter.");
                return;
            }
            String templatePath = args[2];

            check(dbName, templatePath);
        } else if (option.equals("export")) {
            if (args.length < 2) {
                System.out.println("Choose a json file.");
                return;
            }
            String templatePath = args[1];
            export(templatePath);
        } else {
            System.out.println(String.format("Invalid option %s", option));
            return;
        }

    }

    private static void export(final String templatePath) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        final List<UseCase> useCases = mapper.readValue(new File(templatePath), new TypeReference<List<UseCase>>() {
        });
        System.out.println(command(useCases, GRANT_TEMPLATE));
    }

    private static void check(final String dbName, final String templatePath) throws IOException, SQLException {
        ObjectMapper mapper = new ObjectMapper();
        final List<UseCase> templateUseCases = mapper.readValue(new File(templatePath), new TypeReference<List<UseCase>>() {
        });
        final String database = databases.get(dbName);
        if (database == null) {
            System.out.println(String.format("Invalid database %s", dbName));
            return;
        }
        List<UseCase> useCases = getUseCases(database);
        printCommands(templateUseCases, useCases);
    }

    private static List<UseCase> getUseCases(final String connectionString) throws SQLException {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(connectionString);
            return repository.getUseCases(connection);
        } finally {
            if (connection != null || !connection.isClosed()) {
                connection.close();
            }
        }
    }

    private static void printCommands(final List<UseCase> templateUseCases, final List<UseCase> useCases) {
        List<UseCase> grants = UseCase.minus(templateUseCases, useCases);
        List<UseCase> revokes = UseCase.minus(useCases, templateUseCases);
        if (!grants.isEmpty() || !revokes.isEmpty()) {
            String grantCommands = command(grants, GRANT_TEMPLATE);
            String revokeCommands = command(revokes, REVOKE_TEMPLATE);
            System.out.println(grantCommands + revokeCommands);
        } else {
            System.out.println("No changes detected!");
        }

    }

    private static void report(final String dbName) throws SQLException, JsonProcessingException {
        final String dbConnectionString = databases.get(dbName);
        if (dbConnectionString == null) {
            System.out.println(String.format("Invalid database %s", dbName));
            return;
        }
        List<UseCase> useCases = getUseCases(dbConnectionString);
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        System.out.println(mapper.writeValueAsString(useCases));
    }

    private static void compare(final String targetUrl, final String templateUrl) throws SQLException {
        DriverManager.registerDriver(new oracle.jdbc.OracleDriver());

        final String templateDatabase = databases.get(templateUrl);
        if (templateDatabase == null) {
            System.out.println(String.format("Invalid database %s", templateUrl));
            return;
        }
        List<UseCase> templateUseCases = getUseCases(templateDatabase);

        final String targetDatabase = databases.get(targetUrl);
        if (targetDatabase == null) {
            System.out.println(String.format("Invalid database %s", targetUrl));
            return;
        }
        List<UseCase> targetUseCases = getUseCases(targetDatabase);

        printCommands(templateUseCases, targetUseCases);
    }

    private static String command(final List<UseCase> usecases, final String command) {
        StringBuilder sb = new StringBuilder();
        for (UseCase useCase : usecases) {
            for (GrantedObject object : useCase.getObjects()) {
                for (String privilege : object.getPrivileges()) {
                    sb.append("\n");
                    sb.append(String.format(command, privilege, object.getName(), useCase.getRole()));
                }
            }
        }
        return sb.toString();
    }
}
