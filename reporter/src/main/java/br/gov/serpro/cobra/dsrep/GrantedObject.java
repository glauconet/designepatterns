package br.gov.serpro.cobra.dsrep;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class GrantedObject {
    private final String name;
    private List<String> privileges = new ArrayList<String>();

    @JsonCreator
    public GrantedObject(@JsonProperty("name") final String objectName) {
        this.name = objectName;
    }

    public GrantedObject(final GrantedObject templateObject) {
        this(templateObject.getName());
        this.privileges.addAll(templateObject.privileges);
    }

    public void addPrivilege(String privilege) {
        privileges.add(privilege);
    }

    public String getName() {
        return name;
    }

    public List<String> getPrivileges() {
        return privileges;
    }

    public GrantedObject minus(GrantedObject other) {
        if(!other.name.equals(this.name)) {
            throw new IllegalArgumentException(String.format("Cannot subtract %s from %s", other.getName(), this.getName()));
        }
        GrantedObject diffObject = new GrantedObject(this);
        diffObject.privileges.removeAll(other.privileges);
        return diffObject;
    }


    public static List<GrantedObject> minus(List<GrantedObject> first, List<GrantedObject> second) {
        List<GrantedObject> difference = new ArrayList<GrantedObject>();
        for (GrantedObject firstObject : first) {
            if(second.contains(firstObject)) {
                GrantedObject secondObject = Utils.getCorresponding(second, firstObject);
                GrantedObject diffObject = firstObject.minus(secondObject);
                if(!diffObject.privileges.isEmpty()) {
                    difference.add(diffObject);
                }
            } else {
                difference.add(firstObject);
            }
        }
        return difference;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final GrantedObject other = (GrantedObject) o;
        return name.equals(other.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return "GrantedObject{" +
                "name='" + name + '\'' +
                ", privileges=" + privileges +
                '}';
    }
}
