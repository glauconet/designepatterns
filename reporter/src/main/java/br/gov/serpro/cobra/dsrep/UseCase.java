package br.gov.serpro.cobra.dsrep;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class UseCase {
    private final String description;
    private final String role;
    private List<GrantedObject> objects = new ArrayList<GrantedObject>();

    public void addObject(GrantedObject object) {
        objects.add(object);
    }

    @JsonCreator
    public UseCase(@JsonProperty("description") final String description, @JsonProperty("role") final String role) {
        this.description = description;
        this.role = role;
    }

    public String getDescription() {
        return description;
    }

    public String getRole() {
        return role;
    }

    public List<GrantedObject> getObjects() {
        return objects;
    }

    public UseCase minus(UseCase other) {
        if (!this.role.equals(other.role)) {
            throw new IllegalArgumentException(String.format("Cannot subtract %s from %s", other.role, this.role));
        }
        UseCase diff = new UseCase(description, this.role);
        diff.objects = GrantedObject.minus(this.objects, other.objects);
        return diff;
    }

    public static List<UseCase> minus(List<UseCase> firstUseCases, List<UseCase> secondUseCases) {
        List<UseCase> difference = new ArrayList<UseCase>();
        for (UseCase firstUseCase : firstUseCases) {
            if (secondUseCases.contains(firstUseCase)) {
                UseCase secondUseCase = Utils.getCorresponding(secondUseCases, firstUseCase);
                final UseCase diffUseCase = firstUseCase.minus(secondUseCase);
                if (!diffUseCase.objects.isEmpty()) {
                    difference.add(diffUseCase);
                }
            } else {
                difference.add(firstUseCase);
            }
        }
        return difference;
    }

    @Override
    public String toString() {
        return "UseCase{" +
                "role='" + role + '\'' +
                ", objects=" + objects +
                '}';
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final UseCase other = (UseCase) o;
        return this.role.equals(other.role);
    }

    @Override
    public int hashCode() {
        return role.hashCode();
    }
}