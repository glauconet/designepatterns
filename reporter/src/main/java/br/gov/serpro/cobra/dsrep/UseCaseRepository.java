package br.gov.serpro.cobra.dsrep;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UseCaseRepository {
    public List<UseCase> getUseCases(Connection connection) throws SQLException {
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select distinct role_tx as description, role_nm as role_name, table_name object_name, privilege from transacoes \n" +
                    "    join roles on tras_role_id = role_id\n" +
                    "    join dba_tab_privs on grantee = role_nm\n" +
                    "    where tras_aplc_cd = 'COBI' and not (table_name like 'BIN$%')\n" +
                    "    order by role_nm, object_name, privilege"
            );
            List<UseCase> useCases = new ArrayList<UseCase>();
            UseCase useCase = null;
            GrantedObject object = null;
            while (resultSet.next()) {
                String objectName = resultSet.getString("object_name");
                String roleName = resultSet.getString("role_name");
                String privilege = resultSet.getString("privilege");
                String description = resultSet.getString("description");
                if (useCase == null || !useCase.getRole().equals(roleName)) {
                    useCase = new UseCase(description, roleName);
                    useCases.add(useCase);
                }

                if (object == null || !object.getName().equals(objectName)) {
                    object = new GrantedObject(objectName);
                    useCase.addObject(object);
                }

                object.addPrivilege(privilege);
            }
            return useCases;

        } finally {
           if(resultSet != null && !resultSet.isClosed()) {
               resultSet.close();
           }
           if(statement != null && !statement.isClosed()) {
               statement.close();
           }
        }
    }
}
