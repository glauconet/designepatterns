package br.gov.serpro.cobra.dsrep;

import java.util.List;

public class Utils {
    static <T> T getCorresponding(final List<T> list, final T object) {
        return list.get(list.indexOf(object));
    }
}