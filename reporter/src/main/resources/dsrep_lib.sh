#!/bin/bash

# dsrep_lib
# ---------
# Lib Shellscript para aplicativo dsrep.

# Baixa uma nova versão do arquivo roles.json
# Sem parâmetros
baixa_novo_arquivo() {
    if [ -e roles.json ]
    then
        rm roles.json
    fi

    cvs co 21103cobra/01-Sistema/05-Implementacao/01-Aplicacao/cobra-build/src/main/resources/roles.json
    mv 21103cobra/01-Sistema/05-Implementacao/01-Aplicacao/cobra-build/src/main/resources/roles.json roles.json
    chmod +rw roles.json
    rm -Rf 21103cobra
}

# Executa comando check
# $1 - Parâmetro de banco
executa_check() {
    COMMAND="java -jar dsrep.jar check $1 roles.json"
    eval $COMMAND
}

# Executa comando export
executa_export() {
    COMMAND="java -jar dsrep.jar export roles.json"
    eval $COMMAND
}

# Executa comando compare
executa_compare() {
    COMMAND="java -jar dsrep.jar compare $1 $2"
    eval $COMMAND
}

executa_report() {
    COMMAND="java -jar dsrep.jar report $1"
    eval $COMMAND
}
